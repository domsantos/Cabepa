inherited frm_PesquisaContribuinte: Tfrm_PesquisaContribuinte
  Left = 335
  Top = 215
  Caption = 'Pesquisa Contribuinte'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    inherited Label1: TLabel
      Left = 159
      ExplicitLeft = 159
    end
    inherited Label2: TLabel
      Left = 8
      ExplicitLeft = 8
    end
    inherited edtArgumento: TEdit
      Left = 159
      ExplicitLeft = 159
    end
    inherited cbPesquisa: TComboBox
      Left = 8
      TabOrder = 2
      Text = 'Nome Pastor'
      Items.Strings = (
        'Nome Pastor'
        'C'#243'digo'
        'CPF'
        'Conjuge')
      ExplicitLeft = 8
    end
    inherited btnPesquisa: TBitBtn
      TabOrder = 1
      OnClick = btnPesquisaClick
    end
  end
  inherited DBGrid1: TDBGrid
    OnDrawDataCell = DBGrid1DrawDataCell
  end
  inherited dsPesquisa: TDataSource
    DataSet = QPesquisa
    Left = 560
    Top = 88
  end
  object QPesquisa: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT cd_cadastro'
      '      ,cd_formTeologica'
      '      ,cd_natPastor'
      '      ,cd_escPastor'
      '      ,cd_nacPastor'
      '      ,cd_estCivil'
      '      ,cd_categoria'
      '      ,nm_pastor'
      '      ,no_regConv'
      '      ,dt_nascPastor'
      '      ,tp_sanguineo'
      '      ,no_regGeral'
      '      ,no_cpf'
      '      ,ds_endereco'
      '      ,ds_compEndPastor'
      '      ,no_cepPastor'
      '      ,dt_filiacao'
      '      ,nm_pai'
      '      ,nm_mae'
      '      ,nm_conjuge'
      '      ,dt_nascConjuge'
      '      ,no_fone'
      '      ,dt_batismo'
      '      ,ds_localBatismo'
      '      ,dt_autEvangelista'
      '      ,dt_consagEvangelista'
      '      ,dt_ordenacPastor'
      '      ,ds_localConsagracao'
      '      ,ds_campo'
      '      ,ds_supervisao'
      '      ,no_certCasamento'
      '      ,cd_sitJubilamento'
      '  FROM tbContribuinte'
      'where cd_sitJubilamento = 0 and st_arquivoMorto = 0'
      ''
      ''
      '')
    Left = 488
    Top = 88
    object QPesquisacd_formTeologica: TIntegerField
      FieldName = 'cd_formTeologica'
      Visible = False
    end
    object QPesquisacd_natPastor: TIntegerField
      FieldName = 'cd_natPastor'
      Visible = False
    end
    object QPesquisacd_escPastor: TIntegerField
      FieldName = 'cd_escPastor'
      Visible = False
    end
    object QPesquisacd_nacPastor: TIntegerField
      FieldName = 'cd_nacPastor'
      Visible = False
    end
    object QPesquisacd_estCivil: TIntegerField
      FieldName = 'cd_estCivil'
      Visible = False
    end
    object QPesquisacd_categoria: TIntegerField
      FieldName = 'cd_categoria'
      Visible = False
    end
    object QPesquisanm_pastor: TStringField
      DisplayLabel = 'Nome Pastor'
      FieldName = 'nm_pastor'
      Size = 60
    end
    object QPesquisano_regConv: TIntegerField
      FieldName = 'no_regConv'
      Visible = False
    end
    object QPesquisadt_nascPastor: TDateTimeField
      DisplayLabel = 'Data Nascimento'
      FieldName = 'dt_nascPastor'
    end
    object QPesquisatp_sanguineo: TStringField
      DisplayLabel = 'Tipo Sanguineo'
      FieldName = 'tp_sanguineo'
      FixedChar = True
    end
    object QPesquisano_regGeral: TStringField
      DisplayLabel = 'RG'
      FieldName = 'no_regGeral'
      FixedChar = True
    end
    object QPesquisano_cpf: TStringField
      DisplayLabel = 'CPF'
      FieldName = 'no_cpf'
      FixedChar = True
      Size = 14
    end
    object QPesquisads_compEndPastor: TStringField
      DisplayLabel = 'Cemplemento'
      FieldName = 'ds_compEndPastor'
      Size = 40
    end
    object QPesquisads_endereco: TStringField
      DisplayLabel = 'Endere'#231'o'
      FieldName = 'ds_endereco'
      Size = 60
    end
    object QPesquisano_cepPastor: TStringField
      DisplayLabel = 'CEP'
      FieldName = 'no_cepPastor'
      FixedChar = True
      Size = 8
    end
    object QPesquisadt_filiacao: TDateTimeField
      FieldName = 'dt_filiacao'
      Visible = False
    end
    object QPesquisanm_pai: TStringField
      FieldName = 'nm_pai'
      Visible = False
      Size = 60
    end
    object QPesquisanm_mae: TStringField
      FieldName = 'nm_mae'
      Visible = False
      Size = 60
    end
    object QPesquisanm_conjuge: TStringField
      FieldName = 'nm_conjuge'
      Visible = False
      Size = 60
    end
    object QPesquisadt_nascConjuge: TDateTimeField
      FieldName = 'dt_nascConjuge'
      Visible = False
    end
    object QPesquisano_fone: TStringField
      DisplayLabel = 'Fone'
      FieldName = 'no_fone'
      Size = 15
    end
    object QPesquisadt_batismo: TDateTimeField
      FieldName = 'dt_batismo'
      Visible = False
    end
    object QPesquisads_localBatismo: TStringField
      FieldName = 'ds_localBatismo'
      Visible = False
      Size = 40
    end
    object QPesquisadt_autEvangelista: TDateTimeField
      FieldName = 'dt_autEvangelista'
      Visible = False
    end
    object QPesquisadt_consagEvangelista: TDateTimeField
      FieldName = 'dt_consagEvangelista'
      Visible = False
    end
    object QPesquisadt_ordenacPastor: TDateTimeField
      FieldName = 'dt_ordenacPastor'
      Visible = False
    end
    object QPesquisads_localConsagracao: TStringField
      FieldName = 'ds_localConsagracao'
      Visible = False
      Size = 40
    end
    object QPesquisads_campo: TStringField
      FieldName = 'ds_campo'
      Visible = False
      Size = 40
    end
    object QPesquisads_supervisao: TStringField
      FieldName = 'ds_supervisao'
      Visible = False
      Size = 40
    end
    object QPesquisano_certCasamento: TIntegerField
      FieldName = 'no_certCasamento'
      Visible = False
    end
    object QPesquisacd_sitJubilamento: TStringField
      FieldName = 'cd_sitJubilamento'
      FixedChar = True
      Size = 1
    end
    object QPesquisacd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
end
