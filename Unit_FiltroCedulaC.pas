unit Unit_FiltroCedulaC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons;

type
  Tfrm_FiltroCedulaC = class(TForm)
    txCdCabepa: TEdit;
    Label1: TLabel;
    btImprimir: TBitBtn;
    Label2: TLabel;
    txAno: TEdit;
    gbTipo: TRadioGroup;
    brBuscar: TButton;
    procedure btImprimirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_FiltroCedulaC: Tfrm_FiltroCedulaC;

implementation

uses Unit_RelCedulaCIndividual, Unit_RelatorioCedulaC;

{$R *.dfm}

procedure Tfrm_FiltroCedulaC.btImprimirClick(Sender: TObject);
begin
  if txAno.Text = '' then
  begin
    ShowMessage('Digite um ano de referencia!');
    txAno.SetFocus;
    exit;
  end;



  if gbTipo.ItemIndex = 0 then
  begin
    //ShowMessage('Todos');
    Application.CreateForm(Tfrm_relatorioCedulaC,frm_relatorioCedulaC);
    frm_relatorioCedulaC.qRelatorio.Parameters.ParamByName('a1').Value := strtoint(txAno.Text);
    frm_relatorioCedulaC.qRelatorio.Parameters.ParamByName('a2').Value := strtoint(txAno.Text);
    frm_relatorioCedulaC.qRelatorio.Parameters.ParamByName('a3').Value := strtoint(txAno.Text);
    frm_relatorioCedulaC.qRelatorio.Parameters.ParamByName('a4').Value := strtoint(txAno.Text);
    frm_relatorioCedulaC.qRelatorio.Parameters.ParamByName('a5').Value := strtoint(txAno.Text);
    frm_relatorioCedulaC.qRelatorio.Parameters.ParamByName('a6').Value := strtoint(txAno.Text);
    frm_relatorioCedulaC.lbAnoCalendario.Caption := 'ANO CALENDARIO '+txAno.Text;
    frm_relatorioCedulaC.qRelatorio.Open;
    frm_relatorioCedulaC.RLReport1.Preview();
    frm_relatorioCedulaC.Free;

  end;

  if gbTipo.ItemIndex = 1 then
  begin
    if txCdCabepa.Text = '' then
    begin
      ShowMessage('Digite o codigo Cabepa');
      txCdCabepa.SetFocus;
      exit;
    end;


    Application.CreateForm(Tfrm_RelCedulaCIndividual,frm_RelCedulaCIndividual);

    frm_RelCedulaCIndividual.qRelatorio.Parameters.ParamByName('codigo').Value := strtoint(txCdCabepa.Text);
    frm_RelCedulaCIndividual.qRelatorio.Parameters.ParamByName('a1').Value := strtoint(txAno.Text);
    frm_RelCedulaCIndividual.qRelatorio.Parameters.ParamByName('a2').Value := strtoint(txAno.Text);
    frm_RelCedulaCIndividual.qRelatorio.Parameters.ParamByName('a3').Value := strtoint(txAno.Text);
    frm_RelCedulaCIndividual.qRelatorio.Parameters.ParamByName('a4').Value := strtoint(txAno.Text);
    frm_RelCedulaCIndividual.qRelatorio.Parameters.ParamByName('a5').Value := strtoint(txAno.Text);
    frm_RelCedulaCIndividual.qRelatorio.Parameters.ParamByName('a6').Value := strtoint(txAno.Text);
    frm_RelCedulaCIndividual.lbAnoCalendario.Caption := 'ANO CALENDARIO '+txAno.Text;
    frm_RelCedulaCIndividual.qRelatorio.Open;
    frm_RelCedulaCIndividual.RLReport1.Preview();
    frm_RelCedulaCIndividual.Free;



  end;
end;

procedure Tfrm_FiltroCedulaC.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

end.
