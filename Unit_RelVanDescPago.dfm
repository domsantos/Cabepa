inherited frm_RelVanDescPago: Tfrm_RelVanDescPago
  Caption = 'frm_RelVanDescPago'
  ExplicitWidth = 870
  ExplicitHeight = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = 24
    ExplicitLeft = 24
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 248
        Caption = 'Listagem de Vantagens/Descontos Pagos'
        ExplicitWidth = 248
      end
      object RLLabel7: TRLLabel
        Left = 96
        Top = 54
        Width = 65
        Height = 16
        Caption = 'RUBRICA:'
      end
    end
    inherited RLBand2: TRLBand
      object RLLabel6: TRLLabel
        Left = 632
        Top = 26
        Width = 34
        Height = 16
        Caption = 'Valor'
      end
    end
    inherited RLBand3: TRLBand
      inherited RLDBText1: TRLDBText
        Width = 67
        DataField = 'cd_cabepa'
        ExplicitWidth = 67
      end
      inherited RLDBText2: TRLDBText
        Width = 94
        DataField = 'nm_beneficiado'
        ExplicitWidth = 94
      end
      object DBEdit1: TDBEdit
        Left = 592
        Top = 24
        Width = 147
        Height = 24
        DataField = 'vl_Rubrica'
        DataSource = dsRelatorio
        TabOrder = 2
      end
      object RLDBText3: TRLDBText
        Left = 632
        Top = 2
        Width = 63
        Height = 16
        DataField = 'vl_Rubrica'
        DataSource = dsRelatorio
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = frm_FiltroVanDescPaga.qListaPessoas
    Left = 120
    Top = 264
  end
end
