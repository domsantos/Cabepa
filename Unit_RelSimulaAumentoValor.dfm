inherited frm_RelSimulaAumentoValor: Tfrm_RelSimulaAumentoValor
  Caption = 'frm_RelSimulaAumentoValor'
  ExplicitWidth = 862
  ExplicitHeight = 633
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = 0
    ExplicitLeft = 0
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 191
        Caption = 'Simula'#231#227'o de Aumento de Valor'
        ExplicitWidth = 191
      end
      object lblAumento: TRLLabel
        Left = 96
        Top = 54
        Width = 108
        Height = 16
        Caption = 'Valor Aumentado:'
      end
    end
    inherited RLBand2: TRLBand
      inherited RLLabel3: TRLLabel
        Width = 91
        Caption = 'BENEFICIADO'
        ExplicitWidth = 91
      end
      inherited RLLabel4: TRLLabel
        Left = 392
        Width = 93
        Caption = 'VALOR ATUAL'
        ExplicitLeft = 392
        ExplicitWidth = 93
      end
      object RLLabel7: TRLLabel
        Left = 552
        Top = 32
        Width = 150
        Height = 16
        Caption = 'VALOR COM AUMENTO'
      end
    end
    inherited RLBand4: TRLBand
      Top = 225
      ExplicitTop = 225
    end
    inherited RLBand3: TRLBand
      inherited RLDBText1: TRLDBText
        Width = 65
        DataField = 'nm_pastor'
        ExplicitWidth = 65
      end
      inherited RLDBText2: TRLDBText
        Left = 413
        Width = 72
        Alignment = taRightJustify
        DataField = 'vl_Beneficio'
        ExplicitLeft = 413
        ExplicitWidth = 72
      end
      object RLDBText3: TRLDBText
        Left = 632
        Top = 4
        Width = 69
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_aumento'
        DataSource = dsRelatorio
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 190
      Width = 718
      Height = 35
      BandType = btSummary
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = False
      object RLDBResult1: TRLDBResult
        Left = 593
        Top = 13
        Width = 108
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_aumento'
        DataSource = dsRelatorio
        Info = riSum
      end
      object RLDBResult2: TRLDBResult
        Left = 373
        Top = 13
        Width = 111
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_Beneficio'
        DataSource = dsRelatorio
        Info = riSum
      end
      object RLDBResult3: TRLDBResult
        Left = 180
        Top = 13
        Width = 46
        Height = 16
        DataField = 'nm_pastor'
        DataSource = dsRelatorio
        Info = riCount
      end
      object RLLabel8: TRLLabel
        Left = 3
        Top = 13
        Width = 175
        Height = 16
        Caption = 'No. de Beneficiados Afetados'
      end
      object RLLabel6: TRLLabel
        Left = 263
        Top = 13
        Width = 49
        Height = 16
        Caption = 'TOTAIS'
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = qRelatorio
  end
  object qRelatorio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'aumento'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'valor'
        DataType = ftString
        Size = 1
        Value = '1'
      end>
    SQL.Strings = (
      'declare @valor money'
      'set @valor = :aumento'
      ''
      'declare @mes int, @ano int'
      'select @ano = max(dt_anoPagamento) from tbPagamentoMensal'
      ''
      'select @mes =max(dt_mesPagamento) from tbPagamentoMensal '
      'where dt_anoPagamento = @ano'
      ''
      'select'
      '                 c.nm_pastor'
      #9',@valor vl_aumento'
      #9',max(v.vl_Beneficio) as vl_Beneficio'
      #9'from tbValBeneficio  v'
      
        #9'inner join tbRequerimentoBeneficio r on v.no_reqBeneficio = r.n' +
        'o_reqBeneficio'
      #9'inner join tbContribuinte c on c.cd_CABEPA = r.cd_CABEPA'
      'where v.vl_Beneficio = :valor'
      'and c.cd_cabepa in('
      #9'select cd_cabepa from tbPagamentoMensal'
      #9#9'where dt_anoPagamento = @ano'
      #9#9'and dt_mesPagamento = @mes)'
      'group by  c.nm_pastor'
      'order by c.nm_pastor')
    Left = 328
    Top = 328
    object qRelatoriovl_aumento: TBCDField
      FieldName = 'vl_aumento'
      ReadOnly = True
      DisplayFormat = 'R$ #,##0.00'
      currency = True
      Precision = 19
    end
    object qRelatoriovl_Beneficio: TBCDField
      FieldName = 'vl_Beneficio'
      DisplayFormat = 'R$ #,##0.00'
      currency = True
      Precision = 10
      Size = 2
    end
    object qRelatorionm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
  end
end
