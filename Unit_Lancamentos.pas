unit Unit_Lancamentos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, Grids, DBGrids, ExtCtrls, StdCtrls, DB, ADODB, Buttons,
  Mask, rxToolEdit, rxCurrEdit, Menus;

type
  Tfrm_Lancamentos = class(TForm)
    Panel1: TPanel;
    dbFilho: TDBGrid;
    Panel2: TPanel;
    Panel3: TPanel;
    dsPai: TDataSource;
    dsFilho: TDataSource;
    qListaVantagemDesconto: TADOQuery;
    qListaVantagemDescontonm_Rubrica: TStringField;
    qListaVantagemDescontono_freqRubrica: TSmallintField;
    qListaVantagemDescontovl_Rubrica: TBCDField;
    qListaVantagemDescontodt_tipo: TStringField;
    qListaBeneficiados: TADOQuery;
    qListaBeneficiadosnm_pastor: TStringField;
    qListaBeneficiadosvl_vantagem: TBCDField;
    qListaBeneficiadosvl_desconto: TBCDField;
    qListaBeneficiadosvl_liquido: TBCDField;
    qListaBeneficiadosdt_mesPagamento: TSmallintField;
    qListaBeneficiadosdt_anoPagamento: TSmallintField;
    Label1: TLabel;
    Label2: TLabel;
    edtMes: TEdit;
    Label3: TLabel;
    edtAno: TEdit;
    Label4: TLabel;
    BitBtn1: TBitBtn;
    Label5: TLabel;
    lblNomeContribuinte: TLabel;
    qListaRubricas: TADOQuery;
    cbRubrica: TDBLookupComboBox;
    dsRubricas: TDataSource;
    qListaRubricascd_tipRubrica: TSmallintField;
    qListaRubricasds_rubrica: TStringField;
    dbPai: TDBGrid;
    DBNavigator1: TDBNavigator;
    Label6: TLabel;
    Label7: TLabel;
    edtValor: TCurrencyEdit;
    qRegistraLancamento: TADOQuery;
    qListaRubricasno_Rubrica: TSmallintField;
    BitBtn2: TBitBtn;
    qVerificaRef: TADOQuery;
    qVerificaRefnr_registros: TIntegerField;
    qCalculaIR: TADOQuery;
    qCalculaIRvl_ir: TBCDField;
    qValorBruto: TADOQuery;
    qValorBrutovl_beneficio: TBCDField;
    Button1: TButton;
    edtCodigo: TEdit;
    BitBtn3: TBitBtn;
    Label8: TLabel;
    PopupMenu1: TPopupMenu;
    RemoverLanamento1: TMenuItem;
    qListaVantagemDescontocd_tipRubrica: TSmallintField;
    qListaVantagemDescontono_Rubrica: TSmallintField;
    qListaBeneficiadoscd_cadastro: TIntegerField;
    procedure edtMesKeyPress(Sender: TObject; var Key: Char);
    procedure edtAnoKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure dbPaiDblClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure RemoverLanamento1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Lancamentos: Tfrm_Lancamentos;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_Lancamentos.BitBtn1Click(Sender: TObject);
begin
  if edtMes.Text = '' then
  begin
    showmessage('Informe m�s de refer�ncia');
    edtMes.SetFocus;
    exit;
  end;
  if edtAno.Text = '' then
  begin
    showmessage('Informe ano de refer�ncia');
    edtAno.SetFocus;
    exit;
  end;

  qVerificaRef.Close;

  qVerificaRef.Parameters.ParamByName('mes').Value := edtMes.Text;
  qVerificaRef.Parameters.ParamByName('ano').Value := edtAno.Text;

  qVerificaRef.Open;
  if qVerificaRefnr_registros.Value <= 0 then
  begin
    showmessage('N�o h� folha aberta para esta refer�ncia');
    exit;
  end;


  qListaBeneficiados.Close;
  qListaVantagemDesconto.Close;
  qListaBeneficiados.Parameters.ParamByName('mes').Value := edtMes.Text;
  qListaBeneficiados.Parameters.ParamByName('ano').Value := edtAno.Text;

  qListaBeneficiados.Open;
  qListaVantagemDesconto.open;

end;

procedure Tfrm_Lancamentos.BitBtn2Click(Sender: TObject);
begin
  dm.ADOConn.BeginTrans;


  qRegistraLancamento.Parameters.ParamByName('cd_cadastro').Value :=
    qListaBeneficiadoscd_cadastro.Value;
  qRegistraLancamento.Parameters.ParamByName('dt_anoPagamento').Value :=
    qListaBeneficiadosdt_anoPagamento.Value;
  qRegistraLancamento.Parameters.ParamByName('dt_mesPagamento').Value :=
    qListaBeneficiadosdt_mesPagamento.Value;
  qRegistraLancamento.Parameters.ParamByName('cd_tipRubrica').Value :=
    qListaRubricascd_tipRubrica.Value;
  qRegistraLancamento.Parameters.ParamByName('no_Rubrica').Value :=
    qListaRubricasno_Rubrica.Value;
  qRegistraLancamento.Parameters.ParamByName('vl_Rubrica').Value :=
    edtValor.Value;

  try
    qRegistraLancamento.ExecSQL;
    showmessage('Lan�amento registrado');
    qListaBeneficiados.Close;
    qListaBeneficiados.open;
    qListaVantagemDesconto.Close;
    qListaVantagemDesconto.open;
    Panel2.Enabled := false;
    dm.ADOConn.CommitTrans;
  except
    on e:Exception do
    begin
      showmessage('Erro ao registrar lan�amento: '+e.Message);
      Panel2.Enabled := false;
      dm.ADOConn.RollbackTrans;
    end;

  end;
  edtValor.Value := 0;
  Panel2.Enabled := false;
  qListaVantagemDesconto.Close;


  


end;

procedure Tfrm_Lancamentos.BitBtn3Click(Sender: TObject);
begin
  dsPai.DataSet.locate('cd_cadastro',edtCodigo.text,[loCaseInsensitive, loPartialKey]);

end;

procedure Tfrm_Lancamentos.Button1Click(Sender: TObject);
begin
  //showmessage(inttostr(qListaRubricascd_tipRubrica.Value));
  if qListaRubricascd_tipRubrica.Value = 4 then
  begin
    qCalculaIR.Close;
    qValorBruto.Close;
    qValorBruto.Parameters.ParamByName('codigo').Value :=
      qListaBeneficiadoscd_cadastro.Value;
    qValorBruto.Parameters.ParamByName('mes').Value := edtMes.Text;
    qValorBruto.Parameters.ParamByName('ano').Value := edtAno.Text;

    qValorBruto.Open;
    qCalculaIR.Parameters.ParamByName('valor').Value := qValorBrutovl_beneficio.Value;
    qCalculaIR.Open;

    edtValor.Value := qCalculaIRvl_ir.Value;
  end;
end;

procedure Tfrm_Lancamentos.dbPaiDblClick(Sender: TObject);
begin
  lblNomeContribuinte.Caption := qListaBeneficiadosnm_pastor.Value;
  Panel2.Enabled := true;
  edtValor.Value := 0;
  qListaVantagemDesconto.Open;

end;

procedure Tfrm_Lancamentos.edtAnoKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_Lancamentos.edtMesKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_Lancamentos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

procedure Tfrm_Lancamentos.FormCreate(Sender: TObject);
begin
  qListaVantagemDesconto.Open;
  qListaRubricas.Open;
end;

procedure Tfrm_Lancamentos.RemoverLanamento1Click(Sender: TObject);
var
  query : TADOQuery;
begin
  // Remove lan�amento Selecionado
  query := TADOQuery.Create(self);

  with query do
  begin
    Connection := dm.ADOConn;
    Close;
    sql.Clear;

    sql.Add('delete from tbPagamentoMensal where cd_cadastro = :codigo ');
    sql.Add(' and dt_anoPagamento = :ano and dt_mesPagamento = :mes ');
    sql.Add(' and cd_tipRubrica = :rubrica and no_Rubrica = :tipo ');

    Parameters.ParamByName('codigo').Value := qListaBeneficiadoscd_cadastro.Value;
    Parameters.ParamByName('ano').Value := qListaBeneficiadosdt_anoPagamento.Value;
    Parameters.ParamByName('mes').Value := qListaBeneficiadosdt_mesPagamento.Value;
    Parameters.ParamByName('rubrica').Value := qListaVantagemDescontocd_tipRubrica.Value;
    Parameters.ParamByName('tipo').Value := qListaVantagemDescontono_Rubrica.Value;

    try
      ExecSQL;
    except
      showmessage('Erro ao apagar lan�amento');
    end;

    qListaVantagemDesconto.Close;
    qListaVantagemDesconto.open;
    qListaBeneficiados.Refresh;
  end;




end;

end.
