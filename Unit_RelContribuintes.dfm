object frm_RelContribuntes: Tfrm_RelContribuntes
  Left = 0
  Top = 0
  Caption = 'frm_RelContribuntes'
  ClientHeight = 409
  ClientWidth = 703
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = 8
    Top = 8
    Width = 1123
    Height = 794
    DataSource = dsListaAtivos
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    PageSetup.Orientation = poLandscape
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 1047
      Height = 83
      BandType = btHeader
      object RLLabel2: TRLLabel
        Left = 16
        Top = 32
        Width = 254
        Height = 22
        Caption = 'Relat'#243'rio de Doadores Ativos'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel9: TRLLabel
        Left = 16
        Top = 54
        Width = 42
        Height = 18
        Caption = 'Data:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLSystemInfo1: TRLSystemInfo
        Left = 64
        Top = 54
        Width = 48
        Height = 18
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel10: TRLLabel
        Left = 916
        Top = 3
        Width = 35
        Height = 16
        Caption = 'Pag.:'
      end
      object RLSystemInfo2: TRLSystemInfo
        Left = 957
        Top = 3
        Width = 87
        Height = 16
        Info = itPageNumber
      end
      object RLLabel1: TRLLabel
        Left = 19
        Top = 6
        Width = 299
        Height = 27
        Caption = 'Associa'#231#227'o Casa do Pastor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
    object RLBand2: TRLBand
      Left = 38
      Top = 121
      Width = 1047
      Height = 29
      BandType = btColumnHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = True
      object RLLabel3: TRLLabel
        Left = 0
        Top = 10
        Width = 55
        Height = 16
        Caption = 'CODIGO'
      end
      object RLLabel4: TRLLabel
        Left = 80
        Top = 10
        Width = 43
        Height = 16
        Caption = 'NOME'
      end
      object RLLabel5: TRLLabel
        Left = 544
        Top = 10
        Width = 82
        Height = 16
        Caption = 'DATA NASC.'
      end
      object RLLabel6: TRLLabel
        Left = 680
        Top = 10
        Width = 66
        Height = 16
        Caption = 'CONJUGE'
      end
    end
    object RLBand3: TRLBand
      Left = 38
      Top = 150
      Width = 1047
      Height = 24
      object RLDBText1: TRLDBText
        Left = 680
        Top = 3
        Width = 74
        Height = 16
        DataField = 'nm_conjuge'
        DataSource = dsListaAtivos
      end
      object RLDBText2: TRLDBText
        Left = 544
        Top = 3
        Width = 88
        Height = 16
        DataField = 'dt_nascPastor'
        DataSource = dsListaAtivos
      end
      object RLDBText3: TRLDBText
        Left = 80
        Top = 3
        Width = 65
        Height = 16
        DataField = 'nm_pastor'
        DataSource = dsListaAtivos
      end
      object RLDBText4: TRLDBText
        Left = 4
        Top = 3
        Width = 75
        Height = 16
        DataField = 'cd_cadastro'
        DataSource = dsListaAtivos
      end
    end
  end
  object qListaAtivos: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_cadastro,nm_pastor,nm_conjuge,dt_nascPastor'
      'from tbContribuinte'
      'where cd_sitJubilamento is null or cd_sitJubilamento = 0'
      'order by nm_pastor')
    Left = 544
    Top = 216
    object qListaAtivosnm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qListaAtivosnm_conjuge: TStringField
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object qListaAtivosdt_nascPastor: TDateTimeField
      FieldName = 'dt_nascPastor'
    end
    object qListaAtivoscd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object dsListaAtivos: TDataSource
    DataSet = qListaAtivos
    Left = 544
    Top = 272
  end
end
