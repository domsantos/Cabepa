unit Unit_relSimulaPercentual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, ADODB, RLReport, jpeg;

type
  Tfrm_RelSimulaAumentoPercentual = class(Tfrm_RelatorioPadrao)
    ValorRef: TRLLabel;
    Percentual: TRLLabel;
    RLLabel8: TRLLabel;
    RLDBText3: TRLDBText;
    QspSimulaAumento: TADOQuery;
    RLBand5: TRLBand;
    RLDBResult2: TRLDBResult;
    RLDBResult1: TRLDBResult;
    RLLabel6: TRLLabel;
    RLDBResult3: TRLDBResult;
    RLLabel7: TRLLabel;
    QspSimulaAumentonm_pastor: TStringField;
    QspSimulaAumentovl_aumento: TBCDField;
    QspSimulaAumentonr_requerimento: TIntegerField;
    QspSimulaAumentovl_Beneficio: TBCDField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelSimulaAumentoPercentual: Tfrm_RelSimulaAumentoPercentual;

implementation

uses Unit_DM;

{$R *.dfm}

end.
