unit Unit_MovimentoCaixa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ExtCtrls, StdCtrls, Mask, Buttons, rxToolEdit,
  rxCurrEdit, DBCtrls, DB, ADODB;

type
  Tfrm_MovimentoCaixa = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    edtNumCaixa: TEdit;
    Label2: TLabel;
    edtDataCaixa: TMaskEdit;
    btnImprimir: TBitBtn;
    btnAbrir: TBitBtn;
    gbTipoMovimento: TRadioGroup;
    Label3: TLabel;
    Label4: TLabel;
    edtValor: TCurrencyEdit;
    BitBtn1: TBitBtn;
    Label5: TLabel;
    Label6: TLabel;
    lblEntrada: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    lblSaida: TLabel;
    Label7: TLabel;
    lblTotal: TLabel;
    qListaTipoMovimento: TADOQuery;
    qListaTipoMovimentocdTipoMovimento: TAutoIncField;
    qListaTipoMovimentodsTipoMovimento: TStringField;
    dsTipoMovimento: TDataSource;
    cbTipoMovimento: TDBLookupComboBox;
    qListaMovimento: TADOQuery;
    dsListaMovimento: TDataSource;
    qVerificaCaixaAberto: TADOQuery;
    qVerificaCaixaAbertoidCaixa: TAutoIncField;
    qVerificaCaixaAbertoidUsuario: TIntegerField;
    qAbreCaixa: TADOQuery;
    qInsereMovimento: TADOQuery;
    qListaMovimentoidMovimento: TAutoIncField;
    qListaMovimentodtMovimento: TDateTimeField;
    qListaMovimentovlMovimento: TBCDField;
    qListaMovimentodsTipoMovimento: TStringField;
    qListaMovimentotpMovimento: TStringField;
    qVerificaCaixaAbertodtAbreCaixa: TWideStringField;
    qVerificaCaixaAbertodtFechaCaixa: TWideStringField;
    procedure FormCreate(Sender: TObject);
    procedure btnAbrirClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure totalizarValores;
    function getTipoMovimento(idx:Integer):string;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_MovimentoCaixa: Tfrm_MovimentoCaixa;
  idCaixaAtual : integer;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_MovimentoCaixa.BitBtn1Click(Sender: TObject);
begin
  try
    qInsereMovimento.Close;

    qInsereMovimento.Parameters.ParamByName('vlMovimento').Value := edtValor.Value;
    qInsereMovimento.Parameters.ParamByName('cdTipoMovimento').Value := qListaTipoMovimentocdTipoMovimento.Value;
    qInsereMovimento.Parameters.ParamByName('idCaixa').Value := qVerificaCaixaAbertoidCaixa.Value;
    qInsereMovimento.Parameters.ParamByName('tpMovimento').Value := getTipoMovimento(gbTipoMovimento.ItemIndex);

    qInsereMovimento.ExecSQL;

    totalizarValores;

  except
    on Exception do
    begin
      ShowMessage('Erro ao registrar movimento');
    end;

  end;
end;

procedure Tfrm_MovimentoCaixa.btnAbrirClick(Sender: TObject);
begin
  try
    qVerificaCaixaAberto.Close;

    ShowMessage('aqui1');
    qVerificaCaixaAberto.Parameters.ParamByName('nrCaixa').Value := StrToInt(Trim(edtNumCaixa.Text));
    qVerificaCaixaAberto.Parameters.ParamByName('dtAbreCaixa').Value := StrToDateTime(edtDataCaixa.Text);

    qVerificaCaixaAberto.Open;

    if qVerificaCaixaAberto.IsEmpty then
    begin

      if MessageDlg('N�o h� caixa aberto nesta data. Deseja abrir caixa para registro?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin

        qAbreCaixa.Close;
        qAbreCaixa.Parameters.ParamByName('nrCaixa').Value := strtoint(edtNumCaixa.Text);
        qAbreCaixa.Parameters.ParamByName('idUsuario').Value := DM.idLogin;

        qAbreCaixa.ExecSQL;

        qVerificaCaixaAberto.Close;

        qVerificaCaixaAberto.Open;

        if qVerificaCaixaAberto.IsEmpty = false then
        begin
          ShowMessage(inttostr(qVerificaCaixaAbertoidCaixa.Value));
        end;
      end;
         ShowMessage('aqui2');

    end
    else
    begin
      if StrToDateTime(qVerificaCaixaAbertodtAbreCaixa.Value) <> now then
      begin
        ShowMessage('Caixa n�o � do dia atual. Voc� poder� somente imprimir-lo');
        qListaTipoMovimento.Close;
        qListaMovimento.Parameters.ParamByName('idCaixa').Value := qVerificaCaixaAbertoidCaixa.Value;
        idCaixaAtual := qVerificaCaixaAbertoidCaixa.Value;

        totalizarValores;

      end
      else
      begin
        if qVerificaCaixaAbertoidUsuario.Value = dm.idLogin then
        begin
          ShowMessage('Caixa j� aberto');
          qListaTipoMovimento.Close;
          qListaMovimento.Parameters.ParamByName('idCaixa').Value := qVerificaCaixaAbertoidCaixa.Value;

          totalizarValores;
          qListaTipoMovimento.Open;
          Panel2.Enabled := true;
          idCaixaAtual := qVerificaCaixaAbertoidCaixa.Value;
        end
        else
          ShowMessage(IntToStr(qVerificaCaixaAbertoidUsuario.Value));

      end;
    end;

  except
    on E: Exception do
    begin
      ShowMessage('Erro ao verificr caixa'+e.Message);
    end;

  end;
end;

procedure Tfrm_MovimentoCaixa.btnImprimirClick(Sender: TObject);
begin
  if idCaixaAtual > 0 then
  begin
    dm.imprimeCaixa(idCaixaAtual);
    ShowMessage('Imprimirndo caixa');
  end
  else
    ShowMessage('N�o h� caixa selecionado para impress�o.');
end;

procedure Tfrm_MovimentoCaixa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qListaTipoMovimento.close;
  qListaMovimento.Close;
  self := nil;
  Action := caFree;
end;

procedure Tfrm_MovimentoCaixa.FormCreate(Sender: TObject);
begin
  qListaTipoMovimento.Open;
  idCaixaAtual := 0;
end;

function Tfrm_MovimentoCaixa.getTipoMovimento(idx: Integer): string;
var
  retorno : string;
begin
  if idx = 0 then
    Result := 'E'
  else
    Result := 'S';

end;

procedure Tfrm_MovimentoCaixa.totalizarValores;
var
  vlEntrada, vlSaida, vlTotal : Double;
begin

  qListaMovimento.Close;
  qListaMovimento.Open;
  qListaMovimento.First;

  vlEntrada := 0;
  vlSaida := 0;
  vlTotal := 0;
  while qListaMovimento.Recordset.EOF = false do
  begin
    if qListaMovimentotpMovimento.Value = 'E' then
      vlEntrada := vlEntrada + qListaMovimentovlMovimento.Value;


    if qListaMovimentotpMovimento.Value = 'S' then
      vlSaida := vlSaida + qListaMovimentovlMovimento.Value;


    qListaMovimento.Next;
  end;

  vlTotal := vlEntrada - vlSaida;

  lblEntrada.Caption := FloatToStrF(vlEntrada,ffCurrency,15,2);
  lblSaida.Caption := FloatToStrF(vlSaida,ffCurrency,15,2);
  lblTotal.Caption := FloatToStrF(vlTotal,ffCurrency,15,2);


end;

end.
