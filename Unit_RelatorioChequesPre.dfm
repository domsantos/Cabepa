inherited frm_RelatorioChequePre: Tfrm_RelatorioChequePre
  Caption = 'Relat'#243'rio de Cheques-Pr'#233
  ClientWidth = 927
  ExplicitWidth = 935
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = 0
    Width = 1123
    Height = 794
    PageSetup.Orientation = poLandscape
    ExplicitLeft = 0
    ExplicitWidth = 1123
    ExplicitHeight = 794
    inherited RLBand1: TRLBand
      Width = 1047
      ExplicitWidth = 1047
      inherited RLLabel1: TRLLabel
        Top = -1
        ExplicitTop = -1
      end
      inherited RLLabel2: TRLLabel
        Width = 152
        Caption = 'Relat'#243'rio de Cheques-Pr'#233
        ExplicitWidth = 152
      end
    end
    inherited RLBand2: TRLBand
      Width = 1047
      ExplicitWidth = 1047
      inherited RLLabel3: TRLLabel
        Top = 30
        Width = 87
        Caption = 'REFER'#202'NCIA'
        ExplicitTop = 30
        ExplicitWidth = 87
      end
      inherited RLLabel4: TRLLabel
        Left = 101
        Top = 30
        Width = 97
        Caption = 'CONTRIBUINTE'
        ExplicitLeft = 101
        ExplicitTop = 30
        ExplicitWidth = 97
      end
      object RLLabel6: TRLLabel
        Left = 912
        Top = 28
        Width = 48
        Height = 16
        Caption = 'VALOR'
      end
      object RLLabel7: TRLLabel
        Left = 520
        Top = 28
        Width = 50
        Height = 16
        Caption = 'BANCO'
      end
      object RLLabel8: TRLLabel
        Left = 416
        Top = 28
        Width = 83
        Height = 16
        Caption = 'No. CHEQUE'
      end
      object RLLabel9: TRLLabel
        Left = 649
        Top = 28
        Width = 62
        Height = 16
        Caption = 'AG'#202'NCIA'
      end
    end
    inherited RLBand4: TRLBand
      Width = 1047
      Height = 67
      ExplicitWidth = 1047
      ExplicitHeight = 67
      inherited RLSystemInfo2: TRLSystemInfo
        Left = 957
        Top = 48
        ExplicitLeft = 957
        ExplicitTop = 48
      end
      object RLDBResult1: TRLDBResult
        Left = 654
        Top = 24
        Width = 79
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_pagtoContribuicao'
        DataSource = frm_ChequesPre.dsListaCheques
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Info = riSum
        ParentFont = False
      end
      object RLLabel10: TRLLabel
        Left = 480
        Top = 24
        Width = 157
        Height = 16
        Caption = 'Valor Total de Cheques:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    inherited RLBand3: TRLBand
      Width = 1047
      ExplicitWidth = 1047
      inherited RLDBText1: TRLDBText
        Width = 60
        DataField = 'referencia'
        ExplicitWidth = 60
      end
      inherited RLDBText2: TRLDBText
        Left = 101
        Width = 65
        DataField = 'nm_pastor'
        ExplicitLeft = 101
        ExplicitWidth = 65
      end
      object RLDBText3: TRLDBText
        Left = 416
        Top = 3
        Width = 72
        Height = 16
        DataField = 'no_chequePagto'
        DataSource = frm_ChequesPre.dsListaCheques
      end
      object RLDBText4: TRLDBText
        Left = 520
        Top = 2
        Width = 41
        Height = 16
        DataField = 'ds_bancoCheque'
        DataSource = frm_ChequesPre.dsListaCheques
      end
      object RLDBText5: TRLDBText
        Left = 649
        Top = 2
        Width = 51
        Height = 16
        DataField = 'ds_agenciaCheque'
        DataSource = frm_ChequesPre.dsListaCheques
      end
      object RLDBText6: TRLDBText
        Left = 912
        Top = 2
        Width = 34
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_pagtoContribuicao'
        DataSource = frm_ChequesPre.dsListaCheques
        DisplayMask = '###,##0.00'
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = frm_ChequesPre.qListaCheques
    Left = 176
    Top = 272
  end
end
