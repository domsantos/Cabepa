unit Unit_FiltroContraCheque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, DBCtrls, Grids, DBGrids, DB, ADODB;

type
  Tfrm_FiltroContraCheque = class(TForm)
    lblContribuinte: TLabel;
    Label4: TLabel;
    rbAbrangencia: TRadioGroup;
    edtAno: TEdit;
    edtContribuinte: TEdit;
    Label6: TLabel;
    Button1: TButton;
    edtMes: TEdit;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    qVerificaRef: TADOQuery;
    qVerificaRefnr_registros: TIntegerField;
    dsBancos: TDataSource;
    Label5: TLabel;
    cbBancos: TDBLookupComboBox;
    ckFicha: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_FiltroContraCheque: Tfrm_FiltroContraCheque;

implementation

uses Unit_PesquisaContribuinte, Unit_ContraCheque, Unit_ContraChequeIndividual,
  Unit_DM, Unit_FichaFinanceira;

{$R *.dfm}

procedure Tfrm_FiltroContraCheque.BitBtn1Click(Sender: TObject);
begin


  if edtMes.Text = '' then
  begin
    showmessage('Informe m�s de refer�ncia');
    edtMes.SetFocus;
    exit;
  end;

  if edtAno.Text = '' then
  begin
    showmessage('Informe ano de refer�ncia');
    edtAno.SetFocus;
    exit;
  end;

  qVerificaRef.Close;
  qVerificaRef.Parameters.ParamByName('mes').Value := edtMes.Text;
  qVerificaRef.Parameters.ParamByName('ano').Value := edtAno.Text;

  if rbAbrangencia.ItemIndex = 1 then
    qVerificaRef.Parameters.ParamByName('codigo').Value := edtContribuinte.Text
  else
    qVerificaRef.Parameters.ParamByName('codigo').Value := 0;

  qVerificaRef.Open;

  if qVerificaRefnr_registros.Value <= 0 then
  begin
    showmessage('N�o h� registros para esta refer�ncia');
    exit;
  end;

  if ckFicha.Checked = false then
  begin

    Application.CreateForm(Tfrm_ContraCheque,frm_ContraCheque);

    frm_ContraCheque.qListaBeneficiados.Close;
    frm_ContraCheque.qListaBeneficiados.Parameters.ParamByName('mes').Value := edtMes.Text;
    frm_ContraCheque.qListaBeneficiados.Parameters.ParamByName('ano').Value := strtoint(edtAno.Text);
    frm_ContraCheque.qListaBeneficiados.Parameters.ParamByName('banco').Value := dm.qListaBancoscd_banco.Value;

    
    frm_ContraCheque.lblReferencia.Caption := DM.getMesExtenso(strtoint( edtmes.Text))+'/'+edtAno.Text;
    //frm_ContraCheque.RLLabel28.Caption := DM.getMesExtenso(strtoint( edtmes.Text))+'/'+edtAno.Text;


    //frm_ContraCheque.lblReferencia.Caption := 'REFER�NCIA: '+ datetostr(Now);
    //frm_ContraCheque.RLLabel28.Caption := 'REFER�NCIA: '+datetostr(Now);

    if rbAbrangencia.ItemIndex = 1 then
      frm_ContraCheque.qListaBeneficiados.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text)
    else
      frm_ContraCheque.qListaBeneficiados.Parameters.ParamByName('codigo').Value := 0;
    begin


      frm_ContraCheque.qListaBeneficiados.Open;




      if frm_ContraCheque.qListaBeneficiados.RecordCount <= 0then
      begin
        ShowMessage('Recibo(s) n�o encontrado para esta refer�ncia');
        frm_ContraCheque.Destroy;
        exit;
      end;
      frm_ContraCheque.qListaVantagemDesconto.Open;
      frm_ContraCheque.RLReport1.Preview();
    end;
  end
  else
  begin
    // Imprimir Ficha Financeira
    {Application.CreateForm(Tfrm_FichaFinanceira,frm_FichaFinanceira);

    frm_FichaFinanceira.qListaBeneficiados.Close;
    frm_FichaFinanceira.qListaBeneficiados.Parameters.ParamByName('mes').Value := edtMes.Text;
    frm_FichaFinanceira.qListaBeneficiados.Parameters.ParamByName('ano').Value := strtoint(edtAno.Text);
    frm_FichaFinanceira.qListaBeneficiados.Parameters.ParamByName('banco').Value := dm.qListaBancoscd_banco.Value;

    if rbAbrangencia.ItemIndex = 1 then
      frm_FichaFinanceira.qListaBeneficiados.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text)
    else
      frm_FichaFinanceira.qListaBeneficiados.Parameters.ParamByName('codigo').Value := 0;



    frm_FichaFinanceira.qListaBeneficiados.Open;
    frm_FichaFinanceira.RLLabel3.Caption :=
      frm_FichaFinanceira.RLLabel3.Caption + ' ' + frm_FichaFinanceira.qListaBeneficiadosnm_banco.Value;

    if frm_FichaFinanceira.qListaBeneficiados.RecordCount <= 0 then
      begin
        ShowMessage('Contra-cheque n�o encontrado para esta refer�ncia aqui');
        frm_FichaFinanceira.Destroy;
        exit;
      end;
      frm_FichaFinanceira.qListaVantagemDesconto.Open;

      frm_FichaFinanceira.RLReport1.Preview();
     }
  end;





end;

procedure Tfrm_FiltroContraCheque.Button1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
  frm_PesquisaContribuinte.origem := 9;
  frm_PesquisaContribuinte.show;
end;

procedure Tfrm_FiltroContraCheque.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

procedure Tfrm_FiltroContraCheque.FormCreate(Sender: TObject);
begin
  dm.qListaBancos.Open;
end;

procedure Tfrm_FiltroContraCheque.FormKeyPress(Sender: TObject; var Key: Char);
begin
  If key = #13 then
  Begin
    Key:= #0;
    Perform(Wm_NextDlgCtl,0,0);
  end;
end;

procedure Tfrm_FiltroContraCheque.FormShow(Sender: TObject);
begin
  edtMes.SetFocus;
end;

end.
