unit Unit_Contribuintes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, FMTBcd, ActnList, Provider, DBClient, DB, SqlExpr,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, Mask, ADODB;

type
  Tfrm_Contribuinte = class(Tfrm_PadraoCadastro)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    dsCategoria: TDataSource;
    DBLookupComboBox1: TDBLookupComboBox;
    DBLookupComboBox2: TDBLookupComboBox;
    dsEstadoCivil: TDataSource;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    Label9: TLabel;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    DBEdit9: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit10: TDBEdit;
    Label13: TLabel;
    DBEdit11: TDBEdit;
    Label14: TLabel;
    DBEdit12: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBLookupComboBox3: TDBLookupComboBox;
    DBLookupComboBox4: TDBLookupComboBox;
    DBLookupComboBox5: TDBLookupComboBox;
    Label18: TLabel;
    DBEdit13: TDBEdit;
    Label19: TLabel;
    DBLookupComboBox6: TDBLookupComboBox;
    Label20: TLabel;
    DBEdit14: TDBEdit;
    Label21: TLabel;
    DBEdit15: TDBEdit;
    Label22: TLabel;
    DBEdit16: TDBEdit;
    Label23: TLabel;
    DBEdit17: TDBEdit;
    Label24: TLabel;
    DBEdit18: TDBEdit;
    Label25: TLabel;
    DBEdit19: TDBEdit;
    Label26: TLabel;
    DBEdit20: TDBEdit;
    Label27: TLabel;
    DBEdit21: TDBEdit;
    Label28: TLabel;
    DBEdit22: TDBEdit;
    QCategoria: TADOQuery;
    QEstadoCivil: TADOQuery;
    QPrincipalcd_formTeologica: TIntegerField;
    QPrincipalcd_natPastor: TIntegerField;
    QPrincipalcd_escPastor: TIntegerField;
    QPrincipalcd_nacPastor: TIntegerField;
    QPrincipalcd_estCivil: TIntegerField;
    QPrincipalcd_categoria: TIntegerField;
    QPrincipalnm_pastor: TStringField;
    QPrincipalno_regConv: TIntegerField;
    QPrincipaldt_nascPastor: TDateTimeField;
    QPrincipaltp_sanguineo: TStringField;
    QPrincipalno_regGeral: TStringField;
    QPrincipalno_cpf: TStringField;
    QPrincipalds_endereco: TStringField;
    QPrincipalds_compEndPastor: TStringField;
    QPrincipalno_cepPastor: TStringField;
    QPrincipaldt_filiacao: TDateTimeField;
    QPrincipalnm_pai: TStringField;
    QPrincipalnm_mae: TStringField;
    QPrincipalnm_conjuge: TStringField;
    QPrincipaldt_nascConjuge: TDateTimeField;
    QPrincipalno_fone: TStringField;
    QPrincipaldt_batismo: TDateTimeField;
    QPrincipalds_localBatismo: TStringField;
    QPrincipaldt_autEvangelista: TDateTimeField;
    QPrincipaldt_consagEvangelista: TDateTimeField;
    QPrincipaldt_ordenacPastor: TDateTimeField;
    QPrincipalds_localConsagracao: TStringField;
    QPrincipalds_campo: TStringField;
    QPrincipalds_supervisao: TStringField;
    QPrincipalno_certCasamento: TIntegerField;
    CDSPrincipalcd_formTeologica: TIntegerField;
    CDSPrincipalcd_natPastor: TIntegerField;
    CDSPrincipalcd_escPastor: TIntegerField;
    CDSPrincipalcd_nacPastor: TIntegerField;
    CDSPrincipalcd_estCivil: TIntegerField;
    CDSPrincipalcd_categoria: TIntegerField;
    CDSPrincipalnm_pastor: TStringField;
    CDSPrincipalno_regConv: TIntegerField;
    CDSPrincipaldt_nascPastor: TDateTimeField;
    CDSPrincipaltp_sanguineo: TStringField;
    CDSPrincipalno_regGeral: TStringField;
    CDSPrincipalno_cpf: TStringField;
    CDSPrincipalds_endereco: TStringField;
    CDSPrincipalds_compEndPastor: TStringField;
    CDSPrincipalno_cepPastor: TStringField;
    CDSPrincipaldt_filiacao: TDateTimeField;
    CDSPrincipalnm_pai: TStringField;
    CDSPrincipalnm_mae: TStringField;
    CDSPrincipalnm_conjuge: TStringField;
    CDSPrincipaldt_nascConjuge: TDateTimeField;
    CDSPrincipalno_fone: TStringField;
    CDSPrincipaldt_batismo: TDateTimeField;
    CDSPrincipalds_localBatismo: TStringField;
    CDSPrincipaldt_autEvangelista: TDateTimeField;
    CDSPrincipaldt_consagEvangelista: TDateTimeField;
    CDSPrincipaldt_ordenacPastor: TDateTimeField;
    CDSPrincipalds_localConsagracao: TStringField;
    CDSPrincipalds_campo: TStringField;
    CDSPrincipalds_supervisao: TStringField;
    CDSPrincipalno_certCasamento: TIntegerField;
    QNacionalidade: TADOQuery;
    dsNacionalidade: TDataSource;
    dsNaturalidade: TDataSource;
    QNaturalidade: TADOQuery;
    QEscolaridade: TADOQuery;
    dsEscolaridade: TDataSource;
    dsFormTeologica: TDataSource;
    QFormTeologica: TADOQuery;
    Label29: TLabel;
    DBEdit23: TDBEdit;
    Label30: TLabel;
    DBEdit24: TDBEdit;
    Label31: TLabel;
    DBEdit25: TDBEdit;
    QPrincipalds_orgaoemissorrg: TStringField;
    QPrincipaldt_emissao: TDateTimeField;
    QPrincipalds_bairro: TStringField;
    QPrincipalds_uf: TStringField;
    QPrincipalds_cidade: TStringField;
    CDSPrincipalds_orgaoemissorrg: TStringField;
    CDSPrincipaldt_emissao: TDateTimeField;
    CDSPrincipalds_bairro: TStringField;
    CDSPrincipalds_uf: TStringField;
    CDSPrincipalds_cidade: TStringField;
    BitBtn1: TBitBtn;
    QPrincipaldt_falecimento: TWideStringField;
    QPrincipalcd_banco: TIntegerField;
    CDSPrincipaldt_falecimento: TWideStringField;
    CDSPrincipalcd_banco: TIntegerField;
    qBancos: TADOQuery;
    qBancoscd_banco: TAutoIncField;
    qBancosnm_banco: TStringField;
    dsBancos: TDataSource;
    DBLookupComboBox7: TDBLookupComboBox;
    lbl: TLabel;
    Label32: TLabel;
    DBEdit26: TDBEdit;
    QPrincipalcd_cadastro: TIntegerField;
    CDSPrincipalcd_cadastro: TIntegerField;
    procedure btnNovoClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnRelatorioClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Contribuinte: Tfrm_Contribuinte;

implementation

uses Unit_DM, Unit_PesquisaContribuinte, Unit_RelatorioContribuintes,
  Unit_FichaContribuinte;

{$R *.dfm}

procedure Tfrm_Contribuinte.btnNovoClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
  if not qCategoria.Active then qCategoria.Open;

  if not QEstadoCivil.Active then QEstadoCivil.Open;

  if not QFormTeologica.Active then QFormTeologica.Open;

  if not QNacionalidade.Active then  QNacionalidade.Open;

  if not QNaturalidade.Active then QNaturalidade.Open;

  if not QEscolaridade.Active then QEscolaridade.Open;

  if not qBancos.Active then qBancos.Open;

  dm.qNoCabepa.Open;
  DBEdit1.Text :=  inttostr(dm.qNoCabepacd_cadastro.Value);
  dm.qNoCabepa.close;


end;

procedure Tfrm_Contribuinte.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
  frm_PesquisaContribuinte.origem := 1;
  frm_PesquisaContribuinte.ShowModal;
end;

procedure Tfrm_Contribuinte.btnRelatorioClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(Tfrm_RelatorioContribuintes,frm_RelatorioContribuintes);
  frm_RelatorioContribuintes.qRelatorio.Open;
  frm_RelatorioContribuintes.RLReport1.Preview();
  frm_RelatorioContribuintes.qRelatorio.Close;
  frm_RelatorioContribuintes.Free;
end;

procedure Tfrm_Contribuinte.btnSalvarClick(Sender: TObject);
begin

  if DBEdit2.Text = '' then
  begin
    MessageDlg('Campo Obrigat�rio: Nome Pastor', mtInformation, [mbOk], 0);
    DBEdit2.SetFocus;
    abort;
  end;
  if DBLookupComboBox1.Text = '' then
  begin
    MessageDlg('Campo Obrigat�rio: Categoria', mtInformation, [mbOk], 0);
    DBLookupComboBox1.SetFocus;
    abort;
  end;
  if DBEdit3.Text = '' then
  begin
    MessageDlg('Campo Obrigat�rio: No. Registro Convens�o', mtInformation, [mbOk], 0);
    DBEdit3.SetFocus;
    abort;
  end;
  if DBEdit4.Text = '  /  /    ' then
  begin
    MessageDlg('Campo Obrigat�rio: Data Nascimento', mtInformation, [mbOk], 0);
    DBEdit4.SetFocus;
    abort;
  end;
  if DBEdit7.Text = '   .   .   -  ' then
  begin
    MessageDlg('Campo Obrigat�rio: No.CPF', mtInformation, [mbOk], 0);
    DBEdit7.SetFocus;
    abort;
  end;
  if DBEdit10.Text = '' then
  begin
    MessageDlg('Campo Obrigat�rio: Nome da M�e', mtInformation, [mbOk], 0);
    DBEdit10.SetFocus;
    abort;
  end;
  if DBLookupComboBox2.Text = '' then
  begin
    MessageDlg('Campo Obrigat�rio: Estado Civil', mtInformation, [mbOk], 0);
    DBLookupComboBox2.SetFocus;
    abort;
  end;
  if DBLookupComboBox7.Text = '' then
  begin
    MessageDlg('Campo Obrigat�rio: Banco de Recebimento', mtInformation, [mbOk], 0);
    DBLookupComboBox7.SetFocus;
    abort;
  end;

  if DBLookupComboBox3.Text = '' then
  begin
    MessageDlg('Campo Obrigat�rio: Nacionalidade', mtInformation, [mbOk], 0);
    DBLookupComboBox3.SetFocus;
    abort;
  end;
  if DBLookupComboBox4.Text = '' then
  begin
    MessageDlg('Campo Obrigat�rio: Naturalidade', mtInformation, [mbOk], 0);
    DBLookupComboBox4.SetFocus;
    abort;
  end;
  if DBLookupComboBox5.Text = '' then
  begin
    MessageDlg('Campo Obrigat�rio: Escolaridade', mtInformation, [mbOk], 0);
    DBLookupComboBox5.SetFocus;
    abort;
  end;
  if DBLookupComboBox6.Text = '' then
  begin
    MessageDlg('Campo Obrigat�rio: Forma��o Teol�gica', mtInformation, [mbOk], 0);
    DBLookupComboBox6.SetFocus;
    abort;
  end;
  if DBEdit23.Text = '' then
  begin
    MessageDlg('Campo Obrigat�rio: Endere�o', mtInformation, [mbOk], 0);
    DBEdit23.SetFocus;
    abort;
  end;



  inherited;

end;

procedure Tfrm_Contribuinte.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if  qCategoria.Active then qCategoria.close;

  if  QEstadoCivil.Active then QEstadoCivil.close;

  if  QFormTeologica.Active then QFormTeologica.close;

  if  QNacionalidade.Active then  QNacionalidade.close;

  if  QNaturalidade.Active then QNaturalidade.close;

  if  QEscolaridade.Active then QEscolaridade.close;

  if  qBancos.Active then qBancos.close;
  inherited;

end;

procedure Tfrm_Contribuinte.FormCreate(Sender: TObject);
begin
  inherited;
  self.WindowState := wsMaximized;
end;

procedure Tfrm_Contribuinte.BitBtn1Click(Sender: TObject);
begin
  inherited;

  if CDSPrincipal.Active then
  begin
    Application.CreateForm(Tfrm_FichaContribuinte,frm_FichaContribuinte);
    frm_FichaContribuinte.qRelatorio.close;
    frm_FichaContribuinte.qRelatorio.Parameters.ParamByName('codigo').Value := CDSPrincipalcd_cadastro.Value;
    frm_FichaContribuinte.qRelatorio.Open;
    frm_FichaContribuinte.RLReport1.Preview();
    frm_FichaContribuinte.Free;
  end
  else
    showmessage('Selecione um contribuinte!');

end;

end.
