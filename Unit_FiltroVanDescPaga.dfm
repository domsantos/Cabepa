object frm_FiltroVanDescPaga: Tfrm_FiltroVanDescPaga
  Left = 10
  Top = 10
  Caption = 'Vantagem Desconto Paga'
  ClientHeight = 390
  ClientWidth = 666
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 666
    Height = 89
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 43
      Width = 19
      Height = 13
      Caption = 'M'#234's'
    end
    object Label2: TLabel
      Left = 63
      Top = 43
      Width = 19
      Height = 13
      Caption = 'Ano'
    end
    object Label3: TLabel
      Left = 8
      Top = 24
      Width = 52
      Height = 13
      Caption = 'Refer'#234'ncia'
    end
    object Rubrica: TLabel
      Left = 119
      Top = 43
      Width = 36
      Height = 13
      Caption = 'Rubrica'
    end
    object DBNavigator1: TDBNavigator
      Left = 430
      Top = 5
      Width = 228
      Height = 25
      DataSource = dsLista
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbRefresh]
      TabOrder = 0
    end
    object edtMes: TEdit
      Left = 8
      Top = 62
      Width = 49
      Height = 21
      TabOrder = 1
      OnKeyPress = edtMesKeyPress
    end
    object edtAno: TEdit
      Left = 63
      Top = 62
      Width = 50
      Height = 21
      TabOrder = 2
      OnKeyPress = edtAnoKeyPress
    end
    object BitBtn1: TBitBtn
      Left = 270
      Top = 44
      Width = 99
      Height = 39
      Caption = 'Pesquisar'
      TabOrder = 4
      OnClick = BitBtn1Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F1863314A734EFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75E724E6B2DCE39FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        186393526A2DEE3D1863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7F186394528B31CE395A6BFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75EB5568C35CE3D5A6BFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863
        D55A8B31CD395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7F1863D65AAD35CE395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F9C73396739677B6FFF7FFF7FFF7FBD77B556AD39CE395A6BFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F5A6BD65A1863596B5A6B3967F75E1863BD77B556
        CE39CE395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A39677B6F7B6F5A6B
        5A6B7B6F9C739C73D65A8C35734EBD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        D65A5A6B5A6B5A6B5A6B5A6B5A6B5A6B5A6B7A6FBD779452DE7BFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7F7B6F18635A6B5A6B5A6B7A6F7B6F7B6F7A6F5A6B5A6B
        5A6B9C73F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A5A6B5A6B7B6F7B6F
        7B6F7B6F7B6F7B6F7B6F7B6F5A6B9C73F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FF75E396B7B6F7B6F9B739C739C739C739C739B737B6F7B6F7B6F39675A6B
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F18635A6B9C739B739C739C73BD77BD779C73
        9C739C737B6F7B6F5A6B3967FF7FFF7FFF7FFF7FFF7FFF7FFF7F38677B6FBD77
        BD77BD77BD77BD77BD77BD77BD77BD77BD779C73596B3967FF7FFF7FFF7FFF7F
        FF7FFF7FFF7FF75E9C73DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BBD77BD77
        F7627B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7F18637B6FDE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BBD77B556FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77
        18639C73FF7FDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B39673967FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F18635A6BBD77FF7FFF7FFF7FFF7FFF7FFF7FDE7B
        7B6FB556FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F396718637B6F
        BD77BD77BD77BD779C731863D65AFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FBD77396B186318631863F75E18637B6FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F}
    end
    object cbRubrica: TDBLookupComboBox
      Left = 119
      Top = 62
      Width = 145
      Height = 21
      KeyField = 'CD_tipRubrica'
      ListField = 'ds_rubrica'
      ListSource = DataSource1
      TabOrder = 3
    end
    object BitBtn2: TBitBtn
      Left = 544
      Top = 43
      Width = 114
      Height = 40
      Caption = 'Imprimir'
      TabOrder = 5
      OnClick = BitBtn2Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7F9C73F75EF75EF75EF75EF75EF75EF75EF75EF75E
        F75EF75EF75EF75EF75E9C73FF7FFF7FFF7FFF7FFF7FFF7F9C739C73FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C739C73FF7FFF7FFF7FFF7F
        FF7FFF7FDE7B5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        5A6BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863BD779C739C739C739C739C73
        9C739C739C739C739C73BD771863FF7FFF7FFF7FFF7FFF7FDE7B9C739C73734E
        3967F75EF75EF75EF75EF75EF75EF75EF75EF75EF75E3967524A9C739C73DE7B
        FF7F5A6BB5569452945231469452945294529452945294529452945294529452
        94529452314694529452B5565A6B1863734E3A67F75E6C314B296B2D6B2D6B2D
        6B2D6B2D6B2D6B2D6B2D6B2D6B2D6B2D8C31D75A3A67734E18635A6BB6569C73
        5B6B6B2D29252925292529252925292509250925082508210821082109253967
        9C73B6565A6B9C731863BD73BD773146EF3DCE3DCE39AD358D358C316B2D4A2D
        4A2929252925E72029257B6FBD7718639C73BD7739677B6F9D739452524A524A
        31461042EF41EF3DCE39AD39AD358C316B2D4A298C319C737B6F3967BD77DE7B
        7B6B5A6B9C73D65AD65AF75E1863186339673967186339671863F75EB556734E
        524A9C735A6B7B6BDE7BFF7F7B6F5A6BBD77D656F75E18631863F75EF75E1963
        1863F75ED75A1863185FF85EF75EBD775A6B7B6FFF7FFF7FBD7718637B6FD65A
        F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75ED65A5A6B1863BD77
        FF7FFF7FFF7FBD779C731863DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
        DE7BFF7F18639C73BD77FF7FFF7FFF7FFF7FFF7FFF7F7A6BBE77DE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B7B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7F7B6FBD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDF7B1863FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73BD77DE7BDE7BDE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7B5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6FBD77DE7B
        DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BBD779C73FF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F7B6FBD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
        9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6FBD77DE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BDE7B9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F
        BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B9C73FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FBD77F75EF75EF75EF75EF75EF75EF75EF75EF75EF75E
        F75EF75E9C73FF7FFF7FFF7FFF7F}
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 89
    Width = 666
    Height = 301
    Align = alClient
    DataSource = dsLista
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object qListaPessoas: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'ano'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'mes'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'rubrica'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select'
      '  p.cd_cadastro,'
      #9'case c.cd_sitJubilamento'
      #9#9'when 1 then c.nm_pastor'
      #9#9'when 2 then c.nm_conjuge'
      #9'end as nm_beneficiado,'
      #9'p.vl_Rubrica,'
      #9'v.nm_Rubrica'
      'from tbPagamentoMensal p'
      'inner join tbContribuinte c on c.cd_cadastro = p.cd_cadastro'
      'inner join tbVantDesc v on v.cd_tipRubrica = p.cd_tipRubrica'
      'where'
      #9'p.dt_anoPagamento = :ano'
      #9'and p.dt_mesPagamento = :mes'
      #9'and p.cd_tipRubrica = :rubrica'
      'order by 2')
    Left = 576
    Top = 96
    object qListaPessoasnm_beneficiado: TStringField
      DisplayLabel = 'Beneficiado'
      FieldName = 'nm_beneficiado'
      ReadOnly = True
      Size = 60
    end
    object qListaPessoasnm_Rubrica: TStringField
      DisplayLabel = 'Rubrica'
      FieldName = 'nm_Rubrica'
    end
    object qListaPessoasvl_Rubrica: TBCDField
      DisplayLabel = 'Valor'
      FieldName = 'vl_Rubrica'
      currency = True
      Precision = 10
      Size = 2
    end
    object qListaPessoascd_cadastro: TSmallintField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'cd_cadastro'
    end
  end
  object dsLista: TDataSource
    DataSet = qListaPessoas
    Left = 504
    Top = 96
  end
  object qListaRubricas: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      #9'v.cd_tipRubrica,'
      #9'case v.no_Rubrica'
      #9#9'when 1 then '#39'VN'#39
      #9#9'when 2 then '#39'DN'#39
      #9'end + '#39' - '#39' + v.nm_Rubrica as ds_rubrica'
      #9',v.no_Rubrica'
      'from tbVantDesc v'
      'where v.cd_tipRubrica <> 1'
      'order by v.no_Rubrica,v.nm_Rubrica ')
    Left = 576
    Top = 168
    object qListaRubricascd_tipRubrica: TSmallintField
      FieldName = 'cd_tipRubrica'
      ReadOnly = True
    end
    object qListaRubricasds_rubrica: TStringField
      FieldName = 'ds_rubrica'
      ReadOnly = True
      Size = 25
    end
    object qListaRubricasno_Rubrica: TSmallintField
      FieldName = 'no_Rubrica'
    end
  end
  object DataSource1: TDataSource
    DataSet = qListaRubricas
    Left = 576
    Top = 224
  end
end
