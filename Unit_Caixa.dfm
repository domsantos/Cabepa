object frmCaixa: TfrmCaixa
  Left = 0
  Top = 0
  ActiveControl = edtData
  Caption = 'Caixa'
  ClientHeight = 547
  ClientWidth = 706
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 706
    Height = 91
    Align = alTop
    TabOrder = 0
    ExplicitTop = -6
    DesignSize = (
      706
      91)
    object Label2: TLabel
      Left = 8
      Top = 16
      Width = 72
      Height = 19
      Caption = 'Data Incio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 215
      Top = 16
      Width = 60
      Height = 19
      Caption = 'Usu'#225'rio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 49
      Width = 70
      Height = 19
      Caption = 'Data Final'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 215
      Top = 49
      Width = 38
      Height = 19
      Caption = 'Tipo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object edtData: TMaskEdit
      Left = 89
      Top = 13
      Width = 120
      Height = 27
      EditMask = '!99/99/0000;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '  /  /    '
    end
    object BitBtn1: TBitBtn
      Left = 599
      Top = 57
      Width = 98
      Height = 28
      Anchors = [akTop, akRight]
      Caption = 'Pesquisar'
      TabOrder = 2
      OnClick = BitBtn1Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F1863314A734EFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75E724E6B2DCE39FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        186393526A2DEE3D1863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7F186394528B31CE395A6BFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75EB5568C35CE3D5A6BFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863
        D55A8B31CD395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7F1863D65AAD35CE395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F9C73396739677B6FFF7FFF7FFF7FBD77B556AD39CE395A6BFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F5A6BD65A1863596B5A6B3967F75E1863BD77B556
        CE39CE395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A39677B6F7B6F5A6B
        5A6B7B6F9C739C73D65A8C35734EBD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        D65A5A6B5A6B5A6B5A6B5A6B5A6B5A6B5A6B7A6FBD779452DE7BFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7F7B6F18635A6B5A6B5A6B7A6F7B6F7B6F7A6F5A6B5A6B
        5A6B9C73F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A5A6B5A6B7B6F7B6F
        7B6F7B6F7B6F7B6F7B6F7B6F5A6B9C73F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FF75E396B7B6F7B6F9B739C739C739C739C739B737B6F7B6F7B6F39675A6B
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F18635A6B9C739B739C739C73BD77BD779C73
        9C739C737B6F7B6F5A6B3967FF7FFF7FFF7FFF7FFF7FFF7FFF7F38677B6FBD77
        BD77BD77BD77BD77BD77BD77BD77BD77BD779C73596B3967FF7FFF7FFF7FFF7F
        FF7FFF7FFF7FF75E9C73DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BBD77BD77
        F7627B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7F18637B6FDE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BBD77B556FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77
        18639C73FF7FDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B39673967FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F18635A6BBD77FF7FFF7FFF7FFF7FFF7FFF7FDE7B
        7B6FB556FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F396718637B6F
        BD77BD77BD77BD779C731863D65AFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FBD77396B186318631863F75E18637B6FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F}
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 281
      Top = 13
      Width = 248
      Height = 27
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'id_usuario'
      ListField = 'nm_usuario'
      ListSource = DataSource2
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 507
    Width = 706
    Height = 40
    Align = alBottom
    TabOrder = 2
    object blbValor: TLabel
      Left = 671
      Top = 6
      Width = 3
      Height = 13
      Alignment = taRightJustify
    end
    object Label1: TLabel
      Left = 554
      Top = 6
      Width = 28
      Height = 13
      Caption = 'Total:'
    end
    object BitBtn2: TBitBtn
      Left = 8
      Top = 6
      Width = 105
      Height = 31
      Caption = 'Imprimir'
      TabOrder = 0
      OnClick = BitBtn2Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7F9C73F75EF75EF75EF75EF75EF75EF75EF75EF75E
        F75EF75EF75EF75EF75E9C73FF7FFF7FFF7FFF7FFF7FFF7F9C739C73FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C739C73FF7FFF7FFF7FFF7F
        FF7FFF7FDE7B5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        5A6BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863BD779C739C739C739C739C73
        9C739C739C739C739C73BD771863FF7FFF7FFF7FFF7FFF7FDE7B9C739C73734E
        3967F75EF75EF75EF75EF75EF75EF75EF75EF75EF75E3967524A9C739C73DE7B
        FF7F5A6BB5569452945231469452945294529452945294529452945294529452
        94529452314694529452B5565A6B1863734E3A67F75E6C314B296B2D6B2D6B2D
        6B2D6B2D6B2D6B2D6B2D6B2D6B2D6B2D8C31D75A3A67734E18635A6BB6569C73
        5B6B6B2D29252925292529252925292509250925082508210821082109253967
        9C73B6565A6B9C731863BD73BD773146EF3DCE3DCE39AD358D358C316B2D4A2D
        4A2929252925E72029257B6FBD7718639C73BD7739677B6F9D739452524A524A
        31461042EF41EF3DCE39AD39AD358C316B2D4A298C319C737B6F3967BD77DE7B
        7B6B5A6B9C73D65AD65AF75E1863186339673967186339671863F75EB556734E
        524A9C735A6B7B6BDE7BFF7F7B6F5A6BBD77D656F75E18631863F75EF75E1963
        1863F75ED75A1863185FF85EF75EBD775A6B7B6FFF7FFF7FBD7718637B6FD65A
        F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75ED65A5A6B1863BD77
        FF7FFF7FFF7FBD779C731863DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
        DE7BFF7F18639C73BD77FF7FFF7FFF7FFF7FFF7FFF7F7A6BBE77DE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B7B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7F7B6FBD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDF7B1863FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73BD77DE7BDE7BDE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7B5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6FBD77DE7B
        DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BBD779C73FF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F7B6FBD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
        9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6FBD77DE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BDE7B9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F
        BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B9C73FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FBD77F75EF75EF75EF75EF75EF75EF75EF75EF75EF75E
        F75EF75E9C73FF7FFF7FFF7FFF7F}
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 91
    Width = 706
    Height = 416
    Align = alClient
    DataSource = DataSource1
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object edtFim: TMaskEdit
    Left = 89
    Top = 46
    Width = 120
    Height = 27
    EditMask = '!99/99/0000;1;_'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 10
    ParentFont = False
    TabOrder = 1
    Text = '  /  /    '
  end
  object DBLookupComboBox2: TDBLookupComboBox
    Left = 281
    Top = 46
    Width = 248
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyField = 'cd_tipoContribuicao'
    ListField = 'nm_tipoContribuicao'
    ListSource = DataSource3
    ParentFont = False
    TabOrder = 3
  end
  object qCaixa: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'diainicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'diafim'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'tipo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select c.*,p.nm_pastor,u.nm_usuario,t.nm_tipoContribuicao from t' +
        'bContribuicoes c'
      'inner join tbUsuario u on u.id_usuario = c.id_usuario'
      'inner join tbContribuinte p on p.cd_cadastro = c.cd_cadastro'
      
        'inner join tbTipoContribuicao t on t.cd_tipoContribuicao = c.cd_' +
        'tipoContribuicao'
      
        'where dt_Contribuicao between :diainicio and :diafim and c.id_us' +
        'uario = :id'
      'and t.cd_tipoContribuicao in(:tipo)'
      'order by dt_Contribuicao')
    Left = 512
    Top = 120
    object qCaixanm_pastor: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qCaixanm_tipoContribuicao: TStringField
      DisplayLabel = 'Contribui'#231#227'o'
      DisplayWidth = 15
      FieldName = 'nm_tipoContribuicao'
      Size = 50
    end
    object qCaixadt_anoRefContribuicao: TIntegerField
      DisplayLabel = 'Ano Ref'
      FieldName = 'dt_anoRefContribuicao'
    end
    object qCaixadt_mesRefContribuicao: TIntegerField
      DisplayLabel = 'M'#234's Ref'
      FieldName = 'dt_mesRefContribuicao'
    end
    object qCaixavl_ContribuicaoVencimento: TBCDField
      DisplayLabel = 'Valor'
      FieldName = 'vl_ContribuicaoVencimento'
      currency = True
      Precision = 10
      Size = 2
    end
    object qCaixacd_tipoContribuicao: TIntegerField
      FieldName = 'cd_tipoContribuicao'
      Visible = False
    end
    object qCaixacd_sitQuitacao: TSmallintField
      FieldName = 'cd_sitQuitacao'
      Visible = False
    end
    object qCaixadt_Contribuicao: TDateTimeField
      DisplayLabel = 'Data'
      FieldName = 'dt_Contribuicao'
    end
    object qCaixaid_usuario: TIntegerField
      FieldName = 'id_usuario'
      Visible = False
    end
    object qCaixanm_usuario: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'nm_usuario'
      Size = 100
    end
    object qCaixacd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object DataSource1: TDataSource
    DataSet = qCaixa
    Left = 512
    Top = 168
  end
  object qUsuarios: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select id_usuario, nm_usuario'
      'from tbUsuario')
    Left = 608
    Top = 112
    object qUsuariosid_usuario: TAutoIncField
      FieldName = 'id_usuario'
      ReadOnly = True
    end
    object qUsuariosnm_usuario: TStringField
      FieldName = 'nm_usuario'
      Size = 100
    end
  end
  object DataSource2: TDataSource
    DataSet = qUsuarios
    Left = 608
    Top = 168
  end
  object qTipo: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_tipoContribuicao, nm_tipoContribuicao'
      'from tbTipoContribuicao')
    Left = 416
    Top = 120
    object qTipocd_tipoContribuicao: TAutoIncField
      FieldName = 'cd_tipoContribuicao'
      ReadOnly = True
    end
    object qTiponm_tipoContribuicao: TStringField
      FieldName = 'nm_tipoContribuicao'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qTipo
    Left = 416
    Top = 176
  end
  object qTotal: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'diainicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'diafim'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'tipo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select sum(c.vl_ContribuicaoVencimento) total from tbContribuico' +
        'es c'
      'inner join tbUsuario u on u.id_usuario = c.id_usuario'
      'inner join tbContribuinte p on p.cd_cadastro = c.cd_cadastro'
      
        'inner join tbTipoContribuicao t on t.cd_tipoContribuicao = c.cd_' +
        'tipoContribuicao'
      
        'where dt_Contribuicao between :diainicio and :diafim and c.id_us' +
        'uario = :id'
      'and t.cd_tipoContribuicao in(:tipo)')
    Left = 512
    Top = 240
    object qTotaltotal: TBCDField
      FieldName = 'total'
      ReadOnly = True
      Precision = 38
      Size = 2
    end
  end
end
