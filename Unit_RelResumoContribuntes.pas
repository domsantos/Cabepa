unit Unit_RelResumoContribuntes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, ADODB, RLReport, jpeg;

type
  Tfrm_ResumoContribuintes = class(Tfrm_RelatorioPadrao)
    qRelatorio: TADOQuery;
    qRelatoriocd_sitJubilamento: TStringField;
    qRelatoriods_situacao: TStringField;
    qRelatorioqtd: TIntegerField;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_ResumoContribuintes: Tfrm_ResumoContribuintes;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_ResumoContribuintes.FormCreate(Sender: TObject);
begin
  inherited;
  qRelatorio.Open;
end;

end.
