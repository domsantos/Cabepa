unit Unit_RelCedulaCIndividual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RLReport, DB, ADODB;

type
  Tfrm_RelCedulaCIndividual = class(TForm)
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLDraw1: TRLDraw;
    RLDraw2: TRLDraw;
    RLLabel1: TRLLabel;
    RLDraw3: TRLDraw;
    RLLabel2: TRLLabel;
    RLDraw4: TRLDraw;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLDraw7: TRLDraw;
    RLLabel10: TRLLabel;
    RLLabel11: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLDraw8: TRLDraw;
    RLDraw6: TRLDraw;
    RLLabel8: TRLLabel;
    RLLabel14: TRLLabel;
    RLLabel15: TRLLabel;
    RLDraw10: TRLDraw;
    RLDraw9: TRLDraw;
    RLDraw11: TRLDraw;
    RLDraw5: TRLDraw;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel16: TRLLabel;
    RLLabel17: TRLLabel;
    RLLabel18: TRLLabel;
    RLLabel19: TRLLabel;
    RLDraw12: TRLDraw;
    RLLabel20: TRLLabel;
    RLLabel21: TRLLabel;
    RLDraw13: TRLDraw;
    RLDraw15: TRLDraw;
    RLLabel22: TRLLabel;
    RLDraw16: TRLDraw;
    RLDraw17: TRLDraw;
    RLDraw18: TRLDraw;
    RLLabel23: TRLLabel;
    RLDraw19: TRLDraw;
    RLLabel24: TRLLabel;
    RLDraw20: TRLDraw;
    RLDraw21: TRLDraw;
    RLLabel25: TRLLabel;
    RLLabel26: TRLLabel;
    RLLabel27: TRLLabel;
    RLDraw22: TRLDraw;
    RLDraw14: TRLDraw;
    RLLabel29: TRLLabel;
    RLDraw23: TRLDraw;
    RLLabel30: TRLLabel;
    RLLabel31: TRLLabel;
    RLDraw24: TRLDraw;
    RLDraw25: TRLDraw;
    RLLabel32: TRLLabel;
    RLLabel33: TRLLabel;
    RLDraw26: TRLDraw;
    RLDraw27: TRLDraw;
    RLLabel34: TRLLabel;
    RLLabel35: TRLLabel;
    RLDraw28: TRLDraw;
    RLDraw29: TRLDraw;
    RLLabel36: TRLLabel;
    RLLabel37: TRLLabel;
    RLDraw30: TRLDraw;
    RLDraw31: TRLDraw;
    RLLabel38: TRLLabel;
    RLLabel39: TRLLabel;
    RLDraw32: TRLDraw;
    RLDraw33: TRLDraw;
    RLLabel40: TRLLabel;
    RLLabel41: TRLLabel;
    RLLabel42: TRLLabel;
    RLDraw34: TRLDraw;
    RLDraw35: TRLDraw;
    RLLabel43: TRLLabel;
    RLLabel44: TRLLabel;
    RLLabel45: TRLLabel;
    RLDraw36: TRLDraw;
    RLLabel46: TRLLabel;
    RLDraw37: TRLDraw;
    RLLabel47: TRLLabel;
    RLDraw38: TRLDraw;
    RLLabel48: TRLLabel;
    RLDraw39: TRLDraw;
    RLLabel49: TRLLabel;
    RLLabel50: TRLLabel;
    RLDraw40: TRLDraw;
    RLDraw41: TRLDraw;
    RLLabel51: TRLLabel;
    RLLabel52: TRLLabel;
    RLLabel53: TRLLabel;
    RLLabel54: TRLLabel;
    RLLabel55: TRLLabel;
    RLDraw42: TRLDraw;
    qRelatorio: TADOQuery;
    dsRelatorio: TDataSource;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    RLDBText3: TRLDBText;
    RLDBText5: TRLDBText;
    RLMemo1: TRLMemo;
    lbAnoCalendario: TRLLabel;
    RLMemo2: TRLMemo;
    qRelatorionm_beneficiado: TStringField;
    qRelatoriono_cpf: TStringField;
    qRelatoriovl_rendimento_total: TBCDField;
    qRelatoriovl_dizimo: TBCDField;
    qRelatoriovl_ir: TBCDField;
    RLLabel28: TRLLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelCedulaCIndividual: Tfrm_RelCedulaCIndividual;

implementation

uses Unit_DM;

{$R *.dfm}

end.
