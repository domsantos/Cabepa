unit Unit_FichaContribuinte;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, RLReport, jpeg, ADODB;

type
  Tfrm_FichaContribuinte = class(Tfrm_RelatorioPadrao)
    qRelatorio: TADOQuery;
    RLLabel6: TRLLabel;
    qRelatoriocd_formTeologica: TIntegerField;
    qRelatoriocd_natPastor: TIntegerField;
    qRelatoriocd_escPastor: TIntegerField;
    qRelatoriocd_nacPastor: TIntegerField;
    qRelatoriocd_estCivil: TIntegerField;
    qRelatoriocd_categoria: TIntegerField;
    qRelatorionm_pastor: TStringField;
    qRelatoriono_regConv: TIntegerField;
    qRelatoriodt_nascPastor: TDateTimeField;
    qRelatoriotp_sanguineo: TStringField;
    qRelatoriono_regGeral: TStringField;
    qRelatoriono_cpf: TStringField;
    qRelatoriods_endereco: TStringField;
    qRelatoriods_compEndPastor: TStringField;
    qRelatoriono_cepPastor: TStringField;
    qRelatoriodt_filiacao: TDateTimeField;
    qRelatorionm_pai: TStringField;
    qRelatorionm_mae: TStringField;
    qRelatorionm_conjuge: TStringField;
    qRelatoriodt_nascConjuge: TDateTimeField;
    qRelatoriono_fone: TStringField;
    qRelatoriodt_batismo: TDateTimeField;
    qRelatoriods_localBatismo: TStringField;
    qRelatoriodt_autEvangelista: TDateTimeField;
    qRelatoriodt_consagEvangelista: TDateTimeField;
    qRelatoriodt_ordenacPastor: TDateTimeField;
    qRelatoriods_localConsagracao: TStringField;
    qRelatoriods_campo: TStringField;
    qRelatoriods_supervisao: TStringField;
    qRelatoriono_certCasamento: TIntegerField;
    qRelatoriods_orgaoemissorrg: TStringField;
    qRelatoriodt_emissao: TDateTimeField;
    qRelatoriods_bairro: TStringField;
    qRelatoriods_uf: TStringField;
    qRelatoriods_cidade: TStringField;
    qRelatoriods_categoria: TStringField;
    qRelatoriods_escPastor: TStringField;
    qRelatoriods_estCivil: TStringField;
    qRelatoriods_formTeologica: TStringField;
    qRelatoriods_nacPastor: TStringField;
    qRelatoriods_natPastor: TStringField;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel10: TRLLabel;
    RLLabel11: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLDraw1: TRLDraw;
    RLDraw2: TRLDraw;
    RLLabel14: TRLLabel;
    RLLabel15: TRLLabel;
    RLLabel16: TRLLabel;
    RLLabel17: TRLLabel;
    RLLabel18: TRLLabel;
    RLLabel19: TRLLabel;
    RLLabel20: TRLLabel;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLLabel23: TRLLabel;
    RLLabel24: TRLLabel;
    RLLabel25: TRLLabel;
    RLLabel26: TRLLabel;
    RLDraw3: TRLDraw;
    RLLabel27: TRLLabel;
    RLLabel28: TRLLabel;
    RLLabel29: TRLLabel;
    RLLabel30: TRLLabel;
    RLLabel31: TRLLabel;
    RLLabel32: TRLLabel;
    RLLabel33: TRLLabel;
    RLLabel34: TRLLabel;
    RLLabel35: TRLLabel;
    RLLabel36: TRLLabel;
    RLLabel37: TRLLabel;
    RLLabel38: TRLLabel;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBText6: TRLDBText;
    RLDBText7: TRLDBText;
    RLDBText8: TRLDBText;
    RLDBText9: TRLDBText;
    RLDBText10: TRLDBText;
    RLDBText11: TRLDBText;
    RLDBText12: TRLDBText;
    RLDBText13: TRLDBText;
    RLDBText14: TRLDBText;
    RLDBText15: TRLDBText;
    RLDBText16: TRLDBText;
    RLDBText17: TRLDBText;
    RLDBText18: TRLDBText;
    RLDBText19: TRLDBText;
    RLDBText20: TRLDBText;
    RLDBText21: TRLDBText;
    RLDBText22: TRLDBText;
    RLDBText23: TRLDBText;
    RLDBText24: TRLDBText;
    RLDBText25: TRLDBText;
    RLDBText26: TRLDBText;
    RLDBText27: TRLDBText;
    RLDBText28: TRLDBText;
    RLDBText29: TRLDBText;
    RLDBText30: TRLDBText;
    RLDBText31: TRLDBText;
    RLDBText32: TRLDBText;
    RLDBText33: TRLDBText;
    RLLabel39: TRLLabel;
    RLLabel40: TRLLabel;
    RLDBText34: TRLDBText;
    RLDBText35: TRLDBText;
    RLDBText36: TRLDBText;
    RLDBText37: TRLDBText;
    qRelatoriocd_cadastro: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_FichaContribuinte: Tfrm_FichaContribuinte;

implementation

uses Unit_DM;

{$R *.dfm}

end.
