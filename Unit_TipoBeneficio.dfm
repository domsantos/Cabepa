inherited frm_TipoBeneficio: Tfrm_TipoBeneficio
  Caption = 'Tipo Benef'#237'cio'
  ExplicitWidth = 728
  ExplicitHeight = 513
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel2: TPanel
    ExplicitTop = 41
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 66
      Height = 16
      Caption = 'Tipo Benef'#237'cio'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 38
      Width = 324
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_tipoBeneficio'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 68
      Width = 320
      Height = 120
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalcd_tipoBeneficio: TSmallintField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'cd_tipoBeneficio'
      ReadOnly = True
    end
    object CDSPrincipalds_tipoBeneficio: TStringField
      DisplayLabel = 'Tipo Benef'#237'cio'
      FieldName = 'ds_tipoBeneficio'
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT cd_tipoBeneficio'
      '      ,ds_tipoBeneficio'
      '  FROM tbTipoBeneficio'
      ''
      '')
    object QPrincipalcd_tipoBeneficio: TSmallintField
      FieldName = 'cd_tipoBeneficio'
      ReadOnly = True
    end
    object QPrincipalds_tipoBeneficio: TStringField
      FieldName = 'ds_tipoBeneficio'
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 352
    Top = 240
  end
end
