unit Unit_EstadoCivil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, FMTBcd, DB, StdCtrls, Mask, DBCtrls, ActnList,
  Provider, DBClient, SqlExpr, Buttons, ExtCtrls, ADODB, Grids, DBGrids;

type
  Tfrm_EstadoCivil = class(Tfrm_PadraoCadastro)
    QPrincipalcd_estcivil: TIntegerField;
    QPrincipalds_estCivil: TStringField;
    CDSPrincipalcd_estcivil: TIntegerField;
    CDSPrincipalds_estCivil: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnRelatorioClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_EstadoCivil: Tfrm_EstadoCivil;

implementation

uses  Unit_RelatorioEstadoCivil, Unit_DM;

{$R *.dfm}


{ Tfrm_EstadoCivil }

procedure Tfrm_EstadoCivil.btnPesquisarClick(Sender: TObject);
begin
  CDSPrincipal.Open
end;

procedure Tfrm_EstadoCivil.btnRelatorioClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TFrm_RelatorioEstadoCivil,Frm_RelatorioEstadoCivil);
  Frm_RelatorioEstadoCivil.dsRelatorio.DataSet := CDSPrincipal;
  CDSPrincipal.Open;
  Frm_RelatorioEstadoCivil.RLReport1.Preview();
end;

end.
