unit Unit_FichaFinanceira;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, RLReport, jpeg;

type
  Tfrm_FichaFinanceira = class(TForm)
    RLReport1: TRLReport;
    RLGroup1: TRLGroup;
    RLSubDetail1: TRLSubDetail;
    RLBand4: TRLBand;
    RLDBText5: TRLDBText;
    RLDBText6: TRLDBText;
    RLDBText7: TRLDBText;
    RLDBText8: TRLDBText;
    RLBand2: TRLBand;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel10: TRLLabel;
    RLLabel11: TRLLabel;
    RLLabel5: TRLLabel;
    RLDBText2: TRLDBText;
    RLLabel6: TRLLabel;
    RLDBText3: TRLDBText;
    RLLabel7: TRLLabel;
    RLDBText4: TRLDBText;
    RLLabel4: TRLLabel;
    RLDBText1: TRLDBText;
    qListaBeneficiados: TADOQuery;
    qListaBeneficiadosnm_banco: TStringField;
    qListaBeneficiadoscd_CABEPA: TIntegerField;
    qListaBeneficiadosnm_pastor: TStringField;
    qListaBeneficiadosvl_vantagem: TBCDField;
    qListaBeneficiadosvl_desconto: TBCDField;
    qListaBeneficiadosvl_liquido: TBCDField;
    qListaBeneficiadosdt_mesPagamento: TSmallintField;
    qListaBeneficiadosdt_anoPagamento: TSmallintField;
    qListaBeneficiadosds_mensagem: TStringField;
    dsListaBeneficiados: TDataSource;
    qListaVantagemDesconto: TADOQuery;
    qListaVantagemDescontonm_Rubrica: TStringField;
    qListaVantagemDescontono_freqRubrica: TSmallintField;
    qListaVantagemDescontovl_Rubrica: TBCDField;
    qListaVantagemDescontodt_tipo: TStringField;
    dsListaVantagemDesconto: TDataSource;
    RLBand1: TRLBand;
    RLImage1: TRLImage;
    RLLabel1: TRLLabel;
    RLLabel14: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel2: TRLLabel;
    RLSystemInfo1: TRLSystemInfo;
    RLLabel13: TRLLabel;
    RLDraw2: TRLDraw;
    RLLabel3: TRLLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_FichaFinanceira: Tfrm_FichaFinanceira;

implementation

uses Unit_DM;

{$R *.dfm}

end.
