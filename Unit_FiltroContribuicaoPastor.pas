unit Unit_FiltroContribuicaoPastor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB, Mask, Buttons, ExtCtrls, Grids, DBGrids, Menus;

type
  Tfrm_FiltroContribuicaoPastor = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    Button1: TButton;
    ckTodas: TCheckBox;
    edtContribuinte: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    rbContribuicao: TRadioGroup;
    rbTipoCon: TRadioGroup;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    QspListaContribuicao: TADOQuery;
    QspListaContribuicaocd_pastor: TIntegerField;
    QspListaContribuicaonr_mes: TIntegerField;
    QspListaContribuicaonr_ano: TIntegerField;
    QspListaContribuicaovl_pago: TBCDField;
    QspListaContribuicaotp_contribuicao: TIntegerField;
    QspListaContribuicaonm_tipoContribuicao: TStringField;
    QspListaContribuicaodt_contribuicao: TDateTimeField;
    BitBtn2: TBitBtn;
    PopupMenu1: TPopupMenu;
    ExcluirContribuio1: TMenuItem;
    N1: TMenuItem;
    procedure edtContribuinteKeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure edtContribuinteExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn2Click(Sender: TObject);
    procedure ExcluirContribuio1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_FiltroContribuicaoPastor: Tfrm_FiltroContribuicaoPastor;

implementation

uses Unit_PesquisaContribuinte, Unit_DM, Unit_RelatorioContribuicao;

{$R *.dfm}

procedure Tfrm_FiltroContribuicaoPastor.BitBtn1Click(Sender: TObject);
begin

  if ckTodas.Checked then
  begin
    dm.qBuscaContribuinte.Close;
    dm.qBuscaContribuinte.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    dm.qBuscaContribuinte.Open;



    Application.CreateForm(Tfrm_RelatorioContribuicao,frm_RelatorioContribuicao);
    frm_RelatorioContribuicao.lblContribuinte.caption := Label3.Caption;
    frm_RelatorioContribuicao.lblNo.Caption := edtContribuinte.Text;
    frm_RelatorioContribuicao.lblRelatorioRef.caption := 'De '+ FormatDateTime('DD/MM/YYYY',dm.qBuscaContribuintedt_filiacao.Value)+ ' at� '+ FormatDateTime('DD/MM/YYYY',date);
    frm_RelatorioContribuicao.QspListaContribuicao.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    frm_RelatorioContribuicao.QspListaContribuicao.Parameters.ParamByName('data_inicio').Value := FormatDateTime('YYYY-MM-DD',dm.qBuscaContribuintedt_filiacao.Value);
    frm_RelatorioContribuicao.QspListaContribuicao.Parameters.ParamByName('data_fim').Value := FormatDateTime('YYYY-MM-DD',date);
    frm_RelatorioContribuicao.QspListaContribuicao.Parameters.ParamByName('tipocon').Value := rbContribuicao.ItemIndex;


  end
  else
  begin

    if MaskEdit1.Text = '  /  /    ' then
    begin
      showmessage('Digite uma data v�lida');
      MaskEdit1.SetFocus;
      exit;
    end;
    if MaskEdit2.Text = '  /  /    ' then
    begin
      showmessage('Digite uma data v�lida');
      MaskEdit2.SetFocus;
      exit;
    end;


    Application.CreateForm(Tfrm_RelatorioContribuicao,frm_RelatorioContribuicao);
    frm_RelatorioContribuicao.lblContribuinte.caption := Label3.Caption;
    frm_RelatorioContribuicao.lblRelatorioRef.caption := 'De '+ MaskEdit1.Text+ ' at� '+ MaskEdit2.Text;
    frm_RelatorioContribuicao.lblNo.Caption := edtContribuinte.Text;

    frm_RelatorioContribuicao.QspListaContribuicao.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    frm_RelatorioContribuicao.QspListaContribuicao.Parameters.ParamByName('data_inicio').Value := FormatDateTime('YYYY-MM-DD',strtodatetime(MaskEdit1.Text));
    frm_RelatorioContribuicao.QspListaContribuicao.Parameters.ParamByName('data_fim').Value := FormatDateTime('YYYY-MM-DD',strtodatetime(MaskEdit2.Text));



  end;


  if rbTipoCon.ItemIndex = 0 then
  begin
    frm_RelatorioContribuicao.QspListaContribuicao.Parameters.ParamByName('debito').Value := 0;
    frm_RelatorioContribuicao.lblRelatorio.Caption := 'Todas as Doa��es'
  end
  else if rbTipoCon.ItemIndex = 1 then
  begin
    frm_RelatorioContribuicao.QspListaContribuicao.Parameters.ParamByName('debito').Value := 1;
    frm_RelatorioContribuicao.lblRelatorio.Caption := 'Doa��es Pagas'
  end
  else
  begin
    frm_RelatorioContribuicao.QspListaContribuicao.Parameters.ParamByName('debito').Value := 2;
    frm_RelatorioContribuicao.lblRelatorio.Caption := 'Doa��es Em Aberto'
  end;


  frm_RelatorioContribuicao.QspListaContribuicao.Open;

  frm_RelatorioContribuicao.RLReport1.Preview();




end;

procedure Tfrm_FiltroContribuicaoPastor.BitBtn2Click(Sender: TObject);
begin
//  ShowMessage(inttostr(rbContribuicao.ItemIndex));
  QspListaContribuicao.Close;
  if ckTodas.Checked then
  begin

    QspListaContribuicao.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    QspListaContribuicao.Parameters.ParamByName('data_inicio').Value := FormatDateTime('YYYY-MM-DD',dm.qBuscaContribuintedt_filiacao.Value);
    QspListaContribuicao.Parameters.ParamByName('data_fim').Value := FormatDateTime('YYYY-MM-DD',date);
    QspListaContribuicao.Parameters.ParamByName('tipocon').Value := rbContribuicao.ItemIndex;

  end
  else
  begin

    if MaskEdit1.Text = '  /  /    ' then
    begin
      showmessage('Digite uma data v�lida');
      MaskEdit1.SetFocus;
      exit;
    end;
    if MaskEdit2.Text = '  /  /    ' then
    begin
      showmessage('Digite uma data v�lida');
      MaskEdit2.SetFocus;
      exit;
    end;


    QspListaContribuicao.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    QspListaContribuicao.Parameters.ParamByName('data_inicio').Value := FormatDateTime('YYYY-MM-DD',strtodatetime(MaskEdit1.Text));
    QspListaContribuicao.Parameters.ParamByName('data_fim').Value := FormatDateTime('YYYY-MM-DD',strtodatetime(MaskEdit2.Text));



  end;


  if rbTipoCon.ItemIndex = 0 then
  begin
    QspListaContribuicao.Parameters.ParamByName('debito').Value := 0;
  end
  else if rbTipoCon.ItemIndex = 1 then
  begin
    QspListaContribuicao.Parameters.ParamByName('debito').Value := 1;
  end
  else
  begin
    QspListaContribuicao.Parameters.ParamByName('debito').Value := 2;
  end;


  QspListaContribuicao.Open;




end;


procedure Tfrm_FiltroContribuicaoPastor.Button1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
  frm_PesquisaContribuinte.origem := 3;
  frm_PesquisaContribuinte.Show;
end;

procedure Tfrm_FiltroContribuicaoPastor.edtContribuinteExit(Sender: TObject);
begin
if edtContribuinte.Text <> '' then
  begin
    dm.qBuscaContribuinte.Close;
    dm.qBuscaContribuinte.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    dm.qBuscaContribuinte.Open;

    if dm.qBuscaContribuinte.RecordCount = 0 then
    begin
      showmessage('Contribuinte n�o encontrado');
      edtContribuinte.SetFocus;
    end
    else
      Label3.Caption := dm.qBuscaContribuintenm_pastor.Value;
  end;
end;

procedure Tfrm_FiltroContribuicaoPastor.edtContribuinteKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_FiltroContribuicaoPastor.ExcluirContribuio1Click(
  Sender: TObject);
var
  msg : string;
begin

  {
  msg := QspListaContribuicaonm_tipoContribuicao.Value+
        ' Referencia: '+inttostr(QspListaContribuicaonr_mes.Value)+
        '/' + inttostr(QspListaContribuicaonr_ano.Value);

  if Dialogs.MessageDlg('Deseja realmente excluir o pagamento'+#13+'da refer�ncia '+msg+'?',
    mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
  begin
    qspExcluiPagamento.Close;

    qspExcluiPagamento.Parameters.ParamByName('codigo').Value :=
      QspListaContribuicaocd_pastor.Value;
    qspExcluiPagamento.Parameters.ParamByName('ano').Value :=
      QspListaContribuicaonr_ano.Value;
    qspExcluiPagamento.Parameters.ParamByName('mes').Value :=
      QspListaContribuicaonr_mes.Value;

    qspExcluiPagamento.Parameters.ParamByName('codigo1').Value :=
      QspListaContribuicaocd_pastor.Value;
    qspExcluiPagamento.Parameters.ParamByName('ano1').Value :=
      QspListaContribuicaonr_ano.Value;
    qspExcluiPagamento.Parameters.ParamByName('mes1').Value :=
      QspListaContribuicaonr_mes.Value;


    try
      qspExcluiPagamento.ExecSQL;
      showmessage('Pagamento apagado!');
    finally
      QspListaContribuicao.Close;
    QspListaContribuicao.Open;
    end;



  end;

   }
end;

procedure Tfrm_FiltroContribuicaoPastor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

procedure Tfrm_FiltroContribuicaoPastor.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  If key = #13 then
  Begin
  Key:= #0;
  Perform(Wm_NextDlgCtl,0,0);
  end;
end;

end.
