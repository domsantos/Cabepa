unit Unit_Escolaridade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, FMTBcd, DB, StdCtrls, Mask, DBCtrls, ActnList,
  Provider, DBClient, SqlExpr, Buttons, ExtCtrls, ADODB, Grids, DBGrids;

type
  Tfrm_Escolaridade = class(Tfrm_PadraoCadastro)
    QPrincipalcd_escpastor: TIntegerField;
    QPrincipalds_escpastor: TStringField;
    CDSPrincipalcd_escpastor: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    CDSPrincipalds_escpastor: TStringField;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnRelatorioClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Escolaridade: Tfrm_Escolaridade;

implementation

uses Unit_PesquisaEscolaridade, Unit_RelatorioEscolaridade, Unit_DM;

{$R *.dfm}

procedure Tfrm_Escolaridade.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Open;
end;

procedure Tfrm_Escolaridade.btnRelatorioClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(Tfrm_RelatorioEscolaridade,frm_RelatorioEscolaridade);

  frm_RelatorioEscolaridade.dsRelatorio.DataSet := CDSPrincipal;
  CDSPrincipal.Open;
  frm_RelatorioEscolaridade.RLReport1.Preview();
end;

procedure Tfrm_Escolaridade.btnCancelarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.close;
end;

end.
