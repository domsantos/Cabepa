unit Unit_RelatorioContribuicoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Mask, StdCtrls, ExtCtrls, Buttons, RLReport, jpeg;

type
  Tfrm_RelatorioContribuicoes = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    Label2: TLabel;
    RadioGroup1: TRadioGroup;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    QPesqMestre: TADOQuery;
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel2: TRLLabel;
    dsMestre: TDataSource;
    QPesqMestrenm_pastor: TStringField;
    QPesqMestredt_mesRefContribuicao: TIntegerField;
    QPesqMestredt_anoRefContribuicao: TIntegerField;
    QPesqMestrevl_ContribuicaoVencimento: TBCDField;
    RLLabel1: TRLLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelatorioContribuicoes: Tfrm_RelatorioContribuicoes;

implementation

uses Unit_DM;

{$R *.dfm}

end.
