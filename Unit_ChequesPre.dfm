object frm_ChequesPre: Tfrm_ChequesPre
  Left = 10
  Top = 10
  ActiveControl = DBGrid1
  Caption = 'Visualiza'#231#227'o de Cheques-Pr'#233
  ClientHeight = 347
  ClientWidth = 731
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 81
    Width = 731
    Height = 224
    Align = alClient
    DataSource = dsListaCheques
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 731
    Height = 81
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 179
      Height = 19
      Caption = 'LISTA DE CHEQUES-PR'#201
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 35
      Width = 51
      Height = 13
      Caption = 'Data In'#237'cio'
    end
    object Label3: TLabel
      Left = 87
      Top = 35
      Width = 42
      Height = 13
      Caption = 'Data Fim'
    end
    object BitBtn1: TBitBtn
      Left = 584
      Top = 33
      Width = 137
      Height = 42
      Caption = 'Imprimir Relat'#243'rio'
      TabOrder = 0
      OnClick = BitBtn1Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7F9C73F75EF75EF75EF75EF75EF75EF75EF75EF75E
        F75EF75EF75EF75EF75E9C73FF7FFF7FFF7FFF7FFF7FFF7F9C739C73FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C739C73FF7FFF7FFF7FFF7F
        FF7FFF7FDE7B5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        5A6BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863BD779C739C739C739C739C73
        9C739C739C739C739C73BD771863FF7FFF7FFF7FFF7FFF7FDE7B9C739C73734E
        3967F75EF75EF75EF75EF75EF75EF75EF75EF75EF75E3967524A9C739C73DE7B
        FF7F5A6BB5569452945231469452945294529452945294529452945294529452
        94529452314694529452B5565A6B1863734E3A67F75E6C314B296B2D6B2D6B2D
        6B2D6B2D6B2D6B2D6B2D6B2D6B2D6B2D8C31D75A3A67734E18635A6BB6569C73
        5B6B6B2D29252925292529252925292509250925082508210821082109253967
        9C73B6565A6B9C731863BD73BD773146EF3DCE3DCE39AD358D358C316B2D4A2D
        4A2929252925E72029257B6FBD7718639C73BD7739677B6F9D739452524A524A
        31461042EF41EF3DCE39AD39AD358C316B2D4A298C319C737B6F3967BD77DE7B
        7B6B5A6B9C73D65AD65AF75E1863186339673967186339671863F75EB556734E
        524A9C735A6B7B6BDE7BFF7F7B6F5A6BBD77D656F75E18631863F75EF75E1963
        1863F75ED75A1863185FF85EF75EBD775A6B7B6FFF7FFF7FBD7718637B6FD65A
        F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75ED65A5A6B1863BD77
        FF7FFF7FFF7FBD779C731863DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
        DE7BFF7F18639C73BD77FF7FFF7FFF7FFF7FFF7FFF7F7A6BBE77DE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B7B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7F7B6FBD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDF7B1863FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73BD77DE7BDE7BDE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7B5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6FBD77DE7B
        DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BBD779C73FF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F7B6FBD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
        9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6FBD77DE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BDE7B9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F
        BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B9C73FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FBD77F75EF75EF75EF75EF75EF75EF75EF75EF75EF75E
        F75EF75E9C73FF7FFF7FFF7FFF7F}
    end
    object dtInicio: TMaskEdit
      Left = 8
      Top = 54
      Width = 73
      Height = 21
      EditMask = '!99/99/0000;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object dtFim: TMaskEdit
      Left = 87
      Top = 54
      Width = 66
      Height = 21
      EditMask = '!99/99/0000;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object BitBtn2: TBitBtn
      Left = 159
      Top = 33
      Width = 138
      Height = 42
      Caption = 'Pesquisa de Cheque'
      TabOrder = 3
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F1863314A734EFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75E724E6B2DCE39FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        186393526A2DEE3D1863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7F186394528B31CE395A6BFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75EB5568C35CE3D5A6BFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863
        D55A8B31CD395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7F1863D65AAD35CE395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F9C73396739677B6FFF7FFF7FFF7FBD77B556AD39CE395A6BFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F5A6BD65A1863596B5A6B3967F75E1863BD77B556
        CE39CE395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A39677B6F7B6F5A6B
        5A6B7B6F9C739C73D65A8C35734EBD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        D65A5A6B5A6B5A6B5A6B5A6B5A6B5A6B5A6B7A6FBD779452DE7BFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7F7B6F18635A6B5A6B5A6B7A6F7B6F7B6F7A6F5A6B5A6B
        5A6B9C73F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A5A6B5A6B7B6F7B6F
        7B6F7B6F7B6F7B6F7B6F7B6F5A6B9C73F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FF75E396B7B6F7B6F9B739C739C739C739C739B737B6F7B6F7B6F39675A6B
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F18635A6B9C739B739C739C73BD77BD779C73
        9C739C737B6F7B6F5A6B3967FF7FFF7FFF7FFF7FFF7FFF7FFF7F38677B6FBD77
        BD77BD77BD77BD77BD77BD77BD77BD77BD779C73596B3967FF7FFF7FFF7FFF7F
        FF7FFF7FFF7FF75E9C73DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BBD77BD77
        F7627B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7F18637B6FDE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BBD77B556FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77
        18639C73FF7FDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B39673967FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F18635A6BBD77FF7FFF7FFF7FFF7FFF7FFF7FDE7B
        7B6FB556FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F396718637B6F
        BD77BD77BD77BD779C731863D65AFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FBD77396B186318631863F75E18637B6FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F}
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 305
    Width = 731
    Height = 42
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Label4: TLabel
      Left = 480
      Top = 16
      Width = 145
      Height = 16
      Caption = 'Valor Total dos Cheques:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 672
      Top = 16
      Width = 37
      Height = 16
      BiDiMode = bdRightToLeft
      Caption = 'Label5'
      ParentBiDiMode = False
    end
    object BitBtn3: TBitBtn
      Left = 8
      Top = 8
      Width = 145
      Height = 29
      Caption = 'Baixa de Cheque'
      TabOrder = 0
      OnClick = BitBtn3Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F94528C358C358C358C358C358C35
        8C358C358C358C35AC35AD358C35AD353967FF7FFF7FFF7FFF7FFF7FDE7B8C35
        C618E71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CF75EFF7F
        FF7FFF7FFF7FFF7F1863C6182925D65AF75EF75EF75ED75AD65AD65AD65AD65A
        9452955294520821D65AFF7FFF7BFF7FFF7FFF7F1863A5183146DE7BBD739D73
        9C739C739C739C737B6F955210421042F85E2925D65AFF7FDE77BE73DF77DE7B
        524A08217B6FBE779C739C739C739C6F7C6FB556CE3931463967534ACE392925
        D65ADF7BBE779E6B7D5F396394563146D65A5A6BBD77BD779C73BD73F75ECE39
        F75EBD77BD775A6BAD35E720D65ADF7BBE737E679D6FD65ABD737B6FF75E944E
        945218639C73DE77B55639679C737B6F7B6F9C73D65A2925F75EDE779D6B7E67
        7B6B5A6BBD779C739C6F7B6F1963B5569452F75E1042524ABD737B6F7B6F9C73
        7C6FCF391863BE737D679E6F1863DE7BBD77BD779C739C73FA4EF856524ACF39
        524AEF3D18639C737C739B6FB76B3763744EDF7B7E677B675A6BDF77DE7FDE7B
        DE7B3A635342CE3531465A6BF75E2925F03D9C73986B8E574B4BF456D65AFF7F
        FF7FB5567B6B3C575D579C6BD65ACE39EF3DF75EBD73DE7B185F0E4A6A31934E
        4D4FEC462F420925F75EFF7FFF7FDE7B9452D65ED756CF35AD35B5569C73BE77
        9C737B6FD75A6D5A67624A2D1042734E39672929D65AFF7FFF7F9C737B6FDE7B
        734E524A7B6BBD77BD777B6F396739673A672E46E876863D4B297B6F7B6F0821
        D65AFF7FFF7FF75EFF7FFF7FD65A7B6BBD777B6F396739673A6739679C733A67
        8B5E866649295B6B524AA518F75EFF7FDE7BF75E1867DE7B7B6FD65AFF7F7B6F
        396739675A6BDE7BFF7FF8568C290977863DAE352925C618D65AFF7FFF7FFF7F
        BD771863396794529C73FF7F5B6BBD77FF7F5A6B132A71016F198B5E866AC61C
        C718C6185A6BFF7FFF7FFF7FFF7FFF7FBD77D65E734EFF7FFF7F7B77753AB301
        B209954A9C772F4A094AAC354A291863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        7B6F396BD74E140EF405753A7B73FF7FFF7F185F954EB55AAD39FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F944AF4055532524A9C77FF7FFF7FFF7FBD77
        8C39E7505256FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B5A6BFF7F
        DE7B3967F75E9C73FF7F1863BD771867BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B7B6FD65A5A6BFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        BD77FF7FFF7FFF7FFF7FFF7FFF7F}
    end
  end
  object dsListaCheques: TDataSource
    DataSet = qListaCheques
    Left = 488
    Top = 96
  end
  object qListaCheques: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      #9'pc.id_PagtoContribuincao'
      #9',pc.cd_cadastro'
      #9',pc.cd_tipoContribuicao'
      #9',pc.dt_mesRefContribuicao'
      #9',pc.dt_anoRefContribuicao'
      
        #9',convert(varchar,pc.dt_mesRefContribuicao) +'#39'/'#39'+convert(varchar' +
        ',pc.dt_anoRefContribuicao) as referencia'
      #9',pc.vl_pagtoContribuicao'
      #9',pc.no_chequePagto'
      #9',pc.ds_bancoCheque'
      #9',pc.ds_agenciaCheque'
      #9',pc.dt_resgChequePre'
      #9',pc.sq_chequePagto'
      #9',c.nm_pastor'
      '                ,case '
      #9#9'when pc.dt_resgChequePre > GETDATE() then '#39'A VENCER'#39
      #9#9'else '#39'VENCIDO'#39
      #9'end as situacao'
      'from tbPagtoContribuicao pc'
      'inner join tbContribuinte c on pc.cd_cadastro =  c.cd_cadastro'
      'where pc.cd_formPagto = 3 and pc.sq_chequePagto is null'
      'order by pc.dt_resgChequePre')
    Left = 488
    Top = 152
    object qListaChequescd_tipoContribuicao: TIntegerField
      FieldName = 'cd_tipoContribuicao'
      Visible = False
    end
    object qListaChequesdt_mesRefContribuicao: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
      Visible = False
    end
    object qListaChequesdt_anoRefContribuicao: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
      Visible = False
    end
    object qListaChequesnm_pastor: TStringField
      DisplayLabel = 'Nome Contribuinte'
      DisplayWidth = 40
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qListaChequesreferencia: TStringField
      DisplayLabel = 'Refer'#234'ncia'
      DisplayWidth = 15
      FieldName = 'referencia'
      ReadOnly = True
      Size = 61
    end
    object qListaChequesvl_pagtoContribuicao: TBCDField
      DisplayLabel = 'Valor'
      FieldName = 'vl_pagtoContribuicao'
      EditFormat = '#,##0.00'
      currency = True
      Precision = 10
      Size = 2
    end
    object qListaChequesno_chequePagto: TStringField
      DisplayLabel = 'No. Cheque'
      FieldName = 'no_chequePagto'
    end
    object qListaChequesds_bancoCheque: TStringField
      DisplayLabel = 'Banco'
      DisplayWidth = 25
      FieldName = 'ds_bancoCheque'
      Size = 40
    end
    object qListaChequesds_agenciaCheque: TStringField
      DisplayLabel = 'Agencia'
      FieldName = 'ds_agenciaCheque'
    end
    object qListaChequesdt_resgChequePre: TDateTimeField
      DisplayLabel = 'Data Resgate'
      FieldName = 'dt_resgChequePre'
    end
    object qListaChequessq_chequePagto: TSmallintField
      FieldName = 'sq_chequePagto'
      Visible = False
    end
    object qListaChequessituacao: TStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'situacao'
      ReadOnly = True
      Size = 8
    end
    object qListaChequesid_PagtoContribuincao: TAutoIncField
      FieldName = 'id_PagtoContribuincao'
      ReadOnly = True
    end
    object qListaChequescd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object qBaixaCheque: TADOQuery
    Connection = DM.ADOConn
    Parameters = <
      item
        Name = 'data'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'update tbPagtoContribuicao set dt_pagtoContribuicao= :data'
      ' ,sq_chequePagto = 1'
      'where id_PagtoContribuincao = :codigo')
    Left = 488
    Top = 224
  end
end
