object frm_InformaFalecimento: Tfrm_InformaFalecimento
  Left = 10
  Top = 10
  Caption = 'Informar Falecimento'
  ClientHeight = 419
  ClientWidth = 753
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 416
    Height = 419
    Align = alClient
    DataSource = dsBeneficiados
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
  end
  object Panel1: TPanel
    Left = 416
    Top = 0
    Width = 337
    Height = 419
    Align = alRight
    TabOrder = 1
    object Label1: TLabel
      Left = 6
      Top = 16
      Width = 119
      Height = 13
      Caption = 'Contribuinte Selecionado'
    end
    object lblContrbuinte: TLabel
      Left = 6
      Top = 35
      Width = 3
      Height = 13
    end
    object Label2: TLabel
      Left = 6
      Top = 64
      Width = 87
      Height = 13
      Caption = 'Data Falecimento:'
    end
    object Label3: TLabel
      Left = 6
      Top = 184
      Width = 131
      Height = 13
      Caption = 'Pensionado do Falecido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 6
      Top = 203
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object lblPensionado: TLabel
      Left = 6
      Top = 222
      Width = 3
      Height = 13
    end
    object Label5: TLabel
      Left = 6
      Top = 297
      Width = 66
      Height = 13
      Caption = 'Valor Pens'#227'o:'
    end
    object lblValor: TLabel
      Left = 6
      Top = 316
      Width = 3
      Height = 13
    end
    object Label6: TLabel
      Left = 6
      Top = 248
      Width = 70
      Height = 13
      Caption = 'Valor Benef'#237'cio'
    end
    object lblValorBeneficio: TLabel
      Left = 6
      Top = 267
      Width = 3
      Height = 13
    end
    object Label7: TLabel
      Left = 6
      Top = 368
      Width = 81
      Height = 13
      Caption = 'Procurar Jubiado'
    end
    object edtData: TMaskEdit
      Left = 6
      Top = 83
      Width = 91
      Height = 21
      EditMask = '!99/99/0000;1;_'
      MaxLength = 10
      TabOrder = 0
      Text = '  /  /    '
    end
    object BitBtn1: TBitBtn
      Left = 6
      Top = 120
      Width = 147
      Height = 33
      Caption = 'Registrar Falecimento'
      TabOrder = 1
      OnClick = BitBtn1Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F7A6BD3528C3E8836A736A7368836
        8C3ED3527A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73B14A8736
        C53AC53AA536A532A532A536C53AC53A8736B14A9C73FF7FFF7FFF7FFF7FFF7F
        FF7FFF7F59676A3AC53AA536652A252625222522252225222526652AA536C53A
        6A3A5967FF7FFF7FFF7FFF7FFF7F38638836E53A652A2526452645264526452A
        452A4526452625262526652AC53A68363863FF7FFF7FFF7F9C6F4936C536452A
        4526452A652A652A44264526652A652A652A452A452A45264526C53649369C6F
        FF7FFF7FB14AA536652A452A652A652E652E43268B3AD34E662A652E652E652E
        652A652A4526652AA532B14AFF7F7A6B8732852E452A652E652E852E63268A36
        7A6B9E77AC3A642A8532852E652E652E652E452A852E67327A6BB34EA52E652E
        652E852E8532632A8B367B6BBD779D77385F652A843285328532852E652E652E
        652A852EB34E6C3A8532652E85328532632A8B3A7B6FBD77BD779D779E77D046
        632AA536853285328532652E652E852E6C3A6932853285328532842EAD3E7C6F
        BE7BBD777A679C73BE7B7B6B8836A332A536A53685328532652E852E69326832
        A736A636A536652E385FDF7FDF7B355B662E355BDE7BDF7B1557842EC436A536
        A536A532A636A736682E8932C93EC83EC83EA636EF4ABE7B15578532A232A93A
        9D73DE7BBE77CD42A332C63EC73EC83EC83AC93A69326A32EA42E942E946E946
        CA3ED046C93AE742E842C63AF24EDF7FDF7F7B6BAA3AE842EA46E942C942EA42
        6A326D3A0B470B4B0B4B0B4B0A47C83E0A4B0B4B0B4B2A4BC93E7967FF7FFF7F
        365BC93A0A4BEB46EB460B476D3AB34E0C472C4F0C4B0C4F2C4F2C4F2C4F2C4F
        2C4F2C4F0A4BCE46BD73FF7FFF7F1353EA420C4B0C4FEC46B34E7A6BAD3E4F57
        2D4F2D4F2D532D532D532D532D532D534D57EB46124FFF7FFF7F1453EB422D4F
        4F57AD3E7A6BFF7FB24A30534F5B2E534E574E574E574E574E574E574E574E57
        EC4635571457ED4A2E5350573053B24AFF7FFF7F9B6F6D3A9363505B4F575057
        4F5B4F5B4F5B6F5B4F5B705B6F5BED460D4B4F57715B935F6D3A9B6FFF7FFF7F
        FF7F3863AE42B5679363515B505B715B715F715F715F715F715B705B515B9363
        B567AE423863FF7FFF7FFF7FFF7FFF7F58638F42755FB66F94677263725F725F
        725F725F72639467B66F755F8F425863FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C6F
        B24AF14A9663B86FD86FB76FB76FB86FB86F9663D14AB24A9C6FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F7A6BD452B146D14AF24EF24ED14AB146D452
        7A6BFF7FFF7FFF7FFF7FFF7FFF7F}
    end
    object BitBtn2: TBitBtn
      Left = 159
      Top = 120
      Width = 162
      Height = 33
      Caption = 'Confirmar Falecimento'
      Enabled = False
      TabOrder = 2
      OnClick = BitBtn2Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F7A6BD3528C3E8836A736A7368836
        8C3ED3527A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73B14A8736
        C53AC53AA536A532A532A536C53AC53A8736B14A9C73FF7FFF7FFF7FFF7FFF7F
        FF7FFF7F59676A3AC53AA536652A252625222522252225222526652AA536C53A
        6A3A5967FF7FFF7FFF7FFF7FFF7F38638836E53A652A2526452645264526452A
        452A4526452625262526652AC53A68363863FF7FFF7FFF7F9C6F4936C536452A
        4526452A652A652A44264526652A652A652A452A452A45264526C53649369C6F
        FF7FFF7FB14AA536652A452A652A652E652E43268B3AD34E662A652E652E652E
        652A652A4526652AA532B14AFF7F7A6B8732852E452A652E652E852E63268A36
        7A6B9E77AC3A642A8532852E652E652E652E452A852E67327A6BB34EA52E652E
        652E852E8532632A8B367B6BBD779D77385F652A843285328532852E652E652E
        652A852EB34E6C3A8532652E85328532632A8B3A7B6FBD77BD779D779E77D046
        632AA536853285328532652E652E852E6C3A6932853285328532842EAD3E7C6F
        BE7BBD777A679C73BE7B7B6B8836A332A536A53685328532652E852E69326832
        A736A636A536652E385FDF7FDF7B355B662E355BDE7BDF7B1557842EC436A536
        A536A532A636A736682E8932C93EC83EC83EA636EF4ABE7B15578532A232A93A
        9D73DE7BBE77CD42A332C63EC73EC83EC83AC93A69326A32EA42E942E946E946
        CA3ED046C93AE742E842C63AF24EDF7FDF7F7B6BAA3AE842EA46E942C942EA42
        6A326D3A0B470B4B0B4B0B4B0A47C83E0A4B0B4B0B4B2A4BC93E7967FF7FFF7F
        365BC93A0A4BEB46EB460B476D3AB34E0C472C4F0C4B0C4F2C4F2C4F2C4F2C4F
        2C4F2C4F0A4BCE46BD73FF7FFF7F1353EA420C4B0C4FEC46B34E7A6BAD3E4F57
        2D4F2D4F2D532D532D532D532D532D534D57EB46124FFF7FFF7F1453EB422D4F
        4F57AD3E7A6BFF7FB24A30534F5B2E534E574E574E574E574E574E574E574E57
        EC4635571457ED4A2E5350573053B24AFF7FFF7F9B6F6D3A9363505B4F575057
        4F5B4F5B4F5B6F5B4F5B705B6F5BED460D4B4F57715B935F6D3A9B6FFF7FFF7F
        FF7F3863AE42B5679363515B505B715B715F715F715F715F715B705B515B9363
        B567AE423863FF7FFF7FFF7FFF7FFF7F58638F42755FB66F94677263725F725F
        725F725F72639467B66F755F8F425863FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C6F
        B24AF14A9663B86FD86FB76FB76FB86FB86F9663D14AB24A9C6FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F7A6BD452B146D14AF24EF24ED14AB146D452
        7A6BFF7FFF7FFF7FFF7FFF7FFF7F}
    end
    object edtNome: TEdit
      Left = 6
      Top = 387
      Width = 227
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 3
    end
    object btLocalicar: TBitBtn
      Left = 239
      Top = 383
      Width = 90
      Height = 25
      Caption = 'Localicar'
      TabOrder = 4
      OnClick = btLocalicarClick
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F1863314A734EFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75E724E6B2DCE39FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        186393526A2DEE3D1863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7F186394528B31CE395A6BFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF75EB5568C35CE3D5A6BFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863
        D55A8B31CD395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7F1863D65AAD35CE395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F9C73396739677B6FFF7FFF7FFF7FBD77B556AD39CE395A6BFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F5A6BD65A1863596B5A6B3967F75E1863BD77B556
        CE39CE395A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A39677B6F7B6F5A6B
        5A6B7B6F9C739C73D65A8C35734EBD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        D65A5A6B5A6B5A6B5A6B5A6B5A6B5A6B5A6B7A6FBD779452DE7BFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7F7B6F18635A6B5A6B5A6B7A6F7B6F7B6F7A6F5A6B5A6B
        5A6B9C73F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A5A6B5A6B7B6F7B6F
        7B6F7B6F7B6F7B6F7B6F7B6F5A6B9C73F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FF75E396B7B6F7B6F9B739C739C739C739C739B737B6F7B6F7B6F39675A6B
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F18635A6B9C739B739C739C73BD77BD779C73
        9C739C737B6F7B6F5A6B3967FF7FFF7FFF7FFF7FFF7FFF7FFF7F38677B6FBD77
        BD77BD77BD77BD77BD77BD77BD77BD77BD779C73596B3967FF7FFF7FFF7FFF7F
        FF7FFF7FFF7FF75E9C73DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BBD77BD77
        F7627B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7F18637B6FDE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BBD77B556FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77
        18639C73FF7FDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B39673967FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F18635A6BBD77FF7FFF7FFF7FFF7FFF7FFF7FDE7B
        7B6FB556FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F396718637B6F
        BD77BD77BD77BD779C731863D65AFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FBD77396B186318631863F75E18637B6FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F}
    end
  end
  object dsBeneficiados: TDataSource
    DataSet = qListaBeneficiados
    Left = 40
    Top = 136
  end
  object qListaBeneficiados: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '
      #9'cd_cadastro,'
      #9'nm_pastor,'
      #9'nm_conjuge,'
      #9'case cd_sitJubilamento'
      #9#9'when 0 then '#39'CONTRIBUINTE'#39
      #9#9'when 1 then '#39'JUBILADO'#39
      #9'end tp_jubilacao'
      'from tbContribuinte'
      'where cd_sitJubilamento in (0,1)'
      'order by nm_pastor')
    Left = 128
    Top = 136
    object qListaBeneficiadoscd_cadastro: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'cd_cadastro'
    end
    object qListaBeneficiadosnm_pastor: TStringField
      DisplayLabel = 'Contribuinte'
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qListaBeneficiadosnm_conjuge: TStringField
      DisplayLabel = 'Conjuge'
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object qListaBeneficiadostp_jubilacao: TStringField
      DisplayLabel = 'Tipo Jubila'#231#227'o'
      FieldName = 'tp_jubilacao'
      ReadOnly = True
      Size = 12
    end
  end
  object qValorBeneficio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      'from tbValBeneficio'
      'where no_reqBeneficio ='
      #9'(select MAX(no_reqBeneficio) '
      '                  from tbRequerimentoBeneficio '
      '                  where cd_cadastro = :codigo)')
    Left = 128
    Top = 200
    object qValorBeneficiono_reqBeneficio: TIntegerField
      FieldName = 'no_reqBeneficio'
    end
    object qValorBeneficiono_seqBeneficio: TAutoIncField
      FieldName = 'no_seqBeneficio'
      ReadOnly = True
    end
    object qValorBeneficiodt_Valor: TDateTimeField
      FieldName = 'dt_Valor'
    end
    object qValorBeneficiovl_Beneficio: TBCDField
      FieldName = 'vl_Beneficio'
      Precision = 10
      Size = 2
    end
  end
  object qSpInsereBeneficio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'tipo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 2
      end
      item
        Name = 'tipoJubilacao'
        Size = -1
        Value = Null
      end
      item
        Name = 'valor'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = 10000c
      end>
    SQL.Strings = (
      'DECLARE'#9'@return_value int,'
      #9#9'@resultado bit'
      ''
      'EXEC'#9'@return_value = [dbo].[sp_InserePedidoBeneficio]'
      #9#9'@vCodigo = :codigo,'
      #9#9'@vTipo = :tipo,'
      
        '                                @vTipoJubilacao = :tipoJubilacao' +
        ','
      #9#9'@vValor = :valor,'
      #9#9'@resultado = @resultado OUTPUT'
      ''
      'SELECT'#9'@resultado as N'#39'resultado'#39
      ''
      '')
    Left = 128
    Top = 264
    object qSpInsereBeneficioresultado: TBooleanField
      FieldName = 'resultado'
      ReadOnly = True
    end
  end
  object qAtualizaCadastro: TADOQuery
    Connection = DM.ADOConn
    Parameters = <>
    Left = 128
    Top = 328
  end
  object qDataPagamento: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = 2
      end>
    SQL.Strings = (
      #9
      'select max(dt_anoPagamento) ano,max(dt_mesPagamento) mes'
      'from tbPagamentoMensal'
      'where cd_cadastro = :codigo and cd_tipRubrica = 1')
    Left = 232
    Top = 200
    object qDataPagamentoano: TSmallintField
      FieldName = 'ano'
      ReadOnly = True
    end
    object qDataPagamentomes: TSmallintField
      FieldName = 'mes'
      ReadOnly = True
    end
  end
end
