unit Unit_SalMinimo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, ADODB, Provider, ActnList, DB, DBClient, StdCtrls,
  Buttons, ExtCtrls, Mask, DBCtrls, Grids, DBGrids;

type
  Tfrm_SalMinimo = class(Tfrm_PadraoCadastro)
    QPrincipaldt_anoSalMin: TSmallintField;
    QPrincipaldt_mesSalMin: TSmallintField;
    QPrincipalvl_salMin: TBCDField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBGrid1: TDBGrid;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_SalMinimo: Tfrm_SalMinimo;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_SalMinimo.btnNovoClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure Tfrm_SalMinimo.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Open;
end;

end.
