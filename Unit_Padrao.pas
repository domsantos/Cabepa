unit Unit_Padrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, FMTBcd, DB, SqlExpr, Provider,
  DBClient, ActnList, ADODB;

type
  Tfrm_PadraoCadastro = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    btnNovo: TBitBtn;
    btnSalvar: TBitBtn;
    btnPesquisar: TBitBtn;
    btnApagar: TBitBtn;
    btnRelatorio: TBitBtn;
    btnCancelar: TBitBtn;
    CDSPrincipal: TClientDataSet;
    ActionList1: TActionList;
    acNovo: TAction;
    acSalvar: TAction;
    acPesquisar: TAction;
    acApagar: TAction;
    acRelatorio: TAction;
    acCancelar: TAction;
    acSair: TAction;
    DSPPrincipal: TDataSetProvider;
    QPrincipal: TADOQuery;
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnApagarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure acSairExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnPesquisarClick(Sender: TObject);
    procedure acNovoExecute(Sender: TObject);
    procedure acSalvarExecute(Sender: TObject);
    procedure acPesquisarExecute(Sender: TObject);
    procedure acApagarExecute(Sender: TObject);
    procedure acRelatorioExecute(Sender: TObject);
    procedure acCancelarExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function ValidaForm:Boolean;
  end;

var
  frm_PadraoCadastro: Tfrm_PadraoCadastro;

implementation

uses Unit_DM;


{$R *.dfm}

{ TfrmPadraoCadastro }

function Tfrm_PadraoCadastro.ValidaForm: Boolean;
var
  i:integer;
  campos:TStrings;
begin

  // validação de campos obrigatorios
  campos := TStringList.Create;
  for i := 0 to QPrincipal.Fields.Count - 1 do
  begin
     if (QPrincipal.Fields[i].Required) then
        if (QPrincipal.Fields[i].AsString=EmptyStr) then
          Campos.Add('- ' + QPrincipal.Fields[i].DisplayName);
  end;

   if (Campos.Text<>EmptyStr) then
    begin
      Campos.Insert(0, 'Preencha os campos obrigatórios:');
      Campos.Insert(1, EmptyStr);
      ShowMessage(Campos.Text);
      result := false;
    end
    else result := true;

end;

procedure Tfrm_PadraoCadastro.btnNovoClick(Sender: TObject);
begin
  try
    if NOT CDSPrincipal.Active then CDSPrincipal.Open;
    Panel2.Enabled := True;
    CDSPrincipal.Insert;
    
  except
    on E:Exception do begin
      MessageDlg('Ocorreu um erro ao criar uma nova entrada. Contate o suporte.'+E.Message, mtInformation, [mbOk], 0);
      abort;
    end;
  end
end;

procedure Tfrm_PadraoCadastro.btnSalvarClick(Sender: TObject);
var
  i : integer;
begin
  //if validaForm then
  begin
    if (CDSPrincipal.State = dsEdit) or (CDSPrincipal.State = dsInsert) then
    begin
      {
      for I := 0 to QPrincipal.FieldList.Count - 1 do
      begin

        if QPrincipal.Fields.Fields[i].Required then
        begin
          showmessage(QPrincipal.Fields.Fields[i].Text);

          if QPrincipal.Fields.Fields[i].Text = '' then
          begin
            MessageDlg('Campo Obrigatório: '+QPrincipal.Fields.Fields[i].DisplayName, mtInformation, [mbOk], 0);
            abort;
          end;

        end;

      end;
      }
      try
        CDSPrincipal.Post;
        CDSPrincipal.ApplyUpdates(-1);
        CDSPrincipal.Close;
        Panel2.Enabled := False;
        MessageDlg('Registro Salvo.', mtInformation, [mbOk], 0);
      except
        on E:Exception do begin
          MessageDlg('Ocorreu um erro ao salvar a informação. Contate o suporte.'+#13+E.Message, mtInformation, [mbOk], 0);
          abort;
        end
      end;
    end;
  end;
end;

procedure Tfrm_PadraoCadastro.btnApagarClick(Sender: TObject);
begin
  if NOT CDSPrincipal.Active then
  begin
    ShowMessage('Selecione um registro');
    exit;
  end;


  if MessageDlg('Deseja apagar o registro?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    if CDSPrincipal.State = dsEdit then
      CDSPrincipal.Cancel;

    if CDSPrincipal.State = dsBrowse then
    begin
      try
        CDSPrincipal.Delete;
        CDSPrincipal.ApplyUpdates(-1);
      except
        on E:Exception do begin
          MessageDlg('Ocorreu um erro ao apagar a informação. Contate o suporte.'+E.Message, mtInformation, [mbOk], 0);
          abort;
        end;
      end;

    end;
    QPrincipal.Close;
    end;

end;

procedure Tfrm_PadraoCadastro.btnCancelarClick(Sender: TObject);
begin
  if CDSPrincipal.Active then
  begin
    if CDSPrincipal.State <> dsBrowse then
    begin
      CDSPrincipal.Cancel;
      CDSPrincipal.Close;
    end;
  end;
end;

procedure Tfrm_PadraoCadastro.acSairExecute(Sender: TObject);
begin
  self.Close;
end;

procedure Tfrm_PadraoCadastro.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  if QPrincipal.Active then
    QPrincipal.Close;

  self := nil;
  Action := caFree;
end;

procedure Tfrm_PadraoCadastro.FormCreate(Sender: TObject);
begin
  self.Top := 10;
  self.Left := 10;
end;

procedure Tfrm_PadraoCadastro.FormKeyPress(Sender: TObject; var Key: Char);
begin
  If key = #13 then
  Begin
  Key:= #0;
  Perform(Wm_NextDlgCtl,0,0);
  end;
end;

procedure Tfrm_PadraoCadastro.btnPesquisarClick(Sender: TObject);
begin
  Panel2.Enabled := true;

end;

procedure Tfrm_PadraoCadastro.acNovoExecute(Sender: TObject);
begin
  btnNovo.Click;
end;

procedure Tfrm_PadraoCadastro.acSalvarExecute(Sender: TObject);
begin
  btnSalvar.Click;
end;

procedure Tfrm_PadraoCadastro.acPesquisarExecute(Sender: TObject);
begin
  btnPesquisar.Click;
end;

procedure Tfrm_PadraoCadastro.acApagarExecute(Sender: TObject);
begin
  btnApagar.Click;
end;

procedure Tfrm_PadraoCadastro.acRelatorioExecute(Sender: TObject);
begin
  btnRelatorio.Click;
end;

procedure Tfrm_PadraoCadastro.acCancelarExecute(Sender: TObject);
begin
  btnCancelar.Click;
end;

end.
