unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, ADODB, RLReport, jpeg;

type
  Tfrm_RelSimulaAumentoPercentual = class(Tfrm_RelatorioPadrao)
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLDBText3: TRLDBText;
    QspSimulaAumento: TADOQuery;
    QspSimulaAumentovl_aumento: TBCDField;
    QspSimulaAumentonr_requerimento: TIntegerField;
    QspSimulaAumentovl_Beneficio: TBCDField;
    QspSimulaAumentonm_pastor: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelSimulaAumentoPercentual: Tfrm_RelSimulaAumentoPercentual;

implementation

uses Unit_DM;

{$R *.dfm}

end.
