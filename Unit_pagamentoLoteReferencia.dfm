object frm_pagamentoLoteReferencia: Tfrm_pagamentoLoteReferencia
  Left = 10
  Top = 10
  Caption = 'Pagamento Lote por Refer'#234'ncia'
  ClientHeight = 554
  ClientWidth = 545
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 8
    Top = 8
    Width = 95
    Height = 13
    Caption = 'Informe N'#186' CABEPA'
  end
  object Label4: TLabel
    Left = 8
    Top = 59
    Width = 59
    Height = 19
    Caption = 'Pastor:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblContribuinte: TLabel
    Left = 8
    Top = 78
    Width = 241
    Height = 19
    Caption = 'Nenhum Contribuinte Selecionado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 103
    Width = 36
    Height = 13
    Caption = 'Per'#237'odo'
  end
  object Label6: TLabel
    Left = 8
    Top = 122
    Width = 48
    Height = 13
    Caption = 'Data Pgto'
  end
  object Label15: TLabel
    Left = 210
    Top = 120
    Width = 83
    Height = 13
    Caption = 'Tipo Contribui'#231#227'o'
  end
  object Label16: TLabel
    Left = 251
    Top = 136
    Width = 79
    Height = 26
    Caption = '1 - Contribui'#231#227'o '#13#10'2 - Seguro'
  end
  object Label8: TLabel
    Left = 357
    Top = 122
    Width = 77
    Height = 13
    Caption = 'Tipo Pagamento'
  end
  object Label12: TLabel
    Left = 9
    Top = 170
    Width = 49
    Height = 13
    Caption = 'Valor total'
  end
  object Banco: TLabel
    Left = 138
    Top = 168
    Width = 29
    Height = 13
    Caption = 'Banco'
  end
  object Label11: TLabel
    Left = 251
    Top = 168
    Width = 38
    Height = 13
    Caption = 'Agencia'
  end
  object Label9: TLabel
    Left = 378
    Top = 168
    Width = 57
    Height = 13
    Caption = 'No. Cheque'
  end
  object Label10: TLabel
    Left = 450
    Top = 168
    Width = 66
    Height = 13
    Caption = 'Data Resgate'
  end
  object Label13: TLabel
    Left = 8
    Top = 214
    Width = 209
    Height = 19
    Caption = 'Adicionando Valor/Refer'#234'ncia'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 8
    Top = 239
    Width = 19
    Height = 13
    Caption = 'M'#234's'
  end
  object Label14: TLabel
    Left = 50
    Top = 239
    Width = 19
    Height = 13
    Caption = 'Ano'
  end
  object Label17: TLabel
    Left = 111
    Top = 241
    Width = 24
    Height = 13
    Caption = 'Valor'
  end
  object Label18: TLabel
    Left = 8
    Top = 285
    Width = 159
    Height = 13
    Caption = 'Lista de valores/refer'#234'ncias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblvlacul: TLabel
    Left = 339
    Top = 527
    Width = 83
    Height = 13
    Caption = 'Valor Acumulado:'
  end
  object edtContribuinte: TEdit
    Left = 8
    Top = 27
    Width = 54
    Height = 21
    TabOrder = 0
    OnExit = edtContribuinteExit
  end
  object Button1: TButton
    Left = 71
    Top = 25
    Width = 72
    Height = 25
    Caption = 'Buscar'
    TabOrder = 15
    OnClick = Button1Click
  end
  object edtInicio: TMaskEdit
    Left = 8
    Top = 141
    Width = 77
    Height = 21
    EditMask = '!99/99/0000;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
  object edtTipoContribuicao: TEdit
    Left = 210
    Top = 139
    Width = 35
    Height = 21
    TabOrder = 2
  end
  object cbFormaPagamento: TDBLookupComboBox
    Left = 357
    Top = 141
    Width = 144
    Height = 21
    KeyField = 'cd_formPagto'
    ListField = 'ds_formPagto'
    ListSource = dsFormaPagamento
    TabOrder = 3
  end
  object edtValor: TCurrencyEdit
    Left = 9
    Top = 187
    Width = 120
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    TabOrder = 4
  end
  object edtBanco: TEdit
    Left = 138
    Top = 187
    Width = 107
    Height = 21
    TabOrder = 5
  end
  object edtAgencia: TEdit
    Left = 250
    Top = 187
    Width = 120
    Height = 21
    TabOrder = 6
  end
  object edtCheque: TEdit
    Left = 378
    Top = 187
    Width = 57
    Height = 21
    TabOrder = 7
  end
  object edtdataResgate: TMaskEdit
    Left = 450
    Top = 187
    Width = 66
    Height = 21
    EditMask = '!99/99/0000;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object edtMes: TEdit
    Left = 8
    Top = 258
    Width = 36
    Height = 21
    TabOrder = 9
  end
  object edtAno: TEdit
    Left = 50
    Top = 258
    Width = 54
    Height = 21
    TabOrder = 10
  end
  object edtValorParcial: TCurrencyEdit
    Left = 111
    Top = 258
    Width = 86
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    TabOrder = 11
  end
  object BitBtn1: TBitBtn
    Left = 203
    Top = 239
    Width = 109
    Height = 40
    Caption = 'Registrar Valor'
    TabOrder = 12
    OnClick = BitBtn1Click
    Glyph.Data = {
      0A040000424D0A04000000000000420000002800000016000000160000000100
      100003000000C8030000120B0000120B00000000000000000000007C0000E003
      00001F000000FF7FFF7FFF7FFF7FFF7FFF7F7A6BD24A8B3A8836A736A7368736
      6A3690465967FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C736F428632
      C53AC536A536852E852E8532C536C53AA5326D3A5A6BFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F5A6B6832E53E853245262522252625262526252625224526852EE53E
      672E175FFF7FFF7FFF7FFF7FFF7F39676832C53A452A45264526452A45264422
      44224526452A452645264526A5368632175FFF7FFF7FFF7F9C736832C5364526
      4526652A652A452A6B36D24ED24E8C3A4526652A452A45264526A536662E5A6B
      FF7FFF7F9146C536452A452A652A652E652E642AD0469E779E77F34E4426652E
      652A652A452A4526C5366D3AFF7F9C73672E852E652A652E652E852E8532642E
      CF469D739D77F24E642A852E652E652E652E652A652A852E5967D4528532652E
      652E852E853285328532842ECF469D779D77F34E642E853285328532852E652E
      652A852E6F426D3A852E652E8532852E642E842E842E632ACF46BE77BE77F24E
      632A842E642E642A642E852E652E852E4A32482E8532853284328A3614531353
      1353F24E575FBD77BD775967F34E135313531457AD3E642E852E852E682E482A
      A736A736A432CE46DF7FDE7BDE7BDE7BBE7BBD77BD77BD77DE7BDE7BDE7BDF7F
      1353842EA736A736682E482AC83EC83EC73ACF46DF7FDE7BDE7BDE7BBE7BBD77
      BD77BE7BDE7BDE7BDE7BDF7F1353A736C83EC93E692E4A32EA42EA42E942CB3E
      124F114F114FF04E565FDE7BDE7B5863F04A114F114F1253CD42C942EA42EA42
      6A326F3E0B470B4B0B4B0A4BE946E9460947E842114FDF7FDF7F1457C83E0947
      E946E946EA460B4BEB460B476C36D556EC420C4F0C4B0C4F2C4F2C4F2C4F0B4B
      1253FF7FFF7F355BEA462C4F2C4F0C4F0C4F0C4B0C4B0C479146BD778C3A4F57
      2D4F2D532D532D532D532C4F3357FF7FFF7F355B0B4B2D532D532D532D4F2D4F
      4E53AC3E7B6FFF7FB24E304F4F572E534E574E574E572D533357FF7FFF7F365B
      0C4F4F574E572E532E534F5751576F3EFF7FFF7FDE7B6D3A735F505B4F57505B
      505B4F5BEF4A12531253EF464F57505B505B5057505B735F6D369C73FF7FFF7F
      FF7F7B6F6D3694637363715B715B715F715F705B705B715F715F715B515B725F
      95678D3A5967FF7FFF7FFF7FFF7FFF7F9B6F8F3E745FB66B94677263725F725F
      725F725F725F9463B66B95638E3E5967FF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B
      B34EAF42755FB76BB86FB86FB76FB86FB76B7563D046B24ABD73FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77165BB146AF42CF42CF42AF429046D556
      9C73FF7FFF7FFF7FFF7FFF7FFF7F}
  end
  object BitBtn2: TBitBtn
    Left = 318
    Top = 239
    Width = 116
    Height = 40
    Caption = 'Registrar Valores'
    Enabled = False
    TabOrder = 13
    OnClick = BitBtn2Click
    Glyph.Data = {
      0A040000424D0A04000000000000420000002800000016000000160000000100
      100003000000C8030000120B0000120B00000000000000000000007C0000E003
      00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73D756F85E7B6BBE77FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73
      9229D004CE0C2E1DD135754A195F7C6FDE77FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F9D7392295701D300340D5511B100AF04CD0C4F21D135954E
      195F9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77B32999019901F30479425D67
      BB4E3836961D340DD100CF04CD0CF239BD77FF7FFF7FFF7FFF7FFF7FBE77D331
      B901FC019901340D99469D6F5D673C631C5FDB527942F72976192E1DF85EFF7F
      FF7FFF7FFF7FFF7F563EBA011D02FB019901340D9A4A9D6F5D673C631B5BFB56
      DB52DA52BB4A163A754AFF7FFF7FFF7FFF7FFF7FF91D3E021C02FC01B9013411
      BA4A9E737D6B1C5B9B46BB4A9B46BA4ABA4A373E754AFF7FFF7FFF7FFF7FFF7F
      F91D5E021D021C02BA015511DB4EBE779E6F7252E94DEB51314A583EDB4E573E
      754EFF7FFF7FFF7FFF7FFF7F191E5E023D021D02BA01120D984ADF779E6F1767
      F662935A2B56964EDC525742754EFF7FFF7FFF7FFF7FFF7F191E5F023E023D02
      DB016B003442FF7F9E739E6F7E675D631C5BFB56FB565746754EFF7FFF7FFF7F
      FF7FFF7F1A1E7F025E023E02DB016C00563EDF779E6F9D6F7D6B5C675D631C5F
      1C577746754EFF7FFF7FFF7FFF7FFF7F1A1E7F025F025E02FB01593A3E57FC4A
      DB46FC4A1D531D53984AB752FB56994EB756FF7FFF7FFF7FFF7FFF7F1A229F06
      5F023E02FE36FF7FDF737E635D5F3D573F575936EE0CED0C0F11B22D7B6BFF7F
      FF7FFF7FFF7FFF7F1A229F0E7F025F029E22BE6FFF7FBE6F5E637F63BB4A173A
      7A427A3EB71D7025BD77FF7FFF7FFF7FFF7FFF7F3A26BF127F0A7F021D027B36
      BE775E5B1E57DC529946DB4EDA4EBA4E192E90259D73FF7FFF7FFF7FFF7FFF7F
      3A26DF1A9F0E7F063E02BB3A7D6BFC4A7A3E574299469A46BA4EBA4E392E9125
      9D73FF7FFF7FFF7FFF7FFF7F3A2ADF1E9F169F0A3E02DC3EBE777D6F8E5E075A
      C94D0D4AB94EDB523A2E91259D73FF7FFF7FFF7FFF7FFF7F3A2AFF26BF1A9F12
      5E02DD3EDE7B9E6F5B6B3B671A5FD75AFA56DB565B3291259D73FF7FFF7FFF7F
      FF7FFF7F3A2EFF2ADF22BF1A7F061E43FE7FBE779E6F7D6B5D633C5FFB5AFB56
      7B3291259D73FF7FFF7FFF7FFF7FFF7F5A2E1F33DF22BF1A9F0EFE327F5F7E5B
      7E5F5D5B5D5F3C5B1C5B1C5BBD36B2259D73FF7FFF7FFF7FFF7FFF7F5A2E3F3F
      3F435F4F7E5F5E5B3D531D4F1D4FFC4ADC429B3A5A2A1A1EDB11353EDE7BFF7F
      FF7FFF7FFF7FFF7FDA4E58425842584258425842373E373A373E373A373A5842
      B94EFA5A5B6BDE77FF7FFF7FFF7F}
  end
  object sgvalores: TStringGrid
    Left = 8
    Top = 304
    Width = 525
    Height = 217
    ColCount = 3
    FixedCols = 0
    RowCount = 2
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goRowSelect]
    ParentFont = False
    PopupMenu = ppMenu
    TabOrder = 14
  end
  object edtVlAcumulado: TCurrencyEdit
    Left = 429
    Top = 525
    Width = 104
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    TabOrder = 16
  end
  object ckCalculo: TCheckBox
    Left = 95
    Top = 145
    Width = 97
    Height = 17
    Caption = 'Calcular Datas'
    TabOrder = 17
    OnClick = ckCalculoClick
  end
  object Panel1: TPanel
    Left = 250
    Top = 304
    Width = 287
    Height = 121
    TabOrder = 18
    Visible = False
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 246
      Height = 16
      Caption = 'Informar Datas de calculo de parcelas'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 30
      Width = 53
      Height = 13
      Caption = 'Data Inicial'
    end
    object Label19: TLabel
      Left = 112
      Top = 30
      Width = 48
      Height = 13
      Caption = 'Data Final'
    end
    object BitBtn3: TBitBtn
      Left = 200
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Cancelar'
      TabOrder = 0
      OnClick = BitBtn3Click
    end
    object BitBtn4: TBitBtn
      Left = 119
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Gravar Datas'
      TabOrder = 1
      OnClick = BitBtn4Click
    end
    object edtInicial: TMaskEdit
      Left = 16
      Top = 49
      Width = 73
      Height = 21
      EditMask = '!99/99/0000;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object edtFinal: TMaskEdit
      Left = 112
      Top = 49
      Width = 73
      Height = 21
      EditMask = '!99/99/0000;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
  end
  object BitBtn5: TBitBtn
    Left = 440
    Top = 239
    Width = 97
    Height = 40
    Caption = 'Limpar'
    TabOrder = 19
    OnClick = BitBtn5Click
    Glyph.Data = {
      0A040000424D0A04000000000000420000002800000016000000160000000100
      100003000000C8030000120B0000120B00000000000000000000007C0000E003
      00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F5C5F7C677C677C677D677D677D67
      7D677D677D677D677D677D677C677C677C677C677C677C675C637C677D631B57
      BC7BBD77BD77BD7BBD7BBD7BBD7BBD7BBD7BBD7BBD7BBD77BD77BC779C779C77
      9C779C739B739C777D671B57BD7BBD77BD77BD77BD77BD77BD7BBD7BBD7BBD77
      BD77BD77BD77BD77BD779C739C739C739C739C779D6F1B57DD7BBD77DE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BBD77BD77BD77BD779C739C739C739C77
      9D6B1B57DE7FDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      BD77BD77BD779C739C739C779D6B1C53FE7FDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BBD77BD77BD779C73BC779D6B1C53FF7FDE7B
      DE7BDE7BDE7BDE7BFF7FFF7FFF7FDE7BDE7BDE7BDE7BDE7BDE7BDE7BBD77BD77
      BD77BD777D633C53FF7FDE7BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B
      DE7BDE7BDE7BDE7BDE7BBD77BD77BD777D5B1C53FF7FDE7BFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7BDE7BDE7BDE7BDE7BBD77BD775D531C4F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7BDE7B
      DE7BDE7BBD77BD7B7D631C4FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FDE7BDE7BDE7BDE7BDD7B7D6B1C4FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7BDE7BDE7BDE7BDD7B
      7D671C4FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FDE7BDE7BDE7BDE7BDE7B7D671C4BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7BDE7BDE7BDE7B7D671C4BFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7BDE7B
      DE7BDE7B5D5BDA429C779C739C739C739C739C739C739C739C739C739C739C73
      9C739C739C739C739C739C739C739C733B5BBA363B5F3B5B3B5B3B5B3B5B3B5B
      3B5B3B5B3B5B3B5B3B5B3B5B3B5B3B5B3B5B3B5B3B5B3B5B3B5B3B5B1B4F7922
      9A2A9A2A9A2A9A2A9A2A9A2A9A2A9A2A9A2A9A2A9A2A9A2A9A2A9A2A9A2A9A2A
      9A2A9A2A9A2A9A2E9A2A9A2E5D533C4F3C4F3C4F3C4F3C4F3C4F3C4F3C4F3C4F
      3C4F3C4F3C4F3C4F3C4F3C4F1C4B1C4B3C4B3C4BFC3EFB3EFB42FB42FB42FB42
      FB42FB42FB42FB42FB42FB42FB42FB42FB42FB42FB42FB42FB42FB42FB42FB42
      FB42FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F}
  end
  object dsFormaPagamento: TDataSource
    DataSet = DM.qFormaPagamento
    Left = 280
    Top = 40
  end
  object ppMenu: TPopupMenu
    Left = 496
    Top = 72
    object RemoverItem1: TMenuItem
      Caption = 'Remover Item'
      OnClick = RemoverItem1Click
    end
  end
  object qInsertPgto: TADOQuery
    Connection = DM.ADOConn
    Parameters = <
      item
        Name = 'codigo'
        Size = -1
        Value = Null
      end
      item
        Name = 'ano'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'mes'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'valorCont'
        DataType = ftFloat
        Size = -1
        Value = Null
      end
      item
        Name = 'tipo'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'st_pgto_cheque'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'dtpagto'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'valorCont1'
        DataType = ftFloat
        Size = -1
        Value = Null
      end
      item
        Name = 'dtpagto1'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'forma1'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'codigo1'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'ano1'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'mes1'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'tipo1'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'banco1'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'agencia1'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'cheque1'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'data1'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'st_pgto_cheque1'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [tbContribuicoes]'
      '           ([cd_cadastro]'
      '           ,[dt_anoRefContribuicao]'
      '           ,[dt_mesRefContribuicao]'
      '           ,[vl_ContribuicaoVencimento]'
      '           ,[cd_tipoContribuicao]'
      '           ,[cd_sitQuitacao]'
      '           ,[dt_Contribuicao])'
      '    VALUES'
      '           (:codigo'
      '           ,:ano'
      '           ,:mes'
      '           ,:valorCont'
      '           ,:tipo'
      '           ,:st_pgto_cheque'
      '           ,:dtpagto)'
      ''
      ''
      ''
      #9#9'INSERT INTO [tbPagtoContribuicao]'
      '           ([vl_pagtoContribuicao]'
      '           ,[dt_pagtoContribuicao]'
      '           ,[cd_formPagto]'
      '           ,[cd_cadastro]'
      '           ,[dt_anoRefContribuicao]'
      '           ,[dt_mesRefContribuicao]'
      '           ,[cd_tipoContribuicao]'
      '           ,ds_bancoCheque'
      '           ,ds_agenciaCheque'
      '           ,no_chequePagto'
      '           ,dt_resgChequePre'
      '           ,sq_chequePagto)'
      #9#9'VALUES'
      '           (:valorCont1'
      '           ,:dtpagto1'
      '           ,:forma1'
      '           ,:codigo1'
      '           ,:ano1'
      '           ,:mes1'
      '           ,:tipo1'
      '           ,:banco1'
      '           ,:agencia1'
      '           ,:cheque1'
      '           ,:data1'
      '           ,:st_pgto_cheque1)')
    Left = 376
    Top = 40
  end
  object qContribuicao: TADOQuery
    Connection = DM.ADOConn
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ano'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'mes'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'valorCont'
        Attributes = [paSigned]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Size = 19
        Value = Null
      end
      item
        Name = 'tipo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'st_pgto_cheque'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'dtpagto'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [tbContribuicoes]'
      '           ([cd_cadastro]'
      '           ,[dt_anoRefContribuicao]'
      '           ,[dt_mesRefContribuicao]'
      '           ,[vl_ContribuicaoVencimento]'
      '           ,[cd_tipoContribuicao]'
      '           ,[cd_sitQuitacao]'
      '           ,[dt_Contribuicao])'
      '    VALUES'
      '           (:codigo'
      '           ,:ano'
      '           ,:mes'
      '           ,:valorCont'
      '           ,:tipo'
      '           ,:st_pgto_cheque'
      '           ,:dtpagto)')
    Left = 464
    Top = 24
  end
end
