unit Unit_PesquisaCategoria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PesquisaPadrao, DB, Grids, DBGrids, StdCtrls, Buttons,
  ExtCtrls, ActnList;

type
  Tfrm_PesquisaCategoria = class(Tfrm_PesquisaPadrao)
    procedure btnPesquisaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_PesquisaCategoria: Tfrm_PesquisaCategoria;

implementation

{$R *.dfm}

procedure Tfrm_PesquisaCategoria.btnPesquisaClick(Sender: TObject);
begin
  inherited;
  dsPesquisa.DataSet.Locate('ds_categoria',edtArgumento.Text,[loPartialKey]);
  
end;

end.
