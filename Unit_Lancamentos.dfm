object frm_Lancamentos: Tfrm_Lancamentos
  Left = 10
  Top = 10
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Lan'#231'amentos'
  ClientHeight = 575
  ClientWidth = 757
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 757
    Height = 61
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 2
      Width = 92
      Height = 19
      Caption = 'Lan'#231'amentos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 63
      Top = 35
      Width = 66
      Height = 16
      Caption = 'Refer'#234'ncia:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 136
      Top = 16
      Width = 19
      Height = 13
      Caption = 'M'#234's'
    end
    object Label4: TLabel
      Left = 192
      Top = 16
      Width = 19
      Height = 13
      Caption = 'Ano'
    end
    object Label8: TLabel
      Left = 433
      Top = 11
      Width = 105
      Height = 13
      Caption = 'Encontrar Beneficiado'
    end
    object edtMes: TEdit
      Left = 136
      Top = 34
      Width = 49
      Height = 21
      TabOrder = 0
      OnKeyPress = edtMesKeyPress
    end
    object edtAno: TEdit
      Left = 191
      Top = 34
      Width = 50
      Height = 21
      TabOrder = 1
      OnKeyPress = edtAnoKeyPress
    end
    object BitBtn1: TBitBtn
      Left = 256
      Top = 16
      Width = 137
      Height = 39
      Caption = 'Abrir Refer'#234'ncia'
      TabOrder = 2
      OnClick = BitBtn1Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73D756F85E7B6BBE77FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73
        9229D004CE0C2E1DD135754A195F7C6FDE77FF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7F9D7392295701D300340D5511B100AF04CD0C4F21D135954E
        195F9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77B32999019901F30479425D67
        BB4E3836961D340DD100CF04CD0CF239BD77FF7FFF7FFF7FFF7FFF7FBE77D331
        B901FC019901340D99469D6F5D673C631C5FDB527942F72976192E1DF85EFF7F
        FF7FFF7FFF7FFF7F563EBA011D02FB019901340D9A4A9D6F5D673C631B5BFB56
        DB52DA52BB4A163A754AFF7FFF7FFF7FFF7FFF7FF91D3E021C02FC01B9013411
        BA4A9E737D6B1C5B9B46BB4A9B46BA4ABA4A373E754AFF7FFF7FFF7FFF7FFF7F
        F91D5E021D021C02BA015511DB4EBE779E6F7252E94DEB51314A583EDB4E573E
        754EFF7FFF7FFF7FFF7FFF7F191E5E023D021D02BA01120D984ADF779E6F1767
        F662935A2B56964EDC525742754EFF7FFF7FFF7FFF7FFF7F191E5F023E023D02
        DB016B003442FF7F9E739E6F7E675D631C5BFB56FB565746754EFF7FFF7FFF7F
        FF7FFF7F1A1E7F025E023E02DB016C00563EDF779E6F9D6F7D6B5C675D631C5F
        1C577746754EFF7FFF7FFF7FFF7FFF7F1A1E7F025F025E02FB01593A3E57FC4A
        DB46FC4A1D531D53984AB752FB56994EB756FF7FFF7FFF7FFF7FFF7F1A229F06
        5F023E02FE36FF7FDF737E635D5F3D573F575936EE0CED0C0F11B22D7B6BFF7F
        FF7FFF7FFF7FFF7F1A229F0E7F025F029E22BE6FFF7FBE6F5E637F63BB4A173A
        7A427A3EB71D7025BD77FF7FFF7FFF7FFF7FFF7F3A26BF127F0A7F021D027B36
        BE775E5B1E57DC529946DB4EDA4EBA4E192E90259D73FF7FFF7FFF7FFF7FFF7F
        3A26DF1A9F0E7F063E02BB3A7D6BFC4A7A3E574299469A46BA4EBA4E392E9125
        9D73FF7FFF7FFF7FFF7FFF7F3A2ADF1E9F169F0A3E02DC3EBE777D6F8E5E075A
        C94D0D4AB94EDB523A2E91259D73FF7FFF7FFF7FFF7FFF7F3A2AFF26BF1A9F12
        5E02DD3EDE7B9E6F5B6B3B671A5FD75AFA56DB565B3291259D73FF7FFF7FFF7F
        FF7FFF7F3A2EFF2ADF22BF1A7F061E43FE7FBE779E6F7D6B5D633C5FFB5AFB56
        7B3291259D73FF7FFF7FFF7FFF7FFF7F5A2E1F33DF22BF1A9F0EFE327F5F7E5B
        7E5F5D5B5D5F3C5B1C5B1C5BBD36B2259D73FF7FFF7FFF7FFF7FFF7F5A2E3F3F
        3F435F4F7E5F5E5B3D531D4F1D4FFC4ADC429B3A5A2A1A1EDB11353EDE7BFF7F
        FF7FFF7FFF7FFF7FDA4E58425842584258425842373E373A373E373A373A5842
        B94EFA5A5B6BDE77FF7FFF7FFF7F}
    end
    object edtCodigo: TEdit
      Left = 433
      Top = 30
      Width = 72
      Height = 21
      TabOrder = 3
    end
    object BitBtn3: TBitBtn
      Left = 544
      Top = 16
      Width = 106
      Height = 39
      Caption = 'Buscar'
      TabOrder = 4
      OnClick = BitBtn3Click
    end
  end
  object dbFilho: TDBGrid
    Left = 0
    Top = 328
    Width = 465
    Height = 247
    Align = alLeft
    DataSource = dsFilho
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'dt_tipo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nm_Rubrica'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'no_freqRubrica'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'vl_Rubrica'
        Width = 100
        Visible = True
      end>
  end
  object Panel2: TPanel
    Left = 465
    Top = 328
    Width = 292
    Height = 247
    Align = alClient
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Label5: TLabel
      Left = 16
      Top = 7
      Width = 134
      Height = 19
      Caption = 'Lancamentos Para:'
    end
    object lblNomeContribuinte: TLabel
      Left = 16
      Top = 38
      Width = 5
      Height = 19
    end
    object Label6: TLabel
      Left = 16
      Top = 71
      Width = 53
      Height = 19
      Caption = 'Rubrica'
    end
    object Label7: TLabel
      Left = 16
      Top = 125
      Width = 37
      Height = 19
      Caption = 'Valor'
    end
    object cbRubrica: TDBLookupComboBox
      Left = 16
      Top = 92
      Width = 265
      Height = 27
      KeyField = 'cd_tipRubrica'
      ListField = 'ds_rubrica'
      ListSource = dsRubricas
      TabOrder = 0
    end
    object edtValor: TCurrencyEdit
      Left = 16
      Top = 148
      Width = 121
      Height = 27
      Margins.Left = 5
      Margins.Top = 1
      TabOrder = 1
    end
    object BitBtn2: TBitBtn
      Left = 16
      Top = 192
      Width = 217
      Height = 33
      Caption = 'Registrar Lan'#231'amento'
      TabOrder = 2
      OnClick = BitBtn2Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F7A6BD3528C3E8836A736A7368836
        8C3ED3527A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73B14A8736
        C53AC53AA536A532A532A536C53AC53A8736B14A9C73FF7FFF7FFF7FFF7FFF7F
        FF7FFF7F59676A3AC53AA536652A252625222522252225222526652AA536C53A
        6A3A5967FF7FFF7FFF7FFF7FFF7F38638836E53A652A2526452645264526452A
        452A4526452625262526652AC53A68363863FF7FFF7FFF7F9C6F4936C536452A
        4526452A652A652A44264526652A652A652A452A452A45264526C53649369C6F
        FF7FFF7FB14AA536652A452A652A652E652E43268B3AD34E662A652E652E652E
        652A652A4526652AA532B14AFF7F7A6B8732852E452A652E652E852E63268A36
        7A6B9E77AC3A642A8532852E652E652E652E452A852E67327A6BB34EA52E652E
        652E852E8532632A8B367B6BBD779D77385F652A843285328532852E652E652E
        652A852EB34E6C3A8532652E85328532632A8B3A7B6FBD77BD779D779E77D046
        632AA536853285328532652E652E852E6C3A6932853285328532842EAD3E7C6F
        BE7BBD777A679C73BE7B7B6B8836A332A536A53685328532652E852E69326832
        A736A636A536652E385FDF7FDF7B355B662E355BDE7BDF7B1557842EC436A536
        A536A532A636A736682E8932C93EC83EC83EA636EF4ABE7B15578532A232A93A
        9D73DE7BBE77CD42A332C63EC73EC83EC83AC93A69326A32EA42E942E946E946
        CA3ED046C93AE742E842C63AF24EDF7FDF7F7B6BAA3AE842EA46E942C942EA42
        6A326D3A0B470B4B0B4B0B4B0A47C83E0A4B0B4B0B4B2A4BC93E7967FF7FFF7F
        365BC93A0A4BEB46EB460B476D3AB34E0C472C4F0C4B0C4F2C4F2C4F2C4F2C4F
        2C4F2C4F0A4BCE46BD73FF7FFF7F1353EA420C4B0C4FEC46B34E7A6BAD3E4F57
        2D4F2D4F2D532D532D532D532D532D534D57EB46124FFF7FFF7F1453EB422D4F
        4F57AD3E7A6BFF7FB24A30534F5B2E534E574E574E574E574E574E574E574E57
        EC4635571457ED4A2E5350573053B24AFF7FFF7F9B6F6D3A9363505B4F575057
        4F5B4F5B4F5B6F5B4F5B705B6F5BED460D4B4F57715B935F6D3A9B6FFF7FFF7F
        FF7F3863AE42B5679363515B505B715B715F715F715F715F715B705B515B9363
        B567AE423863FF7FFF7FFF7FFF7FFF7F58638F42755FB66F94677263725F725F
        725F725F72639467B66F755F8F425863FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C6F
        B24AF14A9663B86FD86FB76FB76FB86FB86F9663D14AB24A9C6FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F7A6BD452B146D14AF24EF24ED14AB146D452
        7A6BFF7FFF7FFF7FFF7FFF7FFF7F}
    end
    object Button1: TButton
      Left = 143
      Top = 144
      Width = 114
      Height = 33
      Caption = 'Calcula Valor'
      TabOrder = 3
      OnClick = Button1Click
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 61
    Width = 757
    Height = 267
    Align = alTop
    Caption = 'Panel3'
    TabOrder = 3
    object dbPai: TDBGrid
      Left = 1
      Top = 26
      Width = 755
      Height = 240
      Align = alClient
      DataSource = dsPai
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = dbPaiDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'nm_pastor'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'vl_vantagem'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'vl_desconto'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'vl_liquido'
          Width = 80
          Visible = True
        end>
    end
    object DBNavigator1: TDBNavigator
      Left = 1
      Top = 1
      Width = 755
      Height = 25
      DataSource = dsPai
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
      Align = alTop
      TabOrder = 1
    end
  end
  object dsPai: TDataSource
    DataSet = qListaBeneficiados
    Left = 48
    Top = 128
  end
  object dsFilho: TDataSource
    DataSet = qListaVantagemDesconto
    Left = 160
    Top = 120
  end
  object qListaVantagemDesconto: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    DataSource = dsPai
    Parameters = <
      item
        Name = 'cd_cabepa'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'dt_anoPagamento'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'dt_mesPagamento'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      
        '      select v.nm_Rubrica,p.no_freqRubrica,p.vl_Rubrica,p.cd_tip' +
        'Rubrica,p.no_Rubrica,'
      'case p.no_Rubrica'
      #9#9'when 1 then '#39'VN'#39
      #9#9'when 2 then '#39'DN'#39#9' '
      #9'end dt_tipo'
      'from tbPagamentoMensal p'
      'inner join tbVantDesc v on v.cd_tipRubrica = p.cd_tipRubrica'
      #9#9#9#9#9#9'and v.no_Rubrica = p.no_Rubrica'
      'where cd_cadastro = :cd_cabepa'
      #9'and dt_anoPagamento = :dt_anoPagamento'
      #9'and dt_mesPagamento = :dt_mesPagamento'
      'order by  p.no_Rubrica,v.cd_tipRubrica')
    Left = 160
    Top = 176
    object qListaVantagemDescontodt_tipo: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'dt_tipo'
      ReadOnly = True
      Size = 2
    end
    object qListaVantagemDescontonm_Rubrica: TStringField
      DisplayLabel = 'Rubrica'
      FieldName = 'nm_Rubrica'
    end
    object qListaVantagemDescontono_freqRubrica: TSmallintField
      DisplayLabel = 'Freq'
      FieldName = 'no_freqRubrica'
    end
    object qListaVantagemDescontovl_Rubrica: TBCDField
      DisplayLabel = 'Valor'
      FieldName = 'vl_Rubrica'
      currency = True
      Precision = 10
      Size = 2
    end
    object qListaVantagemDescontocd_tipRubrica: TSmallintField
      FieldName = 'cd_tipRubrica'
      Visible = False
    end
    object qListaVantagemDescontono_Rubrica: TSmallintField
      FieldName = 'no_Rubrica'
      Visible = False
    end
  end
  object qListaBeneficiados: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'mes'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'ano'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'exec sp_ListaResumoBeneficio '
      #9'@mes = :mes'
      #9',@ano = :ano'
      '               ,@codigo = 0'
      '              ,@status = '#39'P'#39)
    Left = 40
    Top = 184
    object qListaBeneficiadosnm_pastor: TStringField
      DisplayLabel = 'Contribuinte'
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qListaBeneficiadosvl_vantagem: TBCDField
      DisplayLabel = 'Vantagens'
      FieldName = 'vl_vantagem'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qListaBeneficiadosvl_desconto: TBCDField
      DisplayLabel = 'Desconto'
      FieldName = 'vl_desconto'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qListaBeneficiadosvl_liquido: TBCDField
      DisplayLabel = 'Liquido'
      FieldName = 'vl_liquido'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qListaBeneficiadosdt_mesPagamento: TSmallintField
      FieldName = 'dt_mesPagamento'
      ReadOnly = True
    end
    object qListaBeneficiadosdt_anoPagamento: TSmallintField
      FieldName = 'dt_anoPagamento'
      ReadOnly = True
    end
    object qListaBeneficiadoscd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object qListaRubricas: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      #9'v.cd_tipRubrica,'
      #9'case v.no_Rubrica'
      #9#9'when 1 then '#39'VN'#39
      #9#9'when 2 then '#39'DN'#39
      #9'end + '#39' - '#39' + v.nm_Rubrica as ds_rubrica'
      #9',v.no_Rubrica'
      'from tbVantDesc v'
      'where v.cd_tipRubrica <> 1'
      'order by v.no_Rubrica,v.nm_Rubrica ')
    Left = 360
    Top = 128
    object qListaRubricascd_tipRubrica: TSmallintField
      FieldName = 'cd_tipRubrica'
      ReadOnly = True
    end
    object qListaRubricasds_rubrica: TStringField
      FieldName = 'ds_rubrica'
      ReadOnly = True
      Size = 25
    end
    object qListaRubricasno_Rubrica: TSmallintField
      FieldName = 'no_Rubrica'
    end
  end
  object dsRubricas: TDataSource
    DataSet = qListaRubricas
    Left = 360
    Top = 184
  end
  object qRegistraLancamento: TADOQuery
    Connection = DM.ADOConn
    Parameters = <
      item
        Name = 'cd_cadastro'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'dt_anoPagamento'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'dt_mesPagamento'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'cd_tipRubrica'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'no_Rubrica'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'vl_Rubrica'
        Attributes = [paSigned]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Size = 19
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO tbPagamentoMensal'
      '           (cd_cadastro'
      '           ,dt_anoPagamento'
      '           ,dt_mesPagamento'
      '           ,cd_tipRubrica'
      '           ,no_Rubrica'
      '           ,no_freqRubrica'
      '           ,vl_Rubrica)'
      '     VALUES'
      '           ( :cd_cadastro'
      '           , :dt_anoPagamento'
      '           , :dt_mesPagamento'
      '           , :cd_tipRubrica'
      '           , :no_Rubrica'
      '           , 0'
      '           , :vl_Rubrica)')
    Left = 360
    Top = 240
  end
  object qVerificaRef: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'mes'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'ano'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'exec sp_verificaFolhaRef '
      '       @mes = :mes'
      '       ,@ano = :ano'
      '       ,@codigo = 0'
      '      ,@status = '#39'P'#39)
    Left = 520
    Top = 144
    object qVerificaRefnr_registros: TIntegerField
      FieldName = 'nr_registros'
      ReadOnly = True
    end
  end
  object qCalculaIR: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'valor'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = Null
      end>
    SQL.Strings = (
      'EXEC sp_calculaIR @valor = :valor')
    Left = 600
    Top = 144
    object qCalculaIRvl_ir: TBCDField
      FieldName = 'vl_ir'
      ReadOnly = True
      Precision = 19
    end
  end
  object qValorBruto: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = 0
      end
      item
        Name = 'mes'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'ano'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select vl_rubrica as vl_beneficio'
      'from tbPagamentoMensal'
      'where cd_cadastro = :codigo'
      'and dt_mesPagamento = :mes'
      'and dt_anoPagamento = :ano'
      'and cd_tipRubrica in (1,11)')
    Left = 520
    Top = 208
    object qValorBrutovl_beneficio: TBCDField
      FieldName = 'vl_beneficio'
      ReadOnly = True
      Precision = 10
      Size = 2
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 616
    Top = 208
    object RemoverLanamento1: TMenuItem
      Caption = 'Remover Lan'#231'amento'
      OnClick = RemoverLanamento1Click
    end
  end
end
