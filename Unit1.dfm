object Form1: TForm1
  Left = 256
  Top = 161
  Width = 870
  Height = 500
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 112
    Top = 64
    Width = 60
    Height = 13
    Caption = 'cd_CABEPA'
    FocusControl = DBEdit1
  end
  object Label2: TLabel
    Left = 112
    Top = 104
    Width = 85
    Height = 13
    Caption = 'cd_formTeologica'
  end
  object Label3: TLabel
    Left = 112
    Top = 144
    Width = 63
    Height = 13
    Caption = 'cd_natPastor'
    FocusControl = DBEdit3
  end
  object DBEdit1: TDBEdit
    Left = 112
    Top = 80
    Width = 134
    Height = 21
    DataField = 'cd_CABEPA'
    DataSource = DataSource1
    TabOrder = 0
  end
  object DBEdit3: TDBEdit
    Left = 112
    Top = 160
    Width = 134
    Height = 21
    DataField = 'cd_natPastor'
    DataSource = DataSource1
    TabOrder = 1
  end
  object DBNavigator1: TDBNavigator
    Left = 104
    Top = 192
    Width = 240
    Height = 25
    DataSource = DataSource1
    TabOrder = 2
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 112
    Top = 120
    Width = 145
    Height = 21
    DataField = 'cd_formTeologica'
    DataSource = DataSource1
    KeyField = 'cd_formteologica'
    ListField = 'ds_formteologica'
    ListSource = DataSource2
    TabOrder = 3
  end
  object ADOQuery1: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT cd_CABEPA'
      '      ,cd_formTeologica'
      '      ,cd_natPastor'
      '      ,cd_escPastor'
      '      ,cd_nacPastor'
      '      ,cd_estCivil'
      '      ,cd_categoria'
      '      ,nm_pastor'
      '      ,no_regConv'
      '      ,dt_nascPastor'
      '      ,tp_sanguineo'
      '      ,no_regGeral'
      '      ,no_cpf'
      '      ,ds_endereco'
      '      ,ds_compEndPastor'
      '      ,no_cepPastor'
      '      ,dt_filiacao'
      '      ,nm_pai'
      '      ,nm_mae'
      '      ,nm_conjuge'
      '      ,dt_nascConjuge'
      '      ,no_fone'
      '      ,dt_batismo'
      '      ,ds_localBatismo'
      '      ,dt_autEvangelista'
      '      ,dt_consagEvangelista'
      '      ,dt_ordenacPastor'
      '      ,ds_localConsagracao'
      '      ,ds_campo'
      '      ,ds_supervisao'
      '      ,no_certCasamento'
      '  FROM tbContribuinte'
      '')
    Left = 560
    Top = 112
    object ADOQuery1cd_CABEPA: TIntegerField
      FieldName = 'cd_CABEPA'
    end
    object ADOQuery1cd_formTeologica: TIntegerField
      FieldName = 'cd_formTeologica'
    end
    object ADOQuery1cd_natPastor: TIntegerField
      FieldName = 'cd_natPastor'
    end
    object ADOQuery1cd_escPastor: TIntegerField
      FieldName = 'cd_escPastor'
    end
    object ADOQuery1cd_nacPastor: TIntegerField
      FieldName = 'cd_nacPastor'
    end
    object ADOQuery1cd_estCivil: TIntegerField
      FieldName = 'cd_estCivil'
    end
    object ADOQuery1cd_categoria: TIntegerField
      FieldName = 'cd_categoria'
    end
    object ADOQuery1nm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object ADOQuery1no_regConv: TIntegerField
      FieldName = 'no_regConv'
    end
    object ADOQuery1dt_nascPastor: TDateTimeField
      FieldName = 'dt_nascPastor'
    end
    object ADOQuery1tp_sanguineo: TStringField
      FieldName = 'tp_sanguineo'
      FixedChar = True
    end
    object ADOQuery1no_regGeral: TStringField
      FieldName = 'no_regGeral'
      FixedChar = True
    end
    object ADOQuery1no_cpf: TStringField
      FieldName = 'no_cpf'
      FixedChar = True
      Size = 14
    end
    object ADOQuery1ds_endereco: TStringField
      FieldName = 'ds_endereco'
      Size = 60
    end
    object ADOQuery1ds_compEndPastor: TStringField
      FieldName = 'ds_compEndPastor'
      Size = 40
    end
    object ADOQuery1no_cepPastor: TStringField
      FieldName = 'no_cepPastor'
      FixedChar = True
      Size = 8
    end
    object ADOQuery1dt_filiacao: TDateTimeField
      FieldName = 'dt_filiacao'
    end
    object ADOQuery1nm_pai: TStringField
      FieldName = 'nm_pai'
      Size = 60
    end
    object ADOQuery1nm_mae: TStringField
      FieldName = 'nm_mae'
      Size = 60
    end
    object ADOQuery1nm_conjuge: TStringField
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object ADOQuery1dt_nascConjuge: TDateTimeField
      FieldName = 'dt_nascConjuge'
    end
    object ADOQuery1no_fone: TStringField
      FieldName = 'no_fone'
      Size = 15
    end
    object ADOQuery1dt_batismo: TDateTimeField
      FieldName = 'dt_batismo'
    end
    object ADOQuery1ds_localBatismo: TStringField
      FieldName = 'ds_localBatismo'
      Size = 40
    end
    object ADOQuery1dt_autEvangelista: TDateTimeField
      FieldName = 'dt_autEvangelista'
    end
    object ADOQuery1dt_consagEvangelista: TDateTimeField
      FieldName = 'dt_consagEvangelista'
    end
    object ADOQuery1dt_ordenacPastor: TDateTimeField
      FieldName = 'dt_ordenacPastor'
    end
    object ADOQuery1ds_localConsagracao: TStringField
      FieldName = 'ds_localConsagracao'
      Size = 40
    end
    object ADOQuery1ds_campo: TStringField
      FieldName = 'ds_campo'
      Size = 40
    end
    object ADOQuery1ds_supervisao: TStringField
      FieldName = 'ds_supervisao'
      Size = 40
    end
    object ADOQuery1no_certCasamento: TIntegerField
      FieldName = 'no_certCasamento'
    end
  end
  object DataSetProvider1: TDataSetProvider
    DataSet = ADOQuery1
    Left = 552
    Top = 168
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProvider1'
    Left = 560
    Top = 248
    object ClientDataSet1cd_CABEPA: TIntegerField
      FieldName = 'cd_CABEPA'
    end
    object ClientDataSet1cd_formTeologica: TIntegerField
      FieldName = 'cd_formTeologica'
    end
    object ClientDataSet1cd_natPastor: TIntegerField
      FieldName = 'cd_natPastor'
    end
    object ClientDataSet1cd_escPastor: TIntegerField
      FieldName = 'cd_escPastor'
    end
    object ClientDataSet1cd_nacPastor: TIntegerField
      FieldName = 'cd_nacPastor'
    end
    object ClientDataSet1cd_estCivil: TIntegerField
      FieldName = 'cd_estCivil'
    end
    object ClientDataSet1cd_categoria: TIntegerField
      FieldName = 'cd_categoria'
    end
    object ClientDataSet1nm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object ClientDataSet1no_regConv: TIntegerField
      FieldName = 'no_regConv'
    end
    object ClientDataSet1dt_nascPastor: TDateTimeField
      FieldName = 'dt_nascPastor'
    end
    object ClientDataSet1tp_sanguineo: TStringField
      FieldName = 'tp_sanguineo'
      FixedChar = True
    end
    object ClientDataSet1no_regGeral: TStringField
      FieldName = 'no_regGeral'
      FixedChar = True
    end
    object ClientDataSet1no_cpf: TStringField
      FieldName = 'no_cpf'
      FixedChar = True
      Size = 14
    end
    object ClientDataSet1ds_endereco: TStringField
      FieldName = 'ds_endereco'
      Size = 60
    end
    object ClientDataSet1ds_compEndPastor: TStringField
      FieldName = 'ds_compEndPastor'
      Size = 40
    end
    object ClientDataSet1no_cepPastor: TStringField
      FieldName = 'no_cepPastor'
      FixedChar = True
      Size = 8
    end
    object ClientDataSet1dt_filiacao: TDateTimeField
      FieldName = 'dt_filiacao'
    end
    object ClientDataSet1nm_pai: TStringField
      FieldName = 'nm_pai'
      Size = 60
    end
    object ClientDataSet1nm_mae: TStringField
      FieldName = 'nm_mae'
      Size = 60
    end
    object ClientDataSet1nm_conjuge: TStringField
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object ClientDataSet1dt_nascConjuge: TDateTimeField
      FieldName = 'dt_nascConjuge'
    end
    object ClientDataSet1no_fone: TStringField
      FieldName = 'no_fone'
      Size = 15
    end
    object ClientDataSet1dt_batismo: TDateTimeField
      FieldName = 'dt_batismo'
    end
    object ClientDataSet1ds_localBatismo: TStringField
      FieldName = 'ds_localBatismo'
      Size = 40
    end
    object ClientDataSet1dt_autEvangelista: TDateTimeField
      FieldName = 'dt_autEvangelista'
    end
    object ClientDataSet1dt_consagEvangelista: TDateTimeField
      FieldName = 'dt_consagEvangelista'
    end
    object ClientDataSet1dt_ordenacPastor: TDateTimeField
      FieldName = 'dt_ordenacPastor'
    end
    object ClientDataSet1ds_localConsagracao: TStringField
      FieldName = 'ds_localConsagracao'
      Size = 40
    end
    object ClientDataSet1ds_campo: TStringField
      FieldName = 'ds_campo'
      Size = 40
    end
    object ClientDataSet1ds_supervisao: TStringField
      FieldName = 'ds_supervisao'
      Size = 40
    end
    object ClientDataSet1no_certCasamento: TIntegerField
      FieldName = 'no_certCasamento'
    end
  end
  object DataSource1: TDataSource
    DataSet = ClientDataSet1
    Left = 424
    Top = 240
  end
  object ADOQuery2: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_formteologica,ds_formteologica'
      'from tbformteologica')
    Left = 352
    Top = 32
  end
  object DataSource2: TDataSource
    AutoEdit = False
    DataSet = ADOQuery2
    Left = 360
    Top = 88
  end
end
