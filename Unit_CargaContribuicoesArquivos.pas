unit Unit_CargaContribuicoesArquivos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ADODB, FileCtrl;

type
  Tfrm_cargaContribuicoesArquivos = class(TForm)
    listFiles: TFileListBox;
    Panel1: TPanel;
    Button1: TButton;
    Splitter1: TSplitter;
    Memo1: TMemo;
    Pagamento: TADOTable;
    Pagamentovl_pagtoContribuicao: TBCDField;
    Pagamentodt_pagtoContribuicao: TDateTimeField;
    Pagamentocd_formPagto: TSmallintField;
    Pagamentodt_anoRefContribuicao: TIntegerField;
    Pagamentodt_mesRefContribuicao: TIntegerField;
    Pagamentocd_tipoContribuicao: TIntegerField;
    contribicoes: TADOTable;
    contribicoesdt_anoRefContribuicao: TIntegerField;
    contribicoesdt_mesRefContribuicao: TIntegerField;
    contribicoesvl_ContribuicaoVencimento: TBCDField;
    contribicoescd_tipoContribuicao: TIntegerField;
    contribicoescd_sitQuitacao: TSmallintField;
    qBuscaContribuicao: TADOQuery;
    qBuscaContribuicaoqtd: TIntegerField;
    Pagamentocd_cadastro: TIntegerField;
    contribicoescd_cadastro: TIntegerField;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    function formataMoeda(valor : string): string;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_cargaContribuicoesArquivos: Tfrm_cargaContribuicoesArquivos;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_cargaContribuicoesArquivos.Button1Click(Sender: TObject);
var
  f : textfile;
  linha,tipo,data,mes,ano,valor,nome : string;
  start, linhas, cabecalho, i,x : smallint;
  posicao,a : integer;
  erro : Boolean;
begin

  contribicoes.Open;
  Pagamento.Open;


  for a := 0 to listFiles.Items.Count - 1 do
  begin
    //dm.ADOConn.BeginTrans;
    erro := false;
    AssignFile(f,listFiles.Items.Strings[a]);

    nome := listFiles.Items.Strings[a];
    nome := copy(nome,0,pos('.',nome)-1);

    if copy(nome,1,3) = 'txt' then
      nome := copy(nome,4,Length(nome));

    memo1.Lines.Add('Arquivo numero '+nome);
    reset(f);
    start := 0;
    linhas := 43;
    cabecalho := 15;
    i := 0;
    x := 0;
    //memo1.Lines.Clear;


    while not eof(f) do
    begin
      readln(f,linha);
      i := i + 1;
      if i > cabecalho then
      begin
        x := x + 1;
        if x < linhas then
        begin
          if trim(linha) <> ''   then
          begin
            mes := trim(copy(linha,0,2));
            ano := trim(copy(linha,4,4));
            data := copy(linha,11,10);
            tipo := trim(copy(linha,24,20));

            posicao := pos('0,00',linha)+4;
            valor := trim(copy(linha,posicao,20));
            //Memo1.Lines.Add(linha);
            //Memo1.Lines.Add(nome+','+mes+','+ano+','+data+','+tipo+','+valor);


            qBuscaContribuicao.close;
            qBuscaContribuicao.Parameters.ParamByName('codigo').Value :=
              strtoint(nome);
            qBuscaContribuicao.Parameters.ParamByName('mes').Value :=
              strtoint(mes);
            qBuscaContribuicao.Parameters.ParamByName('ano').Value :=
              strtoint(ano);

            qBuscaContribuicao.Open;

            if qBuscaContribuicaoqtd.Value = 0 then
            begin
              contribicoes.Insert;
              contribicoescd_cadastro.Value := strtoint(nome);
              contribicoesdt_anoRefContribuicao.Value := strtoint(ano);
              contribicoesdt_mesRefContribuicao.Value := strtoint(mes);
              contribicoesvl_ContribuicaoVencimento.Value := StrToCurr(formataMoeda(valor));
              if tipo = 'CONTRIBUIÇÃO' then
                contribicoescd_tipoContribuicao.Value := 1
              ELSE
                contribicoescd_tipoContribuicao.Value := 2;

              contribicoescd_sitQuitacao.Value := 1;

            Pagamento.Insert;

            Pagamentocd_cadastro.Value := strtoint(nome);
            Pagamentodt_anoRefContribuicao.Value := strtoint(ano);
            Pagamentodt_mesRefContribuicao.Value := strtoint(mes);

            //showmessage(tipo);

            if tipo = 'CONTRIBUIÇÃO' then
              Pagamentocd_tipoContribuicao.Value := 1
            else
              Pagamentocd_tipoContribuicao.Value := 2;

            Pagamentovl_pagtoContribuicao.Value := StrToCurr(formataMoeda(valor));
            Pagamentodt_pagtoContribuicao.Value := strtodate(data);
            Pagamentocd_formPagto.Value := 1;

            try
              contribicoes.Post;
              Pagamento.Post;
              memo1.Lines.add('Contribuinte No. ' +nome +' referencia: '+mes+'/'+ano+' carregada');
            finally
              //on E:Exception do
              //begin
              memo1.Lines.add('Erro ao carregar contribuinte No. ' +nome +' referencia: '+mes+'/'+ano);//+' : '+E.Message);
              //end;
              //erro := true;
            end;
            end;

          end
          else
            Break;

        end
        else
        begin
          i := 0;
          x := 0;
          cabecalho := 11;
          linhas := 52;
        end; // end if linhas

      end // end if cabecalho



    end; // end while

    CloseFile(F);
    {
    if erro then
      dm.ADOConn.RollbackTrans
    else
      dm.ADOConn.CommitTrans;
      }
  end; // end for

  showmessage('Acabou!');
  contribicoes.Close;
  Pagamento.Close;

end;

function Tfrm_cargaContribuicoesArquivos.formataMoeda(valor: string): string;
var
  i : integer;
  v : string;
begin

  v := '';
  for I := 1 to length(valor) do
  begin
    if copy(valor,I,1) <> '.' then
    begin
      //if copy(valor,I,1) = ',' then
        //v := v + '.'
      //else
        v := v + copy(valor,I,1);
    end;
  end;

  result := v;

end;

procedure Tfrm_cargaContribuicoesArquivos.FormShow(Sender: TObject);
begin

  listFiles.Directory := 'C:\Users\Domingos\Documents\Projetos\Assembleia de Deus\DADOS JUBILAÇÃO\textos';

end;

end.
