inherited frm_Contribuinte: Tfrm_Contribuinte
  Left = 229
  Top = 109
  Caption = 'Contribuintes'
  ClientHeight = 592
  ClientWidth = 962
  WindowState = wsMaximized
  ExplicitWidth = 970
  ExplicitHeight = 619
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel1: TPanel
    Width = 962
    ExplicitWidth = 962
    inherited btnPesquisar: TBitBtn
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73D756F85E7B6BBE77FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73
        9229D004CE0C2E1DD135754A195F7C6FDE77FF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7F9D7392295701D300340D5511B100AF04CD0C4F21D135954E
        195F9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77B32999019901F30479425D67
        BB4E3836961D340DD100CF04CD0CF239BD77FF7FFF7FFF7FFF7FFF7FBE77D331
        B901FC019901340D99469D6F5D673C631C5FDB527942F72976192E1DF85EFF7F
        FF7FFF7FFF7FFF7F563EBA011D02FB019901340D9A4A9D6F5D673C631B5BFB56
        DB52DA52BB4A163A754AFF7FFF7FFF7FFF7FFF7FF91D3E021C02FC01B9013411
        BA4A9E737D6B1C5B9B46BB4A9B46BA4ABA4A373E754AFF7FFF7FFF7FFF7FFF7F
        F91D5E021D021C02BA015511DB4EBE779E6F7252E94DEB51314A583EDB4E573E
        754EFF7FFF7FFF7FFF7FFF7F191E5E023D021D02BA01120D984ADF779E6F1767
        F662935A2B56964EDC525742754EFF7FFF7FFF7FFF7FFF7F191E5F023E023D02
        DB016B003442FF7F9E739E6F7E675D631C5BFB56FB565746754EFF7FFF7FFF7F
        FF7FFF7F1A1E7F025E023E02DB016C00563EDF779E6F9D6F7D6B5C675D631C5F
        1C577746754EFF7FFF7FFF7FFF7FFF7F1A1E7F025F025E02FB01593A3E57FC4A
        DB46FC4A1D531D53984AB752FB56994EB756FF7FFF7FFF7FFF7FFF7F1A229F06
        5F023E02FE36FF7FDF737E635D5F3D573F575936EE0CED0C0F11B22D7B6BFF7F
        FF7FFF7FFF7FFF7F1A229F0E7F025F029E22BE6FFF7FBE6F5E637F63BB4A173A
        7A427A3EB71D7025BD77FF7FFF7FFF7FFF7FFF7F3A26BF127F0A7F021D027B36
        BE775E5B1E57DC529946DB4EDA4EBA4E192E90259D73FF7FFF7FFF7FFF7FFF7F
        3A26DF1A9F0E7F063E02BB3A7D6BFC4A7A3E574299469A46BA4EBA4E392E9125
        9D73FF7FFF7FFF7FFF7FFF7F3A2ADF1E9F169F0A3E02DC3EBE777D6F8E5E075A
        C94D0D4AB94EDB523A2E91259D73FF7FFF7FFF7FFF7FFF7F3A2AFF26BF1A9F12
        5E02DD3EDE7B9E6F5B6B3B671A5FD75AFA56DB565B3291259D73FF7FFF7FFF7F
        FF7FFF7F3A2EFF2ADF22BF1A7F061E43FE7FBE779E6F7D6B5D633C5FFB5AFB56
        7B3291259D73FF7FFF7FFF7FFF7FFF7F5A2E1F33DF22BF1A9F0EFE327F5F7E5B
        7E5F5D5B5D5F3C5B1C5B1C5BBD36B2259D73FF7FFF7FFF7FFF7FFF7F5A2E3F3F
        3F435F4F7E5F5E5B3D531D4F1D4FFC4ADC429B3A5A2A1A1EDB11353EDE7BFF7F
        FF7FFF7FFF7FFF7FDA4E58425842584258425842373E373A373E373A373A5842
        B94EFA5A5B6BDE77FF7FFF7FFF7F}
    end
    inherited btnRelatorio: TBitBtn
      OnClick = btnRelatorioClick
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FDE7BDE7BBD77BD779C739C739C737C6F7C6F7C6F7C6F7C6F
        9C739C739C73BD77BD77BD77DE7BFF7FFF7FFF7F9C73B5560F42EF41EF419256
        925A91567156715671567156915691569156925A925AF6627B737C6B9D6BFF7F
        FF7F4A2D410C410C62104B5A6C626B626B626B5E4B5E4B5E4B5E4B5E4A5E4A5E
        4A5EB0663A5F151A982EFF7FFF7F29296210621083144C5E8C626C626C626C62
        6C5E6C5E6C5E6B5E4B5E4B5E4A624F4A351E5726FB42FF7FFF7F492962106210
        83144C5E8D668C626C626C626C626C626B626B626B5E6B624D52141E5722DB36
        BE6FFF7FFF7F492D6210621083146C5E8D668D628D628C626C626C628F5EB35A
        B35A905E332A371ABA329D67FF7FFF7FFF7F492D83148210A3186C5E8D668D66
        8C626C628E66D45EF75AF65AD65AF75AD752B9361057DC7BFF7FFF7FFF7F6A31
        A3188314A41C6D62AD668C66D16A9A7B5A6BF75E596BBC777873F466F862F562
        2673DB7FFF7FFF7FFF7F6A31A418A318C41C6D628D66136FDD7F146F18631663
        356F5673BC7B9977F4661963546BFF7FFF7FFF7FFF7F8A35C41CA418E4208D62
        CF6ABC7BF26E577339635A6FDD7B9B7757735773146B3967B946DE77FF7FFF7F
        FF7F8B35C520C41CE5248D62126F99771473DE7B39677C6FDE7F7877DD7B7777
        156B5A6B783ABE73FF7FFF7FFF7FAB35E520E52005258D62347379773573BC7B
        166B5B6BBD7757739B7B9A73386B5B67362ABE73FF7FFF7FFF7FAB390525E520
        06298D66F26E9B7BD06ADE7FF26E586F7B6B596B386B7B6F7B73B35A1622BE73
        FF7FFF7FFF7FAC39062506252629AE66CF6EBB7B126F3573DC7B9A777B73596F
        9C73596FD06A8F5ED94EFF7FFF7FFF7FFF7FCC3D26290625272DAE66CE6EF16E
        DD7B136FD06AF06EAE6A346FDD7BCF6AAD6A4C666B5DDE7BFF7FFF7FFF7FCC3D
        27292629472DAE66CF6ECE6AF16E9A779B7B9A77BB7B9A77F16EAD6ACF6A4C66
        83547B7BFF7FFF7FFF7FCD3D272D2729472DAE6ACF6ECF6ECE6ECE6AF06EF16E
        D06EAE6AAE6ACE6ACF6A4C6683547B7BFF7FFF7FFF7FED41472D272D4831AF6A
        EF6ECF6ECF6ECF6ECF6ECE6ECE6ECF6ECF6ECF6ACF6E4C6683507B7BFF7FFF7F
        FF7FED41472D472D6831CF6AEF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6E
        CF6E6D6A6B61BD7BFF7FFF7FFF7FED41482D472D6831CF6AEF6ECF6ECF6ECF6E
        CF6ECF6ECF6ECF6ECF6ECF6ECF6E126FFF7FFF7FFF7FFF7FFF7FED4548314831
        6835CF6AF072F06EF06EF06EF06EF06EF06ED06ECF6ECF6ECF6E1273FF7FFF7F
        FF7FFF7FFF7FF6622F4A304A504E357355775577557755775577557755775577
        557755773573997BFF7FFF7FFF7F}
    end
    object BitBtn1: TBitBtn
      Left = 701
      Top = 8
      Width = 113
      Height = 30
      Caption = 'Imprimir Ficha'
      TabOrder = 6
      OnClick = BitBtn1Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7F9C73F75EF75EF75EF75EF75EF75EF75EF75EF75E
        F75EF75EF75EF75EF75E9C73FF7FFF7FFF7FFF7FFF7FFF7F9C739C73FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C739C73FF7FFF7FFF7FFF7F
        FF7FFF7FDE7B5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        5A6BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863BD779C739C739C739C739C73
        9C739C739C739C739C73BD771863FF7FFF7FFF7FFF7FFF7FDE7B9C739C73734E
        3967F75EF75EF75EF75EF75EF75EF75EF75EF75EF75E3967524A9C739C73DE7B
        FF7F5A6BB5569452945231469452945294529452945294529452945294529452
        94529452314694529452B5565A6B1863734E3A67F75E6C314B296B2D6B2D6B2D
        6B2D6B2D6B2D6B2D6B2D6B2D6B2D6B2D8C31D75A3A67734E18635A6BB6569C73
        5B6B6B2D29252925292529252925292509250925082508210821082109253967
        9C73B6565A6B9C731863BD73BD773146EF3DCE3DCE39AD358D358C316B2D4A2D
        4A2929252925E72029257B6FBD7718639C73BD7739677B6F9D739452524A524A
        31461042EF41EF3DCE39AD39AD358C316B2D4A298C319C737B6F3967BD77DE7B
        7B6B5A6B9C73D65AD65AF75E1863186339673967186339671863F75EB556734E
        524A9C735A6B7B6BDE7BFF7F7B6F5A6BBD77D656F75E18631863F75EF75E1963
        1863F75ED75A1863185FF85EF75EBD775A6B7B6FFF7FFF7FBD7718637B6FD65A
        F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75ED65A5A6B1863BD77
        FF7FFF7FFF7FBD779C731863DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
        DE7BFF7F18639C73BD77FF7FFF7FFF7FFF7FFF7FFF7F7A6BBE77DE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B7B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7F7B6FBD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDF7B1863FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73BD77DE7BDE7BDE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7B5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6FBD77DE7B
        DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BBD779C73FF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F7B6FBD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
        9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6FBD77DE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BDE7B9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F
        BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B9C73FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FBD77F75EF75EF75EF75EF75EF75EF75EF75EF75EF75E
        F75EF75E9C73FF7FFF7FFF7FFF7F}
    end
  end
  inherited Panel2: TPanel
    Width = 962
    Height = 551
    Font.Charset = DEFAULT_CHARSET
    Font.Name = 'MS Sans Serif'
    ParentFont = False
    ExplicitWidth = 962
    ExplicitHeight = 551
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 44
      Height = 16
      Caption = 'C'#243'digo'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 152
      Top = 8
      Width = 79
      Height = 16
      Caption = 'Nome Pastor'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 8
      Top = 56
      Width = 59
      Height = 16
      Caption = 'Categoria'
    end
    object Label4: TLabel
      Left = 176
      Top = 56
      Width = 147
      Height = 16
      Caption = 'No. Registro Conven'#231#227'o'
      FocusControl = DBEdit3
    end
    object Label5: TLabel
      Left = 344
      Top = 56
      Width = 104
      Height = 16
      Caption = 'Data Nascimento'
      FocusControl = DBEdit4
    end
    object Label6: TLabel
      Left = 464
      Top = 56
      Width = 96
      Height = 16
      Caption = 'Tipo Sanguineo'
      FocusControl = DBEdit5
    end
    object Label7: TLabel
      Left = 592
      Top = 56
      Width = 44
      Height = 16
      Caption = 'No. RG'
      FocusControl = DBEdit6
    end
    object Label8: TLabel
      Left = 8
      Top = 104
      Width = 50
      Height = 16
      Caption = 'No. CPF'
    end
    object Label9: TLabel
      Left = 136
      Top = 104
      Width = 80
      Height = 16
      Caption = 'Data Filia'#231#227'o'
      FocusControl = DBEdit8
    end
    object Label10: TLabel
      Left = 240
      Top = 104
      Width = 60
      Height = 16
      Caption = 'Nome Pai'
      FocusControl = DBEdit9
    end
    object Label11: TLabel
      Left = 400
      Top = 152
      Width = 71
      Height = 16
      Caption = 'Estado Civil'
    end
    object Label12: TLabel
      Left = 8
      Top = 152
      Width = 67
      Height = 16
      Caption = 'Nome M'#227'e'
      FocusControl = DBEdit10
    end
    object Label13: TLabel
      Left = 8
      Top = 200
      Width = 50
      Height = 16
      Caption = 'Conjuge'
      FocusControl = DBEdit11
    end
    object Label14: TLabel
      Left = 400
      Top = 200
      Width = 104
      Height = 16
      Caption = 'Data Nascimento'
      FocusControl = DBEdit12
    end
    object Label15: TLabel
      Left = 8
      Top = 248
      Width = 89
      Height = 16
      Caption = 'Nacionalidade'
    end
    object Label16: TLabel
      Left = 168
      Top = 248
      Width = 78
      Height = 16
      Caption = 'Naturalidade'
    end
    object Label17: TLabel
      Left = 320
      Top = 248
      Width = 81
      Height = 16
      Caption = 'Escolaridade'
    end
    object Label18: TLabel
      Left = 472
      Top = 248
      Width = 31
      Height = 16
      Caption = 'Fone'
      FocusControl = DBEdit13
    end
    object Label19: TLabel
      Left = 576
      Top = 248
      Width = 127
      Height = 16
      Caption = 'Forma'#231#227'o Teol'#243'gica'
    end
    object Label20: TLabel
      Left = 8
      Top = 304
      Width = 81
      Height = 16
      Caption = 'Data Batismo'
      FocusControl = DBEdit14
    end
    object Label21: TLabel
      Left = 168
      Top = 304
      Width = 85
      Height = 16
      Caption = 'Local Batismo'
      FocusControl = DBEdit15
    end
    object Label22: TLabel
      Left = 544
      Top = 304
      Width = 128
      Height = 16
      Caption = 'Data Aut. Evangelista'
      FocusControl = DBEdit16
    end
    object Label23: TLabel
      Left = 8
      Top = 352
      Width = 156
      Height = 16
      Caption = 'Data Consag. Evangelista'
      FocusControl = DBEdit17
    end
    object Label24: TLabel
      Left = 320
      Top = 352
      Width = 118
      Height = 16
      Caption = 'Local Consagra'#231#227'o'
      FocusControl = DBEdit18
    end
    object Label25: TLabel
      Left = 8
      Top = 400
      Width = 44
      Height = 16
      Caption = 'Campo'
      FocusControl = DBEdit19
    end
    object Label26: TLabel
      Left = 344
      Top = 400
      Width = 69
      Height = 16
      Caption = 'Supervis'#227'o'
      FocusControl = DBEdit20
    end
    object Label27: TLabel
      Left = 176
      Top = 352
      Width = 121
      Height = 16
      Caption = 'Ordena'#231#227'o a Pastor'
      FocusControl = DBEdit21
    end
    object Label28: TLabel
      Left = 528
      Top = 200
      Width = 123
      Height = 16
      Caption = 'No. Cert. Casamento'
      FocusControl = DBEdit22
    end
    object Label29: TLabel
      Left = 8
      Top = 448
      Width = 59
      Height = 16
      Caption = 'Endere'#231'o'
      FocusControl = DBEdit23
    end
    object Label30: TLabel
      Left = 8
      Top = 496
      Width = 84
      Height = 16
      Caption = 'Complemento'
      FocusControl = DBEdit24
    end
    object Label31: TLabel
      Left = 504
      Top = 448
      Width = 27
      Height = 16
      Caption = 'CEP'
      FocusControl = DBEdit25
    end
    object lbl: TLabel
      Left = 558
      Top = 150
      Width = 142
      Height = 16
      Caption = 'Banco de Recebimento'
    end
    object Label32: TLabel
      Left = 591
      Top = 102
      Width = 106
      Height = 16
      Caption = 'Data Falecimento'
      FocusControl = DBEdit26
    end
    object DBEdit1: TDBEdit
      Left = 12
      Top = 26
      Width = 134
      Height = 24
      DataField = 'cd_cadastro'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 152
      Top = 24
      Width = 377
      Height = 24
      CharCase = ecUpperCase
      DataField = 'nm_pastor'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 8
      Top = 72
      Width = 153
      Height = 24
      DataField = 'cd_categoria'
      DataSource = DataSource1
      KeyField = 'cd_categoria'
      ListField = 'ds_categoria'
      ListSource = dsCategoria
      TabOrder = 2
    end
    object DBLookupComboBox2: TDBLookupComboBox
      Left = 400
      Top = 168
      Width = 145
      Height = 24
      DataField = 'cd_estCivil'
      DataSource = DataSource1
      KeyField = 'cd_estcivil'
      ListField = 'ds_estcivil'
      ListSource = dsEstadoCivil
      TabOrder = 11
    end
    object DBEdit3: TDBEdit
      Left = 176
      Top = 72
      Width = 113
      Height = 24
      DataField = 'no_regConv'
      DataSource = DataSource1
      TabOrder = 3
    end
    object DBEdit4: TDBEdit
      Left = 344
      Top = 72
      Width = 97
      Height = 24
      DataField = 'dt_nascPastor'
      DataSource = DataSource1
      TabOrder = 4
    end
    object DBEdit5: TDBEdit
      Left = 464
      Top = 72
      Width = 121
      Height = 24
      CharCase = ecUpperCase
      DataField = 'tp_sanguineo'
      DataSource = DataSource1
      TabOrder = 5
    end
    object DBEdit6: TDBEdit
      Left = 592
      Top = 72
      Width = 137
      Height = 24
      DataField = 'no_regGeral'
      DataSource = DataSource1
      TabOrder = 6
    end
    object DBEdit7: TDBEdit
      Left = 8
      Top = 120
      Width = 113
      Height = 24
      DataField = 'no_cpf'
      DataSource = DataSource1
      TabOrder = 7
    end
    object DBEdit8: TDBEdit
      Left = 136
      Top = 120
      Width = 97
      Height = 24
      DataField = 'dt_filiacao'
      DataSource = DataSource1
      TabOrder = 8
    end
    object DBEdit9: TDBEdit
      Left = 240
      Top = 120
      Width = 345
      Height = 24
      CharCase = ecUpperCase
      DataField = 'nm_pai'
      DataSource = DataSource1
      TabOrder = 9
    end
    object DBEdit10: TDBEdit
      Left = 8
      Top = 168
      Width = 385
      Height = 24
      CharCase = ecUpperCase
      DataField = 'nm_mae'
      DataSource = DataSource1
      TabOrder = 10
    end
    object DBEdit11: TDBEdit
      Left = 8
      Top = 216
      Width = 385
      Height = 24
      CharCase = ecUpperCase
      DataField = 'nm_conjuge'
      DataSource = DataSource1
      TabOrder = 12
    end
    object DBEdit12: TDBEdit
      Left = 400
      Top = 216
      Width = 105
      Height = 24
      DataField = 'dt_nascConjuge'
      DataSource = DataSource1
      TabOrder = 13
    end
    object DBLookupComboBox3: TDBLookupComboBox
      Left = 8
      Top = 264
      Width = 145
      Height = 24
      DataField = 'cd_nacPastor'
      DataSource = DataSource1
      KeyField = 'cd_nacpastor'
      ListField = 'ds_nacpastor'
      ListSource = dsNacionalidade
      TabOrder = 15
    end
    object DBLookupComboBox4: TDBLookupComboBox
      Left = 168
      Top = 264
      Width = 145
      Height = 24
      DataField = 'cd_natPastor'
      DataSource = DataSource1
      KeyField = 'cd_natpastor'
      ListField = 'ds_natpastor'
      ListSource = dsNaturalidade
      TabOrder = 16
    end
    object DBLookupComboBox5: TDBLookupComboBox
      Left = 320
      Top = 264
      Width = 145
      Height = 24
      DataField = 'cd_escPastor'
      DataSource = DataSource1
      KeyField = 'cd_escpastor'
      ListField = 'ds_escpastor'
      ListSource = dsEscolaridade
      TabOrder = 17
    end
    object DBEdit13: TDBEdit
      Left = 472
      Top = 264
      Width = 97
      Height = 24
      DataField = 'no_fone'
      DataSource = DataSource1
      TabOrder = 18
    end
    object DBLookupComboBox6: TDBLookupComboBox
      Left = 576
      Top = 264
      Width = 145
      Height = 24
      DataField = 'cd_formTeologica'
      DataSource = DataSource1
      KeyField = 'cd_formteologica'
      ListField = 'ds_formteologica'
      ListSource = dsFormTeologica
      TabOrder = 19
    end
    object DBEdit14: TDBEdit
      Left = 8
      Top = 320
      Width = 137
      Height = 24
      DataField = 'dt_batismo'
      DataSource = DataSource1
      TabOrder = 20
    end
    object DBEdit15: TDBEdit
      Left = 168
      Top = 320
      Width = 356
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_localBatismo'
      DataSource = DataSource1
      TabOrder = 21
    end
    object DBEdit16: TDBEdit
      Left = 544
      Top = 320
      Width = 145
      Height = 24
      DataField = 'dt_autEvangelista'
      DataSource = DataSource1
      TabOrder = 22
    end
    object DBEdit17: TDBEdit
      Left = 8
      Top = 368
      Width = 153
      Height = 24
      DataField = 'dt_consagEvangelista'
      DataSource = DataSource1
      TabOrder = 23
    end
    object DBEdit18: TDBEdit
      Left = 320
      Top = 368
      Width = 332
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_localConsagracao'
      DataSource = DataSource1
      TabOrder = 25
    end
    object DBEdit19: TDBEdit
      Left = 8
      Top = 416
      Width = 321
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_campo'
      DataSource = DataSource1
      TabOrder = 26
    end
    object DBEdit20: TDBEdit
      Left = 344
      Top = 416
      Width = 329
      Height = 24
      DataField = 'ds_supervisao'
      DataSource = DataSource1
      TabOrder = 27
    end
    object DBEdit21: TDBEdit
      Left = 176
      Top = 368
      Width = 129
      Height = 24
      DataField = 'dt_ordenacPastor'
      DataSource = DataSource1
      TabOrder = 24
    end
    object DBEdit22: TDBEdit
      Left = 528
      Top = 216
      Width = 121
      Height = 24
      DataField = 'no_certCasamento'
      DataSource = DataSource1
      TabOrder = 14
    end
    object DBEdit23: TDBEdit
      Left = 8
      Top = 464
      Width = 468
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_endereco'
      DataSource = DataSource1
      TabOrder = 28
    end
    object DBEdit24: TDBEdit
      Left = 8
      Top = 512
      Width = 644
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_compEndPastor'
      DataSource = DataSource1
      TabOrder = 30
    end
    object DBEdit25: TDBEdit
      Left = 504
      Top = 464
      Width = 132
      Height = 24
      DataField = 'no_cepPastor'
      DataSource = DataSource1
      TabOrder = 29
    end
    object DBLookupComboBox7: TDBLookupComboBox
      Left = 558
      Top = 168
      Width = 145
      Height = 24
      DataField = 'cd_banco'
      DataSource = DataSource1
      KeyField = 'cd_banco'
      ListField = 'nm_banco'
      ListSource = dsBancos
      TabOrder = 31
    end
    object DBEdit26: TDBEdit
      Left = 591
      Top = 118
      Width = 138
      Height = 24
      DataField = 'dt_falecimento'
      DataSource = DataSource1
      TabOrder = 32
    end
  end
  inherited CDSPrincipal: TClientDataSet
    Left = 184
    Top = 9
    object CDSPrincipalcd_formTeologica: TIntegerField
      FieldName = 'cd_formTeologica'
    end
    object CDSPrincipalcd_natPastor: TIntegerField
      FieldName = 'cd_natPastor'
    end
    object CDSPrincipalcd_escPastor: TIntegerField
      FieldName = 'cd_escPastor'
    end
    object CDSPrincipalcd_nacPastor: TIntegerField
      FieldName = 'cd_nacPastor'
    end
    object CDSPrincipalcd_estCivil: TIntegerField
      FieldName = 'cd_estCivil'
    end
    object CDSPrincipalcd_categoria: TIntegerField
      FieldName = 'cd_categoria'
    end
    object CDSPrincipalnm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object CDSPrincipalno_regConv: TIntegerField
      FieldName = 'no_regConv'
    end
    object CDSPrincipaldt_nascPastor: TDateTimeField
      FieldName = 'dt_nascPastor'
      EditMask = '!99/99/0000;1;_'
    end
    object CDSPrincipaltp_sanguineo: TStringField
      FieldName = 'tp_sanguineo'
      FixedChar = True
    end
    object CDSPrincipalno_regGeral: TStringField
      FieldName = 'no_regGeral'
      FixedChar = True
    end
    object CDSPrincipalno_cpf: TStringField
      FieldName = 'no_cpf'
      EditMask = '000\.000\.000\-00;1;_'
      FixedChar = True
      Size = 14
    end
    object CDSPrincipalds_endereco: TStringField
      FieldName = 'ds_endereco'
      Size = 60
    end
    object CDSPrincipalds_compEndPastor: TStringField
      FieldName = 'ds_compEndPastor'
      Size = 40
    end
    object CDSPrincipalno_cepPastor: TStringField
      FieldName = 'no_cepPastor'
      FixedChar = True
      Size = 8
    end
    object CDSPrincipaldt_filiacao: TDateTimeField
      FieldName = 'dt_filiacao'
      EditMask = '!99/99/0000;1;_'
    end
    object CDSPrincipalnm_pai: TStringField
      FieldName = 'nm_pai'
      Size = 60
    end
    object CDSPrincipalnm_mae: TStringField
      FieldName = 'nm_mae'
      Size = 60
    end
    object CDSPrincipalnm_conjuge: TStringField
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object CDSPrincipaldt_nascConjuge: TDateTimeField
      FieldName = 'dt_nascConjuge'
      EditMask = '!99/99/0000;1;_'
    end
    object CDSPrincipalno_fone: TStringField
      FieldName = 'no_fone'
      Size = 15
    end
    object CDSPrincipaldt_batismo: TDateTimeField
      FieldName = 'dt_batismo'
      EditMask = '!99/99/0000;1;_'
    end
    object CDSPrincipalds_localBatismo: TStringField
      FieldName = 'ds_localBatismo'
      Size = 40
    end
    object CDSPrincipaldt_autEvangelista: TDateTimeField
      FieldName = 'dt_autEvangelista'
      EditMask = '!99/99/0000;1;_'
    end
    object CDSPrincipaldt_consagEvangelista: TDateTimeField
      FieldName = 'dt_consagEvangelista'
      EditMask = '!99/99/0000;1;_'
    end
    object CDSPrincipaldt_ordenacPastor: TDateTimeField
      FieldName = 'dt_ordenacPastor'
      EditMask = '!99/99/0000;1;_'
    end
    object CDSPrincipalds_localConsagracao: TStringField
      FieldName = 'ds_localConsagracao'
      Size = 40
    end
    object CDSPrincipalds_campo: TStringField
      FieldName = 'ds_campo'
      Size = 40
    end
    object CDSPrincipalds_supervisao: TStringField
      FieldName = 'ds_supervisao'
      Size = 40
    end
    object CDSPrincipalno_certCasamento: TIntegerField
      FieldName = 'no_certCasamento'
    end
    object CDSPrincipalds_orgaoemissorrg: TStringField
      FieldName = 'ds_orgaoemissorrg'
      Size = 30
    end
    object CDSPrincipaldt_emissao: TDateTimeField
      FieldName = 'dt_emissao'
    end
    object CDSPrincipalds_bairro: TStringField
      FieldName = 'ds_bairro'
      Size = 40
    end
    object CDSPrincipalds_uf: TStringField
      FieldName = 'ds_uf'
      Size = 50
    end
    object CDSPrincipalds_cidade: TStringField
      FieldName = 'ds_cidade'
      Size = 50
    end
    object CDSPrincipaldt_falecimento: TWideStringField
      FieldName = 'dt_falecimento'
      Size = 10
    end
    object CDSPrincipalcd_banco: TIntegerField
      FieldName = 'cd_banco'
    end
    object CDSPrincipalcd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  inherited ActionList1: TActionList
    Left = 840
    Top = 9
  end
  inherited DSPPrincipal: TDataSetProvider
    Left = 224
    Top = 73
  end
  inherited QPrincipal: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT cd_cadastro '
      '      ,cd_formTeologica'
      '      ,cd_natPastor'
      '      ,cd_escPastor'
      '      ,cd_nacPastor'
      '      ,cd_estCivil'
      '      ,cd_categoria'
      '      ,nm_pastor'
      '      ,no_regConv'
      '      ,dt_nascPastor'
      '      ,tp_sanguineo'
      '      ,no_regGeral'
      '      ,no_cpf'
      '      ,ds_endereco'
      '      ,ds_compEndPastor'
      '      ,no_cepPastor'
      '      ,dt_filiacao'
      '      ,nm_pai'
      '      ,nm_mae'
      '      ,nm_conjuge'
      '      ,dt_nascConjuge'
      '      ,no_fone'
      '      ,dt_batismo'
      '      ,ds_localBatismo'
      '      ,dt_autEvangelista'
      '      ,dt_consagEvangelista'
      '      ,dt_ordenacPastor'
      '      ,ds_localConsagracao'
      '      ,ds_campo'
      '      ,ds_supervisao'
      '      ,no_certCasamento'
      '     ,dt_falecimento'
      '     ,cd_banco'
      ', ds_orgaoemissorrg, dt_emissao, ds_bairro, ds_uf, ds_cidade '
      '  FROM tbContribuinte'
      'order by nm_pastor')
    Left = 88
    Top = 9
    object QPrincipalnm_pastor: TStringField
      FieldName = 'nm_pastor'
      Required = True
      Size = 60
    end
    object QPrincipalcd_categoria: TIntegerField
      FieldName = 'cd_categoria'
      Required = True
    end
    object QPrincipalno_regConv: TIntegerField
      FieldName = 'no_regConv'
      Required = True
    end
    object QPrincipaldt_nascPastor: TDateTimeField
      FieldName = 'dt_nascPastor'
      Required = True
      EditMask = '!99/99/0000;1;_'
    end
    object QPrincipalno_cpf: TStringField
      FieldName = 'no_cpf'
      Required = True
      EditMask = '000\.000\.000\-00;1;_'
      FixedChar = True
      Size = 14
    end
    object QPrincipalnm_mae: TStringField
      FieldName = 'nm_mae'
      Required = True
      Size = 60
    end
    object QPrincipalcd_estCivil: TIntegerField
      FieldName = 'cd_estCivil'
      Required = True
    end
    object QPrincipalcd_banco: TIntegerField
      FieldName = 'cd_banco'
      Required = True
    end
    object QPrincipalcd_nacPastor: TIntegerField
      FieldName = 'cd_nacPastor'
      Required = True
    end
    object QPrincipalcd_natPastor: TIntegerField
      FieldName = 'cd_natPastor'
      Required = True
    end
    object QPrincipalcd_escPastor: TIntegerField
      FieldName = 'cd_escPastor'
      Required = True
    end
    object QPrincipalcd_formTeologica: TIntegerField
      FieldName = 'cd_formTeologica'
      Required = True
    end
    object QPrincipaltp_sanguineo: TStringField
      FieldName = 'tp_sanguineo'
      FixedChar = True
    end
    object QPrincipalno_regGeral: TStringField
      FieldName = 'no_regGeral'
      FixedChar = True
    end
    object QPrincipalds_endereco: TStringField
      FieldName = 'ds_endereco'
      Required = True
      Size = 60
    end
    object QPrincipalds_compEndPastor: TStringField
      FieldName = 'ds_compEndPastor'
      Size = 40
    end
    object QPrincipalno_cepPastor: TStringField
      FieldName = 'no_cepPastor'
      EditMask = '00.000-000;1;_'
      FixedChar = True
      Size = 8
    end
    object QPrincipaldt_filiacao: TDateTimeField
      FieldName = 'dt_filiacao'
      EditMask = '!99/99/0000;1;_'
    end
    object QPrincipalnm_pai: TStringField
      FieldName = 'nm_pai'
      Size = 60
    end
    object QPrincipalnm_conjuge: TStringField
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object QPrincipaldt_nascConjuge: TDateTimeField
      FieldName = 'dt_nascConjuge'
      EditMask = '!99/99/0000;1;_'
    end
    object QPrincipalno_fone: TStringField
      FieldName = 'no_fone'
      Size = 15
    end
    object QPrincipaldt_batismo: TDateTimeField
      FieldName = 'dt_batismo'
      EditMask = '!99/99/0000;1;_'
    end
    object QPrincipalds_localBatismo: TStringField
      FieldName = 'ds_localBatismo'
      Size = 40
    end
    object QPrincipaldt_autEvangelista: TDateTimeField
      FieldName = 'dt_autEvangelista'
      EditMask = '!99/99/0000;1;_'
    end
    object QPrincipaldt_consagEvangelista: TDateTimeField
      FieldName = 'dt_consagEvangelista'
      EditMask = '!99/99/0000;1;_'
    end
    object QPrincipaldt_ordenacPastor: TDateTimeField
      FieldName = 'dt_ordenacPastor'
      EditMask = '!99/99/0000;1;_'
    end
    object QPrincipalds_localConsagracao: TStringField
      FieldName = 'ds_localConsagracao'
      Size = 40
    end
    object QPrincipalds_campo: TStringField
      FieldName = 'ds_campo'
      Size = 40
    end
    object QPrincipalds_supervisao: TStringField
      FieldName = 'ds_supervisao'
      Size = 40
    end
    object QPrincipalno_certCasamento: TIntegerField
      FieldName = 'no_certCasamento'
    end
    object QPrincipalds_orgaoemissorrg: TStringField
      FieldName = 'ds_orgaoemissorrg'
      Size = 30
    end
    object QPrincipaldt_emissao: TDateTimeField
      FieldName = 'dt_emissao'
      EditMask = '!99/99/0000;1;_'
    end
    object QPrincipalds_bairro: TStringField
      FieldName = 'ds_bairro'
      Size = 40
    end
    object QPrincipalds_uf: TStringField
      FieldName = 'ds_uf'
      Size = 50
    end
    object QPrincipalds_cidade: TStringField
      FieldName = 'ds_cidade'
      Size = 50
    end
    object QPrincipaldt_falecimento: TWideStringField
      FieldName = 'dt_falecimento'
      EditMask = '!99/99/0000;1;_'
      Size = 10
    end
    object QPrincipalcd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 288
    Top = 48
  end
  object dsCategoria: TDataSource
    AutoEdit = False
    DataSet = QCategoria
    Left = 88
    Top = 73
  end
  object dsEstadoCivil: TDataSource
    DataSet = QEstadoCivil
    Left = 456
    Top = 161
  end
  object QCategoria: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT cd_categoria,ds_categoria'
      'from tbCategoria'
      'order by ds_categoria')
    Left = 32
    Top = 65
  end
  object QEstadoCivil: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_estcivil,ds_estcivil'
      'from tbestcivil'
      'order by ds_estcivil')
    Left = 400
    Top = 153
  end
  object QNacionalidade: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_nacpastor,ds_nacpastor'
      'from tbNacionalidade'
      'order by ds_nacpastor')
    Left = 112
    Top = 313
  end
  object dsNacionalidade: TDataSource
    DataSet = QNacionalidade
    Left = 40
    Top = 305
  end
  object dsNaturalidade: TDataSource
    DataSet = QNaturalidade
    Left = 256
    Top = 313
  end
  object QNaturalidade: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_natpastor,ds_natpastor'
      'from tbNaturalidade'
      'order by ds_natpastor')
    Left = 184
    Top = 305
  end
  object QEscolaridade: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_escpastor,ds_escpastor'
      'from tbescolaridade'
      'order by ds_escpastor')
    Left = 352
    Top = 313
  end
  object dsEscolaridade: TDataSource
    DataSet = QEscolaridade
    Left = 424
    Top = 313
  end
  object dsFormTeologica: TDataSource
    DataSet = QFormTeologica
    Left = 608
    Top = 305
  end
  object QFormTeologica: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_formteologica,ds_formteologica'
      'from tbformteologica'
      'order by ds_formteologica')
    Left = 664
    Top = 305
  end
  object qBancos: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_banco,nm_banco'
      'from tbBancos'
      'order by nm_banco')
    Left = 600
    Top = 56
    object qBancoscd_banco: TAutoIncField
      FieldName = 'cd_banco'
      ReadOnly = True
    end
    object qBancosnm_banco: TStringField
      FieldName = 'nm_banco'
      Size = 40
    end
  end
  object dsBancos: TDataSource
    DataSet = qBancos
    Left = 656
    Top = 56
  end
end
