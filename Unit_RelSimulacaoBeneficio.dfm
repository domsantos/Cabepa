inherited frm_RelSimulacaoBeneficio: Tfrm_RelSimulacaoBeneficio
  Caption = 'frm_RelSimulacaoBeneficio'
  ExplicitWidth = 870
  ExplicitHeight = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 162
        Caption = 'SIMULA'#199#195'O DE DOA'#199#195'O'
        ExplicitWidth = 162
      end
      object RLLabel15: TRLLabel
        Left = 97
        Top = 54
        Width = 61
        Height = 16
        Caption = 'PASTOR:'
      end
      object lblPastor: TRLLabel
        Left = 164
        Top = 54
        Width = 55
        Height = 16
      end
    end
    inherited RLBand2: TRLBand
      Height = 192
      ExplicitHeight = 192
      object RLDraw1: TRLDraw [0]
        Left = 3
        Top = 6
        Width = 712
        Height = 171
      end
      inherited RLLabel3: TRLLabel
        Left = 3
        Top = 215
        Width = 87
        Caption = 'REF'#202'RENCIA'
        ExplicitLeft = 3
        ExplicitTop = 215
        ExplicitWidth = 87
      end
      inherited RLLabel4: TRLLabel
        Left = 146
        Top = 214
        Width = 48
        Caption = 'VALOR'
        ExplicitLeft = 146
        ExplicitTop = 214
        ExplicitWidth = 48
      end
      object RLLabel6: TRLLabel
        Left = 11
        Top = 38
        Width = 175
        Height = 16
        Caption = 'Quantidade de Contribui'#231#245'es:'
      end
      object lblQtdContribuicoes: TRLLabel
        Left = 272
        Top = 38
        Width = 140
        Height = 16
        Alignment = taRightJustify
      end
      object RLLabel7: TRLLabel
        Left = 11
        Top = 60
        Width = 251
        Height = 16
        Caption = 'Valor M'#233'dio das 36 '#218'ltimas Contribui'#231#245'es:'
      end
      object lblValorMedioContr: TRLLabel
        Left = 272
        Top = 60
        Width = 140
        Height = 16
        Alignment = taRightJustify
      end
      object RLLabel8: TRLLabel
        Left = 11
        Top = 82
        Width = 171
        Height = 16
        Caption = 'Valor Total de Contribui'#231#245'es:'
      end
      object lblValorTotal: TRLLabel
        Left = 272
        Top = 82
        Width = 140
        Height = 16
        Alignment = taRightJustify
      end
      object RLLabel9: TRLLabel
        Left = 11
        Top = 104
        Width = 132
        Height = 16
        Caption = 'Anos de Contribui'#231#227'o:'
      end
      object lblAnosContr: TRLLabel
        Left = 272
        Top = 104
        Width = 140
        Height = 16
        Alignment = taRightJustify
      end
      object RLLabel10: TRLLabel
        Left = 11
        Top = 126
        Width = 133
        Height = 16
        Caption = 'Valor Base de Calculo'
      end
      object lblValorBeneficio: TRLLabel
        Left = 272
        Top = 126
        Width = 140
        Height = 16
        Alignment = taRightJustify
      end
      object RLLabel11: TRLLabel
        Left = 272
        Top = 16
        Width = 151
        Height = 16
        Caption = 'Valor Prov'#225'vel de D'#237'zimo:'
        Visible = False
      end
      object lblValorDizimo: TRLLabel
        Left = 533
        Top = 16
        Width = 140
        Height = 16
        Alignment = taRightJustify
        Visible = False
      end
      object RLLabel12: TRLLabel
        Left = 11
        Top = 148
        Width = 95
        Height = 16
        Caption = 'Valor Benef'#237'cio:'
      end
      object lblValorLiquido: TRLLabel
        Left = 272
        Top = 148
        Width = 140
        Height = 16
        Alignment = taRightJustify
      end
      object RLLabel13: TRLLabel
        Left = 11
        Top = 12
        Width = 127
        Height = 16
        Caption = 'Resumo de Valores'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    inherited RLBand4: TRLBand
      Top = 331
      Height = 46
      ExplicitTop = 331
      ExplicitHeight = 46
      inherited RLSystemInfo1: TRLSystemInfo
        Left = 92
        Top = 27
        Width = 39
        ExplicitLeft = 92
        ExplicitTop = 27
        ExplicitWidth = 39
      end
      inherited RLLabel5: TRLLabel
        Left = 4
        Top = 27
        ExplicitLeft = 4
        ExplicitTop = 27
      end
      inherited RLSystemInfo2: TRLSystemInfo
        Left = 628
        Top = 27
        ExplicitLeft = 628
        ExplicitTop = 27
      end
    end
    inherited RLBand3: TRLBand
      Top = 313
      Height = 18
      ExplicitTop = 313
      ExplicitHeight = 18
      inherited RLDBText1: TRLDBText
        Left = -113
        Top = 0
        Width = 138
        Alignment = taRightJustify
        DataField = 'dt_mesRefContribuicao'
        ExplicitLeft = -113
        ExplicitTop = 0
        ExplicitWidth = 138
      end
      inherited RLDBText2: TRLDBText
        Left = 146
        Top = -1
        Width = 160
        Alignment = taRightJustify
        DataField = 'vl_ContribuicaoVencimento'
        ExplicitLeft = 146
        ExplicitTop = -1
        ExplicitWidth = 160
      end
      object RLLabel14: TRLLabel
        Left = 25
        Top = 0
        Width = 8
        Height = 16
        Caption = '/'
      end
      object RLDBText3: TRLDBText
        Left = 36
        Top = 0
        Width = 134
        Height = 16
        DataField = 'dt_anoRefContribuicao'
        DataSource = dsRelatorio
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = qListaContribuicoes
    Left = 504
    Top = 272
  end
  object qListaContribuicoes: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select top 36 vl_ContribuicaoVencimento,dt_anoRefContribuicao,dt' +
        '_mesRefContribuicao'
      'from tbContribuicoes '
      'where cd_cadastro = :codigo'
      '      and cd_tipoContribuicao = 1'
      '      and cd_sitQuitacao = 1'
      'order by dt_anoRefContribuicao desc, dt_mesRefContribuicao desc')
    Left = 504
    Top = 216
    object qListaContribuicoesvl_ContribuicaoVencimento: TBCDField
      FieldName = 'vl_ContribuicaoVencimento'
      currency = True
      Precision = 10
      Size = 2
    end
    object qListaContribuicoesdt_anoRefContribuicao: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
    end
    object qListaContribuicoesdt_mesRefContribuicao: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
    end
  end
end
