inherited frm_RelValorBeneficio: Tfrm_RelValorBeneficio
  Caption = 'frm_RelValorBeneficio'
  ExplicitWidth = 862
  ExplicitHeight = 629
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 180
        Caption = 'Relat'#243'rio de Valor de Doa'#231#245'es'
        ExplicitWidth = 180
      end
    end
    inherited RLBand2: TRLBand
      inherited RLLabel3: TRLLabel
        Left = 1
        ExplicitLeft = 1
      end
      inherited RLLabel4: TRLLabel
        Left = 64
        Width = 100
        Caption = 'NOME PASTOR'
        ExplicitLeft = 64
        ExplicitWidth = 100
      end
      object RLLabel6: TRLLabel
        Left = 640
        Top = 32
        Width = 48
        Height = 16
        Caption = 'VALOR'
      end
      object RLLabel8: TRLLabel
        Left = 296
        Top = 32
        Width = 109
        Height = 16
        Caption = 'NOME CONJUGE'
      end
      object RLLabel9: TRLLabel
        Left = 528
        Top = 32
        Width = 95
        Height = 16
        Caption = 'FALECIMENTO'
      end
    end
    inherited RLBand4: TRLBand
      Top = 208
      Height = 35
      ExplicitTop = 208
      ExplicitHeight = 35
      inherited RLSystemInfo2: TRLSystemInfo
        Left = 628
        Top = 13
        ExplicitLeft = 628
        ExplicitTop = 13
      end
    end
    inherited RLBand3: TRLBand
      Height = 16
      ExplicitHeight = 16
      inherited RLDBText1: TRLDBText
        Left = 0
        Top = 1
        Width = 64
        Height = 14
        DataField = 'cd_cadastro'
        Font.Height = -11
        ParentFont = False
        ExplicitLeft = 0
        ExplicitTop = 1
        ExplicitWidth = 64
        ExplicitHeight = 14
      end
      inherited RLDBText2: TRLDBText
        Left = 66
        Top = 1
        Width = 54
        Height = 14
        DataField = 'nm_pastor'
        Font.Height = -11
        ParentFont = False
        ExplicitLeft = 66
        ExplicitTop = 1
        ExplicitWidth = 54
        ExplicitHeight = 14
      end
      object RLDBText3: TRLDBText
        Left = 654
        Top = 1
        Width = 61
        Height = 14
        Alignment = taRightJustify
        DataField = 'vl_beneficio'
        DataSource = dsRelatorio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLDBText4: TRLDBText
        Left = 296
        Top = 2
        Width = 61
        Height = 14
        DataField = 'nm_conjuge'
        DataSource = dsRelatorio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLDBText5: TRLDBText
        Left = 528
        Top = 3
        Width = 73
        Height = 14
        DataField = 'dt_falecimento'
        DataSource = dsRelatorio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 185
      Width = 718
      Height = 23
      BandType = btSummary
      object RLDBResult1: TRLDBResult
        Left = 600
        Top = 6
        Width = 109
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_beneficio'
        DataSource = dsRelatorio
        DisplayMask = '#,##0.00'
        Info = riSum
      end
      object RLLabel7: TRLLabel
        Left = 427
        Top = 4
        Width = 96
        Height = 16
        Caption = 'VALOR TOTAL:'
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = qValorBeneficio
  end
  object qValorBeneficio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '
      #9'c.cd_cadastro'
      #9',c.nm_pastor'
      #9',c.nm_conjuge'
      #9',CONVERT(char,c.dt_falecimento,101) dt_falecimento'
      
        #9',(select max(v.vl_Beneficio) from tbValBeneficio v where v.no_r' +
        'eqBeneficio = '
      
        #9'(select max(b.no_reqBeneficio) from tbRequerimentoBeneficio b w' +
        'here b.cd_cadastro = c.cd_cadastro))  vl_beneficio'
      'from tbContribuinte c'
      'where c.cd_sitJubilamento in (1,2)'
      'order by c.nm_pastor ')
    Left = 472
    Top = 328
    object qValorBeneficionm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qValorBeneficionm_conjuge: TStringField
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object qValorBeneficiodt_falecimento: TStringField
      FieldName = 'dt_falecimento'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object qValorBeneficiovl_beneficio: TBCDField
      FieldName = 'vl_beneficio'
      ReadOnly = True
      currency = True
      Precision = 10
      Size = 2
    end
    object qValorBeneficiocd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
end
