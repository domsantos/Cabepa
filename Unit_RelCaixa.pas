unit Unit_RelCaixa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, RLReport, jpeg, RLFilters, RLXLSFilter;

type
  Tfrm_RelCaixa = class(Tfrm_RelatorioPadrao)
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel10: TRLLabel;
    RLLabel11: TRLLabel;
    RLDBText3: TRLDBText;
    RLLabel12: TRLLabel;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBResult1: TRLDBResult;
    RLLabel13: TRLLabel;
    RLDBText6: TRLDBText;
    RLXLSFilter1: TRLXLSFilter;
    RLLabel14: TRLLabel;
    lblPeriodo: TRLLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelCaixa: Tfrm_RelCaixa;

implementation

uses Unit_Caixa;

{$R *.dfm}

end.
