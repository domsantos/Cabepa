unit Unit_TipoBeneficio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, ADODB, Provider, ActnList, DB, DBClient, StdCtrls,
  Buttons, ExtCtrls, Grids, DBGrids, Mask, DBCtrls;

type
  Tfrm_TipoBeneficio = class(Tfrm_PadraoCadastro)
    QPrincipalcd_tipoBeneficio: TSmallintField;
    QPrincipalds_tipoBeneficio: TStringField;
    CDSPrincipalcd_tipoBeneficio: TSmallintField;
    CDSPrincipalds_tipoBeneficio: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure btnPesquisarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_TipoBeneficio: Tfrm_TipoBeneficio;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_TipoBeneficio.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Open;
end;

end.
