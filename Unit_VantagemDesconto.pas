unit Unit_VantagemDesconto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, ADODB, Provider, ActnList, DB, DBClient, StdCtrls,
  Buttons, ExtCtrls, DBCtrls, Grids, DBGrids, Mask, rxToolEdit, RXDBCtrl;

type
  Tfrm_VantagemDesconto = class(Tfrm_PadraoCadastro)
    QPrincipalcd_tipRubrica: TSmallintField;
    QPrincipalno_Rubrica: TSmallintField;
    QPrincipalnm_Rubrica: TStringField;
    QPrincipaldt_criaRubrica: TDateTimeField;
    QPrincipalcd_sitRubrica: TSmallintField;
    QPrincipalcd_tipCalcRubrica: TSmallintField;
    CDSPrincipalcd_tipRubrica: TSmallintField;
    CDSPrincipalno_Rubrica: TSmallintField;
    CDSPrincipalnm_Rubrica: TStringField;
    CDSPrincipaldt_criaRubrica: TDateTimeField;
    CDSPrincipalcd_sitRubrica: TSmallintField;
    CDSPrincipalcd_tipCalcRubrica: TSmallintField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    DBDateEdit1: TDBDateEdit;
    Label8: TLabel;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_VantagemDesconto: Tfrm_VantagemDesconto;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_VantagemDesconto.btnCancelarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Cancel;
  CDSPrincipal.Close;
end;

procedure Tfrm_VantagemDesconto.btnNovoClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

procedure Tfrm_VantagemDesconto.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Open;
end;

end.
