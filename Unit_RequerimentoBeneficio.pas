unit Unit_RequerimentoBeneficio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, RxLookup, DBCtrls, Buttons, Mask;

type
  Tfrm_RequerimentoBeneficio = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    edtContribuinte: TEdit;
    Button1: TButton;
    Label4: TLabel;
    Label3: TLabel;
    edtNomeRequerente: TEdit;
    lblNomeRequerente: TLabel;
    cbTipoBeneficio: TDBLookupComboBox;
    dsTipoBeneficio: TDataSource;
    BitBtn1: TBitBtn;
    Label5: TLabel;
    Label6: TLabel;
    edtCpf: TMaskEdit;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure edtContribuinteKeyPress(Sender: TObject; var Key: Char);
    procedure edtContribuinteExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    id : integer;
  end;

var
  frm_RequerimentoBeneficio: Tfrm_RequerimentoBeneficio;

implementation

uses Unit_PesquisaContribuinte, Unit_DM;

{$R *.dfm}

procedure Tfrm_RequerimentoBeneficio.BitBtn1Click(Sender: TObject);
begin

  if edtContribuinte.Text = '' then
  begin
    showmessage('Campo obrigatório');
    edtContribuinte.SetFocus;
    exit;
  end;
  if  edtNomeRequerente.Text = '' then
  begin
    showmessage('Campo obrigatório');
    edtNomeRequerente.SetFocus;
    exit;
  end;
  if   edtCpf.Text = '   .   .   -  ' then
  begin
    showmessage('Campo obrigatório');
    edtCpf.SetFocus;
    exit;
  end;
  if  cbTipoBeneficio.KeyValue < 1 then
  begin
    showmessage('Campo obrigatório');
    cbTipoBeneficio.SetFocus;
    exit;
  end;


  dm.spInserePedidoBeneficio.Parameters.ParamByName('@vCodigo').Value :=
    edtContribuinte.Text;
  dm.spInserePedidoBeneficio.Parameters.ParamByName('@vTipo').Value :=
    cbTipoBeneficio.KeyValue;
  dm.spInserePedidoBeneficio.Parameters.ParamByName('@vNome').Value :=
    edtNomeRequerente.Text;
  dm.spInserePedidoBeneficio.Parameters.ParamByName('@vCPF').Value :=
    edtCpf.Text;
  dm.spInserePedidoBeneficio.Parameters.ParamByName('@vAno').Value :=
    FormatDateTime('YYYY',date);


  try
    dm.spInserePedidoBeneficio.ExecProc;
    showmessage('Solicitação registrada');
  except
    showmessage('Erro no registro da solicitação');
  end;



end;

procedure Tfrm_RequerimentoBeneficio.Button1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
  frm_PesquisaContribuinte.origem := 6;
  frm_PesquisaContribuinte.Show;
end;

procedure Tfrm_RequerimentoBeneficio.edtContribuinteExit(Sender: TObject);
begin
if edtContribuinte.Text <> '' then
  begin
    dm.qBuscaContribuinte.Close;
    dm.qBuscaContribuinte.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    dm.qBuscaContribuinte.Open;

    if dm.qBuscaContribuinte.RecordCount = 0 then
    begin
      showmessage('Contribuinte não encontrado');
      edtContribuinte.SetFocus;
    end
    else
      Label3.Caption := dm.qBuscaContribuintenm_pastor.Value;
  end;
end;

procedure Tfrm_RequerimentoBeneficio.edtContribuinteKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_RequerimentoBeneficio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  self := nil;
  Action := caFree;
end;

procedure Tfrm_RequerimentoBeneficio.FormCreate(Sender: TObject);
begin
  dm.qTipoBeneficio.open;

end;

end.
