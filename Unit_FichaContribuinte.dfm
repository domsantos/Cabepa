inherited frm_FichaContribuinte: Tfrm_FichaContribuinte
  Left = 251
  Top = 172
  Caption = 'frm_FichaContribuinte'
  ExplicitLeft = -40
  ExplicitWidth = 862
  ExplicitHeight = 629
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = 24
    Top = -8
    ExplicitLeft = 24
    ExplicitTop = -8
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 139
        Caption = 'FICHA CONTRIBUINTE'
        ExplicitWidth = 139
      end
    end
    inherited RLBand2: TRLBand
      Height = 8
      ExplicitHeight = 8
    end
    inherited RLBand4: TRLBand
      Top = 953
      ExplicitTop = 953
    end
    inherited RLBand3: TRLBand
      Top = 129
      Height = 824
      ExplicitTop = 129
      ExplicitHeight = 824
      object RLDraw1: TRLDraw [0]
        Left = 1
        Top = 0
        Width = 716
        Height = 161
      end
      inherited RLDBText1: TRLDBText
        Left = 608
        Top = 8
        Width = 65
        DataField = 'nm_pastor'
        Visible = False
        ExplicitLeft = 608
        ExplicitTop = 8
        ExplicitWidth = 65
      end
      inherited RLDBText2: TRLDBText
        Left = 608
        Top = 32
        Width = 75
        DataField = 'cd_cadastro'
        Visible = False
        ExplicitLeft = 608
        ExplicitTop = 32
        ExplicitWidth = 75
      end
      object RLLabel6: TRLLabel
        Left = 2
        Top = 0
        Width = 42
        Height = 16
        Caption = 'Nome:'
      end
      object RLLabel7: TRLLabel
        Left = 2
        Top = 24
        Width = 48
        Height = 16
        Caption = 'C'#243'digo:'
      end
      object RLLabel8: TRLLabel
        Left = 2
        Top = 48
        Width = 63
        Height = 16
        Caption = 'Categoria:'
      end
      object RLLabel9: TRLLabel
        Left = 2
        Top = 72
        Width = 84
        Height = 16
        Caption = 'Data Filia'#231#227'o:'
      end
      object RLLabel10: TRLLabel
        Left = 346
        Top = 72
        Width = 158
        Height = 16
        Caption = 'Autoriza'#231#227'o a Evangelista:'
      end
      object RLLabel11: TRLLabel
        Left = 2
        Top = 104
        Width = 165
        Height = 16
        Caption = 'Consagra'#231#227'o  a Evangelista'
      end
      object RLLabel12: TRLLabel
        Left = 345
        Top = 105
        Width = 124
        Height = 16
        Caption = 'Ordena'#231#227'o a Pastor:'
      end
      object RLLabel13: TRLLabel
        Left = 2
        Top = 136
        Width = 137
        Height = 16
        Caption = 'Local de Consagra'#231#227'o:'
      end
      object RLDraw2: TRLDraw
        Left = 1
        Top = 167
        Width = 716
        Height = 274
      end
      object RLLabel14: TRLLabel
        Left = 1
        Top = 172
        Width = 27
        Height = 16
        Caption = 'Pai:'
      end
      object RLLabel15: TRLLabel
        Left = 1
        Top = 196
        Width = 33
        Height = 16
        Caption = 'M'#227'e:'
      end
      object RLLabel16: TRLLabel
        Left = 1
        Top = 220
        Width = 89
        Height = 16
        Caption = 'Nacionalidade:'
      end
      object RLLabel17: TRLLabel
        Left = 304
        Top = 220
        Width = 80
        Height = 16
        Caption = 'Naturalidade:'
      end
      object RLLabel18: TRLLabel
        Left = 504
        Top = 220
        Width = 76
        Height = 16
        Caption = 'Estado Civil:'
      end
      object RLLabel19: TRLLabel
        Left = 1
        Top = 245
        Width = 83
        Height = 16
        Caption = 'Escolaridade:'
      end
      object RLLabel20: TRLLabel
        Left = 304
        Top = 245
        Width = 125
        Height = 16
        Caption = 'Forma'#231#227'o Teol'#243'gica:'
      end
      object RLLabel21: TRLLabel
        Left = 1
        Top = 272
        Width = 23
        Height = 16
        Caption = 'RG'
      end
      object RLLabel22: TRLLabel
        Left = 1
        Top = 296
        Width = 34
        Height = 16
        Caption = 'CPF:'
      end
      object RLLabel23: TRLLabel
        Left = 152
        Top = 296
        Width = 77
        Height = 16
        Caption = 'Nascimento:'
      end
      object RLLabel24: TRLLabel
        Left = 352
        Top = 296
        Width = 97
        Height = 16
        Caption = 'Tipo Sanguineo:'
      end
      object RLLabel25: TRLLabel
        Left = 1
        Top = 323
        Width = 97
        Height = 16
        Caption = 'Batismo/'#193'guas:'
      end
      object RLLabel26: TRLLabel
        Left = 304
        Top = 323
        Width = 91
        Height = 16
        Caption = 'Local Batismo:'
      end
      object RLDraw3: TRLDraw
        Left = 1
        Top = 447
        Width = 716
        Height = 140
      end
      object RLLabel27: TRLLabel
        Left = 1
        Top = 351
        Width = 55
        Height = 16
        Caption = 'Conjuge:'
      end
      object RLLabel28: TRLLabel
        Left = 496
        Top = 351
        Width = 77
        Height = 16
        Caption = 'Nascimento:'
      end
      object RLLabel29: TRLLabel
        Left = 1
        Top = 405
        Width = 71
        Height = 16
        Caption = 'Supervis'#227'o:'
      end
      object RLLabel30: TRLLabel
        Left = 312
        Top = 405
        Width = 49
        Height = 16
        Caption = 'Campo:'
      end
      object RLLabel31: TRLLabel
        Left = 1
        Top = 375
        Width = 150
        Height = 16
        Caption = 'No. Certid'#227'o Casamento:'
      end
      object RLLabel32: TRLLabel
        Left = 1
        Top = 456
        Width = 63
        Height = 16
        Caption = 'Endere'#231'o:'
      end
      object RLLabel33: TRLLabel
        Left = 1
        Top = 488
        Width = 88
        Height = 16
        Caption = 'Complemento:'
      end
      object RLLabel34: TRLLabel
        Left = 536
        Top = 456
        Width = 35
        Height = 16
        Caption = 'CEP:'
      end
      object RLLabel35: TRLLabel
        Left = 1
        Top = 520
        Width = 42
        Height = 16
        Caption = 'Bairro:'
      end
      object RLLabel36: TRLLabel
        Left = 304
        Top = 520
        Width = 48
        Height = 16
        Caption = 'Cidade:'
      end
      object RLLabel37: TRLLabel
        Left = 608
        Top = 520
        Width = 25
        Height = 16
        Caption = 'UF:'
      end
      object RLLabel38: TRLLabel
        Left = 1
        Top = 560
        Width = 56
        Height = 16
        Caption = 'Telefone:'
      end
      object RLDBText3: TRLDBText
        Left = 72
        Top = 48
        Width = 78
        Height = 16
        DataField = 'ds_categoria'
        DataSource = dsRelatorio
      end
      object RLDBText4: TRLDBText
        Left = 88
        Top = 72
        Width = 62
        Height = 16
        DataField = 'dt_filiacao'
        DataSource = dsRelatorio
      end
      object RLDBText5: TRLDBText
        Left = 168
        Top = 104
        Width = 130
        Height = 16
        DataField = 'dt_consagEvangelista'
        DataSource = dsRelatorio
      end
      object RLDBText6: TRLDBText
        Left = 136
        Top = 136
        Width = 128
        Height = 16
        DataField = 'ds_localConsagracao'
        DataSource = dsRelatorio
      end
      object RLDBText7: TRLDBText
        Left = 505
        Top = 72
        Width = 106
        Height = 16
        DataField = 'dt_autEvangelista'
        DataSource = dsRelatorio
      end
      object RLDBText8: TRLDBText
        Left = 480
        Top = 105
        Width = 106
        Height = 16
        DataField = 'dt_ordenacPastor'
        DataSource = dsRelatorio
      end
      object RLDBText9: TRLDBText
        Left = 32
        Top = 172
        Width = 46
        Height = 16
        DataField = 'nm_pai'
        DataSource = dsRelatorio
      end
      object RLDBText10: TRLDBText
        Left = 40
        Top = 196
        Width = 54
        Height = 16
        DataField = 'nm_mae'
        DataSource = dsRelatorio
      end
      object RLDBText11: TRLDBText
        Left = 96
        Top = 220
        Width = 84
        Height = 16
        DataField = 'ds_nacPastor'
        DataSource = dsRelatorio
      end
      object RLDBText12: TRLDBText
        Left = 88
        Top = 245
        Width = 84
        Height = 16
        DataField = 'ds_escPastor'
        DataSource = dsRelatorio
      end
      object RLDBText13: TRLDBText
        Left = 386
        Top = 220
        Width = 81
        Height = 16
        DataField = 'ds_natPastor'
        DataSource = dsRelatorio
      end
      object RLDBText14: TRLDBText
        Left = 592
        Top = 220
        Width = 66
        Height = 16
        DataField = 'ds_estCivil'
        DataSource = dsRelatorio
      end
      object RLDBText15: TRLDBText
        Left = 440
        Top = 245
        Width = 105
        Height = 16
        DataField = 'ds_formTeologica'
        DataSource = dsRelatorio
      end
      object RLDBText16: TRLDBText
        Left = 25
        Top = 272
        Width = 74
        Height = 16
        DataField = 'no_regGeral'
        DataSource = dsRelatorio
      end
      object RLDBText17: TRLDBText
        Left = 41
        Top = 296
        Width = 42
        Height = 16
        DataField = 'no_cpf'
        DataSource = dsRelatorio
      end
      object RLDBText18: TRLDBText
        Left = 234
        Top = 296
        Width = 88
        Height = 16
        DataField = 'dt_nascPastor'
        DataSource = dsRelatorio
      end
      object RLDBText19: TRLDBText
        Left = 456
        Top = 296
        Width = 81
        Height = 16
        DataField = 'tp_sanguineo'
        DataSource = dsRelatorio
      end
      object RLDBText20: TRLDBText
        Left = 104
        Top = 323
        Width = 68
        Height = 16
        DataField = 'dt_batismo'
        DataSource = dsRelatorio
      end
      object RLDBText21: TRLDBText
        Left = 408
        Top = 323
        Width = 100
        Height = 16
        DataField = 'ds_localBatismo'
        DataSource = dsRelatorio
      end
      object RLDBText22: TRLDBText
        Left = 60
        Top = 351
        Width = 74
        Height = 16
        DataField = 'nm_conjuge'
        DataSource = dsRelatorio
      end
      object RLDBText23: TRLDBText
        Left = 576
        Top = 351
        Width = 97
        Height = 16
        DataField = 'dt_nascConjuge'
        DataSource = dsRelatorio
      end
      object RLDBText24: TRLDBText
        Left = 156
        Top = 375
        Width = 113
        Height = 16
        DataField = 'no_certCasamento'
        DataSource = dsRelatorio
      end
      object RLDBText25: TRLDBText
        Left = 78
        Top = 405
        Width = 86
        Height = 16
        DataField = 'ds_supervisao'
        DataSource = dsRelatorio
      end
      object RLDBText26: TRLDBText
        Left = 368
        Top = 405
        Width = 64
        Height = 16
        DataField = 'ds_campo'
        DataSource = dsRelatorio
      end
      object RLDBText27: TRLDBText
        Left = 72
        Top = 456
        Width = 78
        Height = 16
        DataField = 'ds_endereco'
        DataSource = dsRelatorio
      end
      object RLDBText28: TRLDBText
        Left = 96
        Top = 488
        Width = 118
        Height = 16
        DataField = 'ds_compEndPastor'
        DataSource = dsRelatorio
      end
      object RLDBText29: TRLDBText
        Left = 56
        Top = 520
        Width = 57
        Height = 16
        DataField = 'ds_bairro'
        DataSource = dsRelatorio
      end
      object RLDBText30: TRLDBText
        Left = 64
        Top = 560
        Width = 49
        Height = 16
        DataField = 'no_fone'
        DataSource = dsRelatorio
      end
      object RLDBText31: TRLDBText
        Left = 356
        Top = 520
        Width = 63
        Height = 16
        DataField = 'ds_cidade'
        DataSource = dsRelatorio
      end
      object RLDBText32: TRLDBText
        Left = 576
        Top = 456
        Width = 84
        Height = 16
        DataField = 'no_cepPastor'
        DataSource = dsRelatorio
      end
      object RLDBText33: TRLDBText
        Left = 640
        Top = 520
        Width = 35
        Height = 16
        DataField = 'ds_uf'
        DataSource = dsRelatorio
      end
      object RLLabel39: TRLLabel
        Left = 232
        Top = 272
        Width = 95
        Height = 16
        Caption = #211'rg'#227'o Emissor:'
      end
      object RLLabel40: TRLLabel
        Left = 480
        Top = 272
        Width = 90
        Height = 16
        Caption = 'Data Emiss'#227'o:'
      end
      object RLDBText34: TRLDBText
        Left = 328
        Top = 272
        Width = 114
        Height = 16
        DataField = 'ds_orgaoemissorrg'
        DataSource = dsRelatorio
      end
      object RLDBText35: TRLDBText
        Left = 576
        Top = 272
        Width = 71
        Height = 16
        DataField = 'dt_emissao'
        DataSource = dsRelatorio
      end
      object RLDBText36: TRLDBText
        Left = 48
        Top = 0
        Width = 65
        Height = 16
        DataField = 'nm_pastor'
        DataSource = dsRelatorio
      end
      object RLDBText37: TRLDBText
        Left = 56
        Top = 24
        Width = 75
        Height = 16
        DataField = 'cd_cadastro'
        DataSource = dsRelatorio
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = qRelatorio
    Left = 680
    Top = 69
  end
  object qRelatorio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT c.cd_cadastro, c.cd_formTeologica, c.cd_natPastor, c.cd_e' +
        'scPastor, c.cd_nacPastor,'
      
        #9#9#9' c.cd_estCivil, c.cd_categoria, c.nm_pastor, c.no_regConv, c.' +
        'dt_nascPastor, '
      
        #9#9#9' c.tp_sanguineo, c.no_regGeral, c.no_cpf, c.ds_endereco, c.ds' +
        '_compEndPastor, '
      
        #9#9#9' c.no_cepPastor, c.dt_filiacao, c.nm_pai, c.nm_mae, c.nm_conj' +
        'uge, c.dt_nascConjuge, '
      
        #9#9#9' c.no_fone, c.dt_batismo, c.ds_localBatismo, c.dt_autEvangeli' +
        'sta, '
      
        #9#9#9' c.dt_consagEvangelista, c.dt_ordenacPastor, c.ds_localConsag' +
        'racao, '
      
        #9#9#9' c.ds_campo, c.ds_supervisao, c.no_certCasamento, c.ds_orgaoe' +
        'missorrg, '
      
        #9#9#9' c.dt_emissao, c.ds_bairro, c.ds_uf, c.ds_cidade, ct.ds_categ' +
        'oria,'
      
        #9#9#9' e.ds_escPastor,ec.ds_estCivil,f.ds_formTeologica,nc.ds_nacPa' +
        'stor,nt.ds_natPastor'
      'FROM tbContribuinte c'
      'inner join tbcategoria ct on c.cd_categoria = ct.cd_categoria'
      'inner join tbescolaridade e on c.cd_escPastor = e.cd_escPastor'
      'inner join tbestcivil ec on c.cd_estCivil = ec.cd_estCivil'
      
        'inner join tbFormTeologica f on c.cd_formTeologica = f.cd_formTe' +
        'ologica'
      
        'inner join tbNacionalidade nc on c.cd_nacPastor = nc.cd_nacPasto' +
        'r'
      'inner join tbNaturalidade nt on c.cd_natPastor = nt.cd_natPastor'
      'where c.cd_cadastro = :codigo'
      '')
    Left = 622
    Top = 70
    object qRelatoriocd_formTeologica: TIntegerField
      FieldName = 'cd_formTeologica'
    end
    object qRelatoriocd_natPastor: TIntegerField
      FieldName = 'cd_natPastor'
    end
    object qRelatoriocd_escPastor: TIntegerField
      FieldName = 'cd_escPastor'
    end
    object qRelatoriocd_nacPastor: TIntegerField
      FieldName = 'cd_nacPastor'
    end
    object qRelatoriocd_estCivil: TIntegerField
      FieldName = 'cd_estCivil'
    end
    object qRelatoriocd_categoria: TIntegerField
      FieldName = 'cd_categoria'
    end
    object qRelatorionm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qRelatoriono_regConv: TIntegerField
      FieldName = 'no_regConv'
    end
    object qRelatoriodt_nascPastor: TDateTimeField
      FieldName = 'dt_nascPastor'
    end
    object qRelatoriotp_sanguineo: TStringField
      FieldName = 'tp_sanguineo'
      FixedChar = True
    end
    object qRelatoriono_regGeral: TStringField
      FieldName = 'no_regGeral'
      FixedChar = True
    end
    object qRelatoriono_cpf: TStringField
      FieldName = 'no_cpf'
      FixedChar = True
      Size = 14
    end
    object qRelatoriods_endereco: TStringField
      FieldName = 'ds_endereco'
      Size = 60
    end
    object qRelatoriods_compEndPastor: TStringField
      FieldName = 'ds_compEndPastor'
      Size = 40
    end
    object qRelatoriono_cepPastor: TStringField
      FieldName = 'no_cepPastor'
      FixedChar = True
      Size = 8
    end
    object qRelatoriodt_filiacao: TDateTimeField
      FieldName = 'dt_filiacao'
    end
    object qRelatorionm_pai: TStringField
      FieldName = 'nm_pai'
      Size = 60
    end
    object qRelatorionm_mae: TStringField
      FieldName = 'nm_mae'
      Size = 60
    end
    object qRelatorionm_conjuge: TStringField
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object qRelatoriodt_nascConjuge: TDateTimeField
      FieldName = 'dt_nascConjuge'
    end
    object qRelatoriono_fone: TStringField
      FieldName = 'no_fone'
      Size = 15
    end
    object qRelatoriodt_batismo: TDateTimeField
      FieldName = 'dt_batismo'
    end
    object qRelatoriods_localBatismo: TStringField
      FieldName = 'ds_localBatismo'
      Size = 40
    end
    object qRelatoriodt_autEvangelista: TDateTimeField
      FieldName = 'dt_autEvangelista'
    end
    object qRelatoriodt_consagEvangelista: TDateTimeField
      FieldName = 'dt_consagEvangelista'
    end
    object qRelatoriodt_ordenacPastor: TDateTimeField
      FieldName = 'dt_ordenacPastor'
    end
    object qRelatoriods_localConsagracao: TStringField
      FieldName = 'ds_localConsagracao'
      Size = 40
    end
    object qRelatoriods_campo: TStringField
      FieldName = 'ds_campo'
      Size = 40
    end
    object qRelatoriods_supervisao: TStringField
      FieldName = 'ds_supervisao'
      Size = 40
    end
    object qRelatoriono_certCasamento: TIntegerField
      FieldName = 'no_certCasamento'
    end
    object qRelatoriods_orgaoemissorrg: TStringField
      FieldName = 'ds_orgaoemissorrg'
      Size = 30
    end
    object qRelatoriodt_emissao: TDateTimeField
      FieldName = 'dt_emissao'
    end
    object qRelatoriods_bairro: TStringField
      FieldName = 'ds_bairro'
      Size = 40
    end
    object qRelatoriods_uf: TStringField
      FieldName = 'ds_uf'
      Size = 50
    end
    object qRelatoriods_cidade: TStringField
      FieldName = 'ds_cidade'
      Size = 50
    end
    object qRelatoriods_categoria: TStringField
      FieldName = 'ds_categoria'
    end
    object qRelatoriods_escPastor: TStringField
      FieldName = 'ds_escPastor'
    end
    object qRelatoriods_estCivil: TStringField
      FieldName = 'ds_estCivil'
    end
    object qRelatoriods_formTeologica: TStringField
      FieldName = 'ds_formTeologica'
      Size = 40
    end
    object qRelatoriods_nacPastor: TStringField
      FieldName = 'ds_nacPastor'
    end
    object qRelatoriods_natPastor: TStringField
      FieldName = 'ds_natPastor'
    end
    object qRelatoriocd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
end
