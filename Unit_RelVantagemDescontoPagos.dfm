inherited frm_RelVantagemDescontoPagos: Tfrm_RelVantagemDescontoPagos
  Caption = 'frm_RelVantagemDescontoPagos'
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = 0
    ExplicitLeft = 0
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 164
        Caption = 'RELAT'#211'RIO EM PER'#205'ODO'
        ExplicitWidth = 164
      end
      object RLLabel7: TRLLabel
        Left = 96
        Top = 54
        Width = 65
        Height = 16
        Caption = 'RUBRICA:'
      end
      object lblrubrica: TRLLabel
        Left = 167
        Top = 54
        Width = 56
        Height = 16
      end
      object RLLabel8: TRLLabel
        Left = 326
        Top = 54
        Width = 67
        Height = 16
        Caption = 'PER'#205'ODO:'
      end
      object lblPeriodo: TRLLabel
        Left = 399
        Top = 54
        Width = 61
        Height = 16
      end
    end
    inherited RLBand2: TRLBand
      object RLLabel6: TRLLabel
        Left = 640
        Top = 28
        Width = 48
        Height = 16
        Caption = 'VALOR'
      end
    end
    inherited RLBand4: TRLBand
      Top = 232
      ExplicitTop = 232
    end
    inherited RLBand3: TRLBand
      inherited RLDBText1: TRLDBText
        Width = 67
        DataField = 'cd_cabepa'
        ExplicitWidth = 67
      end
      inherited RLDBText2: TRLDBText
        Width = 94
        DataField = 'nm_beneficiado'
        ExplicitWidth = 94
      end
      object RLDBText3: TRLDBText
        Left = 642
        Top = 2
        Width = 63
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_Rubrica'
        DataSource = dsRelatorio
        DisplayMask = 'R$ ###,##0.00'
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 190
      Width = 718
      Height = 42
      BandType = btSummary
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = True
      object RLDBResult1: TRLDBResult
        Left = 603
        Top = 6
        Width = 102
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_Rubrica'
        DataSource = dsRelatorio
        DisplayMask = 'R$ ###,##0.00'
        Info = riSum
      end
      object RLLabel9: TRLLabel
        Left = 488
        Top = 6
        Width = 44
        Height = 16
        Caption = 'TOTAL'
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = frm_FiltroVanDescPaga.qListaPessoas
  end
end
