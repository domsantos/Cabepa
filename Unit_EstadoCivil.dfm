inherited frm_EstadoCivil: Tfrm_EstadoCivil
  Left = 246
  Top = 159
  Caption = 'Estado Civil'
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel1: TPanel
    inherited btnRelatorio: TBitBtn
      OnClick = btnRelatorioClick
    end
  end
  inherited Panel2: TPanel
    object Label1: TLabel
      Left = 14
      Top = 12
      Width = 55
      Height = 16
      Caption = 'Estado Civil'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Left = 14
      Top = 31
      Width = 324
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_estCivil'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 16
      Top = 64
      Width = 320
      Height = 193
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalcd_estcivil: TIntegerField
      FieldName = 'cd_estcivil'
    end
    object CDSPrincipalds_estCivil: TStringField
      DisplayLabel = 'Estado Civil'
      FieldName = 'ds_estCivil'
      Required = True
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    SQL.Strings = (
      'select cd_estcivil,ds_estCivil'
      'from tbEstCivil'
      'order by ds_estCivil')
    object QPrincipalcd_estcivil: TIntegerField
      FieldName = 'cd_estcivil'
    end
    object QPrincipalds_estCivil: TStringField
      FieldName = 'ds_estCivil'
      Required = True
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 648
    Top = 312
  end
end
