unit Unit_ListaSolicitacoesBeneficios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, RXDBCtrl;

type
  Tfrm_ListaSolicitacoesBeneficios = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    dsListaJubilacao: TDataSource;
    BitBtn1: TBitBtn;
    qListaJubilacao: TADOQuery;
    qListaJubilacaonm_pastor: TStringField;
    qListaJubilacaodt_nascPastor: TDateTimeField;
    qListaJubilacaono_cpf: TStringField;
    qListaJubilacaonm_conjuge: TStringField;
    qListaJubilacaodt_nascConjuge: TDateTimeField;
    edtCabepa: TEdit;
    Label1: TLabel;
    BitBtn2: TBitBtn;
    qSpInsereBeneficio: TADOQuery;
    Label2: TLabel;
    lblNome: TLabel;
    qSpInsereBeneficioresultado: TBooleanField;
    qListaJubilacaocd_cadastro: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure edtCabepaKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn2Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_ListaSolicitacoesBeneficios: Tfrm_ListaSolicitacoesBeneficios;

implementation

uses Unit_DM, Unit_AtualizaRequerimentoBeneficio;

{$R *.dfm}

procedure Tfrm_ListaSolicitacoesBeneficios.BitBtn1Click(Sender: TObject);
begin

  if edtCabepa.Text = '' then
  begin
    showmessage('Selecione um contribuinte');
    exit;
  end;



  if Dialogs.MessageDlg('Deseja realmente Jubilar o contribuinte '+ qListaJubilacaonm_pastor.Value,
    mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrNo then
  begin
    showmessage('Jubila��o Cancelada!');
    exit;
  end;

  // Verificar se h� possibilidade de jubila��o
  dm.qExecSpCalculaValorBEneficio.close;
  dm.qExecSpCalculaValorBEneficio.Parameters.ParamByName('codigo').Value := edtCabepa.Text;
  dm.qExecSpCalculaValorBEneficio.Open;

  //showmessage(inttostr(dm.qExecSpCalculaValorBEneficionr_anoscontribuicao.Value));
  //exit;
  if dm.qExecSpCalculaValorBEneficionr_anoscontribuicao.Value < 5 then
  begin
    showmessage('Tempo de contribui��o inferior a 5 anos');
    exit;
  end
  else
  begin
//    showmessage(floattostr(dm.qExecSpCalculaValorBeneficio.FieldByName('vl_beneficio').Value));
    // Registrando beneficio
    qSpInsereBeneficio.Parameters.ParamByName('codigo').Value := strtoint(edtCabepa.Text);
    qSpInsereBeneficio.Parameters.ParamByName('tipo').Value := 2;
    qSpInsereBeneficio.Parameters.ParamByName('tpJubilacao').Value := 1;
    qSpInsereBeneficio.Parameters.ParamByName('valor').Value :=
      dm.qExecSpCalculaValorBEneficio.FieldByName('vl_beneficio').Value;

    qSpInsereBeneficio.open;

    if qSpInsereBeneficioresultado.Value then
      showmessage('Contribuinte Jubilado!')
    else
      showmessage('Erro ao registrar jubila��o!');

    qListaJubilacao.Close;
    qListaJubilacao.Open;

  end;




end;

procedure Tfrm_ListaSolicitacoesBeneficios.BitBtn2Click(Sender: TObject);
begin
  dsListaJubilacao.DataSet.locate('cd_cadastro',edtCabepa.text,[loCaseInsensitive, loPartialKey]);
  lblNome.Caption := qListaJubilacaonm_pastor.Value;
end;

procedure Tfrm_ListaSolicitacoesBeneficios.DBGrid1DblClick(Sender: TObject);
begin
  lblNome.Caption := qListaJubilacaonm_pastor.Value;
  edtCabepa.Text := inttostr(qListaJubilacaocd_cadastro.Value);
end;

procedure Tfrm_ListaSolicitacoesBeneficios.edtCabepaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_ListaSolicitacoesBeneficios.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qListaJubilacao.Close;
  self := nil;
  Action := caFree;
end;

procedure Tfrm_ListaSolicitacoesBeneficios.FormShow(Sender: TObject);
begin
  qListaJubilacao.Open
end;

end.
