unit Unit_ListaParaArquivoMorto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,Unit_DM, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, DBCtrls,
  Buttons;

type
  TfrmListaParaArquivoMorto = class(TForm)
    Panel1: TPanel;
    qListaContribuintes: TADOQuery;
    DBGrid1: TDBGrid;
    qListaContribuintesnm_pastor: TStringField;
    qListaContribuintesnm_conjuge: TStringField;
    qListaContribuintesst_arquivoMorto: TBooleanField;
    dsLista: TDataSource;
    DBCheckBox1: TDBCheckBox;
    BitBtn1: TBitBtn;
    qArquivoMorto: TADOQuery;
    rbPesquisa: TRadioGroup;
    edtPesquisa: TEdit;
    Label1: TLabel;
    BitBtn2: TBitBtn;
    qListaContribuintescd_cadastro: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure DBCheckBox1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmListaParaArquivoMorto: TfrmListaParaArquivoMorto;

implementation



{$R *.dfm}

procedure TfrmListaParaArquivoMorto.BitBtn1Click(Sender: TObject);
begin
  qListaContribuintes.First;
  while not qListaContribuintes.Eof do
  begin
    if DBCheckBox1.Checked then
    begin
      try
        qArquivoMorto.close;
        qArquivoMorto.Parameters.ParamByName('codigo').Value :=
          qListaContribuintescd_cadastro.Value;
        qArquivoMorto.ExecSQL;
      Except
        on e:Exception do
        begin
          ShowMessage('Erro ao tranferi contribuinte!');
        end;
      end;
    end;

    qListaContribuintes.Next;
  end;
  ShowMessage('Transferência realizada!');
  qListaContribuintes.Close;
  qListaContribuintes.Open;


end;

procedure TfrmListaParaArquivoMorto.BitBtn2Click(Sender: TObject);
begin
  if rbPesquisa.ItemIndex = 0 then
  begin
    dsLista.DataSet.Locate('nm_pastor',edtPesquisa.Text,[loCaseInsensitive, loPartialKey]);
  end;

  if rbPesquisa.ItemIndex = 1 then
  begin
    dsLista.DataSet.Locate('cd_cadastro',edtPesquisa.Text,[loCaseInsensitive, loPartialKey]);
  end;

end;

procedure TfrmListaParaArquivoMorto.DBCheckBox1Click(Sender: TObject);
begin
  if DBCheckBox1.Checked then
    DBCheckBox1.Caption := DBCheckBox1.ValueChecked
  else
    DBCheckBox1.Caption := DBCheckBox1.ValueUnChecked;
end;

procedure TfrmListaParaArquivoMorto.DBGrid1ColExit(Sender: TObject);
begin
  if DBGrid1.SelectedField.FieldName = DBCheckBox1.DataField then
    DBCheckBox1.Visible := False
end;

procedure TfrmListaParaArquivoMorto.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const IsChecked : array[Boolean] of Integer =
      (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
var
  DrawState: Integer;
  DrawRect: TRect;
begin
  if (gdFocused in State) then
  begin
    if (Column.Field.FullName = DBCheckBox1.DataField) then
    begin
      DBCheckBox1.Left := Rect.Left + DBGrid1.Left + 2;
      DBCheckBox1.Top := Rect.Top + DBGrid1.top + 2;
      DBCheckBox1.Width := Rect.Right - Rect.Left;
      DBCheckBox1.Height := Rect.Bottom - Rect.Top;
      DBCheckBox1.Visible := True;
    end;
  end
  else
  begin
    if (Column.Field.FieldName = DBCheckBox1.DataField) then
    begin
      DrawRect:=Rect;
      InflateRect(DrawRect,-1,-1);
      DrawState := ISChecked[Column.Field.AsBoolean];
      DBGrid1.Canvas.FillRect(Rect);
      DrawFrameControl(DBGrid1.Canvas.Handle, DrawRect, DFC_BUTTON, DrawState);
    end;
  end;
  
end;

procedure TfrmListaParaArquivoMorto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qListaContribuintes.Close;
  self := nil;
  Action := caFree;
end;

procedure TfrmListaParaArquivoMorto.FormCreate(Sender: TObject);
begin
  DBCheckBox1.ValueChecked := 'Tranferir para arquivo morto.';
  DBCheckBox1.ValueUnChecked := '';
  qListaContribuintes.Open;
end;

end.
