unit Unit_pagamentoLoteReferencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, rxToolEdit, rxCurrEdit, DBCtrls, Mask, StdCtrls, Buttons, Grids,
  Menus, ADODB, ExtCtrls, DateUtils;

type
  Tfrm_pagamentoLoteReferencia = class(TForm)
    Label2: TLabel;
    edtContribuinte: TEdit;
    Button1: TButton;
    Label4: TLabel;
    lblContribuinte: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    edtInicio: TMaskEdit;
    Label15: TLabel;
    edtTipoContribuicao: TEdit;
    Label16: TLabel;
    Label8: TLabel;
    cbFormaPagamento: TDBLookupComboBox;
    Label12: TLabel;
    edtValor: TCurrencyEdit;
    edtBanco: TEdit;
    Banco: TLabel;
    edtAgencia: TEdit;
    Label11: TLabel;
    Label9: TLabel;
    edtCheque: TEdit;
    Label10: TLabel;
    edtdataResgate: TMaskEdit;
    dsFormaPagamento: TDataSource;
    Label13: TLabel;
    Label1: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    edtMes: TEdit;
    edtAno: TEdit;
    edtValorParcial: TCurrencyEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ppMenu: TPopupMenu;
    RemoverItem1: TMenuItem;
    sgvalores: TStringGrid;
    Label18: TLabel;
    lblvlacul: TLabel;
    edtVlAcumulado: TCurrencyEdit;
    qInsertPgto: TADOQuery;
    qContribuicao: TADOQuery;
    ckCalculo: TCheckBox;
    Panel1: TPanel;
    Label3: TLabel;
    Label7: TLabel;
    Label19: TLabel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    edtInicial: TMaskEdit;
    edtFinal: TMaskEdit;
    BitBtn5: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure RemoverItem1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure edtContribuinteExit(Sender: TObject);
    function verificavalores:Boolean;
    procedure BitBtn2Click(Sender: TObject);
    procedure ckCalculoClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_pagamentoLoteReferencia: Tfrm_pagamentoLoteReferencia;
  linhas : integer;
  valorAcumulado : Currency;

implementation

uses Unit_DM, Unit_PesquisaContribuinte, FNLIB1;

{$R *.dfm}

procedure Tfrm_pagamentoLoteReferencia.BitBtn1Click(Sender: TObject);
var
  i : integer;
begin

  if ckCalculo.Checked then
  begin

    if not Panel1.Visible then
    begin
      Panel1.Visible := true;
      edtInicial.SetFocus;
      exit;
    end;
    {
    num_parc := MonthsBetween(StrToDate(edtInicial.Text), StrToDate(edtFinal.Text)) + 1;
    data_parcela := StrToDate(edtInicial.Text);
    showmessage(inttostr(num_parc));

    valor_parc := edtValor.Value / num_parc;

    for I := 1 to num_parc do
    begin
      mes := FormatDateTime('MM',data_parcela);
      ano := FormatDateTime('YYYY',data_parcela);

      with sgvalores do
      begin
        if linhas = 1 then
        begin
          //RowCount := linhas;
          linhas := 2;
          Cells[0,1] := mes;
          Cells[1,1] := ano;
          Cells[2,1] := myPad(CurrToStr(valor_parc),20,'R',false);
          RowCount := linhas;
        end
        else
        begin

          linhas := RowCount;

          Cells[0,linhas] := mes;
          Cells[1,linhas] := ano;
          Cells[2,linhas] := myPad(CurrToStr(valor_parc),20,'R',false);
          linhas := RowCount + 1;
          RowCount := linhas;
        end;

      end;

      data_parcela := IncMonth(data_parcela,1);

    end;



    Panel1.Visible := false;
    exit;
    }
  end;

  // Informar os valores e referencias individualmente
  if edtMes.Text = '' then
  begin
    showmessage('Informe M�s de Refer�ncia');
    edtMes.SetFocus;
    exit;
  end;
  if edtAno.Text = '' then
  begin
    showmessage('Informe Ano de Refer�ncia');
    edtAno.SetFocus;
    exit;
  end;
  if edtValorParcial.Text = '' then
  begin
    showmessage('Informe Valor da Refer�ncia');
    edtValorParcial.SetFocus;
    exit;
  end;


  // Verificando valor informado
  for I := 1 to sgvalores.RowCount - 1 do
  begin
    if (sgvalores.cells[0,i] = edtMes.Text) and (sgvalores.cells[1,i] = edtano.Text)  then
    begin
      showmessage('valor j� informado para esta referncia');
      edtMes.SetFocus;
      exit;
    end;

  end;



  with sgvalores do
  begin

    valorAcumulado := valorAcumulado + edtValorParcial.Value;
    edtVlAcumulado.Value := valorAcumulado;

    if valorAcumulado > edtValor.Value then
    begin
      showmessage('Valor informado da refer�ncia ir� ultrapassar valor total da contribui��o');
      valorAcumulado := valorAcumulado - edtValorParcial.Value;
      edtVlAcumulado.Value := valorAcumulado;
      edtValorParcial.SetFocus;
      exit;
    end;

    if linhas = 1 then
    begin
      //RowCount := linhas;
      linhas := 2;
      Cells[0,1] := edtMes.Text;
      Cells[1,1] := edtAno.Text;
      Cells[2,1] := myPad(edtValorParcial.Text,20,'R',false);
      RowCount := linhas;
    end
    else
    begin

      linhas := RowCount;

      Cells[0,linhas] := edtMes.Text;
      Cells[1,linhas] := edtAno.Text;
      Cells[2,linhas] := myPad(edtValorParcial.Text,20,'R',false);
      linhas := RowCount + 1;
      RowCount := linhas;
    end;

  end;


  edtMes.Text := '';
  edtAno.Text := '';
  edtValorParcial.Text := '';
  edtMes.SetFocus;
  BitBtn2.Enabled := true;



end;

procedure Tfrm_pagamentoLoteReferencia.BitBtn2Click(Sender: TObject);
var
  i : integer;
begin
  if verificavalores then
  begin

    dm.ADOConn.BeginTrans;

    for I := 1 to sgvalores.RowCount - 1 do
    begin

      with qInsertPgto do
      begin
        try
          close;

          Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
          Parameters.ParamByName('mes').Value := strtoint(sgvalores.cells[0,i]);
          Parameters.ParamByName('ano').Value := strtoint(sgvalores.cells[1,i]);
          Parameters.ParamByName('valorCont').Value := StrToFloat(sgvalores.cells[2,i]);
          Parameters.ParamByName('tipo').Value := strtoint(edtTipoContribuicao.Text);
          Parameters.ParamByName('st_pgto_cheque').Value := 0;
          Parameters.ParamByName('dtpagto').Value := StrToDateTime(edtInicio.Text);


          qInsertPgto.Parameters.ParamByName('valorCont1').Value := StrToFloat(sgvalores.cells[2,i]);
          qInsertPgto.Parameters.ParamByName('dtpagto1').Value := StrToDateTime(edtInicio.Text);
          qInsertPgto.Parameters.ParamByName('forma1').Value := cbFormaPagamento.KeyValue;
          qInsertPgto.Parameters.ParamByName('codigo1').Value := strtoint(edtContribuinte.Text);
          qInsertPgto.Parameters.ParamByName('mes1').Value := strtoint(sgvalores.cells[0,i]);
          qInsertPgto.Parameters.ParamByName('ano1').Value := strtoint(sgvalores.cells[1,i]);
          qInsertPgto.Parameters.ParamByName('tipo1').Value := strtoint(edtTipoContribuicao.Text);
          if (cbFormaPagamento.KeyValue > 1) and (cbFormaPagamento.KeyValue < 4) then
          begin
            qInsertPgto.Parameters.ParamByName('banco1').Value := strtoint(edtContribuinte.Text);
            qInsertPgto.Parameters.ParamByName('agencia1').Value := edtBanco.Text;
            qInsertPgto.Parameters.ParamByName('cheque1').Value := edtCheque.Text;
            qInsertPgto.Parameters.ParamByName('data1').Value := StrToDateTime(edtdataResgate.Text);
            qInsertPgto.Parameters.ParamByName('st_pgto_cheque1').Value := 1;
          end;

          ExecSQL;
        except
          on E:Exception do
          begin
            showmessage('Erro ao registrar pagamento.'+#13+'Verifique se j� h� registros para essa refer�ncia.');
            dm.ADOConn.RollbackTrans;
            BitBtn2.Enabled := false;
            exit;
          end;
        end;

      end;

    end;

    dm.ADOConn.CommitTrans;
    showmessage('Pagamento Registrado!');
    BitBtn2.Enabled := false;

  end;

end;

procedure Tfrm_pagamentoLoteReferencia.BitBtn3Click(Sender: TObject);
begin

  Panel1.Visible := false;
end;

procedure Tfrm_pagamentoLoteReferencia.BitBtn4Click(Sender: TObject);
var
  i,num_parc : integer;
  valor_parc,total,resto : Currency;
  vPar : Double;
  mes,ano : string;
  data_parcela : TDate;
begin
  num_parc := MonthsBetween(StrToDate(edtInicial.Text), StrToDate(edtFinal.Text)) + 1;
  data_parcela := StrToDate(edtInicial.Text);
  vPar := edtValor.Value / num_parc;
  valor_parc := myRound(vPar,2,1);
  total := valor_parc * num_parc;
  resto := edtValor.Value - total;

  for I := 1 to num_parc do
  begin
    mes := FormatDateTime('MM',data_parcela);
    ano := FormatDateTime('YYYY',data_parcela);

    if num_parc = i then
      valor_parc := valor_parc + resto; 


    if (sgvalores.cells[0,i] = Mes) and (sgvalores.cells[1,i] = ano)  then
    begin
      showmessage('valor j� informado para esta referncia: '+mes+'/'+ano);
    end
    else
    begin

      with sgvalores do
      begin
        if linhas = 1 then
        begin
          //RowCount := linhas;
          linhas := 2;
          Cells[0,1] := mes;
          Cells[1,1] := ano;
          Cells[2,1] := myPad(CurrToStr(valor_parc),20,'R',false);
          RowCount := linhas;
        end
        else
        begin

          linhas := RowCount;

          Cells[0,linhas] := mes;
          Cells[1,linhas] := ano;
          Cells[2,linhas] := myPad(CurrToStr(valor_parc),20,'R',false);
          linhas := RowCount + 1;
          RowCount := linhas;
        end;

      end;
    end;

    data_parcela := IncMonth(data_parcela,1);
    BitBtn2.Enabled := true;

  end;
  Panel1.Visible := false;
end;

procedure Tfrm_pagamentoLoteReferencia.BitBtn5Click(Sender: TObject);
var
  i : integer;
begin



  for i := 1 to sgvalores.RowCount - 1 do
  begin
    sgvalores.Cells[0,i] := '';
    sgvalores.Cells[1,i] := '';
    sgvalores.Cells[2,i] := '';
  end;


  sgvalores.RowCount := 2;
  edtVlAcumulado.Value := 0;
  sgvalores.Cells[0,1] := '';
  sgvalores.Cells[1,1] := '';
  sgvalores.Cells[2,1] := '';
  linhas := 1;

    BitBtn2.Enabled := false;
end;

procedure Tfrm_pagamentoLoteReferencia.Button1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
  frm_PesquisaContribuinte.origem := 12;
  frm_PesquisaContribuinte.Show;
end;

procedure Tfrm_pagamentoLoteReferencia.ckCalculoClick(Sender: TObject);
begin
  if ckCalculo.Checked then
  begin
    edtInicial.Text := '';
    edtFinal.Text := '';
  end;
end;

procedure Tfrm_pagamentoLoteReferencia.edtContribuinteExit(Sender: TObject);
begin
if edtContribuinte.Text <> '' then
  begin
    dm.qBuscaContribuinte.Close;
    dm.qBuscaContribuinte.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    dm.qBuscaContribuinte.Open;

    if dm.qBuscaContribuinte.RecordCount = 0 then
    begin
      showmessage('Contribuinte n�o encontrado');
      edtContribuinte.SetFocus;
    end
    else
      lblContribuinte.Caption := dm.qBuscaContribuintenm_pastor.Value;
end;
end;

procedure Tfrm_pagamentoLoteReferencia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.qFormaPagamento.Close;
  self := nil;
  Action := caFree;
end;

procedure Tfrm_pagamentoLoteReferencia.FormCreate(Sender: TObject);
begin
  valorAcumulado := 0.0;
  with sgvalores do
  begin

    ColWidths[0] := 40;
    ColWidths[1] := 50;
    ColWidths[2] := 200;

    Cells[0,0] := 'M�s';
    Cells[1,0] := 'Ano';
    Cells[2,0] := 'Valor';

    
  end;
  dm.qFormaPagamento.Open;
  linhas := 1;
end;

procedure Tfrm_pagamentoLoteReferencia.RemoverItem1Click(Sender: TObject);
var
  posicao,i : integer;
begin
  posicao := sgvalores.row;

  valorAcumulado := valorAcumulado - strtofloat(sgvalores.Cells[2,sgvalores.row]);

  for i := posicao to sgvalores.RowCount - 1 do
  begin
    sgvalores.Cells[0,i] := sgvalores.Cells[0,i + 1];
    sgvalores.Cells[1,i] := sgvalores.Cells[1,i + 1];
    sgvalores.Cells[2,i] := sgvalores.Cells[2,i + 1];

  end;

  sgvalores.RowCount := sgvalores.RowCount - 1;
  edtVlAcumulado.Value := valorAcumulado;


end;

function Tfrm_pagamentoLoteReferencia.verificavalores: Boolean;
begin

  if edtContribuinte.Text = '' then
  begin
    showmessage('Informe o No. do Contribuinte');
    result := false;
    edtContribuinte.SetFocus;
    exit;
  end;
  if edtInicio.Text = '  /  /    ' then
  begin
    showmessage('Informe a Data de Pagamento');
    result := false;
    edtInicio.SetFocus;
    exit;
  end;
    if edtTipoContribuicao.Text = '' then
  begin
    showmessage('Informe Tipo de Contribui��o');
    result := false;
    edtTipoContribuicao.SetFocus;
    exit;
  end;
    if cbFormaPagamento.KeyValue = 0 then
  begin
    showmessage('Informe Tipo Pagamento');
    result := false;
    cbFormaPagamento.SetFocus;
    exit;
  end;
  if edtValor.Text = '' then
  begin
    showmessage('Informe Valor Total');
    result := false;
    edtValor.SetFocus;
    exit;
  end;

//  showmessage(inttostr(cbFormaPagamento.KeyValue));

  if (cbFormaPagamento.KeyValue > 1) and (cbFormaPagamento.KeyValue < 4) then
  begin
    if edtBanco.Text = '' then
    begin
      showmessage('Informe Banco');
      result := false;
      edtBanco.SetFocus;
      exit;
    end;

    if edtAgencia.Text = '' then
    begin
      showmessage('Informe Agencia');
      result := false;
      edtAgencia.SetFocus;
      exit;
    end;

    if edtCheque.Text = '' then
    begin
      showmessage('Informe No. Cheque');
      result := false;
      edtCheque.SetFocus;
      exit;
    end;

    if edtdataResgate.Text = '  /  /    ' then
    begin
      showmessage('Informe Data Resgate');
      result := false;
      edtdataResgate.SetFocus;
      exit;
    end;

  end;

  result := true;

end;

end.
