unit Unit_simularJubilacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, Grids;

type
  Tfrm_SimularJubilacao = class(TForm)
    Panel1: TPanel;
    Label2: TLabel;
    edtContribuinte: TEdit;
    Button1: TButton;
    lblContribuinte: TLabel;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label3: TLabel;
    lblQtdContribuicao: TLabel;
    Label4: TLabel;
    lblValorMedio: TLabel;
    Label5: TLabel;
    lblValorBeneficio: TLabel;
    Label6: TLabel;
    lblValorTotal: TLabel;
    Label7: TLabel;
    lblNoAnos: TLabel;
    Label8: TLabel;
    lblValorDizimo: TLabel;
    Label9: TLabel;
    lblValorLiquido: TLabel;
    BitBtn2: TBitBtn;
    procedure Button1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure edtContribuinteExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_SimularJubilacao: Tfrm_SimularJubilacao;

implementation

uses Unit_PesquisaContribuinte, Unit_DM, Unit_RelSimulacaoBeneficio;

{$R *.dfm}

procedure Tfrm_SimularJubilacao.BitBtn1Click(Sender: TObject);
var
  valor,dizimo : Currency;
begin

  dm.qExecSpCalculaValorBEneficio.close;
  dm.qExecSpCalculaValorBEneficio.Parameters.ParamByName('codigo').Value :=
        strtoint(edtContribuinte.Text);
  dm.qExecSpCalculaValorBEneficio.open;

  dm.qValorMinimo.Open;


  if dm.qExecSpCalculaValorBEneficiovl_beneficio.Value < dm.qValorMinimovl_salMin.Value then
    valor := dm.qValorMinimovl_salMin.Value
  else
    valor := dm.qExecSpCalculaValorBEneficiovl_beneficio.Value;

  dm.qExecSpSimulaCalculoDizimo.Parameters.ParamByName('valor').Value := valor;
  dm.qExecSpSimulaCalculoDIzimo.Open;

  lblValorBeneficio.Caption := FormatCurr('###,##0.00', valor);
  lblQtdContribuicao.Caption :=  inttostr(dm.qExecSpCalculaValorBEneficioqt_contribuicao.Value);
  lblValorMedio.Caption := FormatCurr('###,##0.00',dm.qExecSpCalculaValorBEneficiovl_media_contribuicao.Value);
  lblValorTotal.Caption := FormatCurr('###,##0.00',dm.qExecSpCalculaValorBEneficiovl_totalcontribuicao.Value);
  lblNoAnos.Caption := inttostr(dm.qExecSpCalculaValorBEneficionr_anoscontribuicao.Value);


  if valor = dm.qValorMinimovl_salMin.Value then
    dizimo := dm.qExecSpCalculaValorBEneficiovl_minimo.Value * 0.1
  else
    dizimo := dm.qExecSpSimulaCalculoDIzimovl_dizimo.Value;

  lblValorDizimo.Caption := FormatCurr('###,##0.00',dizimo);
  lblValorLiquido.Caption := FormatCurr('###,##0.00',valor - dizimo);



  BitBtn2.Enabled := true;

end;

procedure Tfrm_SimularJubilacao.BitBtn2Click(Sender: TObject);
begin
  Application.CreateForm(TFrm_relSimulacaoBeneficio,Frm_relSimulacaoBeneficio);

  Frm_relSimulacaoBeneficio.lblPastor.Caption := lblContribuinte.Caption;
  Frm_relSimulacaoBeneficio.lblQtdContribuicoes.Caption := lblQtdContribuicao.Caption;
  Frm_relSimulacaoBeneficio.lblValorMedioContr.Caption := lblValorMedio.Caption;
  Frm_relSimulacaoBeneficio.lblValorTotal.Caption := lblValorTotal.Caption;
  Frm_relSimulacaoBeneficio.lblAnosContr.Caption := lblNoAnos.Caption;
  Frm_relSimulacaoBeneficio.lblValorBeneficio.Caption := lblValorBeneficio.Caption;
  Frm_relSimulacaoBeneficio.lblValorDizimo.Caption := lblValorDizimo.Caption;
  Frm_relSimulacaoBeneficio.lblValorLiquido.Caption := lblValorLiquido.Caption;


  Frm_relSimulacaoBeneficio.qListaContribuicoes.Close;
  Frm_relSimulacaoBeneficio.qListaContribuicoes.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
  Frm_relSimulacaoBeneficio.qListaContribuicoes.Open;





  Frm_relSimulacaoBeneficio.RLReport1.Preview();



end;

procedure Tfrm_SimularJubilacao.Button1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
  frm_PesquisaContribuinte.origem := 10;
  frm_PesquisaContribuinte.Show;
end;

procedure Tfrm_SimularJubilacao.edtContribuinteExit(Sender: TObject);
begin

  if edtContribuinte.Text <> '' then
  begin
    dm.qBuscaContribuinte.Close;
    dm.qBuscaContribuinte.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);

    dm.qBuscaContribuinte.Open;

    lblContribuinte.Caption := dm.qBuscaContribuintenm_pastor.Value;
  end;

end;

procedure Tfrm_SimularJubilacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

end.
