unit Unit_CargaContribuicoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls;

type
  Tfrm_CargaContribuicoes = class(TForm)
    OpenDialog1: TOpenDialog;
    Label1: TLabel;
    edtCodigo: TEdit;
    contribicoes: TADOTable;
    contribicoesdt_anoRefContribuicao: TIntegerField;
    contribicoesdt_mesRefContribuicao: TIntegerField;
    contribicoesvl_ContribuicaoVencimento: TBCDField;
    contribicoescd_tipoContribuicao: TIntegerField;
    contribicoescd_sitQuitacao: TSmallintField;
    Pagamento: TADOTable;
    Pagamentovl_pagtoContribuicao: TBCDField;
    Pagamentodt_pagtoContribuicao: TDateTimeField;
    Pagamentocd_formPagto: TSmallintField;
    Pagamentodt_anoRefContribuicao: TIntegerField;
    Pagamentodt_mesRefContribuicao: TIntegerField;
    Pagamentocd_tipoContribuicao: TIntegerField;
    Button1: TButton;
    Memo1: TMemo;
    contribicoescd_cadastro: TIntegerField;
    Pagamentocd_cadastro: TIntegerField;
    procedure Button1Click(Sender: TObject);
    function formataMoeda(valor : string): string;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_CargaContribuicoes: Tfrm_CargaContribuicoes;

implementation

uses Unit_DM, FNLIB1;

{$R *.dfm}

procedure Tfrm_CargaContribuicoes.Button1Click(Sender: TObject);
var
  f : textfile;
  linha : string;
  tipo : string;
  start, linhas, cabecalho, i,x : smallint;
begin

  if OpenDialog1.Execute then
    AssignFile(f,OpenDialog1.FileName);

  reset(f);
  start := 0;
  linhas := 43;
  cabecalho := 15;
  i := 0;
  x := 0;
  //dm.ADOConn.BeginTrans;

  //contribicoes.Open;
  //Pagamento.Open;

  while not eof(f) do
  begin
    readln(f,linha);
    i := i + 1;
    x := x + 1;
    //tipo := trim(copy(linha,24,20));
    if i > cabecalho then
    begin
      if x < linhas then
        Memo1.Lines.Add(linha);
    end
    else
    begin
      i := 0;
      x := 0;
      cabecalho := 11;
      linhas := 52;
    end;


  end;

  ShowMessage('Carga completa');

  //dm.ADOConn.CommitTrans;

end;

function Tfrm_CargaContribuicoes.formataMoeda(valor: string): string;
var
  i : integer;
  v : string;
begin

  v := '';
  for I := 1 to length(valor) do
  begin
    if copy(valor,I,1) <> '.' then
    begin
      //if copy(valor,I,1) = ',' then
        //v := v + '.'
      //else
        v := v + copy(valor,I,1);
    end;
  end;

  result := v;
end;

end.
