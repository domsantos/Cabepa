unit Unit_Categorias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, FMTBcd, DB, StdCtrls, Mask, DBCtrls, ActnList,
  Provider, DBClient, SqlExpr, Buttons, ExtCtrls, ADODB, Grids, DBGrids;

type
  Tfrm_Categorias = class(Tfrm_PadraoCadastro)
    QPrincipalcd_categoria: TIntegerField;
    QPrincipalds_categoria: TStringField;
    CDSPrincipalcd_categoria: TIntegerField;
    CDSPrincipalds_categoria: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnRelatorioClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Categorias: Tfrm_Categorias;

implementation

uses  Unit_RelatorioCategoria, Unit_DM;

{$R *.dfm}

procedure Tfrm_Categorias.btnPesquisarClick(Sender: TObject);
begin
  inherited;

  CDSPrincipal.Open;

end;

procedure Tfrm_Categorias.btnRelatorioClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(Tfrm_RelatorioCategoria,frm_RelatorioCategoria);
  frm_RelatorioCategoria.dsRelatorio.DataSet := CDSPrincipal;
  CDSPrincipal.Open;

  frm_RelatorioCategoria.RLReport1.Preview();

end;

procedure Tfrm_Categorias.btnNovoClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

end.
