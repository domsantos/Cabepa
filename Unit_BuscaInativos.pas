unit Unit_BuscaInativos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, Buttons, StdCtrls;

type
  Tfrm_BuscaInativos = class(TForm)
    qBuscaInativos: TADOQuery;
    Panel1: TPanel;
    dsBuscaInativos: TDataSource;
    DBGrid1: TDBGrid;
    qBuscaInativosnm_pastor: TStringField;
    qBuscaInativosnm_conjuge: TStringField;
    qBuscaInativosdt_nascPastor: TDateTimeField;
    Label1: TLabel;
    edtNome: TEdit;
    Buscar: TButton;
    BitBtn1: TBitBtn;
    qBuscaInativoscd_cadastro: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BuscarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_BuscaInativos: Tfrm_BuscaInativos;

implementation

uses Unit_DM, Unit_RelResumoContribuntes;

{$R *.dfm}

procedure Tfrm_BuscaInativos.BitBtn1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_ResumoContribuintes,frm_ResumoContribuintes);
  frm_ResumoContribuintes.RLReport1.Preview();
  frm_ResumoContribuintes.qRelatorio.Close;
  frm_ResumoContribuintes.Close;
end;

procedure Tfrm_BuscaInativos.BuscarClick(Sender: TObject);
begin
  with  qBuscaInativos do
  begin
    close;
    SQL.Clear;
    SQL.Add('select cd_cadastro,nm_pastor,nm_conjuge,dt_nascPastor ');
    SQL.Add('from tbContribuinte                 ');
    SQL.Add('where cd_sitJubilamento in (1,2) ');
    SQL.Add(' and nm_pastor like ''%'+edtNome.Text+'%''');
    SQL.Add('order by nm_pastor');

    showmessage(sql.GetText);

    Open;
  end;
end;

procedure Tfrm_BuscaInativos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qBuscaInativos.close;
  Action := caFree;
  self := nil;
end;

procedure Tfrm_BuscaInativos.FormShow(Sender: TObject);
begin
  qBuscaInativos.Open;
end;

end.
