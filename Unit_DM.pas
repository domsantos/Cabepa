unit Unit_DM;

interface

uses
  SysUtils, Classes, DB, SqlExpr, Unit_ClassImpressoraMatricial,
  ADODB,INIFiles, RLFilters, RLPDFFilter, RLPrintDialog, RLPreviewForm,
  RLXLSFilter;

type
  TDM = class(TDataModule)

    ADOConn: TADOConnection;
    QImpressaoBoleto: TADOQuery;
    qAutenticacao: TADOQuery;
    QDescricaoContribuicao: TADOQuery;
    QDescricaoContribuicaovl_ContribuicaoVencimento: TBCDField;
    QDescricaoContribuicaonm_tipoContribuicao: TStringField;
    QImpressaoBoletonm_pastor: TStringField;
    QImpressaoBoletoid_PagtoContribuincao: TAutoIncField;
    QImpressaoBoletovl_pagtoContribuicao: TBCDField;
    QImpressaoBoletodt_mesRefContribuicao: TIntegerField;
    QImpressaoBoletodt_anoRefContribuicao: TIntegerField;
    QImpressaoBoletods_formPagto: TStringField;
    QImpressaoBoletonm_tipoContribuicao: TStringField;
    QDescricaoContribuicaodt_mesRefContribuicao: TIntegerField;
    QDescricaoContribuicaodt_anoRefContribuicao: TIntegerField;
    qLogin: TADOQuery;
    qBuscaContribuinte: TADOQuery;
    qBuscaContribuintecd_formTeologica: TIntegerField;
    qBuscaContribuintecd_natPastor: TIntegerField;
    qBuscaContribuintecd_escPastor: TIntegerField;
    qBuscaContribuintecd_nacPastor: TIntegerField;
    qBuscaContribuintecd_estCivil: TIntegerField;
    qBuscaContribuintecd_categoria: TIntegerField;
    qBuscaContribuintenm_pastor: TStringField;
    qBuscaContribuinteno_regConv: TIntegerField;
    qBuscaContribuintedt_nascPastor: TDateTimeField;
    qBuscaContribuintetp_sanguineo: TStringField;
    qBuscaContribuinteno_regGeral: TStringField;
    qBuscaContribuinteno_cpf: TStringField;
    qBuscaContribuinteds_endereco: TStringField;
    qBuscaContribuinteds_compEndPastor: TStringField;
    qBuscaContribuinteno_cepPastor: TStringField;
    qBuscaContribuintedt_filiacao: TDateTimeField;
    qBuscaContribuintenm_pai: TStringField;
    qBuscaContribuintenm_mae: TStringField;
    qBuscaContribuintenm_conjuge: TStringField;
    qBuscaContribuintedt_nascConjuge: TDateTimeField;
    qBuscaContribuinteno_fone: TStringField;
    qBuscaContribuintedt_batismo: TDateTimeField;
    qBuscaContribuinteds_localBatismo: TStringField;
    qBuscaContribuintedt_autEvangelista: TDateTimeField;
    qBuscaContribuintedt_consagEvangelista: TDateTimeField;
    qBuscaContribuintedt_ordenacPastor: TDateTimeField;
    qBuscaContribuinteds_localConsagracao: TStringField;
    qBuscaContribuinteds_campo: TStringField;
    qBuscaContribuinteds_supervisao: TStringField;
    qBuscaContribuinteno_certCasamento: TIntegerField;
    qBuscaContribuinteds_orgaoemissorrg: TStringField;
    qBuscaContribuintedt_emissao: TDateTimeField;
    qBuscaContribuinteds_bairro: TStringField;
    qBuscaContribuinteds_uf: TStringField;
    qBuscaContribuinteds_cidade: TStringField;
    qDescricaoContrib: TADOQuery;
    BCDField1: TBCDField;
    StringField1: TStringField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    qImpressaoPagamentos: TADOQuery;
    qImpressaoPagamentosnm_pastor: TStringField;
    qImpressaoPagamentosid_PagtoContribuincao: TAutoIncField;
    qImpressaoPagamentosvl_pagtoContribuicao: TBCDField;
    qImpressaoPagamentosdt_mesRefContribuicao: TIntegerField;
    qImpressaoPagamentosdt_anoRefContribuicao: TIntegerField;
    qImpressaoPagamentosds_formPagto: TStringField;
    qImpressaoPagamentosnm_tipoContribuicao: TStringField;
    spAtualizaPagamento: TADOStoredProc;
    spVerificaPagamentoChequePre: TADOStoredProc;
    qTipoBeneficio: TADOQuery;
    qTipoBeneficiocd_tipoBeneficio: TSmallintField;
    qTipoBeneficiods_tipoBeneficio: TStringField;
    spInserePedidoBeneficio: TADOStoredProc;
    qSituacaoRequerimento: TADOQuery;
    qSituacaoRequerimentocd_sitRequerimento: TSmallintField;
    qSituacaoRequerimentods_sitRequerimento: TStringField;
    qTempoContribuicao: TADOQuery;
    qTempoContribuicaovalor_total: TBCDField;
    qTempoContribuicaoqtd_contribuicao: TIntegerField;
    qTempoContribuicaoanos_contribuicao: TIntegerField;
    qUpdate: TADOQuery;
    qExecSpCalculaValorBEneficio: TADOQuery;
    qExecSpCalculaValorBEneficiovl_beneficio: TBCDField;
    qExecSpCalculaValorBEneficiovl_media_contribuicao: TBCDField;
    qExecSpCalculaValorBEneficioqt_contribuicao: TIntegerField;
    qExecSpCalculaValorBEneficionr_percentual: TFloatField;
    qExecSpCalculaValorBEneficionr_anoscontribuicao: TSmallintField;
    qInsertValBeneficio: TADOQuery;
    qFormaPagamento: TADOQuery;
    qFormaPagamentocd_formPagto: TSmallintField;
    qFormaPagamentods_formPagto: TStringField;
    spRegistraPagamentoLote: TADOStoredProc;
    qDiferencaDatas: TADOQuery;
    qDiferencaDatasmeses: TIntegerField;
    spCalculoBeneficioMes: TADOStoredProc;
    qListaPagBeneficio: TADOQuery;
    qGetCodigo: TADOQuery;
    qGetCodigocd_codigo: TIntegerField;
    RLPreviewSetup1: TRLPreviewSetup;
    RLPrintDialogSetup1: TRLPrintDialogSetup;
    RLPDFFilter1: TRLPDFFilter;
    qListaBancos: TADOQuery;
    qListaBancoscd_banco: TAutoIncField;
    qListaBancosnm_banco: TStringField;
    qTrocaSenha: TADOQuery;
    qLoginnm_usuario: TStringField;
    qLoginid_perfil: TIntegerField;
    qLoginnm_login: TStringField;
    qLoginid_usuario: TAutoIncField;
    qCheckUsuario: TADOQuery;
    qCheckUsuariovl_senha: TStringField;
    qExecSpCalculaValorBEneficiovl_totalcontribuicao: TBCDField;
    qValorMinimo: TADOQuery;
    qValorMinimovl_salMin: TBCDField;
    qExecSpSimulaCalculoDIzimo: TADOQuery;
    qExecSpSimulaCalculoDIzimovl_acumulado: TBCDField;
    qExecSpSimulaCalculoDIzimovl_indice: TBCDField;
    qExecSpSimulaCalculoDIzimovl_base: TBCDField;
    qExecSpSimulaCalculoDIzimovl_dizimo: TBCDField;
    qNoCabepa: TADOQuery;
    qAumentoSalario: TADOQuery;
    qListaPagBeneficionm_beneficiado: TStringField;
    qListaPagBeneficiodt_falecimento: TWideStringField;
    qListaPagBeneficiotp_beneficio: TStringField;
    qListaJubilados: TADOQuery;
    qListaJubiladosnm_pastor: TStringField;
    qListaJubiladosnm_conjuge: TStringField;
    qListaJubiladosdtJubilacao: TDateTimeField;
    qListaJubiladoscd_sitJubilamento: TStringField;
    qListaJubiladossituacao: TStringField;
    qListaArquivoMorto: TADOQuery;
    qListaArquivoMortonm_pastor: TStringField;
    qListaArquivoMortodt_nascPastor: TDateTimeField;
    qListaArquivoMortono_regGeral: TStringField;
    qListaArquivoMortono_cpf: TStringField;
    qListaArquivoMortonm_conjuge: TStringField;
    qListaArquivoMortodt_nascConjuge: TDateTimeField;
    qListaArquivoMortono_fone: TStringField;
    RLXLSFilter1: TRLXLSFilter;
    qAfastarDesconto: TADOQuery;
    qExecSpCalculaValorBEneficiovl_minimo: TBCDField;
    qListaMovimentosCaixa: TADOQuery;
    qListaMovimentosCaixavlMovimento: TBCDField;
    qListaMovimentosCaixadsTipoMovimento: TStringField;
    QImpressaoBoletocd_cadastro: TIntegerField;
    qBuscaContribuintecd_cadastro: TIntegerField;
    qImpressaoPagamentoscd_cadastro: TIntegerField;
    qNoCabepacd_cadastro: TIntegerField;
    qListaPagBeneficiocd_cadastro: TIntegerField;
    qListaJubiladoscd_cadastro: TIntegerField;
    qListaArquivoMortocd_cadastro: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    function formatarValorDecimal(valor:string):string;
    function imprimeBoleto(codigo,mes,ano : integer):Boolean;
    function getDemonstrativoPagamento(codigo,mes,ano,tipo : integer) : TStringList;
    //function imprimeVariosBoletos(codigo,mes,ano : integer):Boolean;
    function getMesExtenso(numero :integer) : String;
    function TestaCPF(Dado:String):String;
    function validaCPF(Dado:string):Boolean;
    function formataData(dataPTBr:string):string;
    procedure testeImpressao();
    function imprimeCaixa(idCaixa:integer):Boolean;
  private
    { Private declarations }
    Vnome : String;
    Vlogin : string;
    VidLogin : smallint;
    Vperfil : Smallint;
    Vimpressora : TImpressoraMatricial;
    procedure setNome(const value : String);
    procedure setPerfil(const value : Smallint);
    procedure setImpressora(const print :tImpressoraMatricial);
    procedure setLogin (const value : String);
    procedure setidLogin (const value : smallint);
  public
    { Public declarations }
    property nome : String read Vnome write setNome;
    property perfil : Smallint read Vperfil write setPerfil;
    property impressora : tImpressoraMatricial read Vimpressora write setImpressora;
    property login : string read Vlogin write setLogin;
    property idLogin : smallint read VidLogin write setidLogin;
  end;

var
  DM: TDM;
  


implementation

uses fnLib1;


{$R *.dfm}

procedure TDM.DataModuleCreate(Sender: TObject);
var
  arquivo,linha : string;
  F : TextFile;
begin

  ADOConn.Close;
  arquivo := 'config.ini';

  AssignFile(F,arquivo);
  Reset(F);
  Readln(F,linha);
  //
  ADOConn.ConnectionString := linha;

  Readln(F,linha);
  impressora := tImpressoraMatricial.Create;


  impressora.PrinterName := linha;
  impressora.Impressora1 := linha;

  ADOConn.Connected := true;
  CloseFile(F);
end;

procedure TDM.DataModuleDestroy(Sender: TObject);
begin
  ADOConn.Connected := false;
end;

function TDM.formataData(dataPTBr: string): string;
var
  partes : TStringList;
begin
  partes := TStringList.Create;
  partes.Delimiter := '/';
  partes.DelimitedText := dataPTBr;

  result := partes[2]+'-'+partes[1]+'-'+partes[0];
end;

function TDM.formatarValorDecimal(valor: string): string;
var
  i:integer;
  v: string;
begin

  for i:=1 to length(valor) do
  begin
    if valor[i] = ',' then
      v := v+'.'
    else
      v := v+valor[i];
  end;

  result := v;
end;

function TDM.getDemonstrativoPagamento(codigo, mes, ano, tipo: integer): TStringList;
var
  lista : TStringList;
  valor : double;
begin

  lista := TStringList.Create;

  QDescricaoContrib.close;
  
  QDescricaoContrib.Parameters.ParamByName('codigo').Value := codigo;
  QDescricaoContrib.Parameters.ParamByName('mes').Value := mes;
  QDescricaoContrib.Parameters.ParamByName('ano').Value := ano;
  QDescricaoContrib.Parameters.ParamByName('tipo').Value := tipo;

  QDescricaoContrib.Open;
  QDescricaoContrib.First;

  while not QDescricaoContrib.Eof do
  begin
    lista.Add(inttostr(qDescricaoContrib.FieldByName('dt_mesRefContribuicao').Value)+'/'+
             inttostr(QDescricaoContrib.FieldByName('dt_anoRefContribuicao').Value)+'       '+
             myPad(QDescricaoContrib.FieldByName('nm_tipoContribuicao').Value,19,'L',false)+
                myPad(FormatFloat('#,##0.00',QDescricaoContrib.FieldByName('vl_ContribuicaoVencimento').Value),11,'R',true));

    valor := valor + QDescricaoContrib.FieldByName('vl_ContribuicaoVencimento').Value;

    QDescricaoContrib.Next;
  end;

  //lista.Add('--------------------------------------------');
  //lista.Add('VALOR TOTAL DA CONTRIBUICAO     '+myPad(FormatFloat('#,##0.00',valor),12,'R',true));

  result := lista;  
end;


function TDM.getMesExtenso(numero : Integer): String;
var
  mes : String;
begin

  case numero of
    1 : mes := 'JANEIRO';
    2 : mes := 'FEVEREIRO';
    3 : mes := 'MAR�O';
    4 : mes := 'ABRIL';
    5 : mes := 'MAIO';
    6 : mes := 'JUNHO';
    7 : mes := 'JULHO';
    8 : mes := 'AGOSTO';
    9 : mes := 'SETEMBRO';
    10 : mes := 'OUTUBRO';
    11 : mes := 'NOVEMBRO';
    12 : mes := 'DEZEMBRO';
  end;
  result := mes;
end;

function TDM.imprimeBoleto(codigo, mes, ano: integer): Boolean;
var
  valor : double;
  impressao : TextFile;
  obs : Boolean;
begin

  QImpressaoBoleto.Close;
  QImpressaoBoleto.Parameters.ParamByName('codigo').Value := codigo;
  QImpressaoBoleto.Parameters.ParamByName('mes').Value := mes;
  QImpressaoBoleto.Parameters.ParamByName('ano').Value := ano;

  QDescricaoContribuicao.Close;
  QDescricaoContribuicao.Parameters.ParamByName('codigo').Value := codigo;
  QDescricaoContribuicao.Parameters.ParamByName('mes').Value := mes;
  QDescricaoContribuicao.Parameters.ParamByName('ano').Value := ano;

  QImpressaoBoleto.Open;
  QImpressaoBoleto.First;
  QDescricaoContribuicao.Open;
  QDescricaoContribuicao.First;

  obs := false;

  with impressora do
  begin
    Iniciaimpressao;


    imprimeTexto('======== ASSOCIACAO CASA DO PASTOR =========');
//    imprimeTexto('CNPJ: 04.967.386/0001-66');
    imprimeTexto('ROD. MARIO COVAS - PASS. SUELY No.02  -  UNA');
//    imprimeTexto('R. PR. JOAO TRIGUEIRO,N.3 CEP 66.6520170 UNA');
    imprimeTexto('=========== DEMOSTRATIVO DE DOACAO =========');
    imprimeTexto('DATA '+FormatDateTime('dd/mm/yyyy',date));
//    imprimeTexto('NO. CABEPA: '+INTTOSTR(CODIGO));
    imprimeTexto('NOME PASTOR:');
    imprimeTexto(QImpressaoBoletonm_pastor.Value);


    imprimeTexto('REFERENCIA    DESCRICAO                VALOR');
    imprimeTexto('--------------------------------------------');

    valor := 0;
    while not QDescricaoContribuicao.Eof do
    begin
      imprimeTexto(inttostr(QDescricaoContribuicaodt_mesRefContribuicao.Value)+'/'+
               inttostr(QDescricaoContribuicaodt_anoRefContribuicao.Value)+'       '+
               myPad(QDescricaoContribuicaonm_tipoContribuicao.Value,19,'L',false)+
                  myPad(FormatFloat('#,##0.00',QDescricaoContribuicaovl_ContribuicaoVencimento.Value),11,'R',true));
      valor := valor + QDescricaoContribuicaovl_ContribuicaoVencimento.Value;

      QDescricaoContribuicao.Next;
    end;
    imprimeTexto('--------------------------------------------');
    imprimeTexto('VALOR TOTAL DA DOACAO         '+myPad(FormatFloat('#,##0.00',valor),12,'R',true));
    imprimeTexto('');
    imprimeTexto('FORMA DE PAGAMENTO');
    imprimeTexto('DESCRICAO                              VALOR');
    imprimeTexto('--------------------------------------------');
    valor := 0;
    while not QImpressaoBoleto.Eof do
    begin
      imprimeTexto(myPad(QImpressaoBoletods_formPagto.Value,27,'L',false)+'     '+
                  myPad(FormatFloat('#,##0.00',QImpressaoBoletovl_pagtoContribuicao.Value),12,'R',true));
      valor := valor + QImpressaoBoletovl_pagtoContribuicao.Value;

      if QImpressaoBoletods_formPagto.Value = 'CHEQUE-PRE' then
        obs := true;
      
      QImpressaoBoleto.Next;
    end;
    imprimeTexto('--------------------------------------------');
    imprimeTexto('VALOR TOTAL DO DOACAO         '+myPad(FormatFloat('#,##0.00',valor),12,'R',true));

    imprimeTexto('');
    if obs then
    begin
      imprimeTexto('OBS.:');
      imprimeTexto('OS  VALORES  PAGOS  COM  CHEQUES-PRE DATADOS');
      imprimeTexto('SERAO  CONSIDERADOS  QUITADOS  SOMENTE  APOS');
      imprimeTexto('COMPENSACAO DE TODOS OS CHEQUES RELACIONADOS');
      imprimeTexto('NO DEMONSTRATIVO DE DOACAO.');
    end;

    imprimeTexto('');



    imprimeTexto('Impresso por: '+nome);
    imprimeTexto('');
    imprimeTexto('');
    imprimeTexto(myPad('__________________________________',44,'C',false));
    imprimeTexto(myPad('Assinatura',44,'C',false));
    imprimeTexto('');
    imprimeTexto('');
    imprimeTexto('');
    imprimeTexto('');
    imprimeTexto('');
    imprimeTexto('');
    imprimeTexto('');
    imprimeTexto('');
    imprimeTexto('');
    imprimeTexto('');

    FinalizaImpressao
  end;



  result := true;
end;



function TDM.imprimeCaixa(idCaixa: integer): Boolean;
var
  vlEntrada, vlSaida, vlTotal : Double;
begin
{Impress�o de caixa}

  with impressora do
  begin
    Iniciaimpressao;
    imprimeTexto('======== ASSOCIACAO CASA DO PASTOR =========');
//    imprimeTexto('CNPJ: 04.967.386/0001-66');
    imprimeTexto('ROD. MARIO COVAS - PASS. SUELY No.02  -  UNA');
//    imprimeTexto('R. PR. JOAO TRIGUEIRO,N.3 CEP 66.6520170 UNA');
//    imprimeTexto('FONE/FAX:(91) 3245-1739 CEP:66.652-210 BELEM');
    imprimeTexto('============= IMPRESSAO DE CAIXA ===========');
    imprimeTexto('DATA '+FormatDateTime('dd/mm/yyyy',date));
    imprimeTexto('NO. CAIXA: '+INTTOSTR(idCaixa));

    // Impirmir entrada
    imprimeTexto('================== ENTRADAS =================');
    imprimeTexto('DESCRICAO                    VALOR');
    imprimeTexto('--------------------------------------------');

    qListaMovimentosCaixa.Close;
    qListaMovimentosCaixa.Parameters.ParamByName('idCaixa').Value := idCaixa;
    qListaMovimentosCaixa.Parameters.ParamByName('tipo').Value := 'E';
    qListaMovimentosCaixa.Open;

    vlEntrada := 0 ;
    while not qListaMovimentosCaixa.Eof do
    begin
      ImprimeTexto(myPad(qListaMovimentosCaixadsTipoMovimento.value,28,'L',true)+'  '+
        myPad(FormatFloat('#,##0.00',qListaMovimentosCaixavlMovimento.value),15,'R',false));
      vlEntrada := vlEntrada + qListaMovimentosCaixavlMovimento.Value;
      qListaMovimentosCaixa.Next;
    end;
    imprimeTexto('---------------------------------------------');
    imprimeTexto('Total:                        '+myPad(FormatFloat('#,##0.00',vlEntrada),15,'R',false));
    imprimeTexto('');

    imprimeTexto('=================== SAIDAS ==================');
    imprimeTexto('DESCRICAO                    VALOR');
    imprimeTexto('---------------------------------------------');

    qListaMovimentosCaixa.Close;
    qListaMovimentosCaixa.Parameters.ParamByName('idCaixa').Value := idCaixa;
    qListaMovimentosCaixa.Parameters.ParamByName('tipo').Value := 'S';
    qListaMovimentosCaixa.Open;

    while not qListaMovimentosCaixa.Eof do
    begin
      ImprimeTexto(myPad(qListaMovimentosCaixadsTipoMovimento.value,28,'L',true)+'  '+
        myPad(FormatFloat('#,##0.00',qListaMovimentosCaixavlMovimento.value),15,'R',false));

      vlSaida := vlSaida + qListaMovimentosCaixavlMovimento.Value;
      qListaMovimentosCaixa.Next;
    end;
    imprimeTexto('---------------------------------------------');
    imprimeTexto('Total:                        '+myPad(FormatFloat('#,##0.00',vlSaida),15,'R',false));
    imprimeTexto('');

    imprimeTexto('=================== RESUMO ==================');
    ImprimeTexto('ENTRADAS:                     '+
        myPad(FormatFloat('#,##0.00',vlEntrada),15,'R',false));

    ImprimeTexto('SAIDAS:                       '+
        myPad(FormatFloat('#,##0.00',vlEntrada),15,'R',false));
    imprimeTexto('---------------------------------------------');

    ImprimeTexto('SALDO:                        '+
        myPad(FormatFloat('#,##0.00',(vlEntrada-vlSaida)),15,'R',false));
    ImprimeTexto('');
    ImprimeTexto('');
    ImprimeTexto('');
    ImprimeTexto('');
    ImprimeTexto('');
    FinalizaImpressao;
  end;

end;

procedure TDM.setidLogin(const value: smallint);
begin
  VidLogin := value;
end;

procedure TDM.setImpressora(const print: tImpressoraMatricial);
begin
  Vimpressora := print;
end;

procedure TDM.setLogin(const value: String);
begin
  Vlogin := value;
end;

procedure TDM.setNome(const value: String);
begin
  Vnome := value;
end;

procedure TDM.setPerfil(const value: Smallint);
begin
  Vperfil := value;
end;

function TDM.TestaCPF(Dado: String): String;
var
  i: integer;

begin

  for i:= 1 to length(Dado) do begin
    if not (Dado[i] in ['0'..'9']) then delete(Dado,i,1);
  end;

  if length(Dado) = 11 then begin

     if validaCPF(Dado) then
     begin
        insert('-',Dado,10);
        insert('.',Dado,7);
        insert('.',Dado,4);
    end
    else
      Dado := '0';

  end;
  Result := Dado;

end;

procedure TDM.testeImpressao;
begin
//
  impressora.Iniciaimpressao;
  impressora.ImprimeTexto('Teste de Impressao em 40 colunas');
  impressora.ImprimeTexto('Teste de Impressao em 39 colunas');
  impressora.ImprimeTexto('Teste de Impressao em 38 colunas');
  impressora.ImprimeTexto('Teste de Impressao em 37 colunas');
  impressora.ImprimeTexto('Teste de Impressao em 36 colunas');
  impressora.ImprimeTexto('Teste de Impressao em 35 colunas');
  impressora.ImprimeTexto('Teste de Impressao em 34 colunas');

  impressora.ImprimeTexto('');
  impressora.ImprimeTexto('');
  impressora.ImprimeTexto('');
  impressora.ImprimeTexto('');
  impressora.ImprimeTexto('');
  impressora.ImprimeTexto('');

  impressora.FinalizaImpressao;


end;

function TDM.validaCPF(Dado: string): Boolean;
var
  D1: array[1..9] of byte;
  I,
  DF1,
  DF2,
  DF3,
  DF4,
  DF5,
  DF6,
  Resto1,
  Resto2,
  PrimeiroDigito,
  SegundoDigito : integer;

begin
     Result := true;

     if Length(Dado) = 11 then
     begin
           for I := 1 to 9 do
                if Dado[I] in ['0'..'9'] then
                     D1[I] := StrToInt(Dado[I])
                else
                     Result := false;
           if Result then
           begin
                DF1 := 0;
                DF2 := 0;
                DF3 := 0;
                DF4 := 0;
                DF5 := 0;
                DF6 := 0;
                Resto1 := 0;
                Resto2 := 0;
                PrimeiroDigito := 0;
                SegundoDigito := 0;
                DF1 := 10*D1[1] + 9*D1[2] + 8*D1[3] + 7*D1[4] + 6*D1[5] + 5*D1[6] +
                       4*D1[7] + 3*D1[8] + 2*D1[9];
                DF2 := DF1 div 11;
                DF3 := DF2 * 11;
                Resto1 := DF1 - DF3;
                if (Resto1 = 0) or (Resto1 = 1) then
                     PrimeiroDigito := 0
                else
                     PrimeiroDigito := 11 - Resto1;

                DF4 := 11*D1[1] + 10*D1[2] + 9*D1[3] + 8*D1[4] + 7*D1[5] + 6*D1[6] +
                       5*D1[7] + 4*D1[8] + 3*D1[9] + 2*PrimeiroDigito;

               DF5 := DF4 div 11;
                DF6 := DF5 * 11;
                Resto2 := DF4 - DF6;
                if (Resto2 = 0) or (Resto2 = 1) then
                     SegundoDigito := 0
                else
                     SegundoDigito := 11 - Resto2;

               if (PrimeiroDigito <> StrToInt(Dado[10])) or
                   (SegundoDigito <> StrToInt(Dado[11])) then
                     Result := false;
           end;
      end
      else
           if Length(Dado) <> 0 then
                Result := false;

end;

end.
