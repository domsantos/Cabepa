unit Unit_AtualizaRequerimentoBeneficio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, DB, Buttons;

type
  Tfrm_AtualizaRequerimentoBeneficio = class(TForm)
    edtContribuinte: TEdit;
    lblCodigo: TLabel;
    Label1: TLabel;
    lblNomeContribuinte: TLabel;
    Label2: TLabel;
    edtAno: TEdit;
    edtNumRec: TEdit;
    Label3: TLabel;
    cbSituacaoRec: TDBLookupComboBox;
    Label4: TLabel;
    dsSituacaoRec: TDataSource;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure edtContribuinteKeyPress(Sender: TObject; var Key: Char);
    procedure edtAnoKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumRecKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_AtualizaRequerimentoBeneficio: Tfrm_AtualizaRequerimentoBeneficio;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_AtualizaRequerimentoBeneficio.BitBtn1Click(Sender: TObject);
begin
  dm.ADOConn.BeginTrans;


  dm.qUpdate.close;
  dm.qUpdate.SQL.Clear;
  dm.qUpdate.SQL.Add('UPDATE tbRequerimentoBeneficio');
  dm.qUpdate.SQL.Add(' set cd_sitRequerimento = '+inttostr(cbSituacaoRec.KeyValue));
  dm.qUpdate.SQL.Add(' where no_reqBeneficio = '+edtNumRec.Text);
  dm.qUpdate.SQL.Add(' and dt_anoRequerimento = '+edtAno.Text);
  dm.qExecSpCalculaValorBEneficio.Parameters.ParamByName('codigo').Value :=
        strtoint(edtContribuinte.Text);

  dm.qExecSpCalculaValorBEneficio.Open;

      showmessage(
          dm.qExecSpCalculaValorBEneficio.FieldByName('vl_beneficio').Value);

  try
    if cbSituacaoRec.KeyValue = 2 then
    begin


     if dm.qExecSpCalculaValorBEneficio.FieldByName('nr_anoscontribuicao').AsInteger < 3 then
      begin
        showmessage('Tempo de contribuição inferior a 5 anos');
        exit;
      end;

      dm.qUpdate.ExecSQL;

      dm.qInsertValBeneficio.Parameters.ParamByName('ano').Value := edtAno.Text;
      dm.qInsertValBeneficio.Parameters.ParamByName('numrec').Value := edtNumRec.Text;
      dm.qInsertValBeneficio.Parameters.ParamByName('data').Value := date;
      dm.qInsertValBeneficio.Parameters.ParamByName('valor').Value :=
      dm.qExecSpCalculaValorBEneficio.FieldByName('vl_beneficio').Value;

      dm.qInsertValBeneficio.ExecSQL;


    end;
    dm.ADOConn.CommitTrans;
    showmessage('Requerimento atualizado');

  Except
    dm.ADOConn.RollbackTrans;
    showmessage('Erro ao atualizar requerimento!');
  end;


end;

procedure Tfrm_AtualizaRequerimentoBeneficio.edtAnoKeyPress(Sender: TObject;
  var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_AtualizaRequerimentoBeneficio.edtContribuinteKeyPress(
  Sender: TObject; var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_AtualizaRequerimentoBeneficio.edtNumRecKeyPress(Sender: TObject;
  var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_AtualizaRequerimentoBeneficio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.qSituacaoRequerimento.close;
  self := nil;
  Action := caFree;
end;

procedure Tfrm_AtualizaRequerimentoBeneficio.FormCreate(Sender: TObject);
begin
  dm.qSituacaoRequerimento.Open;
end;

end.
