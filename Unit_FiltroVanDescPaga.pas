unit Unit_FiltroVanDescPaga;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, DBCtrls, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons;

type
  Tfrm_FiltroVanDescPaga = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    qListaPessoas: TADOQuery;
    dsLista: TDataSource;
    edtMes: TEdit;
    edtAno: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    Rubrica: TLabel;
    qListaPessoasnm_beneficiado: TStringField;
    qListaPessoasvl_Rubrica: TBCDField;
    qListaPessoasnm_Rubrica: TStringField;
    qListaRubricas: TADOQuery;
    qListaRubricascd_tipRubrica: TSmallintField;
    qListaRubricasds_rubrica: TStringField;
    qListaRubricasno_Rubrica: TSmallintField;
    DataSource1: TDataSource;
    cbRubrica: TDBLookupComboBox;
    BitBtn2: TBitBtn;
    qListaPessoascd_cadastro: TSmallintField;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure edtMesKeyPress(Sender: TObject; var Key: Char);
    procedure edtAnoKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_FiltroVanDescPaga: Tfrm_FiltroVanDescPaga;

implementation

uses Unit_DM, Unit_RelVanDescPago, Unit_RelVantagemDescontoPagos;

{$R *.dfm}

procedure Tfrm_FiltroVanDescPaga.BitBtn1Click(Sender: TObject);
begin

  qListaPessoas.Close;

  qListaPessoas.Parameters.ParamByName('mes').Value := edtMes.Text;
  qListaPessoas.Parameters.ParamByName('Ano').Value := edtAno.Text;
  qListaPessoas.Parameters.ParamByName('rubrica').Value := qListaRubricascd_tipRubrica.Value;

  qListaPessoas.Open;

end;

procedure Tfrm_FiltroVanDescPaga.BitBtn2Click(Sender: TObject);
begin

  Application.CreateForm(Tfrm_RelVantagemDescontoPagos,frm_RelVantagemDescontoPagos);
  frm_RelVantagemDescontoPagos.lblrubrica.Caption := qListaPessoasnm_Rubrica.Value;
  frm_RelVantagemDescontoPagos.lblperiodo.Caption := edtMes.Text+' / '+edtAno.Text;
  frm_RelVantagemDescontoPagos.RLReport1.Preview();

end;

procedure Tfrm_FiltroVanDescPaga.edtAnoKeyPress(Sender: TObject; var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_FiltroVanDescPaga.edtMesKeyPress(Sender: TObject; var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_FiltroVanDescPaga.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

procedure Tfrm_FiltroVanDescPaga.FormCreate(Sender: TObject);
begin
  qListaRubricas.Open;
end;

end.
