object frm_PadraoCadastro: Tfrm_PadraoCadastro
  Left = 10
  Top = 10
  Caption = 'frm_PadraoCadastro'
  ClientHeight = 475
  ClientWidth = 712
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial Narrow'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 712
    Height = 41
    Align = alTop
    Color = clGradientActiveCaption
    ParentBackground = False
    TabOrder = 0
    object btnNovo: TBitBtn
      Left = 8
      Top = 8
      Width = 115
      Height = 30
      Caption = 'Novo (F2)'
      TabOrder = 0
      OnClick = btnNovoClick
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F7A6BD24A8B3A8836A736A7368736
        6A3690465967FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C736F428632
        C53AC536A536852E852E8532C536C53AA5326D3A5A6BFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F5A6B6832E53E853245262522252625262526252625224526852EE53E
        672E175FFF7FFF7FFF7FFF7FFF7F39676832C53A452A45264526452A45264422
        44224526452A452645264526A5368632175FFF7FFF7FFF7F9C736832C5364526
        4526652A652A452A6B36D24ED24E8C3A4526652A452A45264526A536662E5A6B
        FF7FFF7F9146C536452A452A652A652E652E642AD0469E779E77F34E4426652E
        652A652A452A4526C5366D3AFF7F9C73672E852E652A652E652E852E8532642E
        CF469D739D77F24E642A852E652E652E652E652A652A852E5967D4528532652E
        652E852E853285328532842ECF469D779D77F34E642E853285328532852E652E
        652A852E6F426D3A852E652E8532852E642E842E842E632ACF46BE77BE77F24E
        632A842E642E642A642E852E652E852E4A32482E8532853284328A3614531353
        1353F24E575FBD77BD775967F34E135313531457AD3E642E852E852E682E482A
        A736A736A432CE46DF7FDE7BDE7BDE7BBE7BBD77BD77BD77DE7BDE7BDE7BDF7F
        1353842EA736A736682E482AC83EC83EC73ACF46DF7FDE7BDE7BDE7BBE7BBD77
        BD77BE7BDE7BDE7BDE7BDF7F1353A736C83EC93E692E4A32EA42EA42E942CB3E
        124F114F114FF04E565FDE7BDE7B5863F04A114F114F1253CD42C942EA42EA42
        6A326F3E0B470B4B0B4B0A4BE946E9460947E842114FDF7FDF7F1457C83E0947
        E946E946EA460B4BEB460B476C36D556EC420C4F0C4B0C4F2C4F2C4F2C4F0B4B
        1253FF7FFF7F355BEA462C4F2C4F0C4F0C4F0C4B0C4B0C479146BD778C3A4F57
        2D4F2D532D532D532D532C4F3357FF7FFF7F355B0B4B2D532D532D532D4F2D4F
        4E53AC3E7B6FFF7FB24E304F4F572E534E574E574E572D533357FF7FFF7F365B
        0C4F4F574E572E532E534F5751576F3EFF7FFF7FDE7B6D3A735F505B4F57505B
        505B4F5BEF4A12531253EF464F57505B505B5057505B735F6D369C73FF7FFF7F
        FF7F7B6F6D3694637363715B715B715F715F705B705B715F715F715B515B725F
        95678D3A5967FF7FFF7FFF7FFF7FFF7F9B6F8F3E745FB66B94677263725F725F
        725F725F725F9463B66B95638E3E5967FF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B
        B34EAF42755FB76BB86FB86FB76FB86FB76B7563D046B24ABD73FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77165BB146AF42CF42CF42AF429046D556
        9C73FF7FFF7FFF7FFF7FFF7FFF7F}
    end
    object btnSalvar: TBitBtn
      Left = 124
      Top = 8
      Width = 115
      Height = 30
      Caption = 'Salvar (F3)'
      TabOrder = 1
      OnClick = btnSalvarClick
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F7A6BD3528C3E8836A736A7368836
        8C3ED3527A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73B14A8736
        C53AC53AA536A532A532A536C53AC53A8736B14A9C73FF7FFF7FFF7FFF7FFF7F
        FF7FFF7F59676A3AC53AA536652A252625222522252225222526652AA536C53A
        6A3A5967FF7FFF7FFF7FFF7FFF7F38638836E53A652A2526452645264526452A
        452A4526452625262526652AC53A68363863FF7FFF7FFF7F9C6F4936C536452A
        4526452A652A652A44264526652A652A652A452A452A45264526C53649369C6F
        FF7FFF7FB14AA536652A452A652A652E652E43268B3AD34E662A652E652E652E
        652A652A4526652AA532B14AFF7F7A6B8732852E452A652E652E852E63268A36
        7A6B9E77AC3A642A8532852E652E652E652E452A852E67327A6BB34EA52E652E
        652E852E8532632A8B367B6BBD779D77385F652A843285328532852E652E652E
        652A852EB34E6C3A8532652E85328532632A8B3A7B6FBD77BD779D779E77D046
        632AA536853285328532652E652E852E6C3A6932853285328532842EAD3E7C6F
        BE7BBD777A679C73BE7B7B6B8836A332A536A53685328532652E852E69326832
        A736A636A536652E385FDF7FDF7B355B662E355BDE7BDF7B1557842EC436A536
        A536A532A636A736682E8932C93EC83EC83EA636EF4ABE7B15578532A232A93A
        9D73DE7BBE77CD42A332C63EC73EC83EC83AC93A69326A32EA42E942E946E946
        CA3ED046C93AE742E842C63AF24EDF7FDF7F7B6BAA3AE842EA46E942C942EA42
        6A326D3A0B470B4B0B4B0B4B0A47C83E0A4B0B4B0B4B2A4BC93E7967FF7FFF7F
        365BC93A0A4BEB46EB460B476D3AB34E0C472C4F0C4B0C4F2C4F2C4F2C4F2C4F
        2C4F2C4F0A4BCE46BD73FF7FFF7F1353EA420C4B0C4FEC46B34E7A6BAD3E4F57
        2D4F2D4F2D532D532D532D532D532D534D57EB46124FFF7FFF7F1453EB422D4F
        4F57AD3E7A6BFF7FB24A30534F5B2E534E574E574E574E574E574E574E574E57
        EC4635571457ED4A2E5350573053B24AFF7FFF7F9B6F6D3A9363505B4F575057
        4F5B4F5B4F5B6F5B4F5B705B6F5BED460D4B4F57715B935F6D3A9B6FFF7FFF7F
        FF7F3863AE42B5679363515B505B715B715F715F715F715F715B705B515B9363
        B567AE423863FF7FFF7FFF7FFF7FFF7F58638F42755FB66F94677263725F725F
        725F725F72639467B66F755F8F425863FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C6F
        B24AF14A9663B86FD86FB76FB76FB86FB86F9663D14AB24A9C6FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F7A6BD452B146D14AF24EF24ED14AB146D452
        7A6BFF7FFF7FFF7FFF7FFF7FFF7F}
    end
    object btnPesquisar: TBitBtn
      Left = 240
      Top = 8
      Width = 115
      Height = 30
      Caption = 'Pesquisar (F4)'
      TabOrder = 2
      OnClick = btnPesquisarClick
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FDE7BDE7BBD77BD779C739C739C737C6F7C6F7C6F7C6F7C6F
        9C739C739C73BD77BD77BD77DE7BFF7FFF7FFF7F9C73B5560F42EF41EF419256
        925A91567156715671567156915691569156925A925AF6627B737C6B9D6BFF7F
        FF7F4A2D410C410C62104B5A6C626B626B626B5E4B5E4B5E4B5E4B5E4A5E4A5E
        4A5EB0663A5F151A982EFF7FFF7F29296210621083144C5E8C626C626C626C62
        6C5E6C5E6C5E6B5E4B5E4B5E4A624F4A351E5726FB42FF7FFF7F492962106210
        83144C5E8D668C626C626C626C626C626B626B626B5E6B624D52141E5722DB36
        BE6FFF7FFF7F492D6210621083146C5E8D668D628D628C626C626C628F5EB35A
        B35A905E332A371ABA329D67FF7FFF7FFF7F492D83148210A3186C5E8D668D66
        8C626C628E66D45EF75AF65AD65AF75AD752B9361057DC7BFF7FFF7FFF7F6A31
        A3188314A41C6D62AD668C66D16A9A7B5A6BF75E596BBC777873F466F862F562
        2673DB7FFF7FFF7FFF7F6A31A418A318C41C6D628D66136FDD7F146F18631663
        356F5673BC7B9977F4661963546BFF7FFF7FFF7FFF7F8A35C41CA418E4208D62
        CF6ABC7BF26E577339635A6FDD7B9B7757735773146B3967B946DE77FF7FFF7F
        FF7F8B35C520C41CE5248D62126F99771473DE7B39677C6FDE7F7877DD7B7777
        156B5A6B783ABE73FF7FFF7FFF7FAB35E520E52005258D62347379773573BC7B
        166B5B6BBD7757739B7B9A73386B5B67362ABE73FF7FFF7FFF7FAB390525E520
        06298D66F26E9B7BD06ADE7FF26E586F7B6B596B386B7B6F7B73B35A1622BE73
        FF7FFF7FFF7FAC39062506252629AE66CF6EBB7B126F3573DC7B9A777B73596F
        9C73596FD06A8F5ED94EFF7FFF7FFF7FFF7FCC3D26290625272DAE66CE6EF16E
        DD7B136FD06AF06EAE6A346FDD7BCF6AAD6A4C666B5DDE7BFF7FFF7FFF7FCC3D
        27292629472DAE66CF6ECE6AF16E9A779B7B9A77BB7B9A77F16EAD6ACF6A4C66
        83547B7BFF7FFF7FFF7FCD3D272D2729472DAE6ACF6ECF6ECE6ECE6AF06EF16E
        D06EAE6AAE6ACE6ACF6A4C6683547B7BFF7FFF7FFF7FED41472D272D4831AF6A
        EF6ECF6ECF6ECF6ECF6ECE6ECE6ECF6ECF6ECF6ACF6E4C6683507B7BFF7FFF7F
        FF7FED41472D472D6831CF6AEF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6E
        CF6E6D6A6B61BD7BFF7FFF7FFF7FED41482D472D6831CF6AEF6ECF6ECF6ECF6E
        CF6ECF6ECF6ECF6ECF6ECF6ECF6E126FFF7FFF7FFF7FFF7FFF7FED4548314831
        6835CF6AF072F06EF06EF06EF06EF06EF06ED06ECF6ECF6ECF6E1273FF7FFF7F
        FF7FFF7FFF7FF6622F4A304A504E357355775577557755775577557755775577
        557755773573997BFF7FFF7FFF7F}
    end
    object btnApagar: TBitBtn
      Left = 355
      Top = 8
      Width = 115
      Height = 30
      Caption = 'Apagar (F5)'
      TabOrder = 3
      OnClick = btnApagarClick
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FDE7BDE7BBD77BD779C739C739C737C6F7C6F7C6F7C6F9C6F
        9C739C739C73BD77BD77BD77DE7BFF7FFF7FFF7F9C73B5560F42EF41EF419256
        925A91567156715691567156CE4D7156925A925A925A17631967396FFF7FFF7F
        FF7F4A2D410C410C62104B5A6C626B626B626B624B5EC348003CC3484A5E4B5E
        6B5E2D5AC748003C3162FF7FFF7F29296210621083144C5E8C626C626C628C62
        A955003C21400140C4486B5EEA594244214021408444FF7FFF7F492962106210
        83144C5E8D668C626C628C626C62E54C014042442144C44C8348214421444244
        1062FF7FFF7F492D6210621083146C5E8D668D628D626C628C626C62E5502144
        424822484248424842481062FF7FFF7FFF7F492D83148210A3186C5E8D668D66
        8C626C62AE66CF66AF6AE6542248424C424C2248A65DDB7BFF7FFF7FFF7F6A31
        A3188314A41C6D62AD668C66D16A9A77BC7BDC7B3873A450424C42504250424C
        4459BB7BFF7FFF7FFF7F6A31A418A318C41C6D628D66136FDD7F136FF06A4C62
        8350424C4250425042506350424C4B59DE7FFF7FFF7F8A35C41CA418E4208D62
        CF6ABC7BF26E99773873834C224C425042546961075D4254635042508C5DFF7F
        FF7F8B35C520C41CE5248D62126F9977346FFE7F0C62224C42504254316EF16A
        8D66075D42544250C654FF7FFF7FAB35E520E52005258D62347379773573BC7B
        8D66695D85548961787757738C666E5E094DA45CB56EFF7FFF7FAB390525E520
        06298D66F26E9B7BD06ADE7FF16EAE6ABD7F3573136F9A778C6A8F5A17265B6F
        FF7FFF7FFF7FAC39062506252629AE66CF6EBB7B126F3573DD7B9A77FE7F5673
        567357738C6A8F62D94EFF7FFF7FFF7FFF7FCC3D26290625272DAE66CE6EF16E
        DD7B136FD06AF06EAE6A346FDD7BD06AAE6A4C666B5DDE7BFF7FFF7FFF7FCC3D
        27292629472DAE66CF6ECE6AF16E9A779B7B9A77BB7B9A77F16EAD6ACF6A4C66
        83547B7BFF7FFF7FFF7FCD3D272D2729472DAE6ACF6ECF6ECE6ECE6AF06EF16E
        D06EAE6AAE6ACE6ACF6A4C6683547B7BFF7FFF7FFF7FED41472D272D4831AF6A
        EF6ECF6ECF6ECF6ECF6ECE6ECE6ECF6ECF6ECF6ACF6E4C6683507B7BFF7FFF7F
        FF7FED41472D472D6831CF6AEF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6E
        CF6E6D6A6B61BD7BFF7FFF7FFF7FED41482D472D6831CF6AEF6ECF6ECF6ECF6E
        CF6ECF6ECF6ECF6ECF6ECF6ECF6E126FFF7FFF7FFF7FFF7FFF7FED4548314831
        6835CF6AF072F06EF06EF06EF06EF06EF06ED06ECF6ECF6ECF6E1273FF7FFF7F
        FF7FFF7FFF7FF6622F4A304A504E357355775577557755775577557755775577
        557755773573997BFF7FFF7FFF7F}
    end
    object btnRelatorio: TBitBtn
      Left = 471
      Top = 8
      Width = 115
      Height = 30
      Caption = 'Relat'#243'rio (F6)'
      TabOrder = 4
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73D756F85E7B6BBE77FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73
        9229D004CE0C2E1DD135754A195F7C6FDE77FF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7F9D7392295701D300340D5511B100AF04CD0C4F21D135954E
        195F9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77B32999019901F30479425D67
        BB4E3836961D340DD100CF04CD0CF239BD77FF7FFF7FFF7FFF7FFF7FBE77D331
        B901FC019901340D99469D6F5D673C631C5FDB527942F72976192E1DF85EFF7F
        FF7FFF7FFF7FFF7F563EBA011D02FB019901340D9A4A9D6F5D673C631B5BFB56
        DB52DA52BB4A163A754AFF7FFF7FFF7FFF7FFF7FF91D3E021C02FC01B9013411
        BA4A9E737D6B1C5B9B46BB4A9B46BA4ABA4A373E754AFF7FFF7FFF7FFF7FFF7F
        F91D5E021D021C02BA015511DB4EBE779E6F7252E94DEB51314A583EDB4E573E
        754EFF7FFF7FFF7FFF7FFF7F191E5E023D021D02BA01120D984ADF779E6F1767
        F662935A2B56964EDC525742754EFF7FFF7FFF7FFF7FFF7F191E5F023E023D02
        DB016B003442FF7F9E739E6F7E675D631C5BFB56FB565746754EFF7FFF7FFF7F
        FF7FFF7F1A1E7F025E023E02DB016C00563EDF779E6F9D6F7D6B5C675D631C5F
        1C577746754EFF7FFF7FFF7FFF7FFF7F1A1E7F025F025E02FB01593A3E57FC4A
        DB46FC4A1D531D53984AB752FB56994EB756FF7FFF7FFF7FFF7FFF7F1A229F06
        5F023E02FE36FF7FDF737E635D5F3D573F575936EE0CED0C0F11B22D7B6BFF7F
        FF7FFF7FFF7FFF7F1A229F0E7F025F029E22BE6FFF7FBE6F5E637F63BB4A173A
        7A427A3EB71D7025BD77FF7FFF7FFF7FFF7FFF7F3A26BF127F0A7F021D027B36
        BE775E5B1E57DC529946DB4EDA4EBA4E192E90259D73FF7FFF7FFF7FFF7FFF7F
        3A26DF1A9F0E7F063E02BB3A7D6BFC4A7A3E574299469A46BA4EBA4E392E9125
        9D73FF7FFF7FFF7FFF7FFF7F3A2ADF1E9F169F0A3E02DC3EBE777D6F8E5E075A
        C94D0D4AB94EDB523A2E91259D73FF7FFF7FFF7FFF7FFF7F3A2AFF26BF1A9F12
        5E02DD3EDE7B9E6F5B6B3B671A5FD75AFA56DB565B3291259D73FF7FFF7FFF7F
        FF7FFF7F3A2EFF2ADF22BF1A7F061E43FE7FBE779E6F7D6B5D633C5FFB5AFB56
        7B3291259D73FF7FFF7FFF7FFF7FFF7F5A2E1F33DF22BF1A9F0EFE327F5F7E5B
        7E5F5D5B5D5F3C5B1C5B1C5BBD36B2259D73FF7FFF7FFF7FFF7FFF7F5A2E3F3F
        3F435F4F7E5F5E5B3D531D4F1D4FFC4ADC429B3A5A2A1A1EDB11353EDE7BFF7F
        FF7FFF7FFF7FFF7FDA4E58425842584258425842373E373A373E373A373A5842
        B94EFA5A5B6BDE77FF7FFF7FFF7F}
    end
    object btnCancelar: TBitBtn
      Left = 586
      Top = 8
      Width = 115
      Height = 30
      Caption = 'Cancelar (F7)'
      TabOrder = 5
      OnClick = btnCancelarClick
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F5A6F515A4951E654C558C558E654
        2951EF51396BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73CE4DA458
        83648360835C84588458835C8360836484588C4D5A6BFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F5A6B07518368835C834C834883488348834883488348834C83588368
        A550F762FF7FFF7FFF7FFF7FFF7F3967E650836083508348834C834C834C834C
        834C834C834C834C8348834C8360A454F75EFF7FFF7FFF7F9C73074D8360834C
        834C834C6248834C834C834C834C834C834C6248834C834C8348835CC5505A6B
        FF7FFF7F1052835C834C834C834C83488C51A44C83508350835083508348AC51
        A448834C834C834C835C8C4DFF7F9C73C5508354834C8350844C315ADE73B562
        A44C83508354834C525EDE739462A44C834C834C83508454396B945A8358834C
        835062506A51BD739C73BD73D666844C624C725EBD739C73DE77AD51624C8350
        834C8358EF4D8C4D8354835083508354634C0F5ABD779C73DE779462315EDD77
        9C73DE77725E834C8350835083508354284DE64C845483508354835483546250
        0F5ADE77BD77BD77DE77BD77DE77736283508354835483508350A450E64CC648
        C554C554A4548354835883586254EE59BD77BD77BD77DE77515E625083588354
        8354A454C554C554E64CE6480759E758E758E658C558845C6354525EDE77BD77
        BD77DE7BB46684508358C558C658E75807590759E74C2849285D285D285D2861
        2861E6589362FF7BDE7BDE7BDE7BDE7BFF7BF76A075528612861285D285D285D
        284DCE4D496149614961496128599462FF7FDE7BFF7F72620F5EFF7BDE7BFF7F
        F76A295949614961495D4A616B4DB55A6A5D8B656B654A61AC55FF7BFF7FFF7F
        9466295D28615162FF7BFF7FFF7FCE59495D8B656B618B613152BD776B51CD69
        AC65AC696A613162DE7B94664A61AC6DAC6D4A655262DE7B93666A61AC69AC65
        CD696A555A6FFF7F5256EE65EE69CD69CE6D8C65CD598B61CD71CD6DCD6DCE71
        8B65CD598B61CE6DCD69CE690F6ACE4DFF7FFF7FDE7BAC4D5272106EEF6D0F6E
        CE6DEF71EF71EF71EF71EF710F72CE6DEF6DEF6D0F6E52726B4D9C73FF7FFF7F
        FF7F7B6F8C4D94727272106E30723072307230723072307230723072106E5272
        B4768C51396BFF7FFF7FFF7FFF7FFF7F7B73CD51936ED67A9376527251725176
        5176517252727376D576B472AD51396BFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B
        5256CE55946EF776177BF77AF77A177BF77AB572EF5931529C77FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77B5621056CE55CE59CE59CE550F52945A
        9C73FF7FFF7FFF7FFF7FFF7FFF7F}
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 712
    Height = 434
    Align = alClient
    Color = clWindow
    Enabled = False
    ParentBackground = False
    TabOrder = 1
  end
  object CDSPrincipal: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPPrincipal'
    Left = 640
    Top = 185
  end
  object ActionList1: TActionList
    Left = 640
    Top = 249
    object acNovo: TAction
      Caption = 'acNovo'
      ShortCut = 113
      OnExecute = acNovoExecute
    end
    object acSalvar: TAction
      Caption = 'acSalvar'
      ShortCut = 114
      OnExecute = acSalvarExecute
    end
    object acPesquisar: TAction
      Caption = 'acPesquisar'
      ShortCut = 115
      OnExecute = acPesquisarExecute
    end
    object acApagar: TAction
      Caption = 'acApagar'
      ShortCut = 116
      OnExecute = acApagarExecute
    end
    object acRelatorio: TAction
      Caption = 'acRelatorio'
      ShortCut = 117
      OnExecute = acRelatorioExecute
    end
    object acCancelar: TAction
      Caption = 'acCancelar'
      ShortCut = 118
      OnExecute = acCancelarExecute
    end
    object acSair: TAction
      Caption = 'acSair'
      ShortCut = 121
      OnExecute = acSairExecute
    end
  end
  object DSPPrincipal: TDataSetProvider
    DataSet = QPrincipal
    Left = 640
    Top = 121
  end
  object QPrincipal: TADOQuery
    Connection = DM.ADOConn
    Parameters = <>
    Left = 640
    Top = 57
  end
end
