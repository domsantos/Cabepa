unit Unit_SituacaoRequerimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, ADODB, Provider, ActnList, DB, DBClient, StdCtrls,
  Buttons, ExtCtrls, Grids, DBGrids, Mask, DBCtrls;

type
  Tfrm_SituacaoRequerimento = class(Tfrm_PadraoCadastro)
    QPrincipalcd_sitRequerimento: TSmallintField;
    QPrincipalds_sitRequerimento: TStringField;
    CDSPrincipalcd_sitRequerimento: TSmallintField;
    CDSPrincipalds_sitRequerimento: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure btnPesquisarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_SituacaoRequerimento: Tfrm_SituacaoRequerimento;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_SituacaoRequerimento.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Open;
end;

end.
