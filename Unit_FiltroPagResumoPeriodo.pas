unit Unit_FiltroPagResumoPeriodo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, StdCtrls, Buttons, ExtCtrls;

type
  Tfrm_FiltroPagResumoPeriodo = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    edtMes: TEdit;
    edtAno: TEdit;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    dsListagem: TDataSource;
    qListagem: TADOQuery;
    DBGrid1: TDBGrid;
    qListagemcd_tipRubrica: TSmallintField;
    qListagemno_Rubrica: TSmallintField;
    qListagemnm_Rubrica: TStringField;
    qListagemqt_beneficiados: TIntegerField;
    qListagemvl_vantagens: TBCDField;
    qListagemvl_descontos: TBCDField;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn2Click(Sender: TObject);
    procedure edtMesKeyPress(Sender: TObject; var Key: Char);
    procedure edtAnoKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_FiltroPagResumoPeriodo: Tfrm_FiltroPagResumoPeriodo;

implementation

uses Unit_DM, Unit_RelPagResumoPeriodo;

{$R *.dfm}

procedure Tfrm_FiltroPagResumoPeriodo.BitBtn1Click(Sender: TObject);
begin
  qListagem.Close;

  qListagem.Parameters.ParamByName('mes').Value := edtMes.Text;
  qListagem.Parameters.ParamByName('ano').Value := edtAno.Text;

  qListagem.Open;

end;

procedure Tfrm_FiltroPagResumoPeriodo.BitBtn2Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_RelPagResumoPeriodo,frm_RelPagResumoPeriodo);


  frm_RelPagResumoPeriodo.RLLabel7.Caption := edtMes.Text+' / '+edtAno.Text;
  frm_RelPagResumoPeriodo.RLReport1.Preview();
end;

procedure Tfrm_FiltroPagResumoPeriodo.edtAnoKeyPress(Sender: TObject;
  var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_FiltroPagResumoPeriodo.edtMesKeyPress(Sender: TObject;
  var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_FiltroPagResumoPeriodo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

end.
