object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 499
  Width = 777
  object ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=passwd;Persist Security Info=True;U' +
      'ser ID=sa;Initial Catalog=CASADOPASTOR_DB;Data Source=HAL9001'
    IsolationLevel = ilReadCommitted
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 112
    Top = 8
  end
  object QImpressaoBoleto: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'mes'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'ano'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      'select'
      #9'p.cd_cadastro'
      #9',t.nm_pastor'
      #9',p.id_PagtoContribuincao'
      #9',p.vl_pagtoContribuicao'
      #9',p.dt_mesRefContribuicao'
      #9',p.dt_anoRefContribuicao'
      #9',f.ds_formPagto'
      #9',tp.nm_tipoContribuicao'
      'from'
      #9'tbPagtoContribuicao p'
      #9'inner join tbContribuinte t on p.cd_cadastro = t.cd_cadastro'
      #9'inner join tbFormaPagto f on p.cd_formPagto = f.cd_formPagto'
      
        #9'inner join tbTipoContribuicao tp on p.cd_tipoContribuicao = tp.' +
        'cd_tipoContribuicao'
      'where p.cd_cadastro = :codigo'
      #9'and p.dt_mesRefContribuicao = :mes'
      #9'and p.dt_anoRefContribuicao = :ano'
      'order by'
      #9'p.id_PagtoContribuincao, p.cd_tipoContribuicao, f.cd_formPagto')
    Left = 264
    Top = 8
    object QImpressaoBoletonm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object QImpressaoBoletoid_PagtoContribuincao: TAutoIncField
      FieldName = 'id_PagtoContribuincao'
      ReadOnly = True
    end
    object QImpressaoBoletovl_pagtoContribuicao: TBCDField
      FieldName = 'vl_pagtoContribuicao'
      Precision = 10
      Size = 2
    end
    object QImpressaoBoletodt_mesRefContribuicao: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
    end
    object QImpressaoBoletodt_anoRefContribuicao: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
    end
    object QImpressaoBoletods_formPagto: TStringField
      FieldName = 'ds_formPagto'
      Size = 40
    end
    object QImpressaoBoletonm_tipoContribuicao: TStringField
      FieldName = 'nm_tipoContribuicao'
      Size = 50
    end
    object QImpressaoBoletocd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object qAutenticacao: TADOQuery
    Connection = ADOConn
    Parameters = <>
    Left = 32
    Top = 8
  end
  object QDescricaoContribuicao: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'mes'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ano'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'
      #9'c.vl_ContribuicaoVencimento'
      #9',t.nm_tipoContribuicao'
      '                ,c.dt_mesRefContribuicao'
      #9',c.dt_anoRefContribuicao'
      'from tbContribuicoes c'
      
        'inner join tbTipoContribuicao t on c.cd_tipoContribuicao = t.cd_' +
        'tipoContribuicao'
      'where c.cd_cadastro = :codigo'
      #9'and c.dt_mesRefContribuicao = :mes'
      #9'and c.dt_anoRefContribuicao = :ano')
    Left = 264
    Top = 64
    object QDescricaoContribuicaovl_ContribuicaoVencimento: TBCDField
      FieldName = 'vl_ContribuicaoVencimento'
      Precision = 10
      Size = 2
    end
    object QDescricaoContribuicaonm_tipoContribuicao: TStringField
      FieldName = 'nm_tipoContribuicao'
      Size = 50
    end
    object QDescricaoContribuicaodt_mesRefContribuicao: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
    end
    object QDescricaoContribuicaodt_anoRefContribuicao: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
    end
  end
  object qLogin: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'login'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 25
        Value = Null
      end
      item
        Name = 'senha'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select nm_usuario,id_perfil,nm_login,id_usuario from tbUsuario'
      'where nm_login = :login and vl_senha = :senha')
    Left = 264
    Top = 128
    object qLoginnm_usuario: TStringField
      FieldName = 'nm_usuario'
      Size = 100
    end
    object qLoginid_perfil: TIntegerField
      FieldName = 'id_perfil'
    end
    object qLoginnm_login: TStringField
      FieldName = 'nm_login'
      Size = 25
    end
    object qLoginid_usuario: TAutoIncField
      FieldName = 'id_usuario'
      ReadOnly = True
    end
  end
  object qBuscaContribuinte: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cd_cadastro'
      '      ,cd_formTeologica'
      '      ,cd_natPastor'
      '      ,cd_escPastor'
      '      ,cd_nacPastor'
      '      ,cd_estCivil'
      '      ,cd_categoria'
      '      ,nm_pastor'
      '      ,no_regConv'
      '      ,dt_nascPastor'
      '      ,tp_sanguineo'
      '      ,no_regGeral'
      '      ,no_cpf'
      '      ,ds_endereco'
      '      ,ds_compEndPastor'
      '      ,no_cepPastor'
      '      ,dt_filiacao'
      '      ,nm_pai'
      '      ,nm_mae'
      '      ,nm_conjuge'
      '      ,dt_nascConjuge'
      '      ,no_fone'
      '      ,dt_batismo'
      '      ,ds_localBatismo'
      '      ,dt_autEvangelista'
      '      ,dt_consagEvangelista'
      '      ,dt_ordenacPastor'
      '      ,ds_localConsagracao'
      '      ,ds_campo'
      '      ,ds_supervisao'
      '      ,no_certCasamento'
      '      ,ds_orgaoemissorrg'
      '      ,dt_emissao'
      '      ,ds_bairro'
      '      ,ds_uf'
      '      ,ds_cidade'
      '  FROM tbContribuinte'
      'where '
      '   cd_cadastro =:codigo')
    Left = 264
    Top = 192
    object qBuscaContribuintecd_formTeologica: TIntegerField
      FieldName = 'cd_formTeologica'
    end
    object qBuscaContribuintecd_natPastor: TIntegerField
      FieldName = 'cd_natPastor'
    end
    object qBuscaContribuintecd_escPastor: TIntegerField
      FieldName = 'cd_escPastor'
    end
    object qBuscaContribuintecd_nacPastor: TIntegerField
      FieldName = 'cd_nacPastor'
    end
    object qBuscaContribuintecd_estCivil: TIntegerField
      FieldName = 'cd_estCivil'
    end
    object qBuscaContribuintecd_categoria: TIntegerField
      FieldName = 'cd_categoria'
    end
    object qBuscaContribuintenm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qBuscaContribuinteno_regConv: TIntegerField
      FieldName = 'no_regConv'
    end
    object qBuscaContribuintedt_nascPastor: TDateTimeField
      FieldName = 'dt_nascPastor'
    end
    object qBuscaContribuintetp_sanguineo: TStringField
      FieldName = 'tp_sanguineo'
      FixedChar = True
    end
    object qBuscaContribuinteno_regGeral: TStringField
      FieldName = 'no_regGeral'
      FixedChar = True
    end
    object qBuscaContribuinteno_cpf: TStringField
      FieldName = 'no_cpf'
      FixedChar = True
      Size = 14
    end
    object qBuscaContribuinteds_endereco: TStringField
      FieldName = 'ds_endereco'
      Size = 60
    end
    object qBuscaContribuinteds_compEndPastor: TStringField
      FieldName = 'ds_compEndPastor'
      Size = 40
    end
    object qBuscaContribuinteno_cepPastor: TStringField
      FieldName = 'no_cepPastor'
      FixedChar = True
      Size = 8
    end
    object qBuscaContribuintedt_filiacao: TDateTimeField
      FieldName = 'dt_filiacao'
    end
    object qBuscaContribuintenm_pai: TStringField
      FieldName = 'nm_pai'
      Size = 60
    end
    object qBuscaContribuintenm_mae: TStringField
      FieldName = 'nm_mae'
      Size = 60
    end
    object qBuscaContribuintenm_conjuge: TStringField
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object qBuscaContribuintedt_nascConjuge: TDateTimeField
      FieldName = 'dt_nascConjuge'
    end
    object qBuscaContribuinteno_fone: TStringField
      FieldName = 'no_fone'
      Size = 15
    end
    object qBuscaContribuintedt_batismo: TDateTimeField
      FieldName = 'dt_batismo'
    end
    object qBuscaContribuinteds_localBatismo: TStringField
      FieldName = 'ds_localBatismo'
      Size = 40
    end
    object qBuscaContribuintedt_autEvangelista: TDateTimeField
      FieldName = 'dt_autEvangelista'
    end
    object qBuscaContribuintedt_consagEvangelista: TDateTimeField
      FieldName = 'dt_consagEvangelista'
    end
    object qBuscaContribuintedt_ordenacPastor: TDateTimeField
      FieldName = 'dt_ordenacPastor'
    end
    object qBuscaContribuinteds_localConsagracao: TStringField
      FieldName = 'ds_localConsagracao'
      Size = 40
    end
    object qBuscaContribuinteds_campo: TStringField
      FieldName = 'ds_campo'
      Size = 40
    end
    object qBuscaContribuinteds_supervisao: TStringField
      FieldName = 'ds_supervisao'
      Size = 40
    end
    object qBuscaContribuinteno_certCasamento: TIntegerField
      FieldName = 'no_certCasamento'
    end
    object qBuscaContribuinteds_orgaoemissorrg: TStringField
      FieldName = 'ds_orgaoemissorrg'
      Size = 30
    end
    object qBuscaContribuintedt_emissao: TDateTimeField
      FieldName = 'dt_emissao'
    end
    object qBuscaContribuinteds_bairro: TStringField
      FieldName = 'ds_bairro'
      Size = 40
    end
    object qBuscaContribuinteds_uf: TStringField
      FieldName = 'ds_uf'
      Size = 50
    end
    object qBuscaContribuinteds_cidade: TStringField
      FieldName = 'ds_cidade'
      Size = 50
    end
    object qBuscaContribuintecd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object qDescricaoContrib: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'mes'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ano'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'tipo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'
      #9'c.vl_ContribuicaoVencimento'
      #9',t.nm_tipoContribuicao'
      '                ,c.dt_mesRefContribuicao'
      #9',c.dt_anoRefContribuicao'
      'from tbContribuicoes c'
      
        'inner join tbTipoContribuicao t on c.cd_tipoContribuicao = t.cd_' +
        'tipoContribuicao'
      'where c.cd_cadastro = :codigo'
      #9'and c.dt_mesRefContribuicao = :mes'
      #9'and c.dt_anoRefContribuicao = :ano'
      '               and c.cd_tipoContribuicao = :tipo')
    Left = 368
    Top = 8
    object BCDField1: TBCDField
      FieldName = 'vl_ContribuicaoVencimento'
      Precision = 10
      Size = 2
    end
    object StringField1: TStringField
      FieldName = 'nm_tipoContribuicao'
      Size = 50
    end
    object IntegerField1: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
    end
    object IntegerField2: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
    end
  end
  object qImpressaoPagamentos: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'mes'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'ano'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'tipo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'
      #9'p.cd_cadastro '
      #9',t.nm_pastor'
      #9',p.id_PagtoContribuincao'
      #9',p.vl_pagtoContribuicao'
      #9',p.dt_mesRefContribuicao'
      #9',p.dt_anoRefContribuicao'
      #9',f.ds_formPagto'
      #9',tp.nm_tipoContribuicao'
      'from'
      #9'tbPagtoContribuicao p'
      #9'inner join tbContribuinte t on p.cd_cadastro  = t.cd_cadastro '
      #9'inner join tbFormaPagto f on p.cd_formPagto = f.cd_formPagto'
      
        #9'inner join tbTipoContribuicao tp on p.cd_tipoContribuicao = tp.' +
        'cd_tipoContribuicao'
      'where p.cd_cadastro  = :codigo'
      #9'and p.dt_mesRefContribuicao = :mes'
      #9'and p.dt_anoRefContribuicao = :ano'
      '                and p.cd_tipoContribuicao = :tipo'
      'order by'
      #9'p.id_PagtoContribuincao, p.cd_tipoContribuicao, f.cd_formPagto')
    Left = 384
    Top = 64
    object qImpressaoPagamentosnm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qImpressaoPagamentosid_PagtoContribuincao: TAutoIncField
      FieldName = 'id_PagtoContribuincao'
      ReadOnly = True
    end
    object qImpressaoPagamentosvl_pagtoContribuicao: TBCDField
      FieldName = 'vl_pagtoContribuicao'
      Precision = 10
      Size = 2
    end
    object qImpressaoPagamentosdt_mesRefContribuicao: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
    end
    object qImpressaoPagamentosdt_anoRefContribuicao: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
    end
    object qImpressaoPagamentosds_formPagto: TStringField
      FieldName = 'ds_formPagto'
      Size = 40
    end
    object qImpressaoPagamentosnm_tipoContribuicao: TStringField
      FieldName = 'nm_tipoContribuicao'
      Size = 50
    end
    object qImpressaoPagamentoscd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object spAtualizaPagamento: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'sp_atualizaPagamento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@codigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@mes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 64
    Top = 128
  end
  object spVerificaPagamentoChequePre: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'sp_verificaPagamentoChequePre'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@vcodigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@vmes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@vano'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 192
  end
  object qTipoBeneficio: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [cd_tipoBeneficio]'
      '      ,[ds_tipoBeneficio]'
      '  FROM [CABEPA_DB].[dbo].[tbTipoBeneficio]'
      'ORDER BY [ds_tipoBeneficio]')
    Left = 376
    Top = 136
    object qTipoBeneficiocd_tipoBeneficio: TSmallintField
      FieldName = 'cd_tipoBeneficio'
      ReadOnly = True
    end
    object qTipoBeneficiods_tipoBeneficio: TStringField
      FieldName = 'ds_tipoBeneficio'
    end
  end
  object spInserePedidoBeneficio: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'sp_InserePedidoBeneficio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@vCodigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@vTipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@vNome'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@vCPF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@vAno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end>
    Left = 64
    Top = 248
  end
  object qSituacaoRequerimento: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [cd_sitRequerimento]'
      '      ,[ds_sitRequerimento]'
      '  FROM [tbSituacaoRequerimento]')
    Left = 376
    Top = 192
    object qSituacaoRequerimentocd_sitRequerimento: TSmallintField
      FieldName = 'cd_sitRequerimento'
      ReadOnly = True
    end
    object qSituacaoRequerimentods_sitRequerimento: TStringField
      FieldName = 'ds_sitRequerimento'
    end
  end
  object qTempoContribuicao: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select sum(vl_ContribuicaoVencimento) valor_total,'
      #9#9'   COUNT(*) qtd_contribuicao,'
      #9#9'   convert(int,(COUNT(*) /12)) anos_contribuicao'
      #9'from tbContribuicoes'
      #9'where cd_CABEPA = :codigo')
    Left = 264
    Top = 248
    object qTempoContribuicaovalor_total: TBCDField
      FieldName = 'valor_total'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qTempoContribuicaoqtd_contribuicao: TIntegerField
      FieldName = 'qtd_contribuicao'
      ReadOnly = True
    end
    object qTempoContribuicaoanos_contribuicao: TIntegerField
      FieldName = 'anos_contribuicao'
      ReadOnly = True
    end
  end
  object qUpdate: TADOQuery
    Connection = ADOConn
    Parameters = <>
    Left = 376
    Top = 248
  end
  object qExecSpCalculaValorBEneficio: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'exec sp_calculaValorBeneficio'
      #9'@vCodigo = :codigo')
    Left = 64
    Top = 312
    object qExecSpCalculaValorBEneficiovl_beneficio: TBCDField
      FieldName = 'vl_beneficio'
      ReadOnly = True
      Precision = 19
    end
    object qExecSpCalculaValorBEneficiovl_media_contribuicao: TBCDField
      FieldName = 'vl_media_contribuicao'
      ReadOnly = True
      Precision = 19
    end
    object qExecSpCalculaValorBEneficioqt_contribuicao: TIntegerField
      FieldName = 'qt_contribuicao'
      ReadOnly = True
    end
    object qExecSpCalculaValorBEneficionr_percentual: TFloatField
      FieldName = 'nr_percentual'
      ReadOnly = True
    end
    object qExecSpCalculaValorBEneficionr_anoscontribuicao: TSmallintField
      FieldName = 'nr_anoscontribuicao'
      ReadOnly = True
    end
    object qExecSpCalculaValorBEneficiovl_totalcontribuicao: TBCDField
      FieldName = 'vl_totalcontribuicao'
      ReadOnly = True
      Precision = 19
    end
    object qExecSpCalculaValorBEneficiovl_minimo: TBCDField
      FieldName = 'vl_minimo'
      ReadOnly = True
      Precision = 19
    end
  end
  object qInsertValBeneficio: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ano'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'numrec'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'data'
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'valor'
        Attributes = [paSigned]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Size = 19
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO  tbValBeneficio '
      '           ( dt_anoRequerimento '
      '           , no_reqBeneficio '
      '           , dt_Valor '
      '           , vl_Beneficio )'
      '     VALUES'
      '           ( :ano'
      '           , :numrec'
      '           , :data'
      '           , :valor)')
    Left = 376
    Top = 304
  end
  object qFormaPagamento: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [cd_formPagto]'
      '      ,[ds_formPagto]'
      '  FROM [tbFormaPagto]'
      'order by [cd_formPagto]')
    Left = 264
    Top = 304
    object qFormaPagamentocd_formPagto: TSmallintField
      FieldName = 'cd_formPagto'
      ReadOnly = True
    end
    object qFormaPagamentods_formPagto: TStringField
      FieldName = 'ds_formPagto'
      Size = 40
    end
  end
  object spRegistraPagamentoLote: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'sp_registraPagamentoPeriodo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@codigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@valor'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@dini'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 10
        Value = Null
      end
      item
        Name = '@dfim'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 10
        Value = Null
      end
      item
        Name = '@tipo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@forma'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@banco'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@agencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@cheque'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@data'
        Attributes = [paNullable]
        DataType = ftDateTime
        Size = 10
        Value = Null
      end
      item
        Name = '@dtpgto'
        DataType = ftDateTime
        Value = Null
      end>
    Left = 64
    Top = 368
  end
  object qDiferencaDatas: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'dini'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'dfim'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select DATEDIFF(MONTH, :dini, :dfim)+1 as meses')
    Left = 464
    Top = 8
    object qDiferencaDatasmeses: TIntegerField
      FieldName = 'meses'
      ReadOnly = True
    end
  end
  object spCalculoBeneficioMes: TADOStoredProc
    Connection = ADOConn
    ProcedureName = 'sp_calculaValorBeneficioMes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@codigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@mes'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@ano'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 56
    Top = 72
  end
  object qListaPagBeneficio: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '
      #9'cd_cadastro,'
      #9'case cd_sitJubilamento'
      #9#9'when 1 then nm_pastor'
      #9#9'when 2 then nm_conjuge'
      #9'end nm_beneficiado,'
      #9'case cd_sitJubilamento'
      #9#9'when 1 then '#39'BENEFICIO'#39
      #9#9'when 2 then '#39'PENSAO'#39
      #9'end tp_beneficio,'
      #9'dt_falecimento'
      'from tbContribuinte'
      'where cd_sitJubilamento in (1,2)'
      'order by 2')
    Left = 560
    Top = 72
    object qListaPagBeneficiocd_cadastro: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'cd_cadastro'
    end
    object qListaPagBeneficionm_beneficiado: TStringField
      DisplayLabel = 'Beneficiado'
      FieldName = 'nm_beneficiado'
      ReadOnly = True
      Size = 60
    end
    object qListaPagBeneficiotp_beneficio: TStringField
      DisplayLabel = 'Beneficio'
      FieldName = 'tp_beneficio'
      ReadOnly = True
      Size = 9
    end
    object qListaPagBeneficiodt_falecimento: TWideStringField
      DisplayLabel = 'Data Falecimento Conjuge'
      FieldName = 'dt_falecimento'
      Visible = False
      Size = 10
    end
  end
  object qGetCodigo: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'exec sp_getCodigo')
    Left = 472
    Top = 120
    object qGetCodigocd_codigo: TIntegerField
      FieldName = 'cd_codigo'
      ReadOnly = True
    end
  end
  object RLPreviewSetup1: TRLPreviewSetup
    DialogPrint = False
    Left = 472
    Top = 224
  end
  object RLPrintDialogSetup1: TRLPrintDialogSetup
    Copies = 0
    Left = 472
    Top = 280
  end
  object RLPDFFilter1: TRLPDFFilter
    DocumentInfo.Creator = 
      'FortesReport(Open Source) v3.24(BETA13) \251 Copyright '#169' 1999-20' +
      '07 Fortes Inform'#225'tica'
    ViewerOptions = []
    FontEncoding = feNoEncoding
    DisplayName = 'Documento PDF'
    Left = 472
    Top = 176
  end
  object qListaBancos: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_banco,nm_banco'
      'from tbBancos'
      'order by nm_banco')
    Left = 472
    Top = 344
    object qListaBancoscd_banco: TAutoIncField
      FieldName = 'cd_banco'
      ReadOnly = True
    end
    object qListaBancosnm_banco: TStringField
      FieldName = 'nm_banco'
      Size = 40
    end
  end
  object qTrocaSenha: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'senha'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'update tbUsuario set vl_senha = :senha'
      'where id_usuario = :id')
    Left = 264
    Top = 360
  end
  object qCheckUsuario: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select vl_senha from tbUsuario'
      'where id_usuario = :id')
    Left = 376
    Top = 360
    object qCheckUsuariovl_senha: TStringField
      FieldName = 'vl_senha'
    end
  end
  object qValorMinimo: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select vl_salMin from tbSalarioMinimo'
      
        'where dt_anoSalMin = (select MAX(dt_anoSalMin) from tbSalarioMin' +
        'imo)')
    Left = 560
    Top = 8
    object qValorMinimovl_salMin: TBCDField
      FieldName = 'vl_salMin'
      Precision = 19
    end
  end
  object qExecSpSimulaCalculoDIzimo: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'valor'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = 10000c
      end>
    SQL.Strings = (
      'EXEC'#9'[dbo].[sp_SimulaCalculoDizimo]'
      #9'@valor = :valor')
    Left = 64
    Top = 432
    object qExecSpSimulaCalculoDIzimovl_acumulado: TBCDField
      FieldName = 'vl_acumulado'
      Precision = 19
    end
    object qExecSpSimulaCalculoDIzimovl_indice: TBCDField
      FieldName = 'vl_indice'
      Precision = 5
      Size = 3
    end
    object qExecSpSimulaCalculoDIzimovl_base: TBCDField
      FieldName = 'vl_base'
      Precision = 19
    end
    object qExecSpSimulaCalculoDIzimovl_dizimo: TBCDField
      FieldName = 'vl_dizimo'
      ReadOnly = True
      Precision = 19
    end
  end
  object qNoCabepa: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select max(cd_cadastro )+1 cd_cadastro  from tbcontribuinte')
    Left = 560
    Top = 128
    object qNoCabepacd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
      ReadOnly = True
    end
  end
  object qAumentoSalario: TADOQuery
    Connection = ADOConn
    Parameters = <>
    Left = 560
    Top = 184
  end
  object qListaJubilados: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      #9'distinct c.cd_cadastro ,'
      #9'c.nm_pastor,'
      #9'c.nm_conjuge,'
      
        #9'(select MIN(r.dt_Requerimento) from tbRequerimentoBeneficio r w' +
        'here r.cd_cadastro  = c.cd_cadastro ) as dtJubilacao,'
      #9'c.cd_sitJubilamento,'
      #9'case c.cd_sitJubilamento'
      #9#9'when 1 then '#39'JUBILA'#199#195'O'#39
      #9#9'WHEN 2 THEN '#39'AUXILIO'#39
      #9'end as situacao'
      'from tbContribuinte c'
      'where c.cd_sitJubilamento <> 0'
      'order by c.cd_sitJubilamento, c.nm_pastor')
    Left = 560
    Top = 416
    object qListaJubiladosnm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qListaJubiladosnm_conjuge: TStringField
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object qListaJubiladosdtJubilacao: TDateTimeField
      FieldName = 'dtJubilacao'
      ReadOnly = True
    end
    object qListaJubiladoscd_sitJubilamento: TStringField
      FieldName = 'cd_sitJubilamento'
      FixedChar = True
      Size = 1
    end
    object qListaJubiladossituacao: TStringField
      FieldName = 'situacao'
      ReadOnly = True
      Size = 11
    end
    object qListaJubiladoscd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object qListaArquivoMorto: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT cd_cadastro'
      '      ,cd_formTeologica'
      '      ,cd_natPastor'
      '      ,cd_escPastor'
      '      ,cd_nacPastor'
      '      ,cd_estCivil'
      '      ,cd_categoria'
      '      ,nm_pastor'
      '      ,no_regConv'
      '      ,dt_nascPastor'
      '      ,tp_sanguineo'
      '      ,no_regGeral'
      '      ,no_cpf'
      '      ,ds_endereco'
      '      ,ds_compEndPastor'
      '      ,no_cepPastor'
      '      ,dt_filiacao'
      '      ,nm_pai'
      '      ,nm_mae'
      '      ,nm_conjuge'
      '      ,dt_nascConjuge'
      '      ,no_fone'
      '      ,dt_batismo'
      '      ,ds_localBatismo'
      '      ,dt_autEvangelista'
      '      ,dt_consagEvangelista'
      '      ,dt_ordenacPastor'
      '      ,ds_localConsagracao'
      '      ,ds_campo'
      '      ,ds_supervisao'
      '      ,no_certCasamento'
      '      ,ds_orgaoemissorrg'
      '      ,dt_emissao'
      '      ,ds_bairro'
      '      ,ds_uf'
      '      ,ds_cidade'
      '      ,cd_sitJubilamento'
      '      ,dt_falecimento'
      '      ,cd_banco'
      '      ,st_arquivoMorto'
      '  FROM tbContribuinte'
      'where st_arquivoMorto = 1'
      'order by nm_pastor')
    Left = 568
    Top = 248
    object qListaArquivoMortonm_pastor: TStringField
      DisplayLabel = 'Nome Contribuinte'
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qListaArquivoMortodt_nascPastor: TDateTimeField
      DisplayLabel = 'Dat. Nasc.'
      FieldName = 'dt_nascPastor'
    end
    object qListaArquivoMortono_regGeral: TStringField
      DisplayLabel = 'No. RG'
      FieldName = 'no_regGeral'
      FixedChar = True
    end
    object qListaArquivoMortono_cpf: TStringField
      DisplayLabel = 'No. CPF'
      FieldName = 'no_cpf'
      FixedChar = True
      Size = 14
    end
    object qListaArquivoMortonm_conjuge: TStringField
      DisplayLabel = 'Nome Conjuge'
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object qListaArquivoMortodt_nascConjuge: TDateTimeField
      DisplayLabel = 'Dat. Nasc.'
      FieldName = 'dt_nascConjuge'
    end
    object qListaArquivoMortono_fone: TStringField
      DisplayLabel = 'Fone'
      FieldName = 'no_fone'
      Size = 15
    end
    object qListaArquivoMortocd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object RLXLSFilter1: TRLXLSFilter
    DisplayName = 'Planilha Excel'
    Left = 576
    Top = 352
  end
  object qAfastarDesconto: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      'select a.*,c.nm_pastor,v.nm_Rubrica'
      'from tbAfastarDesconto a'
      'inner join tbContribuinte c on c.cd_CABEPA = a.cd_cabepa'
      'inner join tbVantDesc v on v.cd_tipRubrica = a.no_tipRubrica')
    Left = 256
    Top = 416
  end
  object qListaMovimentosCaixa: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'tipo'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'idCaixa'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      '  m.vlMovimento,'
      '  t.dsTipoMovimento'
      'FROM '
      '  tbMovimentoCaixa m'
      
        'inner join tbTipoMovimento t on t.cdTipoMovimento = m.cdTipoMovi' +
        'mento'
      'where m.tpMovimento = :tipo and m.idCaixa = :idCaixa')
    Left = 656
    Top = 88
    object qListaMovimentosCaixavlMovimento: TBCDField
      FieldName = 'vlMovimento'
      Precision = 19
    end
    object qListaMovimentosCaixadsTipoMovimento: TStringField
      FieldName = 'dsTipoMovimento'
      Size = 50
    end
  end
end
