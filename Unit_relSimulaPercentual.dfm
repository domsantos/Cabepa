inherited frm_RelSimulaAumentoPercentual: Tfrm_RelSimulaAumentoPercentual
  Caption = 'frm_RelSimulaAumentoPercentual'
  ExplicitWidth = 862
  ExplicitHeight = 633
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 227
        Caption = 'Simula'#231#227'o de Aumento por Percentual'
        ExplicitWidth = 227
      end
      object ValorRef: TRLLabel
        Left = 96
        Top = 54
        Width = 121
        Height = 16
        Caption = 'Valor de Refer'#234'ncia:'
      end
      object Percentual: TRLLabel
        Left = 416
        Top = 52
        Width = 70
        Height = 16
        Caption = 'Percentual:'
      end
    end
    inherited RLBand2: TRLBand
      inherited RLLabel3: TRLLabel
        Width = 91
        Caption = 'BENEFICIADO'
        ExplicitWidth = 91
      end
      inherited RLLabel4: TRLLabel
        Left = 403
        Top = 28
        Width = 93
        Caption = 'VALOR ATUAL'
        ExplicitLeft = 403
        ExplicitTop = 28
        ExplicitWidth = 93
      end
      object RLLabel8: TRLLabel
        Left = 565
        Top = 28
        Width = 150
        Height = 16
        Caption = 'VALOR COM AUMENTO'
      end
    end
    inherited RLBand4: TRLBand
      Top = 232
      Height = 41
      Borders.Sides = sdCustom
      Borders.DrawTop = True
      ExplicitTop = 232
      ExplicitHeight = 41
    end
    inherited RLBand3: TRLBand
      inherited RLDBText1: TRLDBText
        Width = 65
        DataField = 'nm_pastor'
        ExplicitWidth = 65
      end
      inherited RLDBText2: TRLDBText
        Left = 425
        Top = 3
        Width = 72
        Alignment = taRightJustify
        DataField = 'vl_Beneficio'
        DisplayMask = 'R$ #,##0.00'
        ExplicitLeft = 425
        ExplicitTop = 3
        ExplicitWidth = 72
      end
      object RLDBText3: TRLDBText
        Left = 620
        Top = 3
        Width = 69
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_aumento'
        DataSource = dsRelatorio
        DisplayMask = 'R$ #,##0.00'
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 190
      Width = 718
      Height = 42
      BandType = btSummary
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = False
      object RLDBResult2: TRLDBResult
        Left = 385
        Top = 10
        Width = 111
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_Beneficio'
        DataSource = dsRelatorio
        DisplayMask = 'R$ #,##0.00'
        Info = riSum
      end
      object RLDBResult1: TRLDBResult
        Left = 581
        Top = 10
        Width = 108
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_aumento'
        DataSource = dsRelatorio
        DisplayMask = 'R$ #,##0.00'
        Info = riSum
      end
      object RLLabel6: TRLLabel
        Left = 296
        Top = 10
        Width = 49
        Height = 16
        Caption = 'TOTAIS'
      end
      object RLDBResult3: TRLDBResult
        Left = 194
        Top = 10
        Width = 46
        Height = 16
        DataField = 'nm_pastor'
        DataSource = dsRelatorio
        Info = riCount
      end
      object RLLabel7: TRLLabel
        Left = 3
        Top = 10
        Width = 175
        Height = 16
        Caption = 'No. de Beneficiados Afetados'
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = QspSimulaAumento
    Left = 432
    Top = 272
  end
  object QspSimulaAumento: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'inicio'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = Null
      end
      item
        Name = 'fim'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = Null
      end
      item
        Name = 'percentual'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 6
        Size = 19
        Value = Null
      end>
    SQL.Strings = (
      'EXEC'#9'[dbo].[sp_simulaAumento]'
      #9#9'@valor_inicio = :inicio,'
      #9#9'@valor_final = :fim,'
      #9#9'@percentual = :percentual')
    Left = 560
    Top = 272
    object QspSimulaAumentonm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object QspSimulaAumentovl_aumento: TBCDField
      FieldName = 'vl_aumento'
      ReadOnly = True
      Precision = 19
    end
    object QspSimulaAumentonr_requerimento: TIntegerField
      FieldName = 'nr_requerimento'
      ReadOnly = True
    end
    object QspSimulaAumentovl_Beneficio: TBCDField
      FieldName = 'vl_Beneficio'
      ReadOnly = True
      Precision = 19
    end
  end
end
