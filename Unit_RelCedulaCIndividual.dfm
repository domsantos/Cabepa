object frm_RelCedulaCIndividual: Tfrm_RelCedulaCIndividual
  Left = 0
  Top = 0
  Caption = 'frm_RelCedulaCIndividual'
  ClientHeight = 621
  ClientWidth = 789
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = 8
    Top = 0
    Width = 794
    Height = 1123
    DataSource = dsRelatorio
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 1044
      object RLDraw41: TRLDraw
        Left = 0
        Top = 988
        Width = 716
        Height = 53
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLDraw42: TRLDraw
        Left = 336
        Top = 988
        Width = 117
        Height = 53
      end
      object RLDraw9: TRLDraw
        Left = 1
        Top = 295
        Width = 716
        Height = 39
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLDraw1: TRLDraw
        Left = 0
        Top = 0
        Width = 359
        Height = 105
      end
      object RLDraw2: TRLDraw
        Left = 358
        Top = 0
        Width = 359
        Height = 105
      end
      object RLLabel1: TRLLabel
        Left = 3
        Top = 130
        Width = 387
        Height = 16
        Caption = '1. FONTE PAGADORA PESSOA JUR'#205'DICA OU PESSOA F'#205'SICA'
      end
      object RLDraw3: TRLDraw
        Left = 1
        Top = 152
        Width = 716
        Height = 39
      end
      object RLLabel2: TRLLabel
        Left = 3
        Top = 152
        Width = 96
        Height = 14
        Caption = 'Raz'#227'o Social/Nome'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw4: TRLDraw
        Left = 532
        Top = 152
        Width = 1
        Height = 39
      end
      object RLLabel3: TRLLabel
        Left = 532
        Top = 152
        Width = 50
        Height = 14
        Caption = 'CNPJ/CPF'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel4: TRLLabel
        Left = 3
        Top = 172
        Width = 215
        Height = 16
        Caption = 'CAIXA BENEFICENTE DO PASTOR'
      end
      object RLLabel5: TRLLabel
        Left = 539
        Top = 172
        Width = 118
        Height = 16
        Caption = '04.967.386/0001-66'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'LaBEL'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw7: TRLDraw
        Left = 1
        Top = 228
        Width = 716
        Height = 39
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel10: TRLLabel
        Left = 8
        Top = 228
        Width = 36
        Height = 14
        Caption = 'Cidade'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel11: TRLLabel
        Left = 537
        Top = 228
        Width = 45
        Height = 14
        Caption = 'Telefone'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel12: TRLLabel
        Left = 544
        Top = 248
        Width = 97
        Height = 16
        Caption = '(091) 3245-1739'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'LaBEL'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel13: TRLLabel
        Left = 8
        Top = 248
        Width = 49
        Height = 16
        Caption = 'BEL'#201'M'
      end
      object RLDraw8: TRLDraw
        Left = 532
        Top = 228
        Width = 1
        Height = 39
      end
      object RLDraw6: TRLDraw
        Left = 452
        Top = 228
        Width = 1
        Height = 39
      end
      object RLLabel8: TRLLabel
        Left = 457
        Top = 228
        Width = 16
        Height = 14
        Caption = 'UF'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel14: TRLLabel
        Left = 456
        Top = 248
        Width = 22
        Height = 16
        Caption = 'PA'
      end
      object RLLabel15: TRLLabel
        Left = 3
        Top = 274
        Width = 333
        Height = 16
        Caption = '2. PESSOA F'#205'SICA BENEFICI'#193'RIA DOS RENDIMENTO'
      end
      object RLDraw10: TRLDraw
        Left = 124
        Top = 296
        Width = 1
        Height = 39
      end
      object RLDraw11: TRLDraw
        Left = 1
        Top = 333
        Width = 716
        Height = 39
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLDraw5: TRLDraw
        Left = 1
        Top = 190
        Width = 716
        Height = 39
      end
      object RLLabel6: TRLLabel
        Left = 3
        Top = 190
        Width = 49
        Height = 14
        Caption = 'Endere'#231'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel7: TRLLabel
        Left = 3
        Top = 206
        Width = 280
        Height = 16
        Caption = 'ROD MARIO COVAS - PASS SUELY 02 - UNA'
      end
      object RLLabel9: TRLLabel
        Left = 8
        Top = 296
        Width = 22
        Height = 14
        Caption = 'CPF'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel16: TRLLabel
        Left = 129
        Top = 296
        Width = 77
        Height = 14
        Caption = 'Nome Completo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel17: TRLLabel
        Left = 8
        Top = 334
        Width = 121
        Height = 14
        Caption = 'Natureza do Rendimento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel18: TRLLabel
        Left = 8
        Top = 354
        Width = 32
        Height = 16
        Caption = '0561'
      end
      object RLLabel19: TRLLabel
        Left = 3
        Top = 378
        Width = 479
        Height = 16
        Caption = '3.RENDMENTOS TRIBUTAVEIS, DEDU'#199#213'ES E IMPORTOS RETIDO NA FONTE'
      end
      object RLDraw12: TRLDraw
        Left = 1
        Top = 396
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel20: TRLLabel
        Left = 8
        Top = 400
        Width = 258
        Height = 16
        Caption = '01.Total dos Rendimentos (Incluindo F'#233'rias)'
      end
      object RLLabel21: TRLLabel
        Left = 586
        Top = 379
        Width = 128
        Height = 16
        Caption = 'VALORES EM REAL'
      end
      object RLDraw13: TRLDraw
        Left = 574
        Top = 396
        Width = 1
        Height = 25
      end
      object RLDraw15: TRLDraw
        Left = 1
        Top = 421
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel22: TRLLabel
        Left = 8
        Top = 425
        Width = 223
        Height = 16
        Caption = '02.Contribui'#231#227'o Previd'#234'nciaria Padr'#227'o'
      end
      object RLDraw16: TRLDraw
        Left = 574
        Top = 422
        Width = 1
        Height = 25
      end
      object RLDraw18: TRLDraw
        Left = 1
        Top = 445
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel23: TRLLabel
        Left = 8
        Top = 449
        Width = 558
        Height = 16
        Caption = 
          '03.Contribui'#231#227'o a Previd'#234'ncia Prvada e ao fundo de Aposentadoria' +
          ' Programada Individual- -FAPI'
      end
      object RLDraw17: TRLDraw
        Left = 574
        Top = 446
        Width = 1
        Height = 25
      end
      object RLDraw19: TRLDraw
        Left = 1
        Top = 470
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel24: TRLLabel
        Left = 8
        Top = 474
        Width = 337
        Height = 16
        Caption = '04.Pens'#227'o Aliment'#237'cia (Informar o benef'#237'cio no campo 06)'
      end
      object RLDraw20: TRLDraw
        Left = 574
        Top = 470
        Width = 1
        Height = 25
      end
      object RLDraw21: TRLDraw
        Left = 1
        Top = 495
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel25: TRLLabel
        Left = 8
        Top = 499
        Width = 161
        Height = 16
        Caption = '05.Importo Retido na Fonte'
      end
      object RLLabel26: TRLLabel
        Left = 672
        Top = 426
        Width = 29
        Height = 16
        Caption = '0,00'
      end
      object RLLabel27: TRLLabel
        Left = 672
        Top = 473
        Width = 29
        Height = 16
        Caption = '0,00'
      end
      object RLDraw22: TRLDraw
        Left = 574
        Top = 496
        Width = 1
        Height = 25
      end
      object RLDraw14: TRLDraw
        Left = 0
        Top = 551
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel29: TRLLabel
        Left = 674
        Top = 559
        Width = 29
        Height = 16
        Caption = '0,00'
      end
      object RLDraw23: TRLDraw
        Left = 580
        Top = 552
        Width = 1
        Height = 25
      end
      object RLLabel30: TRLLabel
        Left = 6
        Top = 555
        Width = 515
        Height = 16
        Caption = 
          '01.Parte dos Proventos de aponsetadoria, Reserva,Reforma e Pens'#227 +
          'o (65 anos ou mais)'
      end
      object RLLabel31: TRLLabel
        Left = 3
        Top = 529
        Width = 305
        Height = 16
        Caption = '4. RENDIMENTOS ISENTOS E N'#195'O TRIBUT'#193'VEIS'
      end
      object RLDraw25: TRLDraw
        Left = 0
        Top = 576
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel32: TRLLabel
        Left = 674
        Top = 583
        Width = 29
        Height = 16
        Caption = '0,00'
      end
      object RLLabel33: TRLLabel
        Left = 6
        Top = 580
        Width = 159
        Height = 16
        Caption = '02.Di'#225'ria e Ajuda de Custo'
      end
      object RLDraw24: TRLDraw
        Left = 580
        Top = 577
        Width = 1
        Height = 25
      end
      object RLDraw27: TRLDraw
        Left = 0
        Top = 712
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel34: TRLLabel
        Left = 674
        Top = 715
        Width = 29
        Height = 16
        Caption = '0,00'
      end
      object RLLabel35: TRLLabel
        Left = 6
        Top = 716
        Width = 552
        Height = 15
        Caption = 
          '06.Indeniza'#231#245'es por recis'#227'o de contrato de trabalho, inclusive a' +
          ' t'#237'tulo de PDV, e acidente de trabalho'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw26: TRLDraw
        Left = 580
        Top = 713
        Width = 1
        Height = 25
      end
      object RLDraw29: TRLDraw
        Left = 0
        Top = 737
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel36: TRLLabel
        Left = 674
        Top = 744
        Width = 29
        Height = 16
        Caption = '0,00'
      end
      object RLLabel37: TRLLabel
        Left = 6
        Top = 742
        Width = 135
        Height = 16
        Caption = '07.Outros (especificar)'
      end
      object RLDraw28: TRLDraw
        Left = 580
        Top = 738
        Width = 1
        Height = 25
      end
      object RLDraw31: TRLDraw
        Left = 0
        Top = 601
        Width = 716
        Height = 44
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel38: TRLLabel
        Left = 674
        Top = 608
        Width = 29
        Height = 16
        Caption = '0,00'
      end
      object RLLabel39: TRLLabel
        Left = 7
        Top = 605
        Width = 541
        Height = 16
        Caption = 
          '03.Pens'#227'o, Proventos de Aponsentadoria ou Reforma por Mol'#233'stia G' +
          'rave e Aposentadoria ou'
      end
      object RLDraw30: TRLDraw
        Left = 580
        Top = 602
        Width = 1
        Height = 42
      end
      object RLDraw33: TRLDraw
        Left = 0
        Top = 644
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel40: TRLLabel
        Left = 674
        Top = 651
        Width = 29
        Height = 16
        Caption = '0,00'
      end
      object RLLabel41: TRLLabel
        Left = 6
        Top = 648
        Width = 574
        Height = 16
        Caption = 
          '04.Lucros e dividendos apurados a partir de 1996 pago por PJ (Lu' +
          'cro Real, Presumido ou Arbitrado'
      end
      object RLDraw32: TRLDraw
        Left = 580
        Top = 645
        Width = 1
        Height = 25
      end
      object RLDraw34: TRLDraw
        Left = 0
        Top = 669
        Width = 716
        Height = 44
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLDraw35: TRLDraw
        Left = 580
        Top = 670
        Width = 1
        Height = 42
      end
      object RLLabel43: TRLLabel
        Left = 674
        Top = 676
        Width = 29
        Height = 16
        Caption = '0,00'
      end
      object RLLabel42: TRLLabel
        Left = 6
        Top = 673
        Width = 507
        Height = 16
        Caption = 
          '05.Valores pagos ao Titular ou S'#243'cio da Microempresa ou Empresa ' +
          'de Pequeno Porte,'
      end
      object RLLabel44: TRLLabel
        Left = 26
        Top = 622
        Width = 197
        Height = 16
        Caption = 'Reforma por Acidente em Servi'#231'o'
      end
      object RLLabel45: TRLLabel
        Left = 20
        Top = 690
        Width = 299
        Height = 16
        Caption = 'exceto Pr'#243'-labore, Alugu'#233'is, ou Serv'#231'os Prestados'
      end
      object RLDraw36: TRLDraw
        Left = 0
        Top = 793
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel46: TRLLabel
        Left = 3
        Top = 798
        Width = 160
        Height = 16
        Caption = '01.D'#233'cimo Terceiro Sal'#225'rio'
      end
      object RLDraw37: TRLDraw
        Left = 586
        Top = 794
        Width = 1
        Height = 25
      end
      object RLLabel47: TRLLabel
        Left = 674
        Top = 796
        Width = 29
        Height = 16
        Caption = '0,00'
      end
      object RLDraw38: TRLDraw
        Left = 0
        Top = 818
        Width = 716
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel48: TRLLabel
        Left = 3
        Top = 825
        Width = 61
        Height = 16
        Caption = '02.Outros'
      end
      object RLDraw39: TRLDraw
        Left = 586
        Top = 819
        Width = 1
        Height = 25
      end
      object RLLabel49: TRLLabel
        Left = 674
        Top = 825
        Width = 29
        Height = 16
        Caption = '0,00'
      end
      object RLLabel50: TRLLabel
        Left = 3
        Top = 769
        Width = 500
        Height = 16
        Caption = 
          '5.RENDIMENTOS SUJEITOS '#192' TRIBUTA'#199#195'O EXCLUSIVA (RENDIMENTO L'#205'QUID' +
          'O)'
      end
      object RLDraw40: TRLDraw
        Left = 1
        Top = 871
        Width = 716
        Height = 90
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
      end
      object RLLabel51: TRLLabel
        Left = 1
        Top = 850
        Width = 246
        Height = 16
        Caption = '6. INFORMA'#199#213'ES COMPLEMENTARES'
      end
      object RLLabel52: TRLLabel
        Left = 3
        Top = 966
        Width = 246
        Height = 16
        Caption = '7. RESPONS'#193'VEL PELA INFORMA'#199#195'O'
      end
      object RLLabel53: TRLLabel
        Left = 0
        Top = 988
        Width = 43
        Height = 16
        Caption = 'NOME'
      end
      object RLLabel54: TRLLabel
        Left = 352
        Top = 988
        Width = 38
        Height = 16
        Caption = 'DATA'
      end
      object RLLabel55: TRLLabel
        Left = 467
        Top = 988
        Width = 83
        Height = 16
        Caption = 'ASSNATURA'
      end
      object RLDBText1: TRLDBText
        Left = 8
        Top = 312
        Width = 42
        Height = 16
        DataField = 'no_cpf'
        DataSource = dsRelatorio
      end
      object RLDBText2: TRLDBText
        Left = 131
        Top = 312
        Width = 94
        Height = 16
        DataField = 'nm_beneficiado'
        DataSource = dsRelatorio
      end
      object RLDBText3: TRLDBText
        Left = 587
        Top = 401
        Width = 115
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_rendimento_total'
        DataSource = dsRelatorio
        DisplayMask = '#.###,##0.00'
      end
      object RLDBText5: TRLDBText
        Left = 671
        Top = 502
        Width = 26
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_ir'
        DataSource = dsRelatorio
        DisplayMask = '#.###,##0.00'
      end
      object RLMemo1: TRLMemo
        Left = 392
        Top = 16
        Width = 290
        Height = 48
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Lines.Strings = (
          'COMPROVANTE DE RENDIMENTOS PAGOS'
          'E DE RETEN'#199#195'O DE '
          'IMPOSTO DE RENDA NA FONTE')
        ParentFont = False
      end
      object lbAnoCalendario: TRLLabel
        Left = 467
        Top = 77
        Width = 151
        Height = 16
        Caption = 'ANO CALEND'#193'RIO 2010'
      end
      object RLMemo2: TRLMemo
        Left = 42
        Top = 32
        Width = 266
        Height = 48
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Lines.Strings = (
          'MINIST'#201'RIO DA FAZENDA'
          ''
          'SECRETARIA DA RECEITA FEDERAL')
        ParentFont = False
      end
      object RLLabel28: TRLLabel
        Left = 672
        Top = 451
        Width = 29
        Height = 16
        Caption = '0,00'
      end
    end
  end
  object qRelatorio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'a1'
        Size = -1
        Value = Null
      end
      item
        Name = 'a2'
        Size = -1
        Value = Null
      end
      item
        Name = 'a3'
        Size = -1
        Value = Null
      end
      item
        Name = 'a4'
        Size = -1
        Value = Null
      end
      item
        Name = 'a5'
        Size = -1
        Value = Null
      end
      item
        Name = 'a6'
        Size = -1
        Value = Null
      end
      item
        Name = 'codigo'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select'
      #9'c.cd_cadastro,'
      #9'case c.cd_sitJubilamento'
      #9#9'when 1 then c.nm_pastor'
      #9#9'when 2 then c.nm_conjuge'
      #9'end nm_beneficiado,'
      #9'c.no_cpf,'
      #9'case c.cd_sitJubilamento'
      #9#9'when 1 then (select SUM(vl_Rubrica) from tbPagamentoMensal'
      
        '                where cd_cadastro = c.cd_cadastro and cd_tipRubr' +
        'ica = 1  and dt_anoPagamento = :a1 group by cd_cadastro) -'
      
        '                (select SUM(vl_Rubrica) from tbPagamentoMensal w' +
        'here cd_cadastro = c.cd_cadastro and cd_tipRubrica = 2'
      
        '                     and dt_anoPagamento = :a2 group by cd_cadas' +
        'tro'
      ''
      ''
      ''
      ''
      #9#9'when 2 then (select SUM(vl_Rubrica) from tbPagamentoMensal'
      
        '                 where cd_cadastro = c.cd_cadastro and cd_tipRub' +
        'rica = 11  and dt_anoPagamento = :a3 group by cd_cadastro) -'
      
        '                 (select SUM(vl_Rubrica) from tbPagamentoMensal ' +
        'where cd_cadastro = c.cd_cadastro and cd_tipRubrica = 2'
      
        '                      and dt_anoPagamento = :a4 group by cd_cada' +
        'stro'
      #9'end vl_rendimento_total,'
      
        #9'(select SUM(vl_Rubrica) from tbPagamentoMensal where cd_cadastr' +
        'o = c.cd_cadastro and cd_tipRubrica = 2 and dt_anoPagamento = :a' +
        '5 group by cd_cadastro) vl_dizimo,'
      
        #9'(select SUM(vl_Rubrica) from tbPagamentoMensal where cd_cadastr' +
        'o = c.cd_cadastro and cd_tipRubrica = 4 and dt_anoPagamento = :a' +
        '6 group by cd_cadastro) vl_ir'
      'from tbContribuinte c'
      'where c.cd_sitJubilamento in(1,2) and c.cd_cadastro = :codigo'
      'order by 2')
    Left = 600
    Top = 152
    object qRelatorionm_beneficiado: TStringField
      FieldName = 'nm_beneficiado'
      ReadOnly = True
      Size = 60
    end
    object qRelatoriono_cpf: TStringField
      FieldName = 'no_cpf'
      FixedChar = True
      Size = 14
    end
    object qRelatoriovl_rendimento_total: TBCDField
      FieldName = 'vl_rendimento_total'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qRelatoriovl_dizimo: TBCDField
      FieldName = 'vl_dizimo'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qRelatoriovl_ir: TBCDField
      FieldName = 'vl_ir'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
  end
  object dsRelatorio: TDataSource
    DataSet = qRelatorio
    Left = 496
    Top = 152
  end
end
