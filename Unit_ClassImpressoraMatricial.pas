unit Unit_ClassImpressoraMatricial;

interface
type
    tImpressoraMatricial = Class ( TObject )
  private
    FPrinterOnline: boolean;
    FImpressora2: String;
    Fdocautentico: String;
    Fasssemelhanca: String;
    Fassautentica: string;
    Fescrevente: String;
    FImpressora1: String;
    procedure SetPrinterOnline(const Value: boolean);
    procedure SetImpressora2(const Value: String);
    procedure Setassautentica(const Value: string);
    procedure Setasssemelhanca(const Value: String);
    procedure Setdocautentico(const Value: String);
    procedure SetImpressora1(const Value: String);
      protected
        FFundoTroco: Double;
        FCODOperador: Integer;
        FBoletoROD: String;
        FPrinterName: String;
        FBoletoCAB: String;
        FOperador: String;
        FDataFechamento: TDateTime;
        FDataAbertura: TDateTime;

        fPrinterFile : TextFile;

        procedure SetBoletoCAB(const Value: String);
        procedure SetBoletoROD(const Value: String);
        procedure SetCODOperador(const Value: Integer);
        procedure SetDataAbertura(const Value: TDateTime);
        procedure SetDataFechamento(const Value: TDateTime);
        procedure SetFundoTroco(const Value: Double);
        procedure SetOperador(const Value: String);
        procedure SetPrinterName(const Value: String);
      public
        property PrinterOnline  : boolean read FPrinterOnline write SetPrinterOnline;

        property PrinterName    : String read FPrinterName write SetPrinterName;
        property CODOperador    : Integer read FCODOperador write SetCODOperador;
        property Operador       : String read FOperador write SetOperador;
        property DataAbertura   : TDateTime read FDataAbertura write SetDataAbertura;
        property DataFechamento : TDateTime read FDataFechamento write SetDataFechamento;

        property FundoTroco     : Double read FFundoTroco write SetFundoTroco;
        property BoletoCAB      : String read FBoletoCAB write SetBoletoCAB;
        property BoletoROD      : String read FBoletoROD write SetBoletoROD;
        Property Impressora1    :String read FImpressora1 write SetImpressora1;
        property Impressora2    :String read FImpressora2 write SetImpressora2;
        property asssemelhanca  :String  read Fasssemelhanca write Setasssemelhanca;
        property assautentica   :string  read Fassautentica write Setassautentica;
        property docautentico   :String  read Fdocautentico write Setdocautentico;


        function Iniciaimpressao   : boolean;
        function ImprimeBoletoCAB  : boolean;

        function ImprimeTexto ( Texto : String ) : boolean;

        function ImprimeBoletoROD  : boolean;
        function FinalizaImpressao : boolean;

    end;
implementation

uses classes,SysUtils;
{ tImpressoraMatricial }

function tImpressoraMatricial.FinalizaImpressao: boolean;
begin

  result := printerOnline;
  

  if PrinterOnline
  then
     closeFile(fPrinterFile);

  printerOnline := false;
       
end;

function tImpressoraMatricial.ImprimeBoletoCAB: boolean;
var
  tmpStrList : TStringList;
  i : Integer;
begin
  result := printerOnline;

  if PrinterOnline
  then
     begin
       tmpStrList := TStringList.Create;
       tmpStrList.CommaText := BoletoCab;

       for i := 0 to tmpStrList.Count - 1 do
           ImprimeTexto(tmpstrList[i]);
       tmpStrList.Free;
     end;

end;

function tImpressoraMatricial.ImprimeBoletoROD: boolean;
var
  tmpStrList : TStringList;
  i : Integer;
begin
  result := printerOnline;

  if PrinterOnline
  then
     begin
       tmpStrList := TStringList.Create;
       tmpStrList.CommaText := BoletoROD;

       for i := 0 to tmpStrList.Count - 1 do
           ImprimeTexto(tmpstrList[i]);
       tmpStrList.Free;
     end;

end;



function tImpressoraMatricial.ImprimeTexto(Texto: String): boolean;
begin

  result := printerOnline;

  if printerOnline
  then
     Writeln(fPrinterFile,texto);


end;

function tImpressoraMatricial.Iniciaimpressao: boolean;
begin
  result := not printerOnline;

  if not printerOnline
  then
     begin
       assignFile(fPrinterFile,FPrinterName);
       if fileExists(fPrinterName)
       then
          Append(fPrinterFile)
       else
          Rewrite(fPrinterFile);

       printerOnline := true;
     end;

end;







procedure tImpressoraMatricial.Setassautentica(const Value: string);
begin
  Fassautentica := Value;
end;

procedure tImpressoraMatricial.Setasssemelhanca(const Value: String);
begin
  Fasssemelhanca := Value;
end;

procedure tImpressoraMatricial.SetBoletoCAB(const Value: String);
begin
  FBoletoCAB := Value;
end;

procedure tImpressoraMatricial.SetBoletoROD(const Value: String);
begin
  FBoletoROD := Value;
end;

procedure tImpressoraMatricial.SetCODOperador(const Value: Integer);
begin
  FCODOperador := Value;
end;

procedure tImpressoraMatricial.SetDataAbertura(const Value: TDateTime);
begin
  FDataAbertura := Value;
end;

procedure tImpressoraMatricial.SetDataFechamento(const Value: TDateTime);
begin
  FDataFechamento := Value;
end;

procedure tImpressoraMatricial.Setdocautentico(const Value: String);
begin
  Fdocautentico := Value;
end;


procedure tImpressoraMatricial.SetFundoTroco(const Value: Double);
begin
  FFundoTroco := Value;
end;

procedure tImpressoraMatricial.SetImpressora1(const Value: String);
begin
  FImpressora1 := Value;
end;

procedure tImpressoraMatricial.SetImpressora2(const Value: String);
begin
  FImpressora2 := Value;
end;

procedure tImpressoraMatricial.SetOperador(const Value: String);
begin
  FOperador := Value;
end;

procedure tImpressoraMatricial.SetPrinterName(const Value: String);
begin
  FPrinterName := Value;
end;

procedure tImpressoraMatricial.SetPrinterOnline(const Value: boolean);
begin
  FPrinterOnline := Value;
end;

end.
 