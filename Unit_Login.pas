unit Unit_Login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, Buttons;

type
  Tfrm_Login = class(TForm)
    Label1: TLabel;
    edtUsuario: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    edtSenha: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure edtUsuarioKeyPress(Sender: TObject; var Key: Char);
    procedure edtSenhaKeyPress(Sender: TObject; var Key: Char);

  private
    { Private declarations }
    vLogado : Boolean;
    vUsuario : String;
    procedure setLogado(const value : Boolean);
    procedure setUsuario(const value : String);
  public
    { Public declarations }
    property Logado : Boolean read vLogado write setLogado;
    property Usuario : String read vUsuario write setUsuario;
  end;

var
  frm_Login: Tfrm_Login;

implementation

uses Unit_DM, Unit_main;

{$R *.dfm}

procedure Tfrm_Login.BitBtn1Click(Sender: TObject);
begin
  //Login de acesso

  if edtUsuario.Text = '' then
  begin
    ShowMessage('Digite um usu�rio v�lido');
    edtUsuario.SetFocus;
    exit;
  end;
  if edtSenha.Text = '' then
  begin
    ShowMessage('Digite uma senha ');
    exit;
  end;

  DM.qLogin.Close;
  DM.qLogin.Parameters.ParamByName('login').Value := edtUsuario.Text;
  DM.qLogin.Parameters.ParamByName('senha').Value := edtSenha.Text;
  DM.qLogin.Open;

  if Dm.qLogin.RecordCount > 0 then
  begin
    Logado := true;
    Application.ShowMainForm := true;
    frm_Principal.stBar.Panels[3].Text := DM.qLogin.FieldByName('nm_usuario').AsString;

    DM.nome := DM.qLoginnm_usuario.value;
    DM.idLogin := DM.qLoginid_usuario.Value;
    DM.login := DM.qLoginnm_login.Value;

    frm_Principal.Show;
    FreeAndNil(self);
    
  end
  else
  begin
    showmessage('Usu�rio ou Senha inv�lidos');
    edtUsuario.SetFocus;
  end;
  
  DM.qLogin.Close;

end;

procedure Tfrm_Login.BitBtn2Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure Tfrm_Login.edtSenhaKeyPress(Sender: TObject; var Key: Char);
begin
  If key = #13 then BitBtn1.Click;
end;

procedure Tfrm_Login.edtUsuarioKeyPress(Sender: TObject; var Key: Char);
begin
 If key = #13 then edtSenha.SetFocus;
end;

procedure Tfrm_Login.FormShow(Sender: TObject);
begin
  self.SetFocus;
  Logado := false;
  edtUsuario.SetFocus;
end;

procedure Tfrm_Login.setLogado(const value: Boolean);
begin
  vLogado := value;
end;

procedure Tfrm_Login.setUsuario(const value: String);
begin
  vUsuario := value;
end;

end.
