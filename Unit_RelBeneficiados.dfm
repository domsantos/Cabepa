object frm_RelBeneficiados: Tfrm_RelBeneficiados
  Left = 0
  Top = 0
  Caption = 'frm_RelBeneficiados'
  ClientHeight = 524
  ClientWidth = 736
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = -32
    Top = -24
    Width = 794
    Height = 1123
    DataSource = dsListaJubilados
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    object RLGroup1: TRLGroup
      Left = 38
      Top = 127
      Width = 718
      Height = 173
      DataFields = 'ds_beneficio'
      PageBreaking = pbBeforePrint
      object RLBand2: TRLBand
        Left = 0
        Top = 0
        Width = 718
        Height = 24
        BandType = btColumnHeader
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = True
        Borders.DrawRight = False
        Borders.DrawBottom = True
        object RLLabel3: TRLLabel
          Left = 3
          Top = 6
          Width = 33
          Height = 16
          Caption = 'TIPO'
        end
        object RLLabel4: TRLLabel
          Left = 114
          Top = 6
          Width = 55
          Height = 16
          Caption = 'C'#211'DIGO'
        end
        object RLLabel5: TRLLabel
          Left = 216
          Top = 6
          Width = 43
          Height = 16
          Caption = 'NOME'
        end
        object RLLabel6: TRLLabel
          Left = 624
          Top = 6
          Width = 48
          Height = 16
          Caption = 'VALOR'
        end
      end
      object RLBand3: TRLBand
        Left = 0
        Top = 24
        Width = 718
        Height = 20
        object RLDBText1: TRLDBText
          Left = 3
          Top = 1
          Width = 76
          Height = 16
          DataField = 'ds_beneficio'
          DataSource = dsListaJubilados
        end
        object RLDBText2: TRLDBText
          Left = 114
          Top = 1
          Width = 75
          Height = 16
          DataField = 'cd_cadastro'
          DataSource = dsListaJubilados
        end
        object RLDBText3: TRLDBText
          Left = 216
          Top = 1
          Width = 71
          Height = 16
          DataField = 'nm_pessoa'
          DataSource = dsListaJubilados
        end
        object RLDBText4: TRLDBText
          Left = 643
          Top = 1
          Width = 72
          Height = 16
          Alignment = taRightJustify
          DataField = 'vl_Beneficio'
          DataSource = dsListaJubilados
        end
      end
      object RLBand4: TRLBand
        Left = 0
        Top = 44
        Width = 718
        Height = 28
        BandType = btColumnFooter
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = True
        Borders.DrawRight = False
        Borders.DrawBottom = False
        object RLDBResult1: TRLDBResult
          Left = 604
          Top = 6
          Width = 111
          Height = 16
          Alignment = taRightJustify
          DataField = 'vl_Beneficio'
          DataSource = dsListaJubilados
          DisplayMask = 'R$ #,##0.00'
          Info = riSum
        end
        object RLLabel7: TRLLabel
          Left = 470
          Top = 6
          Width = 70
          Height = 16
          Caption = 'Valor Total:'
        end
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 300
      Width = 718
      Height = 32
      BandType = btSummary
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = True
      object RLLabel8: TRLLabel
        Left = 470
        Top = 6
        Width = 70
        Height = 16
        Caption = 'Valor Total:'
      end
      object RLDBResult2: TRLDBResult
        Left = 604
        Top = 6
        Width = 111
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_Beneficio'
        DataSource = dsListaJubilados
        DisplayMask = 'R$ #,##0.00'
        Info = riSum
      end
    end
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 89
      BandType = btHeader
      object RLLabel2: TRLLabel
        Left = 16
        Top = 32
        Width = 190
        Height = 22
        Caption = 'Relat'#243'rio de Doa'#231#245'es'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel9: TRLLabel
        Left = 16
        Top = 54
        Width = 42
        Height = 18
        Caption = 'Data:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLSystemInfo1: TRLSystemInfo
        Left = 64
        Top = 54
        Width = 48
        Height = 18
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLSystemInfo2: TRLSystemInfo
        Left = 676
        Top = 3
        Width = 87
        Height = 16
        Info = itPageNumber
      end
      object RLLabel10: TRLLabel
        Left = 635
        Top = 3
        Width = 35
        Height = 16
        Caption = 'Pag.:'
      end
      object RLLabel1: TRLLabel
        Left = 19
        Top = 6
        Width = 299
        Height = 27
        Caption = 'Associa'#231#227'o Casa do Pastor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object qListaJubilados: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '
      #9'c.cd_cadastro'
      #9',c.nm_pastor'
      #9',c.nm_conjuge'
      #9',CONVERT(char,c.dt_falecimento,101) dt_falecimento'
      
        #9',(select max(v.dt_Valor) from tbValBeneficio v where v.no_reqBe' +
        'neficio ='
      
        #9#9'(select max(b.no_reqBeneficio) from tbRequerimentoBeneficio b ' +
        'where b.cd_cadastro = c.cd_cadastro))  dt_Valor'
      
        #9',(select max(v.vl_Beneficio) from tbValBeneficio v where v.no_r' +
        'eqBeneficio = '
      
        #9#9'(select max(b.no_reqBeneficio) from tbRequerimentoBeneficio b ' +
        'where b.cd_cadastro = c.cd_cadastro))  vl_beneficio'
      #9',case c.cd_sitJubilamento'
      #9#9'when 1 then '#39'BENEFICIO'#39
      #9#9'when 2 then '#39'AUXILIO'#39
      #9'end ds_beneficio'
      #9',case c.cd_sitJubilamento'
      #9#9'when 1 then nm_pastor'
      #9#9'when 2 then nm_conjuge'
      #9'end nm_pessoa'
      'from tbContribuinte c'
      'where c.cd_sitJubilamento in (1,2)'
      'order by c.cd_sitJubilamento,8')
    Left = 144
    Top = 256
    object qListaJubiladoscd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
    object qListaJubiladosnm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qListaJubiladosnm_conjuge: TStringField
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object qListaJubiladosdt_falecimento: TStringField
      FieldName = 'dt_falecimento'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object qListaJubiladosdt_Valor: TDateTimeField
      FieldName = 'dt_Valor'
      ReadOnly = True
    end
    object qListaJubiladosvl_beneficio: TBCDField
      FieldName = 'vl_beneficio'
      ReadOnly = True
      Precision = 10
      Size = 2
    end
    object qListaJubiladosds_beneficio: TStringField
      FieldName = 'ds_beneficio'
      ReadOnly = True
      Size = 9
    end
    object qListaJubiladosnm_pessoa: TStringField
      FieldName = 'nm_pessoa'
      ReadOnly = True
      Size = 60
    end
  end
  object dsListaJubilados: TDataSource
    DataSet = qListaJubilados
    Left = 144
    Top = 304
  end
end
