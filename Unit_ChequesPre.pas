unit Unit_ChequesPre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, StdCtrls, ExtCtrls, Mask, Buttons;

type
  Tfrm_ChequesPre = class(TForm)
    DBGrid1: TDBGrid;
    dsListaCheques: TDataSource;
    qListaCheques: TADOQuery;
    qListaChequescd_tipoContribuicao: TIntegerField;
    qListaChequesdt_mesRefContribuicao: TIntegerField;
    qListaChequesdt_anoRefContribuicao: TIntegerField;
    qListaChequesreferencia: TStringField;
    qListaChequesvl_pagtoContribuicao: TBCDField;
    qListaChequesno_chequePagto: TStringField;
    qListaChequesds_bancoCheque: TStringField;
    qListaChequesds_agenciaCheque: TStringField;
    qListaChequesdt_resgChequePre: TDateTimeField;
    qListaChequessq_chequePagto: TSmallintField;
    qListaChequesnm_pastor: TStringField;
    qListaChequessituacao: TStringField;
    Panel1: TPanel;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    dtInicio: TMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    dtFim: TMaskEdit;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    BitBtn3: TBitBtn;
    qBaixaCheque: TADOQuery;
    qListaChequesid_PagtoContribuincao: TAutoIncField;
    qListaChequescd_cadastro: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CalculaValorTotal();
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_ChequesPre: Tfrm_ChequesPre;

implementation

uses Unit_DM, Unit_RelatorioChequesPre;

{$R *.dfm}

procedure Tfrm_ChequesPre.BitBtn1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_RelatorioChequePre, frm_RelatorioChequePre);
  frm_RelatorioChequePre.RLReport1.Preview();


end;

procedure Tfrm_ChequesPre.BitBtn3Click(Sender: TObject);
var
  sData : String;
begin
  //
  dm.ADOConn.BeginTrans;
  sData := InputBox('Informe a data da baixa','Baixa de Cheque',datetostr(Date));

  qBaixaCheque.Close;
  qBaixaCheque.Parameters.ParamByName('data').Value := StrToDate(sData);
  qBaixaCheque.Parameters.ParamByName('codigo').Value := qListaChequesid_PagtoContribuincao.Value;

  dm.spVerificaPagamentoChequePre.Parameters.ParamByName('@vcodigo').Value :=
    qListaChequescd_cadastro.Value;
  dm.spVerificaPagamentoChequePre.Parameters.ParamByName('@vano').Value :=
    qListaChequesdt_anoRefContribuicao.Value;
  dm.spVerificaPagamentoChequePre.Parameters.ParamByName('@vmes').Value :=
    qListaChequesdt_mesRefContribuicao.Value;


  try
    qBaixaCheque.ExecSQL;
    dm.spVerificaPagamentoChequePre.ExecProc;
    showmessage('Data da baixa do cheque atualizada');
    dm.ADOConn.CommitTrans;
    qListaCheques.close;
    qListaCheques.Open;
  except
    dm.ADOConn.RollbackTrans;
    showmessage('Erro ao atualizar registro');
  end;
  CalculaValorTotal;
end;

procedure Tfrm_ChequesPre.CalculaValorTotal();
var
  total : double;
begin
  qListaCheques.First;
  total := 0;
  while not qListaCheques.Eof do
  begin
    total := total + qListaChequesvl_pagtoContribuicao.Value;
    qListaCheques.Next;
  end;

  Label5.Caption := FormatCurr('R$ ###,##0.00',total);
end;

procedure Tfrm_ChequesPre.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qListaCheques.Close;

  self := nil;
  Action := caFree;
end;

procedure Tfrm_ChequesPre.FormShow(Sender: TObject);
begin
  qListaCheques.Open;
  CalculaValorTotal();
end;

end.
