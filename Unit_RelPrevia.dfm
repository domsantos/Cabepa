object frm_RelPrevia: Tfrm_RelPrevia
  Left = 0
  Top = 0
  Caption = 'frm_RelPrevia'
  ClientHeight = 511
  ClientWidth = 861
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = 8
    Top = -8
    Width = 794
    Height = 1123
    DataSource = frm_Previas.dsPrevia
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 83
      BandType = btHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = True
      object RLLabel2: TRLLabel
        Left = 0
        Top = 32
        Width = 314
        Height = 22
        Caption = 'Relat'#243'rio de Resumo de Pagamento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel9: TRLLabel
        Left = 0
        Top = 54
        Width = 84
        Height = 18
        Caption = 'Refer'#234'ncia:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLSystemInfo1: TRLSystemInfo
        Left = 580
        Top = 59
        Width = 48
        Height = 18
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel10: TRLLabel
        Left = 548
        Top = 3
        Width = 35
        Height = 16
        Caption = 'Pag.:'
      end
      object RLSystemInfo2: TRLSystemInfo
        Left = 589
        Top = 3
        Width = 87
        Height = 16
        Info = itPageNumber
      end
      object lblRef: TRLLabel
        Left = 90
        Top = 54
        Width = 43
        Height = 18
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel1: TRLLabel
        Left = 3
        Top = 0
        Width = 299
        Height = 27
        Caption = 'Associa'#231#227'o Casa do Pastor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
    object RLGroup1: TRLGroup
      Left = 38
      Top = 121
      Width = 718
      Height = 88
      DataFields = 'ds_beneficio'
      PageBreaking = pbBeforePrint
      object RLBand2: TRLBand
        Left = 0
        Top = 0
        Width = 718
        Height = 33
        BandType = btColumnHeader
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = False
        Borders.DrawRight = False
        Borders.DrawBottom = True
        object RLLabel3: TRLLabel
          Left = 0
          Top = 16
          Width = 33
          Height = 16
          Caption = 'TIPO'
        end
        object RLLabel4: TRLLabel
          Left = 74
          Top = 15
          Width = 55
          Height = 16
          Caption = 'CODIGO'
        end
        object RLLabel5: TRLLabel
          Left = 154
          Top = 15
          Width = 43
          Height = 16
          Caption = 'NOME'
        end
        object RLLabel6: TRLLabel
          Left = 423
          Top = 13
          Width = 48
          Height = 16
          Caption = 'VALOR'
        end
        object RLLabel7: TRLLabel
          Left = 530
          Top = 12
          Width = 47
          Height = 16
          Caption = 'DIZIMO'
        end
        object RLLabel8: TRLLabel
          Left = 625
          Top = 12
          Width = 90
          Height = 16
          Caption = 'VALOR PAGO'
        end
      end
      object RLBand3: TRLBand
        Left = 0
        Top = 33
        Width = 718
        Height = 16
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        object RLDBText1: TRLDBText
          Left = 3
          Top = 1
          Width = 71
          Height = 14
          DataField = 'ds_beneficio'
          DataSource = frm_Previas.dsPrevia
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText2: TRLDBText
          Left = 74
          Top = 1
          Width = 36
          Height = 14
          DataField = 'cd_cadastro'
          DataSource = frm_Previas.dsPrevia
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText3: TRLDBText
          Left = 154
          Top = 3
          Width = 90
          Height = 14
          DataField = 'nm_pastor'
          DataSource = frm_Previas.dsPrevia
        end
        object RLDBText4: TRLDBText
          Left = 455
          Top = 2
          Width = 52
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_vantagem'
          DataSource = frm_Previas.dsPrevia
        end
        object RLDBText5: TRLDBText
          Left = 566
          Top = 1
          Width = 49
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_desconto'
          DataSource = frm_Previas.dsPrevia
        end
        object RLDBText6: TRLDBText
          Left = 678
          Top = 1
          Width = 37
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_liquido'
          DataSource = frm_Previas.dsPrevia
        end
      end
      object RLBand4: TRLBand
        Left = 0
        Top = 49
        Width = 718
        Height = 32
        BandType = btColumnFooter
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = True
        Borders.DrawRight = False
        Borders.DrawBottom = False
        object RLLabel11: TRLLabel
          Left = 311
          Top = 4
          Width = 70
          Height = 16
          Caption = 'Sub-Totais:'
        end
        object RLDBResult1: TRLDBResult
          Left = 412
          Top = 3
          Width = 95
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_vantagem'
          DataSource = frm_Previas.dsPrevia
          DisplayMask = 'R$ #,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Info = riSum
          ParentFont = False
        end
        object RLDBResult2: TRLDBResult
          Left = 524
          Top = 3
          Width = 91
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_desconto'
          DataSource = frm_Previas.dsPrevia
          DisplayMask = 'R$ #,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Info = riSum
          ParentFont = False
        end
        object RLDBResult3: TRLDBResult
          Left = 636
          Top = 3
          Width = 80
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_liquido'
          DataSource = frm_Previas.dsPrevia
          DisplayMask = 'R$ #,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Info = riSum
          ParentFont = False
        end
        object RLLabel12: TRLLabel
          Left = 3
          Top = 6
          Width = 105
          Height = 16
          Caption = 'No. de Registros:'
        end
        object RLDBResult4: TRLDBResult
          Left = 114
          Top = 6
          Width = 46
          Height = 16
          DataField = 'cd_cadastro'
          DataSource = frm_Previas.dsPrevia
          Info = riCount
        end
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 209
      Width = 718
      Height = 48
      BandType = btSummary
      object RLLabel13: TRLLabel
        Left = 3
        Top = 6
        Width = 137
        Height = 16
        Caption = 'No. Total de Registros:'
      end
      object RLDBResult5: TRLDBResult
        Left = 136
        Top = 6
        Width = 46
        Height = 16
        DataField = 'cd_cadastro'
        DataSource = frm_Previas.dsPrevia
        Info = riCount
      end
      object RLLabel14: TRLLabel
        Left = 311
        Top = 6
        Width = 46
        Height = 16
        Caption = 'Totais:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDBResult6: TRLDBResult
        Left = 408
        Top = 6
        Width = 95
        Height = 14
        Alignment = taRightJustify
        DataField = 'vl_vantagem'
        DataSource = frm_Previas.dsPrevia
        DisplayMask = 'R$ #,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Info = riSum
        ParentFont = False
      end
      object RLDBResult7: TRLDBResult
        Left = 524
        Top = 6
        Width = 91
        Height = 14
        Alignment = taRightJustify
        DataField = 'vl_desconto'
        DataSource = frm_Previas.dsPrevia
        DisplayMask = 'R$ #,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Info = riSum
        ParentFont = False
      end
      object RLDBResult8: TRLDBResult
        Left = 635
        Top = 3
        Width = 80
        Height = 14
        Alignment = taRightJustify
        DataField = 'vl_liquido'
        DataSource = frm_Previas.dsPrevia
        DisplayMask = 'R$ #,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Info = riSum
        ParentFont = False
      end
    end
  end
end
