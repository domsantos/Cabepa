unit Unit_RelPagResumoPeriodo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, RLReport, jpeg;

type
  Tfrm_RelPagResumoPeriodo = class(Tfrm_RelatorioPadrao)
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLBand5: TRLBand;
    RLDBResult1: TRLDBResult;
    RLDBResult2: TRLDBResult;
    RLLabel10: TRLLabel;
    RLLabel11: TRLLabel;
    procedure RLBand5BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelPagResumoPeriodo: Tfrm_RelPagResumoPeriodo;

implementation

uses Unit_FiltroPagResumoPeriodo;

{$R *.dfm}

procedure Tfrm_RelPagResumoPeriodo.RLBand5BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  inherited;
  RLLabel10.Caption := FormatCurr('R$ ###,##0.00',(RLDBResult1.Value - RLDBResult2.Value));
end;

end.
