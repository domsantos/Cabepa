inherited frm_Usuario: Tfrm_Usuario
  Left = 235
  Top = 154
  Caption = 'Usu'#225'rio'
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel2: TPanel
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 26
      Height = 16
      Caption = 'Login'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 16
      Top = 56
      Width = 68
      Height = 16
      Caption = 'Nome Usu'#225'rio'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 16
      Top = 104
      Width = 29
      Height = 16
      Caption = 'E-mail'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Left = 16
      Top = 152
      Width = 64
      Height = 16
      Caption = 'Fone Contato'
      FocusControl = DBEdit4
    end
    object Label5: TLabel
      Left = 16
      Top = 208
      Width = 23
      Height = 16
      Caption = 'Perfil'
    end
    object DBEdit1: TDBEdit
      Left = 16
      Top = 24
      Width = 161
      Height = 24
      DataField = 'nm_login'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 16
      Top = 72
      Width = 401
      Height = 24
      DataField = 'nm_usuario'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 16
      Top = 120
      Width = 388
      Height = 24
      DataField = 'ds_email'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Left = 16
      Top = 168
      Width = 180
      Height = 24
      DataField = 'nr_fone_contato'
      DataSource = DataSource1
      TabOrder = 3
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 16
      Top = 224
      Width = 145
      Height = 24
      DataField = 'id_perfil'
      DataSource = DataSource1
      KeyField = 'id_perfil'
      ListField = 'ds_perfil'
      ListSource = dsPErfil
      TabOrder = 4
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT id_usuario'
      '      ,nm_login'
      '      ,nm_usuario'
      '      ,ds_email'
      '      ,nr_fone_contato'
      '      ,lb_foto'
      '      ,id_perfil'
      '  FROM tbUsuario')
    object QPrincipalid_usuario: TAutoIncField
      FieldName = 'id_usuario'
      ReadOnly = True
    end
    object QPrincipalnm_login: TStringField
      FieldName = 'nm_login'
      Size = 25
    end
    object QPrincipalnm_usuario: TStringField
      FieldName = 'nm_usuario'
      Size = 100
    end
    object QPrincipalds_email: TStringField
      FieldName = 'ds_email'
      Size = 100
    end
    object QPrincipalnr_fone_contato: TStringField
      FieldName = 'nr_fone_contato'
      Size = 11
    end
    object QPrincipallb_foto: TBlobField
      FieldName = 'lb_foto'
      Visible = False
    end
    object QPrincipalid_perfil: TIntegerField
      FieldName = 'id_perfil'
    end
  end
  object DataSource1: TDataSource
    DataSet = QPrincipal
    Left = 640
    Top = 312
  end
  object QPerfil: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select id_perfil,ds_perfil'
      'from tbPerfil'
      'order by ds_perfil')
    Left = 440
    Top = 361
  end
  object dsPErfil: TDataSource
    DataSet = QPerfil
    Left = 368
    Top = 353
  end
end
