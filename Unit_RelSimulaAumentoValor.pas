unit Unit_RelSimulaAumentoValor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, RLReport, jpeg, ADODB;

type
  Tfrm_RelSimulaAumentoValor = class(Tfrm_RelatorioPadrao)
    lblAumento: TRLLabel;
    qRelatorio: TADOQuery;
    RLBand5: TRLBand;
    qRelatoriovl_aumento: TBCDField;
    qRelatoriovl_Beneficio: TBCDField;
    qRelatorionm_pastor: TStringField;
    RLLabel7: TRLLabel;
    RLDBText3: TRLDBText;
    RLDBResult1: TRLDBResult;
    RLDBResult2: TRLDBResult;
    RLDBResult3: TRLDBResult;
    RLLabel8: TRLLabel;
    RLLabel6: TRLLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelSimulaAumentoValor: Tfrm_RelSimulaAumentoValor;

implementation

uses Unit_DM;

{$R *.dfm}

end.
