inherited frm_RelJubilados: Tfrm_RelJubilados
  Caption = 'frm_RelJubilados'
  ExplicitLeft = -79
  ExplicitTop = -217
  ExplicitWidth = 870
  ExplicitHeight = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = 72
    Top = 184
    Width = 1123
    Height = 794
    PageSetup.Orientation = poLandscape
    ExplicitLeft = 72
    ExplicitTop = 184
    ExplicitWidth = 1123
    ExplicitHeight = 794
    inherited RLBand1: TRLBand
      Width = 1047
      ExplicitWidth = 1047
      inherited RLLabel2: TRLLabel
        Width = 133
        Caption = 'Listagem de Jubilados'
        ExplicitWidth = 133
      end
    end
    inherited RLBand2: TRLBand
      Width = 1047
      ExplicitWidth = 1047
      inherited RLLabel4: TRLLabel
        Width = 89
        Caption = 'Nome Jubilado'
        ExplicitWidth = 89
      end
      object RLLabel6: TRLLabel
        Left = 440
        Top = 29
        Width = 89
        Height = 16
        Caption = 'Nome Conjuge'
      end
      object RLLabel7: TRLLabel
        Left = 784
        Top = 29
        Width = 89
        Height = 16
        Caption = 'Data Jubila'#231#227'o'
      end
      object RLLabel8: TRLLabel
        Left = 928
        Top = 29
        Width = 55
        Height = 16
        Caption = 'Situa'#231#227'o'
      end
    end
    inherited RLBand4: TRLBand
      Width = 1047
      ExplicitWidth = 1047
      inherited RLSystemInfo2: TRLSystemInfo
        Left = 957
        ExplicitLeft = 957
      end
    end
    inherited RLBand3: TRLBand
      Width = 1047
      ExplicitWidth = 1047
      inherited RLDBText1: TRLDBText
        Width = 72
        DataField = 'cd_CABEPA'
        ExplicitWidth = 72
      end
      inherited RLDBText2: TRLDBText
        Width = 89
        DataField = 'nm_pastor'
        ExplicitWidth = 89
      end
      object RLDBText3: TRLDBText
        Left = 440
        Top = 3
        Width = 89
        Height = 16
        DataField = 'nm_conjuge'
        DataSource = dsRelatorio
      end
      object RLDBText4: TRLDBText
        Left = 784
        Top = 3
        Width = 69
        Height = 16
        DataField = 'dtJubilacao'
        DataSource = dsRelatorio
      end
      object RLDBText5: TRLDBText
        Left = 928
        Top = 3
        Width = 55
        Height = 16
        DataField = 'situacao'
        DataSource = dsRelatorio
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = DM.qListaJubilados
    Left = 352
    Top = 256
  end
end
