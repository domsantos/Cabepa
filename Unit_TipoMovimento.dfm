inherited frm_TipoMovimento: Tfrm_TipoMovimento
  Caption = 'Tipo Movimento'
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel2: TPanel
    ExplicitTop = 44
    object Label1: TLabel
      Left = 8
      Top = 3
      Width = 40
      Height = 16
      Caption = 'Codigo'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 8
      Top = 49
      Width = 90
      Height = 16
      Caption = 'Tipo Movimento'
      FocusControl = DBEdit2
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 19
      Width = 164
      Height = 24
      DataField = 'cdTipoMovimento'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 8
      Top = 67
      Width = 508
      Height = 24
      DataField = 'dsTipoMovimento'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 97
      Width = 508
      Height = 296
      DataSource = DataSource1
      TabOrder = 2
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalcdTipoMovimento: TAutoIncField
      FieldName = 'cdTipoMovimento'
      ReadOnly = True
    end
    object CDSPrincipaldsTipoMovimento: TStringField
      FieldName = 'dsTipoMovimento'
      Size = 50
    end
  end
  inherited QPrincipal: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'select cdTipoMovimento,dsTipoMovimento'
      'from tbTipoMovimento')
    object QPrincipalcdTipoMovimento: TAutoIncField
      FieldName = 'cdTipoMovimento'
      ReadOnly = True
    end
    object QPrincipaldsTipoMovimento: TStringField
      FieldName = 'dsTipoMovimento'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 632
    Top = 328
  end
end
