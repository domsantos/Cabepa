unit Unit_RelAjusteBeneficios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, ADODB, RLReport, jpeg;

type
  Tfrm_RelAjusteValorBeneficio = class(Tfrm_RelatorioPadrao)
    qRelatorio: TADOQuery;
    qRelatoriocd_cabepa: TIntegerField;
    qRelatorionm_pastor: TStringField;
    qRelatoriovl_beneficio: TBCDField;
    qRelatoriovl_dizimo: TBCDField;
    qRelatoriovl_descontos: TBCDField;
    qRelatoriovl_liquidoatual: TBCDField;
    qRelatoriovl_novobruto: TBCDField;
    qRelatoriovl_novoliquido: TBCDField;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelAjusteValorBeneficio: Tfrm_RelAjusteValorBeneficio;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_RelAjusteValorBeneficio.FormCreate(Sender: TObject);
begin
  inherited;
  qRelatorio.open;
  
end;

end.
