unit Unit_FiltroRelatorioNadaConsta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  Tfrm_FiltroRelatorioNadaConsta = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    edtContribuinte: TEdit;
    Button1: TButton;
    Label4: TLabel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    procedure Button1Click(Sender: TObject);
    procedure edtContribuinteKeyPress(Sender: TObject; var Key: Char);
    procedure edtContribuinteExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_FiltroRelatorioNadaConsta: Tfrm_FiltroRelatorioNadaConsta;

implementation

uses Unit_PesquisaContribuinte, Unit_DM;

{$R *.dfm}

procedure Tfrm_FiltroRelatorioNadaConsta.Button1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
  frm_PesquisaContribuinte.origem := 5;
  frm_PesquisaContribuinte.Show;
end;

procedure Tfrm_FiltroRelatorioNadaConsta.edtContribuinteExit(Sender: TObject);
begin
if edtContribuinte.Text <> '' then
  begin
    dm.qBuscaContribuinte.Close;
    dm.qBuscaContribuinte.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    dm.qBuscaContribuinte.Open;

    if dm.qBuscaContribuinte.RecordCount = 0 then
    begin
      showmessage('Contribuinte n�o encontrado');
      edtContribuinte.SetFocus;
    end
    else
      Label3.Caption := dm.qBuscaContribuintenm_pastor.Value;
  end;
end;

procedure Tfrm_FiltroRelatorioNadaConsta.edtContribuinteKeyPress(
  Sender: TObject; var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_FiltroRelatorioNadaConsta.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

end.
