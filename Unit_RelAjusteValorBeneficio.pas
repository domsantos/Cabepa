unit Unit_RelAjusteValorBeneficio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RLReport, jpeg, DB, ADODB;

type
  Tfrm_RelAjusteValorBeneficios = class(TForm)
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLImage1: TRLImage;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLBand2: TRLBand;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLBand3: TRLBand;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBText6: TRLDBText;
    RLBand4: TRLBand;
    RLSystemInfo1: TRLSystemInfo;
    RLLabel5: TRLLabel;
    RLSystemInfo2: TRLSystemInfo;
    qRelatorio: TADOQuery;
    RLGroup1: TRLGroup;
    RLBand5: TRLBand;
    dsRelatorio: TDataSource;
    RLDBText7: TRLDBText;
    RLDBText8: TRLDBText;
    RLLabel10: TRLLabel;
    RLLabel11: TRLLabel;
    RLDBResult1: TRLDBResult;
    RLDBResult2: TRLDBResult;
    RLDBResult3: TRLDBResult;
    RLDBResult4: TRLDBResult;
    RLDBResult5: TRLDBResult;
    RLDBResult6: TRLDBResult;
    RLDBResult7: TRLDBResult;
    RLDBResult8: TRLDBResult;
    RLDBResult9: TRLDBResult;
    RLDBResult10: TRLDBResult;
    RLDBResult11: TRLDBResult;
    RLDBResult12: TRLDBResult;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    qRelatoriocd_sitJubilamento: TStringField;
    qRelatorionm_pastor: TStringField;
    qRelatoriovl_beneficio: TBCDField;
    qRelatoriovl_escalonamento: TBCDField;
    qRelatoriovl_descontos: TBCDField;
    qRelatoriovl_liquidoatual: TBCDField;
    qRelatoriovl_novobruto: TBCDField;
    qRelatoriovl_dizimo: TBCDField;
    qRelatoriovl_novoliquido: TBCDField;
    qRelatoriocd_cadastro: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelAjusteValorBeneficios: Tfrm_RelAjusteValorBeneficios;

implementation

uses Unit_DM;

{$R *.dfm}

end.
