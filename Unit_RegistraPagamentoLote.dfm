object frm_RegistroPagamentoLote: Tfrm_RegistroPagamentoLote
  Left = 10
  Top = 10
  BorderIcons = [biSystemMenu]
  Caption = 'Registro de Contribui'#231#227'o em Lote'
  ClientHeight = 531
  ClientWidth = 408
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 278
    Height = 23
    Caption = 'Registro de Contribui'#231#227'o em Lote'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 40
    Width = 95
    Height = 13
    Caption = 'Informe N'#186' CABEPA'
  end
  object Label4: TLabel
    Left = 8
    Top = 91
    Width = 59
    Height = 19
    Caption = 'Pastor:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 110
    Width = 241
    Height = 19
    Caption = 'Nenhum Contribuinte Selecionado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 135
    Width = 36
    Height = 13
    Caption = 'Per'#237'odo'
  end
  object Label6: TLabel
    Left = 8
    Top = 154
    Width = 25
    Height = 13
    Caption = 'In'#237'cio'
  end
  object Label7: TLabel
    Left = 104
    Top = 154
    Width = 16
    Height = 13
    Caption = 'Fim'
  end
  object Label8: TLabel
    Left = 8
    Top = 200
    Width = 77
    Height = 13
    Caption = 'Tipo Pagamento'
  end
  object Banco: TLabel
    Left = 8
    Top = 246
    Width = 29
    Height = 13
    Caption = 'Banco'
  end
  object Label9: TLabel
    Left = 8
    Top = 301
    Width = 57
    Height = 13
    Caption = 'No. Cheque'
  end
  object Label10: TLabel
    Left = 144
    Top = 299
    Width = 66
    Height = 13
    Caption = 'Data Resgate'
  end
  object Label11: TLabel
    Left = 144
    Top = 244
    Width = 38
    Height = 13
    Caption = 'Agencia'
  end
  object Label12: TLabel
    Left = 144
    Top = 200
    Width = 49
    Height = 13
    Caption = 'Valor total'
  end
  object Label15: TLabel
    Left = 189
    Top = 154
    Width = 83
    Height = 13
    Caption = 'Tipo Contribui'#231#227'o'
  end
  object Label16: TLabel
    Left = 231
    Top = 176
    Width = 129
    Height = 13
    Caption = '1 - Contribui'#231#227'o 2 - Seguro'
  end
  object Label17: TLabel
    Left = 279
    Top = 198
    Width = 80
    Height = 13
    Caption = 'Data Pagamento'
  end
  object edtContribuinte: TEdit
    Left = 8
    Top = 59
    Width = 57
    Height = 21
    TabOrder = 0
    OnExit = edtContribuinteExit
    OnKeyPress = edtContribuinteKeyPress
  end
  object Button1: TButton
    Left = 71
    Top = 57
    Width = 75
    Height = 25
    Caption = 'Buscar'
    TabOrder = 1
    OnClick = Button1Click
  end
  object edtInicio: TMaskEdit
    Left = 8
    Top = 173
    Width = 79
    Height = 21
    EditMask = '!99/99/0000;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object edtFim: TMaskEdit
    Left = 104
    Top = 173
    Width = 79
    Height = 21
    EditMask = '!99/99/0000;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object cbFormaPagamento: TDBLookupComboBox
    Left = 8
    Top = 219
    Width = 121
    Height = 21
    KeyField = 'cd_formPagto'
    ListField = 'ds_formPagto'
    ListSource = dsFormaPagamento
    TabOrder = 5
  end
  object edtdataResgate: TMaskEdit
    Left = 144
    Top = 318
    Width = 97
    Height = 21
    EditMask = '!99/99/0000;1;_'
    MaxLength = 10
    TabOrder = 10
    Text = '  /  /    '
  end
  object edtBanco: TEdit
    Left = 8
    Top = 265
    Width = 108
    Height = 21
    TabOrder = 7
  end
  object edtCheque: TEdit
    Left = 8
    Top = 320
    Width = 121
    Height = 21
    TabOrder = 9
    OnKeyPress = edtChequeKeyPress
  end
  object edtAgencia: TEdit
    Left = 144
    Top = 263
    Width = 121
    Height = 21
    TabOrder = 8
    OnKeyPress = edtAgenciaKeyPress
  end
  object BitBtn1: TBitBtn
    Left = 248
    Top = 495
    Width = 152
    Height = 32
    Caption = 'Registrar Pagamento'
    Enabled = False
    TabOrder = 11
    OnClick = BitBtn1Click
    Glyph.Data = {
      0A040000424D0A04000000000000420000002800000016000000160000000100
      100003000000C8030000120B0000120B00000000000000000000007C0000E003
      00001F000000FF7FFF7FFF7FFF7FFF7FFF7F7A6BD3528C3E8836A736A7368836
      8C3ED3527A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73B14A8736
      C53AC53AA536A532A532A536C53AC53A8736B14A9C73FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F59676A3AC53AA536652A252625222522252225222526652AA536C53A
      6A3A5967FF7FFF7FFF7FFF7FFF7F38638836E53A652A2526452645264526452A
      452A4526452625262526652AC53A68363863FF7FFF7FFF7F9C6F4936C536452A
      4526452A652A652A44264526652A652A652A452A452A45264526C53649369C6F
      FF7FFF7FB14AA536652A452A652A652E652E43268B3AD34E662A652E652E652E
      652A652A4526652AA532B14AFF7F7A6B8732852E452A652E652E852E63268A36
      7A6B9E77AC3A642A8532852E652E652E652E452A852E67327A6BB34EA52E652E
      652E852E8532632A8B367B6BBD779D77385F652A843285328532852E652E652E
      652A852EB34E6C3A8532652E85328532632A8B3A7B6FBD77BD779D779E77D046
      632AA536853285328532652E652E852E6C3A6932853285328532842EAD3E7C6F
      BE7BBD777A679C73BE7B7B6B8836A332A536A53685328532652E852E69326832
      A736A636A536652E385FDF7FDF7B355B662E355BDE7BDF7B1557842EC436A536
      A536A532A636A736682E8932C93EC83EC83EA636EF4ABE7B15578532A232A93A
      9D73DE7BBE77CD42A332C63EC73EC83EC83AC93A69326A32EA42E942E946E946
      CA3ED046C93AE742E842C63AF24EDF7FDF7F7B6BAA3AE842EA46E942C942EA42
      6A326D3A0B470B4B0B4B0B4B0A47C83E0A4B0B4B0B4B2A4BC93E7967FF7FFF7F
      365BC93A0A4BEB46EB460B476D3AB34E0C472C4F0C4B0C4F2C4F2C4F2C4F2C4F
      2C4F2C4F0A4BCE46BD73FF7FFF7F1353EA420C4B0C4FEC46B34E7A6BAD3E4F57
      2D4F2D4F2D532D532D532D532D532D534D57EB46124FFF7FFF7F1453EB422D4F
      4F57AD3E7A6BFF7FB24A30534F5B2E534E574E574E574E574E574E574E574E57
      EC4635571457ED4A2E5350573053B24AFF7FFF7F9B6F6D3A9363505B4F575057
      4F5B4F5B4F5B6F5B4F5B705B6F5BED460D4B4F57715B935F6D3A9B6FFF7FFF7F
      FF7F3863AE42B5679363515B505B715B715F715F715F715F715B705B515B9363
      B567AE423863FF7FFF7FFF7FFF7FFF7F58638F42755FB66F94677263725F725F
      725F725F72639467B66F755F8F425863FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C6F
      B24AF14A9663B86FD86FB76FB76FB86FB86F9663D14AB24A9C6FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F7A6BD452B146D14AF24EF24ED14AB146D452
      7A6BFF7FFF7FFF7FFF7FFF7FFF7F}
  end
  object edtValor: TCurrencyEdit
    Left = 144
    Top = 217
    Width = 121
    Height = 21
    Margins.Left = 4
    Margins.Top = 1
    TabOrder = 6
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 347
    Width = 392
    Height = 142
    Caption = 'Previa do Calculo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    object Label13: TLabel
      Left = 14
      Top = 26
      Width = 127
      Height = 19
      Caption = 'Valor por Parcela:'
    end
    object Label14: TLabel
      Left = 14
      Top = 72
      Width = 115
      Height = 19
      Caption = 'No. de Parcelas:'
    end
    object edtValorParcelas: TCurrencyEdit
      Left = 149
      Top = 24
      Width = 148
      Height = 27
      Margins.Left = 5
      Margins.Top = 1
      Enabled = False
      TabOrder = 0
    end
    object edtParcelas: TEdit
      Left = 149
      Top = 68
      Width = 148
      Height = 27
      Enabled = False
      TabOrder = 1
    end
    object BitBtn2: TBitBtn
      Left = 230
      Top = 101
      Width = 147
      Height = 36
      Caption = 'Calcula Valor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BitBtn2Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F94528C358C358C358C358C358C35
        8C358C358C358C35AC35AD358C35AD353967FF7FFF7FFF7FFF7FFF7FDE7B8C35
        C618E71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CF75EFF7F
        FF7FFF7FFF7FFF7F1863C6182925D65AF75EF75EF75ED75AD65AD65AD65AD65A
        9452955294520821D65AFF7FFF7BFF7FFF7FFF7F1863A5183146DE7BBD739D73
        9C739C739C739C737B6F955210421042F85E2925D65AFF7FDE77BE73DF77DE7B
        524A08217B6FBE779C739C739C739C6F7C6FB556CE3931463967534ACE392925
        D65ADF7BBE779E6B7D5F396394563146D65A5A6BBD77BD779C73BD73F75ECE39
        F75EBD77BD775A6BAD35E720D65ADF7BBE737E679D6FD65ABD737B6FF75E944E
        945218639C73DE77B55639679C737B6F7B6F9C73D65A2925F75EDE779D6B7E67
        7B6B5A6BBD779C739C6F7B6F1963B5569452F75E1042524ABD737B6F7B6F9C73
        7C6FCF391863BE737D679E6F1863DE7BBD77BD779C739C73FA4EF856524ACF39
        524AEF3D18639C737C739B6FB76B3763744EDF7B7E677B675A6BDF77DE7FDE7B
        DE7B3A635342CE3531465A6BF75E2925F03D9C73986B8E574B4BF456D65AFF7F
        FF7FB5567B6B3C575D579C6BD65ACE39EF3DF75EBD73DE7B185F0E4A6A31934E
        4D4FEC462F420925F75EFF7FFF7FDE7B9452D65ED756CF35AD35B5569C73BE77
        9C737B6FD75A6D5A67624A2D1042734E39672929D65AFF7FFF7F9C737B6FDE7B
        734E524A7B6BBD77BD777B6F396739673A672E46E876863D4B297B6F7B6F0821
        D65AFF7FFF7FF75EFF7FFF7FD65A7B6BBD777B6F396739673A6739679C733A67
        8B5E866649295B6B524AA518F75EFF7FDE7BF75E1867DE7B7B6FD65AFF7F7B6F
        396739675A6BDE7BFF7FF8568C290977863DAE352925C618D65AFF7FFF7FFF7F
        BD771863396794529C73FF7F5B6BBD77FF7F5A6B132A71016F198B5E866AC61C
        C718C6185A6BFF7FFF7FFF7FFF7FFF7FBD77D65E734EFF7FFF7F7B77753AB301
        B209954A9C772F4A094AAC354A291863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        7B6F396BD74E140EF405753A7B73FF7FFF7F185F954EB55AAD39FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F944AF4055532524A9C77FF7FFF7FFF7FBD77
        8C39E7505256FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B5A6BFF7F
        DE7B3967F75E9C73FF7F1863BD771867BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B7B6FD65A5A6BFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        BD77FF7FFF7FFF7FFF7FFF7FFF7F}
    end
  end
  object edtTipoContribuicao: TEdit
    Left = 189
    Top = 173
    Width = 36
    Height = 21
    TabOrder = 4
    OnKeyPress = edtTipoContribuicaoKeyPress
  end
  object edtDataPgto: TMaskEdit
    Left = 271
    Top = 217
    Width = 106
    Height = 21
    EditMask = '!99/99/0000;1;_'
    MaxLength = 10
    TabOrder = 13
    Text = '  /  /    '
  end
  object dsFormaPagamento: TDataSource
    DataSet = DM.qFormaPagamento
    Left = 344
    Top = 8
  end
end
