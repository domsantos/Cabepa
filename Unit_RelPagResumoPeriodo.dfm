inherited frm_RelPagResumoPeriodo: Tfrm_RelPagResumoPeriodo
  Caption = 'frm_RelPagResumoPeriodo'
  ExplicitLeft = -46
  ExplicitWidth = 870
  ExplicitHeight = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = 0
    ExplicitLeft = 0
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 243
        Caption = 'RESUMO PAGAMENTO POR PER'#205'ODO'
        ExplicitWidth = 243
      end
      object RLLabel6: TRLLabel
        Left = 96
        Top = 48
        Width = 71
        Height = 16
        Caption = 'PER'#205'ODO: '
      end
      object RLLabel7: TRLLabel
        Left = 173
        Top = 48
        Width = 58
        Height = 16
      end
    end
    inherited RLBand2: TRLBand
      inherited RLLabel3: TRLLabel
        Width = 80
        Caption = 'DESCRI'#199#195'O'
        ExplicitWidth = 80
      end
      inherited RLLabel4: TRLLabel
        Left = 208
        Width = 77
        Caption = 'VANTAGEM'
        ExplicitLeft = 208
        ExplicitWidth = 77
      end
      object RLLabel8: TRLLabel
        Left = 368
        Top = 32
        Width = 76
        Height = 16
        Caption = 'DESCONTO'
      end
      object RLLabel9: TRLLabel
        Left = 613
        Top = 29
        Width = 102
        Height = 16
        Caption = 'QTD. PESSOAS'
      end
      object RLLabel11: TRLLabel
        Left = 512
        Top = 32
        Width = 55
        Height = 16
        Caption = 'LIQUIDO'
      end
    end
    inherited RLBand3: TRLBand
      inherited RLDBText1: TRLDBText
        Top = 2
        Width = 62
        DataField = 'nm_Rubrica'
        ExplicitTop = 2
        ExplicitWidth = 62
      end
      inherited RLDBText2: TRLDBText
        Left = 208
        Top = 3
        Width = 89
        Alignment = taRightJustify
        DataField = 'vl_vantagens'
        ExplicitLeft = 208
        ExplicitTop = 3
        ExplicitWidth = 89
      end
      object RLDBText3: TRLDBText
        Left = 368
        Top = 2
        Width = 89
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_descontos'
        DataSource = dsRelatorio
      end
      object RLDBText4: TRLDBText
        Left = 635
        Top = 2
        Width = 80
        Height = 16
        Alignment = taRightJustify
        DataField = 'qt_beneficiados'
        DataSource = dsRelatorio
      end
    end
    inherited RLBand4: TRLBand
      Top = 243
      Height = 35
      ExplicitTop = 243
      ExplicitHeight = 35
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 190
      Width = 718
      Height = 53
      BandType = btSummary
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = True
      BeforePrint = RLBand5BeforePrint
      object RLDBResult1: TRLDBResult
        Left = 208
        Top = 6
        Width = 105
        Height = 16
        DataField = 'vl_vantagens'
        DataSource = dsRelatorio
        DisplayMask = 'R$ ###,##0.00'
        Info = riSum
      end
      object RLDBResult2: TRLDBResult
        Left = 368
        Top = 6
        Width = 105
        Height = 16
        DataField = 'vl_descontos'
        DataSource = dsRelatorio
        DisplayMask = 'R$ ###,##0.00'
        Info = riSum
      end
      object RLLabel10: TRLLabel
        Left = 512
        Top = 6
        Width = 81
        Height = 16
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = frm_FiltroPagResumoPeriodo.qListagem
    Left = 80
    Top = 272
  end
end
