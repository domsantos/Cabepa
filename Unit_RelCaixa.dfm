inherited frm_RelCaixa: Tfrm_RelCaixa
  Caption = 'Caixa'
  ExplicitWidth = 870
  ExplicitHeight = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = -85
    Width = 1123
    Height = 794
    PageSetup.Orientation = poLandscape
    ExplicitLeft = -85
    ExplicitWidth = 1123
    ExplicitHeight = 794
    inherited RLBand1: TRLBand
      Width = 1047
      ExplicitWidth = 1047
      inherited RLLabel2: TRLLabel
        Width = 180
        Caption = 'Relat'#243'rio de Caixa por Usu'#225'rio'
        ExplicitWidth = 180
      end
      object RLLabel8: TRLLabel
        Left = 96
        Top = 54
        Width = 72
        Height = 16
        Caption = 'Data Caixa:'
      end
      object RLLabel9: TRLLabel
        Left = 174
        Top = 54
        Width = 44
        Height = 16
        Caption = 'lblData'
      end
      object RLLabel10: TRLLabel
        Left = 282
        Top = 54
        Width = 52
        Height = 16
        Caption = 'Usu'#225'rio:'
      end
      object RLLabel11: TRLLabel
        Left = 340
        Top = 54
        Width = 61
        Height = 16
        Caption = 'lblUsuario'
      end
      object RLLabel14: TRLLabel
        Left = 520
        Top = 54
        Width = 52
        Height = 16
        Caption = 'Per'#237'odo:'
      end
      object lblPeriodo: TRLLabel
        Left = 575
        Top = 55
        Width = 61
        Height = 16
      end
    end
    inherited RLBand2: TRLBand
      Width = 1047
      ExplicitWidth = 1047
      inherited RLLabel3: TRLLabel
        Left = 3
        Width = 43
        Caption = 'NOME'
        ExplicitLeft = 3
        ExplicitWidth = 43
      end
      inherited RLLabel4: TRLLabel
        Left = 384
        Top = 28
        Width = 133
        Caption = 'TIPO CONTRIBUI'#199#195'O'
        ExplicitLeft = 384
        ExplicitTop = 28
        ExplicitWidth = 133
      end
      object RLLabel6: TRLLabel
        Left = 787
        Top = 28
        Width = 30
        Height = 16
        Caption = 'REF'
      end
      object RLLabel7: TRLLabel
        Left = 888
        Top = 28
        Width = 48
        Height = 16
        Caption = 'VALOR'
      end
      object RLLabel13: TRLLabel
        Left = 576
        Top = 28
        Width = 38
        Height = 16
        Caption = 'DATA'
      end
    end
    inherited RLBand4: TRLBand
      Width = 1047
      Height = 59
      ExplicitWidth = 1047
      ExplicitHeight = 59
      inherited RLSystemInfo1: TRLSystemInfo
        Top = 40
        Width = 39
        ExplicitTop = 40
        ExplicitWidth = 39
      end
      inherited RLLabel5: TRLLabel
        Top = 40
        ExplicitTop = 40
      end
      inherited RLSystemInfo2: TRLSystemInfo
        Top = 40
        ExplicitTop = 40
      end
      object RLDBResult1: TRLDBResult
        Left = 845
        Top = 14
        Width = 199
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_ContribuicaoVencimento'
        DataSource = dsRelatorio
        DisplayMask = 'R$ #,##0.00'
        Info = riSum
      end
    end
    inherited RLBand3: TRLBand
      Width = 1047
      ExplicitWidth = 1047
      inherited RLDBText1: TRLDBText
        Left = 4
        Top = 0
        Width = 38
        DataField = 'nm_pastor'
        ExplicitLeft = 4
        ExplicitTop = 0
        ExplicitWidth = 38
      end
      inherited RLDBText2: TRLDBText
        Left = 384
        Top = -1
        Width = 122
        DataField = 'nm_tipoContribuicao'
        ExplicitLeft = 384
        ExplicitTop = -1
        ExplicitWidth = 122
      end
      object RLDBText3: TRLDBText
        Left = 787
        Top = -1
        Width = 30
        Height = 16
        Alignment = taRightJustify
        DataField = 'dt_mesRefContribuicao'
        DataSource = dsRelatorio
      end
      object RLLabel12: TRLLabel
        Left = 818
        Top = 0
        Width = 8
        Height = 16
        Caption = '/'
      end
      object RLDBText4: TRLDBText
        Left = 827
        Top = 0
        Width = 48
        Height = 16
        DataField = 'dt_anoRefContribuicao'
        DataSource = dsRelatorio
      end
      object RLDBText5: TRLDBText
        Left = 889
        Top = -1
        Width = 137
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_ContribuicaoVencimento'
        DataSource = dsRelatorio
        DisplayMask = '#,##0.00'
      end
      object RLDBText6: TRLDBText
        Left = 576
        Top = 0
        Width = 31
        Height = 16
        DataField = 'dt_Contribuicao'
        DataSource = dsRelatorio
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = frmCaixa.qCaixa
  end
  object RLXLSFilter1: TRLXLSFilter
    PageSetup.PrintGridLines = True
    PageSetup.FitToPagesTall = False
    DisplayName = 'Relatorio_Entrada'
    Left = 568
    Top = 320
  end
end
