unit Unit_ImpressaoPagamentos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, DB, ADODB, ExtCtrls, RxLookup, Grids, DBGrids,
  Buttons,INIFiles, Mask;

type
  Tfrm_ImpressaoPagamentos = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    qUltimosPagamentos: TADOQuery;
    qUltimosPagamentoscd_tipoContribuicao: TIntegerField;
    qUltimosPagamentosdt_mesRefContribuicao: TIntegerField;
    qUltimosPagamentosdt_anoRefContribuicao: TIntegerField;
    qUltimosPagamentosds_referencia: TWideStringField;
    dsUltimosPagamentos: TDataSource;
    edtContribuinte: TEdit;
    Label2: TLabel;
    Button1: TButton;
    lblContribuinte: TLabel;
    DBGrid1: TDBGrid;
    Button2: TButton;
    lbSelecionados: TListBox;
    Button3: TButton;
    BitBtn1: TBitBtn;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    procedure Button1Click(Sender: TObject);
    procedure edtContribuinteExit(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_ImpressaoPagamentos: Tfrm_ImpressaoPagamentos;

implementation

uses Unit_DM, Unit_PesquisaContribuinte, Unit_ClassImpressoraMatricial, FNLIB1;

{$R *.dfm}

procedure Tfrm_ImpressaoPagamentos.BitBtn1Click(Sender: TObject);
var
  I,x,codigo,mes,ano,tipo : integer;
  linha : string;
  valor : double;
  obs : Boolean;
begin

  dm.qBuscaContribuinte.Close;
  dm.qBuscaContribuinte.Parameters.ParamByName('codigo').Value := edtContribuinte.Text;
  dm.qBuscaContribuinte.Open;
  dm.impressora.Iniciaimpressao ;
  dm.impressora.imprimeTexto('======== ASSOCIACAO CASA DO PASTOR =========');
  dm.impressora.ImprimeTexto('ROD. MARIO COVAS - PASS. SUELY No.02  -  UNA');
//  dm.impressora.ImprimeTexto('R. PR. JOAO TRIGUEIRO,N.3 CEP 66.6520170 UNA');
  //dm.impressora.imprimeTexto('FONE/FAX:(91) 3245-1739 CEP:66.652-210 BELEM');
  dm.impressora.imprimeTexto('==== DEMOSTRATIVO DE RECEBIMENTO DOACAO ====');
  dm.impressora.imprimeTexto('DATA DE IMPRESSAO'+FormatDateTime('dd/mm/yyyy',date));
  dm.impressora.imprimeTexto('No. ASSOCIADO: '+ edtContribuinte.Text);
  dm.impressora.imprimeTexto('NOME PASTOR:');

  dm.impressora.imprimeTexto(dm.qBuscaContribuintenm_pastor.Value);
  dm.impressora.imprimeTexto('DATA          DESCRICAO                VALOR');
  dm.impressora.imprimeTexto('--------------------------------------------');
  obs := false;
  for I := 0 to lbSelecionados.Items.Count - 1 do
  begin
    linha := lbSelecionados.Items.ValueFromIndex[I];

    codigo := strtoint(edtContribuinte.Text);

    tipo := strtoint(copy(linha,1,1));

   // showmessage(trim(copy(linha,17,2)) +'/'+copy(linha,20,4));

    mes := strtoint(trim(copy(linha,17,2)));
    ano := strtoint(copy(linha,20,4));

    dm.QDescricaoContrib.close;

    dm.QDescricaoContrib.Parameters.ParamByName('codigo').Value := codigo;
    dm.QDescricaoContrib.Parameters.ParamByName('mes').Value := mes;
    dm.QDescricaoContrib.Parameters.ParamByName('ano').Value := ano;
    dm.QDescricaoContrib.Parameters.ParamByName('tipo').Value := tipo;

    dm.QDescricaoContrib.Open;
    dm.QDescricaoContrib.First;

    while not dm.QDescricaoContrib.Eof do
    begin
      dm.impressora.imprimeTexto(
                myPad(
               inttostr(dm.qDescricaoContrib.FieldByName('dt_mesRefContribuicao').Value)+'/'+
               inttostr(dm.QDescricaoContrib.FieldByName('dt_anoRefContribuicao').Value),
               14,'L',false)+

               myPad(dm.QDescricaoContrib.FieldByName('nm_tipoContribuicao').Value,19,'L',false)+
               myPad(FormatFloat('#,##0.00',dm.QDescricaoContrib.FieldByName('vl_ContribuicaoVencimento').Value),11,'R',true));

      valor := valor + dm.QDescricaoContrib.FieldByName('vl_ContribuicaoVencimento').Value;

      dm.QDescricaoContrib.Next;
    end; // end while
    end; // enf for

    dm.impressora.imprimeTexto('--------------------------------------------');
    dm.impressora.imprimeTexto('VALOR TOTAL DA CONTRIBUICAO     '+myPad(FormatFloat('#,##0.00',valor),12,'R',true));
    valor := 0;
    dm.impressora.imprimeTexto('');
    dm.impressora.imprimeTexto('FORMA DE DOACAO');
    dm.impressora.imprimeTexto('DESCRICAO                              VALOR');
    dm.impressora.imprimeTexto('--------------------------------------------');
  for I := 0 to lbSelecionados.Items.Count - 1 do
  begin
    linha := lbSelecionados.Items.ValueFromIndex[I];

    codigo := strtoint(edtContribuinte.Text);

    tipo := strtoint(copy(linha,1,1));

    //showmessage(trim(copy(linha,17,2)) +'/'+copy(linha,20,4));

    mes := strtoint(trim(copy(linha,17,2)));
    ano := strtoint(copy(linha,20,4));

    dm.qImpressaoPagamentos.Close;
    dm.qImpressaoPagamentos.Parameters.ParamByName('codigo').Value := codigo;
    dm.qImpressaoPagamentos.Parameters.ParamByName('mes').Value := mes;
    dm.qImpressaoPagamentos.Parameters.ParamByName('ano').Value := ano;
    dm.qImpressaoPagamentos.Parameters.ParamByName('tipo').Value := tipo;

    dm.QImpressaoPagamentos.open;
    dm.QImpressaoPagamentos.first;

    // Detalahndo Forma de Pagamento



    while not dm.QImpressaoPagamentos.Eof do
    begin
      dm.impressora.imprimeTexto(myPad(dm.QImpressaoPagamentosds_formPagto.Value,27,'L',false)+'     '+
                  myPad(FormatFloat('#,##0.00',dm.QImpressaoPagamentosvl_pagtoContribuicao.Value),12,'R',true));
      valor := valor + dm.QImpressaoPagamentosvl_pagtoContribuicao.Value;

      if dm.qImpressaoPagamentosds_formPagto.Value = 'CHEQUE-PRE' then
        obs := true;

      dm.qImpressaoPagamentos.Next;
    end; // end while
  end; // end for

  dm.impressora.imprimeTexto('--------------------------------------------');
  dm.impressora.imprimeTexto('VALOR TOTAL DOS DOACAO      '+myPad(FormatFloat('#,##0.00',valor),12,'R',true));

  dm.impressora.imprimeTexto('');
  if obs then
  begin
    dm.impressora.imprimeTexto('OBS.:');
    dm.impressora.imprimeTexto('OS  VALORES  DOADOS COM  CHEQUES-PRE DATADOS');
    dm.impressora.imprimeTexto('SERAO  CONSIDERADOS  QUITADOS  SOMENTE  APOS');
    dm.impressora.imprimeTexto('COMPENSACAO DE TODOS OS CHEQUES RELACIONADOS');
    dm.impressora.imprimeTexto('NO DEMONSTRATIVO DE DOACAO.');
  end;

  dm.impressora.imprimeTexto('');



  dm.impressora.imprimeTexto('Impresso por: '+dm.nome);
  dm.impressora.imprimeTexto('');
  dm.impressora.imprimeTexto('');
  dm.impressora.imprimeTexto(myPad('__________________________________',44,'C',false));
  dm.impressora.imprimeTexto(myPad('Assinatura',44,'C',false));
  dm.impressora.imprimeTexto('');
  dm.impressora.imprimeTexto('');
  dm.impressora.imprimeTexto('');
  dm.impressora.imprimeTexto('');
  dm.impressora.imprimeTexto('');
  dm.impressora.imprimeTexto('');
  dm.impressora.imprimeTexto('');
  dm.impressora.imprimeTexto('');
  dm.impressora.imprimeTexto('');
  dm.impressora.imprimeTexto('');


  dm.impressora.FinalizaImpressao;
  showmessage('Impress�o Finalizada');
  
end;


procedure Tfrm_ImpressaoPagamentos.Button1Click(Sender: TObject);
begin
  lblContribuinte.Caption := '';
  Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
  frm_PesquisaContribuinte.origem := 4;
  frm_PesquisaContribuinte.Show;
end;

procedure Tfrm_ImpressaoPagamentos.Button2Click(Sender: TObject);
var
  contribuicao : string;
begin
  if qUltimosPagamentoscd_tipoContribuicao.Value = 1 then
    contribuicao := '1Dizimo      '
  else
    contribuicao := '2Outros      ';


  lbSelecionados.Items.Add(' '+
      contribuicao+' - '+
      myPad(inttostr(qUltimosPagamentosdt_mesRefContribuicao.Value),2,'R',false)+'/'+
      inttostr(qUltimosPagamentosdt_anoRefContribuicao.Value)
  );

end;

procedure Tfrm_ImpressaoPagamentos.Button3Click(Sender: TObject);
begin
  lbSelecionados.Items.Delete(lbSelecionados.ItemIndex);
end;

procedure Tfrm_ImpressaoPagamentos.Button4Click(Sender: TObject);
begin
  qUltimosPagamentos.close;

  qUltimosPagamentos.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
  qUltimosPagamentos.Parameters.ParamByName('inicio').Value :=
    FormatDateTime('YYYY-MM-DD',StrToDate(MaskEdit1.Text));
  qUltimosPagamentos.Parameters.ParamByName('fim').Value :=
    FormatDateTime('YYYY-MM-DD 22:00:00',StrToDate(MaskEdit2.Text));

  qUltimosPagamentos.Open;
end;

procedure Tfrm_ImpressaoPagamentos.Button5Click(Sender: TObject);
var
  contribuicao : string;
begin

  qUltimosPagamentos.First;

  while not qUltimosPagamentos.Eof do
  begin
    if qUltimosPagamentoscd_tipoContribuicao.Value = 1 then
      contribuicao := '1Dizimo      '
    else
      contribuicao := '2Outros      ';

    lbSelecionados.Items.Add(' '+
      contribuicao+' - '+
      myPad(inttostr(qUltimosPagamentosdt_mesRefContribuicao.Value),2,'R',false)+'/'+
      inttostr(qUltimosPagamentosdt_anoRefContribuicao.Value)
  );
    qUltimosPagamentos.Next
  end;

end;

procedure Tfrm_ImpressaoPagamentos.Button6Click(Sender: TObject);
var
  I :integer;
begin

  for I := 0 to lbSelecionados.Items.Count - 1 do
  begin
    lbSelecionados.Items.Delete(I);
  end;
end;

procedure Tfrm_ImpressaoPagamentos.DBGrid1DblClick(Sender: TObject);
begin
  Button2.Click;
end;

procedure Tfrm_ImpressaoPagamentos.edtContribuinteExit(Sender: TObject);
begin
  if edtContribuinte.Text <> '' then
  BEGIN
    dm.qBuscaContribuinte.Close;
    dm.qBuscaContribuinte.Parameters.ParamByName('codigo').Value :=
        strtoint(edtContribuinte.Text);

    dm.qBuscaContribuinte.Open;


    if dm.qBuscaContribuinte.RecordCount < 0 then
    begin
      ShowMessage('Nenhum contribuinte encontrado com esse c�digo');
      edtContribuinte.SetFocus;
    end
    else
    begin
      lblContribuinte.Caption := dm.qBuscaContribuintenm_pastor.value;

    end;
  END;
end;

procedure Tfrm_ImpressaoPagamentos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

end.
