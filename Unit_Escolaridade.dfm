inherited frm_Escolaridade: Tfrm_Escolaridade
  Caption = 'Escolaridade'
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel1: TPanel
    inherited btnRelatorio: TBitBtn
      OnClick = btnRelatorioClick
    end
  end
  inherited Panel2: TPanel
    object Label1: TLabel [0]
      Left = 8
      Top = 42
      Width = 61
      Height = 16
      Caption = 'Escolaridade'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit [1]
      Left = 8
      Top = 62
      Width = 325
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_escpastor'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBGrid1: TDBGrid [2]
      Left = 6
      Top = 96
      Width = 329
      Height = 120
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
    inherited Panel3: TPanel
      Caption = ' Cadastro de Escolaridade'
      TabOrder = 2
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalcd_escpastor: TIntegerField
      FieldName = 'cd_escpastor'
      Visible = False
    end
    object CDSPrincipalds_escpastor: TStringField
      DisplayLabel = 'Escolaridade'
      FieldName = 'ds_escpastor'
      Required = True
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    SQL.Strings = (
      'select cd_escpastor, ds_escpastor'
      'from tbEscolaridade'
      'order by ds_escpastor')
    object QPrincipalcd_escpastor: TIntegerField
      FieldName = 'cd_escpastor'
    end
    object QPrincipalds_escpastor: TStringField
      FieldName = 'ds_escpastor'
      Required = True
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 632
    Top = 312
  end
end
