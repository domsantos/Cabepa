Simplistica Icon Set, version 1.01
http://dryicons.com/free-icons/preview/simplistica/

Information
----------------------

This icon set contains 25 quality icons in the following formats:
	High Quality 32-bit Transparent PNG
		16 x 16 px
		24 x 24 px
		32 x 32 px
		48 x 48 px 
		128 x 128 px



Licensing
----------------------

This work is licensed under a Creative Commons Attribution 3.0 License. 
This means that you can do pretty much everything. 
However, you must include a link back to http://dryicons.com/ in your credits. 
Please contact us at contact@dryicons.com to discuss the licensing.
