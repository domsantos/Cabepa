unit Unit_RegistraPagamentoLote;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, DB, rxToolEdit, rxCurrEdit, Buttons, ADODB;

type
  Tfrm_RegistroPagamentoLote = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    edtContribuinte: TEdit;
    Button1: TButton;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    edtInicio: TMaskEdit;
    edtFim: TMaskEdit;
    Label7: TLabel;
    Label8: TLabel;
    dsFormaPagamento: TDataSource;
    cbFormaPagamento: TDBLookupComboBox;
    Banco: TLabel;
    Label9: TLabel;
    edtdataResgate: TMaskEdit;
    edtBanco: TEdit;
    edtCheque: TEdit;
    Label10: TLabel;
    Label11: TLabel;
    edtAgencia: TEdit;
    BitBtn1: TBitBtn;
    edtValor: TCurrencyEdit;
    Label12: TLabel;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    edtValorParcelas: TCurrencyEdit;
    Label14: TLabel;
    edtParcelas: TEdit;
    BitBtn2: TBitBtn;
    edtTipoContribuicao: TEdit;
    Label15: TLabel;
    Label16: TLabel;
    edtDataPgto: TMaskEdit;
    Label17: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure edtContribuinteKeyPress(Sender: TObject; var Key: Char);
    procedure edtContribuinteExit(Sender: TObject);
    procedure edtAgenciaKeyPress(Sender: TObject; var Key: Char);
    procedure edtChequeKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure edtTipoContribuicaoKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RegistroPagamentoLote: Tfrm_RegistroPagamentoLote;

implementation

uses Unit_PesquisaContribuinte, Unit_DM;

{$R *.dfm}

procedure Tfrm_RegistroPagamentoLote.BitBtn1Click(Sender: TObject);

begin

  dm.spRegistraPagamentoLote.Close;
  {@codigo int
	,@valor money
	,@dini date
	,@dfim date
	,@tipo smallint
	,@forma smallint
	,@banco varchar(40)
	,@agencia varchar(20)
	,@cheque varchar(20)
	,@data date }
  dm.spRegistraPagamentoLote.Parameters.ParamByName('@codigo').Value :=
    edtContribuinte.Text;
  dm.spRegistraPagamentoLote.Parameters.ParamByName('@valor').Value :=
    edtValor.Value;
  dm.spRegistraPagamentoLote.Parameters.ParamByName('@dini').Value :=
    StrToDate(edtInicio.Text);
  dm.spRegistraPagamentoLote.Parameters.ParamByName('@dfim').Value :=
    StrToDate(edtFim.Text);
  dm.spRegistraPagamentoLote.Parameters.ParamByName('@forma').Value :=
    cbFormaPagamento.KeyValue;
  dm.spRegistraPagamentoLote.Parameters.ParamByName('@banco').Value := '';
  dm.spRegistraPagamentoLote.Parameters.ParamByName('@agencia').Value := '';
  dm.spRegistraPagamentoLote.Parameters.ParamByName('@cheque').Value := '';
  dm.spRegistraPagamentoLote.Parameters.ParamByName('@data').Value
    := StrToDate(edtDataPgto.Text);
  dm.spRegistraPagamentoLote.Parameters.ParamByName('@dtPgto').Value
    := StrToDate(edtDataPgto.Text);

  dm.spRegistraPagamentoLote.Parameters.ParamByName('@tipo').Value :=
    strtoint(edtTipoContribuicao.Text);

  if cbFormaPagamento.KeyValue = 2 then
  begin
    dm.spRegistraPagamentoLote.Parameters.ParamByName('@banco').Value :=
      edtBanco.Text;
    dm.spRegistraPagamentoLote.Parameters.ParamByName('@agencia').Value :=
      edtAgencia.Text;
    dm.spRegistraPagamentoLote.Parameters.ParamByName('@cheque').Value :=
      edtCheque.Text;

  end
  else if cbFormaPagamento.KeyValue = 3 then
  begin
    dm.spRegistraPagamentoLote.Parameters.ParamByName('@banco').Value :=
      edtBanco.Text;
    dm.spRegistraPagamentoLote.Parameters.ParamByName('@agencia').Value :=
      edtAgencia.Text;
    dm.spRegistraPagamentoLote.Parameters.ParamByName('@cheque').Value :=
      edtCheque.Text;
    dm.spRegistraPagamentoLote.Parameters.ParamByName('@data').Value :=

      StrToDate(edtDataPgto.Text)

  end;


  try
    dm.spRegistraPagamentoLote.ExecProc;
    showmessage('Pagamento Registrado.');
  except
    on E:Exception do begin
      showmessage('Erro ao registrar pagamentos.'+e.Message);
    end;

  end;






end;

procedure Tfrm_RegistroPagamentoLote.BitBtn2Click(Sender: TObject);
begin
  if edtValor.Value = 0 then
  begin
    showmessage('Digite um valor');
    edtValor.SetFocus;
    exit;
  end;

  if edtInicio.Text = '  /   /    ' then
  begin
    showmessage('Digite uma data v�lida ');
    edtInicio.SetFocus;
    exit;
  end;
   if edtFim.Text = '  /   /    ' then
  begin
    showmessage('Digite uma data v�lida ');
    edtFim.SetFocus;
    exit;
  end;
  if edtTipoContribuicao.Text = '' then
  begin
    showmessage('Digite um tipo de contribui��o');
    edtTipoContribuicao.SetFocus;
    exit;
  end; 
  

  dm.qDiferencaDatas.close;
  dm.qDiferencaDatas.Parameters.ParamByName('dini').Value :=
    StrToDateTime(edtInicio.Text);
  dm.qDiferencaDatas.Parameters.ParamByName('dfim').Value :=
    StrToDateTime(edtFim.Text);

  dm.qDiferencaDatas.Open;

  edtValorParcelas.Value := edtValor.Value / dm.qDiferencaDatasmeses.Value;
  edtParcelas.Text := inttostr(dm.qDiferencaDatasmeses.Value);

  BitBtn1.Enabled := true;


end;

procedure Tfrm_RegistroPagamentoLote.Button1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
  frm_PesquisaContribuinte.origem := 7;
  frm_PesquisaContribuinte.Show;
end;

procedure Tfrm_RegistroPagamentoLote.edtAgenciaKeyPress(Sender: TObject;
  var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_RegistroPagamentoLote.edtChequeKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_RegistroPagamentoLote.edtContribuinteExit(Sender: TObject);
begin
if edtContribuinte.Text <> '' then
  begin
    dm.qBuscaContribuinte.Close;
    dm.qBuscaContribuinte.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    dm.qBuscaContribuinte.Open;

    if dm.qBuscaContribuinte.RecordCount = 0 then
    begin
      showmessage('Contribuinte n�o encontrado');
      edtContribuinte.SetFocus;
    end
    else
      Label3.Caption := dm.qBuscaContribuintenm_pastor.Value;
  end;
end;

procedure Tfrm_RegistroPagamentoLote.edtContribuinteKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_RegistroPagamentoLote.edtTipoContribuicaoKeyPress(
  Sender: TObject; var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_RegistroPagamentoLote.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.qFormaPagamento.Close;
  self := nil;
  Action := caFree;
end;

procedure Tfrm_RegistroPagamentoLote.FormCreate(Sender: TObject);
begin
  dm.qFormaPagamento.Open;
  edtDataPgto.Text := FormatDateTime('DD/MM/YYYY',date);
end;

end.
