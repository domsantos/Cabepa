object frm_MovimentoCaixa: Tfrm_MovimentoCaixa
  Left = 0
  Top = 0
  Caption = 'Movimento Caixa'
  ClientHeight = 533
  ClientWidth = 678
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label8: TLabel
    Left = 9
    Top = 41
    Width = 91
    Height = 19
    Caption = 'Entradas: R$'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 177
    Top = 41
    Width = 32
    Height = 19
    Alignment = taRightJustify
    Caption = '0,00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 678
    Height = 57
    Align = alTop
    Color = clGradientActiveCaption
    ParentBackground = False
    TabOrder = 0
    object Label1: TLabel
      Left = 9
      Top = 4
      Width = 66
      Height = 16
      Caption = 'No. CAIXA'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 88
      Top = 4
      Width = 31
      Height = 16
      Caption = 'Data'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtNumCaixa: TEdit
      Left = 9
      Top = 21
      Width = 51
      Height = 31
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object edtDataCaixa: TMaskEdit
      Left = 81
      Top = 20
      Width = 113
      Height = 31
      EditMask = '!99/99/0000;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '  /  /    '
    end
    object btnImprimir: TBitBtn
      Left = 581
      Top = 22
      Width = 89
      Height = 29
      Caption = 'Imprimir'
      TabOrder = 3
      OnClick = btnImprimirClick
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7F9C73F75EF75EF75EF75EF75EF75EF75EF75EF75E
        F75EF75EF75EF75EF75E9C73FF7FFF7FFF7FFF7FFF7FFF7F9C739C73FF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C739C73FF7FFF7FFF7FFF7F
        FF7FFF7FDE7B5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        5A6BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F1863BD779C739C739C739C739C73
        9C739C739C739C739C73BD771863FF7FFF7FFF7FFF7FFF7FDE7B9C739C73734E
        3967F75EF75EF75EF75EF75EF75EF75EF75EF75EF75E3967524A9C739C73DE7B
        FF7F5A6BB5569452945231469452945294529452945294529452945294529452
        94529452314694529452B5565A6B1863734E3A67F75E6C314B296B2D6B2D6B2D
        6B2D6B2D6B2D6B2D6B2D6B2D6B2D6B2D8C31D75A3A67734E18635A6BB6569C73
        5B6B6B2D29252925292529252925292509250925082508210821082109253967
        9C73B6565A6B9C731863BD73BD773146EF3DCE3DCE39AD358D358C316B2D4A2D
        4A2929252925E72029257B6FBD7718639C73BD7739677B6F9D739452524A524A
        31461042EF41EF3DCE39AD39AD358C316B2D4A298C319C737B6F3967BD77DE7B
        7B6B5A6B9C73D65AD65AF75E1863186339673967186339671863F75EB556734E
        524A9C735A6B7B6BDE7BFF7F7B6F5A6BBD77D656F75E18631863F75EF75E1963
        1863F75ED75A1863185FF85EF75EBD775A6B7B6FFF7FFF7FBD7718637B6FD65A
        F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75ED65A5A6B1863BD77
        FF7FFF7FFF7FBD779C731863DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
        DE7BFF7F18639C73BD77FF7FFF7FFF7FFF7FFF7FFF7F7A6BBE77DE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B7B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7F7B6FBD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDF7B1863FF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73BD77DE7BDE7BDE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7B5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6FBD77DE7B
        DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BBD779C73FF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7F7B6FBD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
        9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6FBD77DE7BDE7BDE7BDE7BDE7B
        DE7BDE7BDE7BDE7BDE7BDE7B9C73FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F7B6F
        BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B9C73FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FBD77F75EF75EF75EF75EF75EF75EF75EF75EF75EF75E
        F75EF75E9C73FF7FFF7FFF7FFF7F}
    end
    object btnAbrir: TBitBtn
      Left = 485
      Top = 22
      Width = 90
      Height = 29
      Caption = 'Abrir'
      TabOrder = 2
      OnClick = btnAbrirClick
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F94528C358C358C358C358C358C35
        8C358C358C358C35AC35AD358C35AD353967FF7FFF7FFF7FFF7FFF7FDE7B8C35
        C618E71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CF75EFF7F
        FF7FFF7FFF7FFF7F1863C6182925D65AF75EF75EF75ED75AD65AD65AD65AD65A
        9452955294520821D65AFF7FFF7BFF7FFF7FFF7F1863A5183146DE7BBD739D73
        9C739C739C739C737B6F955210421042F85E2925D65AFF7FDE77BE73DF77DE7B
        524A08217B6FBE779C739C739C739C6F7C6FB556CE3931463967534ACE392925
        D65ADF7BBE779E6B7D5F396394563146D65A5A6BBD77BD779C73BD73F75ECE39
        F75EBD77BD775A6BAD35E720D65ADF7BBE737E679D6FD65ABD737B6FF75E944E
        945218639C73DE77B55639679C737B6F7B6F9C73D65A2925F75EDE779D6B7E67
        7B6B5A6BBD779C739C6F7B6F1963B5569452F75E1042524ABD737B6F7B6F9C73
        7C6FCF391863BE737D679E6F1863DE7BBD77BD779C739C73FA4EF856524ACF39
        524AEF3D18639C737C739B6FB76B3763744EDF7B7E677B675A6BDF77DE7FDE7B
        DE7B3A635342CE3531465A6BF75E2925F03D9C73986B8E574B4BF456D65AFF7F
        FF7FB5567B6B3C575D579C6BD65ACE39EF3DF75EBD73DE7B185F0E4A6A31934E
        4D4FEC462F420925F75EFF7FFF7FDE7B9452D65ED756CF35AD35B5569C73BE77
        9C737B6FD75A6D5A67624A2D1042734E39672929D65AFF7FFF7F9C737B6FDE7B
        734E524A7B6BBD77BD777B6F396739673A672E46E876863D4B297B6F7B6F0821
        D65AFF7FFF7FF75EFF7FFF7FD65A7B6BBD777B6F396739673A6739679C733A67
        8B5E866649295B6B524AA518F75EFF7FDE7BF75E1867DE7B7B6FD65AFF7F7B6F
        396739675A6BDE7BFF7FF8568C290977863DAE352925C618D65AFF7FFF7FFF7F
        BD771863396794529C73FF7F5B6BBD77FF7F5A6B132A71016F198B5E866AC61C
        C718C6185A6BFF7FFF7FFF7FFF7FFF7FBD77D65E734EFF7FFF7F7B77753AB301
        B209954A9C772F4A094AAC354A291863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        7B6F396BD74E140EF405753A7B73FF7FFF7F185F954EB55AAD39FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F944AF4055532524A9C77FF7FFF7FFF7FBD77
        8C39E7505256FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B5A6BFF7F
        DE7B3967F75E9C73FF7F1863BD771867BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B7B6FD65A5A6BFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        BD77FF7FFF7FFF7FFF7FFF7FFF7F}
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 57
    Width = 678
    Height = 96
    Align = alTop
    BevelInner = bvLowered
    Enabled = False
    TabOrder = 1
    object Label3: TLabel
      Left = 160
      Top = 6
      Width = 73
      Height = 19
      Caption = 'Refer'#234'ncia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 496
      Top = 8
      Width = 37
      Height = 19
      Caption = 'Valor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object gbTipoMovimento: TRadioGroup
      Left = 9
      Top = 6
      Width = 128
      Height = 83
      Caption = 'Tipo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Entrada'
        'Saida')
      ParentFont = False
      TabOrder = 0
    end
    object edtValor: TCurrencyEdit
      Left = 496
      Top = 28
      Width = 121
      Height = 27
      Margins.Left = 5
      Margins.Top = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object BitBtn1: TBitBtn
      Left = 560
      Top = 61
      Width = 109
      Height = 29
      Caption = 'Registrar'
      TabOrder = 2
      OnClick = BitBtn1Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F7A6BD24A8B3A8836A736A7368736
        6A3690465967FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C736F428632
        C53AC536A536852E852E8532C536C53AA5326D3A5A6BFF7FFF7FFF7FFF7FFF7F
        FF7FFF7F5A6B6832E53E853245262522252625262526252625224526852EE53E
        672E175FFF7FFF7FFF7FFF7FFF7F39676832C53A452A45264526452A45264422
        44224526452A452645264526A5368632175FFF7FFF7FFF7F9C736832C5364526
        4526652A652A452A6B36D24ED24E8C3A4526652A452A45264526A536662E5A6B
        FF7FFF7F9146C536452A452A652A652E652E642AD0469E779E77F34E4426652E
        652A652A452A4526C5366D3AFF7F9C73672E852E652A652E652E852E8532642E
        CF469D739D77F24E642A852E652E652E652E652A652A852E5967D4528532652E
        652E852E853285328532842ECF469D779D77F34E642E853285328532852E652E
        652A852E6F426D3A852E652E8532852E642E842E842E632ACF46BE77BE77F24E
        632A842E642E642A642E852E652E852E4A32482E8532853284328A3614531353
        1353F24E575FBD77BD775967F34E135313531457AD3E642E852E852E682E482A
        A736A736A432CE46DF7FDE7BDE7BDE7BBE7BBD77BD77BD77DE7BDE7BDE7BDF7F
        1353842EA736A736682E482AC83EC83EC73ACF46DF7FDE7BDE7BDE7BBE7BBD77
        BD77BE7BDE7BDE7BDE7BDF7F1353A736C83EC93E692E4A32EA42EA42E942CB3E
        124F114F114FF04E565FDE7BDE7B5863F04A114F114F1253CD42C942EA42EA42
        6A326F3E0B470B4B0B4B0A4BE946E9460947E842114FDF7FDF7F1457C83E0947
        E946E946EA460B4BEB460B476C36D556EC420C4F0C4B0C4F2C4F2C4F2C4F0B4B
        1253FF7FFF7F355BEA462C4F2C4F0C4F0C4F0C4B0C4B0C479146BD778C3A4F57
        2D4F2D532D532D532D532C4F3357FF7FFF7F355B0B4B2D532D532D532D4F2D4F
        4E53AC3E7B6FFF7FB24E304F4F572E534E574E574E572D533357FF7FFF7F365B
        0C4F4F574E572E532E534F5751576F3EFF7FFF7FDE7B6D3A735F505B4F57505B
        505B4F5BEF4A12531253EF464F57505B505B5057505B735F6D369C73FF7FFF7F
        FF7F7B6F6D3694637363715B715B715F715F705B705B715F715F715B515B725F
        95678D3A5967FF7FFF7FFF7FFF7FFF7F9B6F8F3E745FB66B94677263725F725F
        725F725F725F9463B66B95638E3E5967FF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B
        B34EAF42755FB76BB86FB86FB76FB86FB76B7563D046B24ABD73FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FBD77165BB146AF42CF42CF42AF429046D556
        9C73FF7FFF7FFF7FFF7FFF7FFF7F}
    end
    object cbTipoMovimento: TDBLookupComboBox
      Left = 160
      Top = 28
      Width = 281
      Height = 27
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'cdTipoMovimento'
      ListField = 'dsTipoMovimento'
      ListSource = dsTipoMovimento
      ParentFont = False
      TabOrder = 3
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 464
    Width = 678
    Height = 69
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 2
    object Label5: TLabel
      Left = 9
      Top = 6
      Width = 35
      Height = 13
      Caption = 'Totais'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 9
      Top = 41
      Width = 67
      Height = 19
      Caption = 'Entradas:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblEntrada: TLabel
      Left = 145
      Top = 41
      Width = 32
      Height = 19
      Alignment = taRightJustify
      Caption = '0,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 217
      Top = 41
      Width = 56
      Height = 19
      Caption = 'Sa'#237'das: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblSaida: TLabel
      Left = 361
      Top = 41
      Width = 32
      Height = 19
      Alignment = taRightJustify
      Caption = '0,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 485
      Top = 41
      Width = 45
      Height = 19
      Caption = 'Saldo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblTotal: TLabel
      Left = 637
      Top = 41
      Width = 32
      Height = 19
      Alignment = taRightJustify
      Caption = '0,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 153
    Width = 678
    Height = 311
    Align = alClient
    DataSource = dsListaMovimento
    Enabled = False
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object qListaTipoMovimento: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cdTipoMovimento,dsTipoMovimento'
      'from tbTipoMovimento'
      'order by dsTipoMovimento')
    Left = 544
    Top = 200
    object qListaTipoMovimentocdTipoMovimento: TAutoIncField
      FieldName = 'cdTipoMovimento'
      ReadOnly = True
    end
    object qListaTipoMovimentodsTipoMovimento: TStringField
      FieldName = 'dsTipoMovimento'
      Size = 50
    end
  end
  object dsTipoMovimento: TDataSource
    DataSet = qListaTipoMovimento
    Left = 544
    Top = 264
  end
  object qListaMovimento: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idCaixa'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      '  m.idMovimento,'
      '  m.dtMovimento,'
      '  m.vlMovimento,'
      '  t.dsTipoMovimento,'
      '  m.tpMovimento'
      'FROM '
      '  tbMovimentoCaixa m'
      
        'inner join tbTipoMovimento t on t.cdTipoMovimento = m.cdTipoMovi' +
        'mento'
      'where m.idCaixa = :idCaixa'
      'order by m.dtMovimento desc')
    Left = 160
    Top = 208
    object qListaMovimentoidMovimento: TAutoIncField
      FieldName = 'idMovimento'
      ReadOnly = True
      Visible = False
    end
    object qListaMovimentodtMovimento: TDateTimeField
      DisplayLabel = 'Data'
      FieldName = 'dtMovimento'
    end
    object qListaMovimentovlMovimento: TBCDField
      DisplayLabel = 'Valor'
      FieldName = 'vlMovimento'
      currency = True
      Precision = 19
    end
    object qListaMovimentodsTipoMovimento: TStringField
      DisplayLabel = 'Refer'#234'ncia'
      FieldName = 'dsTipoMovimento'
      Size = 50
    end
    object qListaMovimentotpMovimento: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'tpMovimento'
      FixedChar = True
      Size = 1
    end
  end
  object dsListaMovimento: TDataSource
    DataSet = qListaMovimento
    Left = 160
    Top = 272
  end
  object qVerificaCaixaAberto: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nrCaixa'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'dtAbreCaixa'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select idCaixa, idUsuario,  dtAbreCaixa, dtFechaCaixa'
      'from tbCaixa'
      'where nrCaixa = :nrCaixa and dtAbreCaixa = :dtAbreCaixa')
    Left = 328
    Top = 208
    object qVerificaCaixaAbertoidCaixa: TAutoIncField
      FieldName = 'idCaixa'
      ReadOnly = True
    end
    object qVerificaCaixaAbertoidUsuario: TIntegerField
      FieldName = 'idUsuario'
    end
    object qVerificaCaixaAbertodtAbreCaixa: TWideStringField
      FieldName = 'dtAbreCaixa'
      Size = 10
    end
    object qVerificaCaixaAbertodtFechaCaixa: TWideStringField
      FieldName = 'dtFechaCaixa'
      Size = 10
    end
  end
  object qAbreCaixa: TADOQuery
    Connection = DM.ADOConn
    Parameters = <
      item
        Name = 'nrCaixa'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'idUsuario'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO '
      '  tbCaixa'
      '('
      '  nrCaixa,'
      '  dtAbreCaixa,'
      '  idUsuario'
      ') '
      'VALUES ('
      '  :nrCaixa,'
      '  getdate(),'
      '  :idUsuario'
      ');')
    Left = 328
    Top = 272
  end
  object qInsereMovimento: TADOQuery
    Connection = DM.ADOConn
    Parameters = <
      item
        Name = 'vlMovimento'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = Null
      end
      item
        Name = 'cdTipoMovimento'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'idCaixa'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'tpMovimento'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO '
      '  tbMovimentoCaixa'
      '('
      '  dtMovimento,'
      '  vlMovimento,'
      '  cdTipoMovimento,'
      '  idCaixa,'
      '  tpMovimento'
      ') '
      'VALUES ('
      '  getdate(),'
      '  :vlMovimento,'
      '  :cdTipoMovimento,'
      '  :idCaixa,'
      '  :tpMovimento'
      ');')
    Left = 328
    Top = 336
  end
end
