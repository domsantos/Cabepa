inherited Frm_RelatorioEstadoCivil: TFrm_RelatorioEstadoCivil
  Caption = 'Frm_RelatorioEstadoCivil'
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 147
        Caption = 'Listagem de Estado Civil'
      end
    end
    inherited RLBand3: TRLBand
      inherited RLDBText1: TRLDBText
        Width = 64
        DataField = 'cd_estcivil'
      end
      inherited RLDBText2: TRLDBText
        Width = 64
        DataField = 'ds_estcivil'
      end
    end
  end
end
