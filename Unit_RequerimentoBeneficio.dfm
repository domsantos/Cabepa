object frm_RequerimentoBeneficio: Tfrm_RequerimentoBeneficio
  Left = 0
  Top = 0
  Caption = 'Requerimento de Benef'#237'cio'
  ClientHeight = 343
  ClientWidth = 366
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 213
    Height = 23
    Caption = 'Requerimento de Doa'#231#227'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 40
    Width = 74
    Height = 13
    Caption = 'Informe C'#243'digo'
  end
  object Label4: TLabel
    Left = 8
    Top = 91
    Width = 59
    Height = 19
    Caption = 'Pastor:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 116
    Width = 206
    Height = 19
    Caption = 'Nenhum Doador Selecionado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblNomeRequerente: TLabel
    Left = 8
    Top = 149
    Width = 87
    Height = 13
    Caption = 'Nome Requerente'
  end
  object Label5: TLabel
    Left = 8
    Top = 205
    Width = 79
    Height = 13
    Caption = 'CPF Requerente'
  end
  object Label6: TLabel
    Left = 8
    Top = 251
    Width = 66
    Height = 13
    Caption = 'Tipo Beneficio'
  end
  object edtContribuinte: TEdit
    Left = 8
    Top = 59
    Width = 57
    Height = 21
    TabOrder = 0
    OnExit = edtContribuinteExit
    OnKeyPress = edtContribuinteKeyPress
  end
  object Button1: TButton
    Left = 71
    Top = 57
    Width = 75
    Height = 25
    Caption = 'Buscar'
    TabOrder = 5
    OnClick = Button1Click
  end
  object edtNomeRequerente: TEdit
    Left = 8
    Top = 168
    Width = 281
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
  end
  object cbTipoBeneficio: TDBLookupComboBox
    Left = 8
    Top = 270
    Width = 145
    Height = 21
    KeyField = 'cd_tipoBeneficio'
    ListField = 'ds_tipoBeneficio'
    ListSource = dsTipoBeneficio
    TabOrder = 3
  end
  object BitBtn1: TBitBtn
    Left = 213
    Top = 301
    Width = 145
    Height = 33
    Caption = 'Solicitar Benef'#237'cio'
    TabOrder = 4
    OnClick = BitBtn1Click
    Glyph.Data = {
      0A040000424D0A04000000000000420000002800000016000000160000000100
      100003000000C8030000120B0000120B00000000000000000000007C0000E003
      00001F000000FF7FDE7BDE7BBD77BD779C739C739C737C6F7C6F7C6F7C6F7C6F
      9C739C739C73BD77BD77BD77DE7BFF7FFF7FFF7F9C73B5560F42EF41EF419256
      925A91567156715671567156915691569156925A925AF6627B737C6B9D6BFF7F
      FF7F4A2D410C410C62104B5A6C626B626B626B5E4B5E4B5E4B5E4B5E4A5E4A5E
      4A5EB0663A5F151A982EFF7FFF7F29296210621083144C5E8C626C626C626C62
      6C5E6C5E6C5E6B5E4B5E4B5E4A624F4A351E5726FB42FF7FFF7F492962106210
      83144C5E8D668C626C626C626C626C626B626B626B5E6B624D52141E5722DB36
      BE6FFF7FFF7F492D6210621083146C5E8D668D628D628C626C626C628F5EB35A
      B35A905E332A371ABA329D67FF7FFF7FFF7F492D83148210A3186C5E8D668D66
      8C626C628E66D45EF75AF65AD65AF75AD752B9361057DC7BFF7FFF7FFF7F6A31
      A3188314A41C6D62AD668C66D16A9A7B5A6BF75E596BBC777873F466F862F562
      2673DB7FFF7FFF7FFF7F6A31A418A318C41C6D628D66136FDD7F146F18631663
      356F5673BC7B9977F4661963546BFF7FFF7FFF7FFF7F8A35C41CA418E4208D62
      CF6ABC7BF26E577339635A6FDD7B9B7757735773146B3967B946DE77FF7FFF7F
      FF7F8B35C520C41CE5248D62126F99771473DE7B39677C6FDE7F7877DD7B7777
      156B5A6B783ABE73FF7FFF7FFF7FAB35E520E52005258D62347379773573BC7B
      166B5B6BBD7757739B7B9A73386B5B67362ABE73FF7FFF7FFF7FAB390525E520
      06298D66F26E9B7BD06ADE7FF26E586F7B6B596B386B7B6F7B73B35A1622BE73
      FF7FFF7FFF7FAC39062506252629AE66CF6EBB7B126F3573DC7B9A777B73596F
      9C73596FD06A8F5ED94EFF7FFF7FFF7FFF7FCC3D26290625272DAE66CE6EF16E
      DD7B136FD06AF06EAE6A346FDD7BCF6AAD6A4C666B5DDE7BFF7FFF7FFF7FCC3D
      27292629472DAE66CF6ECE6AF16E9A779B7B9A77BB7B9A77F16EAD6ACF6A4C66
      83547B7BFF7FFF7FFF7FCD3D272D2729472DAE6ACF6ECF6ECE6ECE6AF06EF16E
      D06EAE6AAE6ACE6ACF6A4C6683547B7BFF7FFF7FFF7FED41472D272D4831AF6A
      EF6ECF6ECF6ECF6ECF6ECE6ECE6ECF6ECF6ECF6ACF6E4C6683507B7BFF7FFF7F
      FF7FED41472D472D6831CF6AEF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6E
      CF6E6D6A6B61BD7BFF7FFF7FFF7FED41482D472D6831CF6AEF6ECF6ECF6ECF6E
      CF6ECF6ECF6ECF6ECF6ECF6ECF6E126FFF7FFF7FFF7FFF7FFF7FED4548314831
      6835CF6AF072F06EF06EF06EF06EF06EF06ED06ECF6ECF6ECF6E1273FF7FFF7F
      FF7FFF7FFF7FF6622F4A304A504E357355775577557755775577557755775577
      557755773573997BFF7FFF7FFF7F}
  end
  object edtCpf: TMaskEdit
    Left = 8
    Top = 224
    Width = 120
    Height = 21
    EditMask = '000\.000\.000\-00;1;_'
    MaxLength = 14
    TabOrder = 2
    Text = '   .   .   -  '
  end
  object dsTipoBeneficio: TDataSource
    DataSet = DM.qTipoBeneficio
    Left = 304
    Top = 8
  end
end
