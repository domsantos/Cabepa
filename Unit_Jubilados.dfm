object frm_Jubilados: Tfrm_Jubilados
  Left = 0
  Top = 0
  Caption = 'Jubilados'
  ClientHeight = 462
  ClientWidth = 672
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 672
    Height = 43
    Align = alTop
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 8
      Top = 5
      Width = 193
      Height = 32
      Caption = 'Gerar Primeiro Pagamento'
      TabOrder = 0
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F94528C358C358C358C358C358C35
        8C358C358C358C35AC35AD358C35AD353967FF7FFF7FFF7FFF7FFF7FDE7B8C35
        C618E71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CF75EFF7F
        FF7FFF7FFF7FFF7F1863C6182925D65AF75EF75EF75ED75AD65AD65AD65AD65A
        9452955294520821D65AFF7FFF7BFF7FFF7FFF7F1863A5183146DE7BBD739D73
        9C739C739C739C737B6F955210421042F85E2925D65AFF7FDE77BE73DF77DE7B
        524A08217B6FBE779C739C739C739C6F7C6FB556CE3931463967534ACE392925
        D65ADF7BBE779E6B7D5F396394563146D65A5A6BBD77BD779C73BD73F75ECE39
        F75EBD77BD775A6BAD35E720D65ADF7BBE737E679D6FD65ABD737B6FF75E944E
        945218639C73DE77B55639679C737B6F7B6F9C73D65A2925F75EDE779D6B7E67
        7B6B5A6BBD779C739C6F7B6F1963B5569452F75E1042524ABD737B6F7B6F9C73
        7C6FCF391863BE737D679E6F1863DE7BBD77BD779C739C73FA4EF856524ACF39
        524AEF3D18639C737C739B6FB76B3763744EDF7B7E677B675A6BDF77DE7FDE7B
        DE7B3A635342CE3531465A6BF75E2925F03D9C73986B8E574B4BF456D65AFF7F
        FF7FB5567B6B3C575D579C6BD65ACE39EF3DF75EBD73DE7B185F0E4A6A31934E
        4D4FEC462F420925F75EFF7FFF7FDE7B9452D65ED756CF35AD35B5569C73BE77
        9C737B6FD75A6D5A67624A2D1042734E39672929D65AFF7FFF7F9C737B6FDE7B
        734E524A7B6BBD77BD777B6F396739673A672E46E876863D4B297B6F7B6F0821
        D65AFF7FFF7FF75EFF7FFF7FD65A7B6BBD777B6F396739673A6739679C733A67
        8B5E866649295B6B524AA518F75EFF7FDE7BF75E1867DE7B7B6FD65AFF7F7B6F
        396739675A6BDE7BFF7FF8568C290977863DAE352925C618D65AFF7FFF7FFF7F
        BD771863396794529C73FF7F5B6BBD77FF7F5A6B132A71016F198B5E866AC61C
        C718C6185A6BFF7FFF7FFF7FFF7FFF7FBD77D65E734EFF7FFF7F7B77753AB301
        B209954A9C772F4A094AAC354A291863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        7B6F396BD74E140EF405753A7B73FF7FFF7F185F954EB55AAD39FF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F944AF4055532524A9C77FF7FFF7FFF7FBD77
        8C39E7505256FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B5A6BFF7F
        DE7B3967F75E9C73FF7F1863BD771867BD77FF7FFF7FFF7FFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B7B6FD65A5A6BFF7FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        BD77FF7FFF7FFF7FFF7FFF7FFF7F}
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 422
    Width = 672
    Height = 40
    Align = alBottom
    TabOrder = 1
    object DBNavigator1: TDBNavigator
      Left = 0
      Top = 6
      Width = 225
      Height = 25
      DataSource = dsListaJubilados
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
      TabOrder = 0
    end
  end
  object RxDBGrid1: TRxDBGrid
    Left = 0
    Top = 43
    Width = 672
    Height = 379
    Align = alClient
    DataSource = dsListaJubilados
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object qListaJubilados: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      #9'c.cd_cadastro'
      #9',c.nm_pastor'
      #9',c.nm_conjuge'
      #9',c.dt_nascPastor'
      '    ,c.no_cpf'
      '    ,case c.cd_sitJubilamento'
      #9#9'when 1 then '#39'DOA'#199#227'o'#39
      #9#9'when 2 then '#39'AUXILIO'#39
      #9'end ds_beneficio'
      #9',case c.cd_sitJubilamento'
      #9#9'when 1 then c.nm_pastor'
      #9#9'when 2 then c.nm_conjuge'
      #9'end nm_beneficiado'
      #9',b.dt_Valor'
      #9',b.vl_Beneficio'
      #9',(select count(*) from tbPagamentoMensal p '
      '                          where p.cd_cadastro = c.cd_cadastro'
      '                          and p.cd_tipRubrica = 1) as qt_pgtos'
      'from tbContribuinte c'
      
        'inner join tbrequerimentoBeneficio r on r.cd_cadastro = c.cd_cad' +
        'astro'
      
        'inner join tbValBeneficio b on b.no_reqBeneficio = r.no_reqBenef' +
        'icio'
      'where c.cd_sitJubilamento in (1,2)'
      'order by 6, 7')
    Left = 544
    Top = 112
    object qListaJubiladosds_beneficio: TStringField
      DisplayLabel = 'Beneficio'
      FieldName = 'ds_beneficio'
      ReadOnly = True
      Size = 9
    end
    object qListaJubiladosnm_pastor: TStringField
      DisplayLabel = 'Contribuinte'
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qListaJubiladosno_cpf: TStringField
      DisplayLabel = 'CPF'
      FieldName = 'no_cpf'
      FixedChar = True
      Size = 14
    end
    object qListaJubiladosdt_nascPastor: TDateTimeField
      DisplayLabel = 'DataNasc.'
      FieldName = 'dt_nascPastor'
    end
    object qListaJubiladosnm_conjuge: TStringField
      DisplayLabel = 'Conjuge'
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object qListaJubiladosnm_beneficiado: TStringField
      DisplayLabel = 'Beneficiado'
      FieldName = 'nm_beneficiado'
      ReadOnly = True
      Size = 60
    end
    object qListaJubiladosdt_Valor: TDateTimeField
      DisplayLabel = 'Data Benecifio'
      FieldName = 'dt_Valor'
    end
    object qListaJubiladosvl_Beneficio: TBCDField
      DisplayLabel = 'valor'
      FieldName = 'vl_Beneficio'
      Precision = 10
      Size = 2
    end
    object qListaJubiladosqt_pgtos: TIntegerField
      DisplayLabel = 'No. Pgto.'
      FieldName = 'qt_pgtos'
      ReadOnly = True
    end
  end
  object dsListaJubilados: TDataSource
    DataSet = qListaJubilados
    Left = 544
    Top = 160
  end
end
