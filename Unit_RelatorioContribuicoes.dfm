object frm_RelatorioContribuicoes: Tfrm_RelatorioContribuicoes
  Left = 225
  Top = 124
  Caption = 'Relatorio Contribui'#231#245'es'
  ClientHeight = 547
  ClientWidth = 969
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 96
    Width = 78
    Height = 13
    Caption = 'Codigo CABEPA'
  end
  object Label2: TLabel
    Left = 8
    Top = 144
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object Label3: TLabel
    Left = 8
    Top = 168
    Width = 51
    Height = 13
    Caption = 'Data Inicio'
  end
  object Label4: TLabel
    Left = 8
    Top = 208
    Width = 42
    Height = 13
    Caption = 'Data Fim'
  end
  object Edit1: TEdit
    Left = 8
    Top = 112
    Width = 65
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 272
    Width = 99
    Height = 25
    Caption = 'Imprimir'
    TabOrder = 1
  end
  object RadioGroup1: TRadioGroup
    Left = 8
    Top = 16
    Width = 249
    Height = 73
    Caption = 'Filtar Por'
    Columns = 2
    Items.Strings = (
      'Todos'
      'Por Pastor'
      'Por Per'#237'odo'
      'A partir de uma data')
    TabOrder = 2
  end
  object MaskEdit1: TMaskEdit
    Left = 8
    Top = 184
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'MaskEdit1'
  end
  object MaskEdit2: TMaskEdit
    Left = 8
    Top = 224
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'MaskEdit2'
  end
  object RLReport1: TRLReport
    Left = 136
    Top = 96
    Width = 794
    Height = 1123
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 91
      BandType = btHeader
      object RLLabel2: TRLLabel
        Left = 5
        Top = 40
        Width = 236
        Height = 27
        Caption = 'Relat'#243'rio de Doa'#231#245'es'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object RLLabel1: TRLLabel
    Left = 179
    Top = 141
    Width = 299
    Height = 27
    Caption = 'Associa'#231#227'o Casa do Pastor'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object QPesqMestre: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      #9'c.nm_pastor'
      #9',cb.dt_mesRefContribuicao'
      #9',cb.dt_anoRefContribuicao'
      #9',cb.vl_ContribuicaoVencimento'
      'from tbContribuicoes cb'
      'inner join tbContribuinte c on cb.cd_CABEPA = c.cd_CABEPA'
      'order by c.nm_pastor')
    Left = 40
    Top = 288
    object QPesqMestrenm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object QPesqMestredt_mesRefContribuicao: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
    end
    object QPesqMestredt_anoRefContribuicao: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
    end
    object QPesqMestrevl_ContribuicaoVencimento: TBCDField
      FieldName = 'vl_ContribuicaoVencimento'
      Precision = 10
      Size = 2
    end
  end
  object dsMestre: TDataSource
    DataSet = QPesqMestre
    Left = 40
    Top = 225
  end
end
