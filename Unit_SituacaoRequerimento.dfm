inherited frm_SituacaoRequerimento: Tfrm_SituacaoRequerimento
  Caption = 'Situa'#231#227'o Requerimento'
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel2: TPanel
    object Label1: TLabel
      Left = 8
      Top = 6
      Width = 109
      Height = 16
      Caption = 'Situa'#231#227'o Requerimento'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 22
      Width = 324
      Height = 24
      DataField = 'ds_sitRequerimento'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 64
      Width = 320
      Height = 120
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalcd_sitRequerimento: TSmallintField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'cd_sitRequerimento'
      ReadOnly = True
    end
    object CDSPrincipalds_sitRequerimento: TStringField
      DisplayLabel = 'Situa'#231#227'o Requerimento'
      FieldName = 'ds_sitRequerimento'
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT cd_sitRequerimento'
      '      ,ds_sitRequerimento'
      '  FROM tbSituacaoRequerimento')
    object QPrincipalcd_sitRequerimento: TSmallintField
      FieldName = 'cd_sitRequerimento'
      ReadOnly = True
    end
    object QPrincipalds_sitRequerimento: TStringField
      FieldName = 'ds_sitRequerimento'
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 352
    Top = 240
  end
end
