inherited frm_Categorias: Tfrm_Categorias
  Caption = 'Categorias'
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel1: TPanel
    inherited btnPesquisar: TBitBtn
      Left = 238
      ExplicitLeft = 238
    end
    inherited btnRelatorio: TBitBtn
      OnClick = btnRelatorioClick
    end
  end
  inherited Panel2: TPanel
    object Label1: TLabel
      Left = 10
      Top = 6
      Width = 55
      Height = 16
      Caption = 'Categoria'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Left = 10
      Top = 25
      Width = 325
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_categoria'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 56
      Width = 329
      Height = 225
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalcd_categoria: TIntegerField
      AutoGenerateValue = arAutoInc
      DisplayLabel = 'C'#243'digo'
      FieldName = 'cd_categoria'
      Visible = False
    end
    object CDSPrincipalds_categoria: TStringField
      DisplayLabel = 'Categoria'
      FieldName = 'ds_categoria'
      Required = True
    end
  end
  inherited QPrincipal: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'select cd_categoria,ds_categoria'
      'from tbCategoria'
      'order by ds_categoria')
    object QPrincipalcd_categoria: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'cd_categoria'
    end
    object QPrincipalds_categoria: TStringField
      FieldName = 'ds_categoria'
      Required = True
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 640
    Top = 312
  end
end
