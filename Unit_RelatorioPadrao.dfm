object frm_RelatorioPadrao: Tfrm_RelatorioPadrao
  Left = 311
  Top = 155
  Caption = 'frm_RelatorioPadrao'
  ClientHeight = 602
  ClientWidth = 854
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = -8
    Top = 8
    Width = 794
    Height = 1123
    DataSource = dsRelatorio
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 83
      BandType = btHeader
      object RLLabel1: TRLLabel
        Left = 3
        Top = 0
        Width = 299
        Height = 27
        Caption = 'Associa'#231#227'o Casa do Pastor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel2: TRLLabel
        Left = 3
        Top = 32
        Width = 166
        Height = 16
        Caption = 'Associa'#231#227'o Casa do Pastor'
      end
    end
    object RLBand2: TRLBand
      Left = 38
      Top = 121
      Width = 718
      Height = 48
      BandType = btColumnHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = True
      object RLLabel3: TRLLabel
        Left = 8
        Top = 32
        Width = 55
        Height = 16
        Caption = 'C'#211'DIGO'
      end
      object RLLabel4: TRLLabel
        Left = 112
        Top = 32
        Width = 80
        Height = 16
        Caption = 'DESCRI'#199#195'O'
      end
    end
    object RLBand4: TRLBand
      Left = 38
      Top = 190
      Width = 718
      Height = 32
      BandType = btFooter
      object RLSystemInfo1: TRLSystemInfo
        Left = 88
        Top = 16
        Width = 83
        Height = 16
      end
      object RLLabel5: TRLLabel
        Left = 0
        Top = 16
        Width = 83
        Height = 16
        Caption = 'Impresso em:'
      end
      object RLSystemInfo2: TRLSystemInfo
        Left = 624
        Top = 16
        Width = 87
        Height = 16
        Info = itPageNumber
      end
    end
    object RLBand3: TRLBand
      Left = 38
      Top = 169
      Width = 718
      Height = 21
      object RLDBText1: TRLDBText
        Left = 8
        Top = 4
        Width = 70
        Height = 16
        DataSource = dsRelatorio
      end
      object RLDBText2: TRLDBText
        Left = 112
        Top = 4
        Width = 70
        Height = 16
        DataSource = dsRelatorio
      end
    end
  end
  object dsRelatorio: TDataSource
    Left = 392
    Top = 328
  end
end
