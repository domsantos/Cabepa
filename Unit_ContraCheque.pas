unit Unit_ContraCheque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RLReport, jpeg, DB, ADODB, StdCtrls, Mask, DBCtrls;

type
  Tfrm_ContraCheque = class(TForm)
    RLReport1: TRLReport;
    qListaVantagemDesconto: TADOQuery;
    qListaBeneficiados: TADOQuery;
    dsListaBeneficiados: TDataSource;
    dsListaVantagemDesconto: TDataSource;
    qListaBeneficiadosnm_pastor: TStringField;
    qListaBeneficiadosvl_vantagem: TBCDField;
    qListaBeneficiadosvl_desconto: TBCDField;
    qListaBeneficiadosvl_liquido: TBCDField;
    qListaBeneficiadosdt_mesPagamento: TSmallintField;
    qListaBeneficiadosdt_anoPagamento: TSmallintField;
    qListaBeneficiadosnm_banco: TStringField;
    RLGroup1: TRLGroup;
    RLSubDetail1: TRLSubDetail;
    RLBand4: TRLBand;
    RLBand2: TRLBand;
    RLLabel8: TRLLabel;
    RLLabel10: TRLLabel;
    RLLabel5: TRLLabel;
    RLDBText2: TRLDBText;
    RLLabel6: TRLLabel;
    RLDBText3: TRLDBText;
    RLLabel7: TRLLabel;
    RLDBText4: TRLDBText;
    RLDraw1: TRLDraw;
    RLLabel4: TRLLabel;
    RLDBText1: TRLDBText;
    RLLabel1: TRLLabel;
    RLLabel13: TRLLabel;
    lblReferencia: TRLLabel;
    RLSystemInfo1: TRLSystemInfo;
    RLDraw2: TRLDraw;
    RLBand1: TRLBand;
    RLDBText10: TRLDBText;
    RLLabel15: TRLLabel;
    RLSubDetail2: TRLSubDetail;
    qListaBeneficiadosds_mensagem: TStringField;
    RLLabel32: TRLLabel;
    RLDBMemo1: TRLDBMemo;
    qListaVantagemDescontono_Rubrica: TSmallintField;
    qListaVantagemDescontovl_Rubrica: TBCDField;
    qListaVantagemDescontodt_tipo: TStringField;
    RLDBText5: TRLDBText;
    RLDBText6: TRLDBText;
    RLDBText7: TRLDBText;
    qListaBeneficiadoscd_cadastro: TIntegerField;
    qListaBeneficiadosds_beneficio: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_ContraCheque: Tfrm_ContraCheque;

implementation

uses Unit_DM;

{$R *.dfm}

end.
