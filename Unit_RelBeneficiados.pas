unit Unit_RelBeneficiados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RLReport, jpeg, DB, ADODB;

type
  Tfrm_RelBeneficiados = class(TForm)
    RLReport1: TRLReport;
    qListaJubilados: TADOQuery;
    dsListaJubilados: TDataSource;
    RLGroup1: TRLGroup;
    RLBand2: TRLBand;
    RLBand3: TRLBand;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLBand4: TRLBand;
    RLDBResult1: TRLDBResult;
    RLLabel7: TRLLabel;
    RLBand5: TRLBand;
    RLLabel8: TRLLabel;
    RLDBResult2: TRLDBResult;
    RLBand1: TRLBand;
    RLLabel2: TRLLabel;
    RLLabel9: TRLLabel;
    RLSystemInfo1: TRLSystemInfo;
    RLSystemInfo2: TRLSystemInfo;
    RLLabel10: TRLLabel;
    qListaJubiladosnm_pastor: TStringField;
    qListaJubiladosnm_conjuge: TStringField;
    qListaJubiladosdt_falecimento: TStringField;
    qListaJubiladosdt_Valor: TDateTimeField;
    qListaJubiladosvl_beneficio: TBCDField;
    qListaJubiladosds_beneficio: TStringField;
    qListaJubiladosnm_pessoa: TStringField;
    RLLabel1: TRLLabel;
    qListaJubiladoscd_cadastro: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelBeneficiados: Tfrm_RelBeneficiados;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_RelBeneficiados.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
end;

end.
