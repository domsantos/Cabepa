/*
created		06/03/2015
modified		08/03/2015
project		
model			
company		
author		
version		
database		postgresql 8.0 
*/


/* create indexes */


/* create foreign keys */

alter table "contribuinte" add  foreign key ("cd_banco") references "banco" ("cd_banco") on update restrict on delete restrict;

alter table "movimento_caixa" add  foreign key ("id_caixa") references "caixa" ("id_caixa") on update restrict on delete restrict;

alter table "contribuinte" add  foreign key ("cd_categoria") references "categoria" ("cd_categoria") on update restrict on delete restrict;

alter table "bencao" add  foreign key ("cd_cadastro") references "contribuinte" ("cd_cadastro") on update restrict on delete restrict;

alter table "contribuicao" add  foreign key ("cd_cadastro") references "contribuinte" ("cd_cadastro") on update restrict on delete restrict;

alter table "pagamento_mensal" add  foreign key ("cd_cadastro") references "contribuinte" ("cd_cadastro") on update restrict on delete restrict;

alter table "contribuinte" add  foreign key ("cd_escolaridade") references "escolaridade" ("cd_escolaridade") on update restrict on delete restrict;

alter table "pagamento_contribuicao" add  foreign key ("cd_formpagto") references "forma_pagto" ("cd_formpagto") on update restrict on delete restrict;

alter table "contribuinte" add  foreign key ("cd_formacao_teologica") references "formacao_teologica" ("cd_formacao_teologica") on update restrict on delete restrict;

alter table "contribuinte" add  foreign key ("cd_nacionalidade") references "nacionalidade" ("cd_nacionalidade") on update restrict on delete restrict;

alter table "contribuinte" add  foreign key ("cd_naturalidade") references "naturalidade" ("cd_naturalidade") on update restrict on delete restrict;

alter table "bencao" add  foreign key ("cd_tipo_bencao") references "tipo_bencao" ("cd_tipo_bencao") on update restrict on delete restrict;

alter table "contribuicao" add  foreign key ("cd_tipocontribuicao") references "tipo_contribuicao" ("cd_tipocontribuicao") on update restrict on delete restrict;

alter table "movimento_caixa" add  foreign key () references "tipo_movimento" () on update restrict on delete restrict;

alter table "contribuicao" add  foreign key ("id_usuario") references "usuario" ("id_usuario") on update restrict on delete restrict;

alter table "caixa" add  foreign key ("id_usuario") references "usuario" ("id_usuario") on update restrict on delete restrict;

alter table "pagamento_mensal" add  foreign key ("cd_vantagem_desconto") references "vantagem_desconto" ("cd_vantagem_desconto") on update restrict on delete restrict;

alter table "contribuinte" add  foreign key ("cd_estado_civil") references "estado_civil" ("cd_estado_civil") on update restrict on delete restrict;


