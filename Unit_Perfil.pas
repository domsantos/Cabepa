unit Unit_Perfil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, DB, StdCtrls, Mask, DBCtrls, ADODB, Provider,
  ActnList, DBClient, Buttons, ExtCtrls, Grids, DBGrids;

type
  Tfrm_Perfil = class(Tfrm_PadraoCadastro)
    QPrincipalid_perfil: TAutoIncField;
    QPrincipalds_perfil: TStringField;
    CDSPrincipalid_perfil: TAutoIncField;
    CDSPrincipalds_perfil: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Perfil: Tfrm_Perfil;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_Perfil.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Open;
end;

procedure Tfrm_Perfil.btnCancelarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Close;
end;

procedure Tfrm_Perfil.btnNovoClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

end.
