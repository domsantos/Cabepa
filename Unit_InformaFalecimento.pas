unit Unit_InformaFalecimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Mask, Buttons;

type
  Tfrm_InformaFalecimento = class(TForm)
    DBGrid1: TDBGrid;
    dsBeneficiados: TDataSource;
    Panel1: TPanel;
    qListaBeneficiados: TADOQuery;
    qListaBeneficiadosnm_pastor: TStringField;
    qListaBeneficiadosnm_conjuge: TStringField;
    Label1: TLabel;
    lblContrbuinte: TLabel;
    Label2: TLabel;
    edtData: TMaskEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    qValorBeneficio: TADOQuery;
    qValorBeneficiono_reqBeneficio: TIntegerField;
    qValorBeneficiono_seqBeneficio: TAutoIncField;
    qValorBeneficiodt_Valor: TDateTimeField;
    qValorBeneficiovl_Beneficio: TBCDField;
    Label3: TLabel;
    Label4: TLabel;
    lblPensionado: TLabel;
    Label5: TLabel;
    lblValor: TLabel;
    Label6: TLabel;
    lblValorBeneficio: TLabel;
    qSpInsereBeneficio: TADOQuery;
    qSpInsereBeneficioresultado: TBooleanField;
    qAtualizaCadastro: TADOQuery;
    qDataPagamento: TADOQuery;
    qDataPagamentoano: TSmallintField;
    qDataPagamentomes: TSmallintField;
    edtNome: TEdit;
    Label7: TLabel;
    btLocalicar: TBitBtn;
    qListaBeneficiadostp_jubilacao: TStringField;
    qListaBeneficiadoscd_cadastro: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    function valorPensao(valor:Currency):Currency;
    procedure btLocalicarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_InformaFalecimento: Tfrm_InformaFalecimento;

implementation

Uses Unit_DM;


{$R *.dfm}

procedure Tfrm_InformaFalecimento.BitBtn1Click(Sender: TObject);
var
  mes, ano : integer;
begin

  if edtData.Text = '  /  /    ' then
  begin
    showmessage('Digite uma data v�lida');
    exit;
  end;

  if lblContrbuinte.Caption = '' then
  begin
    showmessage('Selecione um Beneficiado');
    exit;
  end;


  mes := strtoint(copy(edtData.Text,4,2));
  ano := strtoint(copy(edtData.Text,7,4));


  if qListaBeneficiadosnm_conjuge.Value = '' then
  begin
    showmessage('Este Jubilado n�o possue um dependente cadastrado.'+#13
                +'Caso esta informa��o esteja errada, '
                +#13+'favor atualize o cadastro do Jubilado');
  end
  else
  begin
    showmessage('O dependente cadastrado tem direito a uma pens�o.'+#13
                +'Caso esta informa��o esteja errada, '
                +#13+'favor atualize o cadastro do Jubilado.');
  end;


  qValorBeneficio.Close;

  qValorBeneficio.Parameters.ParamByName('codigo').Value :=
    qListaBeneficiadoscd_cadastro.Value;


  qValorBeneficio.Open;

  lblPensionado.Caption := qListaBeneficiadosnm_conjuge.Value;
  lblValorBeneficio.Caption := CurrToStrF(qValorBeneficiovl_Beneficio.Value,ffCurrency,2);
  lblValor.Caption := CurrToStrF(valorPensao(qValorBeneficiovl_Beneficio.Value),ffCurrency,2);

  BitBtn2.Enabled := true;

end;

procedure Tfrm_InformaFalecimento.BitBtn2Click(Sender: TObject);
var
  strSituacao : String;
begin

  // atualiza situa��o contribuinte

  if MessageDlg('Deseja gravar a data de falecimento?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin

    if qListaBeneficiadosnm_conjuge.Value <> ''  then
      strSituacao := '2'
    else
      strSituacao := '4';


    if qListaBeneficiadosnm_conjuge.Value = '' then
    begin
      if MessageDlg('N�o ser� atribuido pens�o para dependente'+#13+'Deseja continuar.?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      begin
        showmessage('Opera��o cancelada');
        exit;
      end;
    end;
    try
      try
        dm.ADOConn.BeginTrans;
        qAtualizaCadastro.Close;

        qAtualizaCadastro.sql.Clear;
        qAtualizaCadastro.sql.Append('update tbContribuinte ');
        qAtualizaCadastro.sql.Append('set cd_sitJubilamento =  '+strSituacao);
        qAtualizaCadastro.sql.Append(',dt_falecimento = '''+edtData.Text+'''');
        qAtualizaCadastro.sql.Append('where cd_cadastro = '+ inttostr(qListaBeneficiadoscd_cadastro.Value));

//        ShowMessage(qAtualizaCadastro.SQL.GetText);
        qAtualizaCadastro.ExecSQL;
        dm.ADOConn.CommitTrans;

      except
        on E : Exception do
        begin
          showmessage('Erro ao atualizar cadastro de Jubilado \n'+E.Message);
          dm.ADOConn.RollbackTrans;
          exit;
        end;

      end;


      if qListaBeneficiadosnm_conjuge.Value <> ''  then
      begin


        try

        dm.ADOConn.BeginTrans;
          qSpInsereBeneficio.Parameters.ParamByName('codigo').Value :=
            qListaBeneficiadoscd_cadastro.Value;
          qSpInsereBeneficio.Parameters.ParamByName('tipo').Value := 3;
          qSpInsereBeneficio.Parameters.ParamByName('tipoJubilacao').Value := 2;

          qSpInsereBeneficio.Parameters.ParamByName('valor').Value :=
             valorPensao(qValorBeneficiovl_Beneficio.Value);


          qSpInsereBeneficio.ExecSQL;
          dm.ADOConn.CommitTrans;

        except
          on exception do
          begin
            showmessage('Erro ao registrar Pens�o');
            dm.ADOConn.RollbackTrans;
            exit;
          end;
        end;

      end;



    finally
      showmessage('Opera��o finalizada');
      qListaBeneficiados.Close;
      qListaBeneficiados.Open;
    end;
  end;

end;

procedure Tfrm_InformaFalecimento.btLocalicarClick(Sender: TObject);
begin
  qListaBeneficiados.Locate('nm_pastor',edtNome.Text,[loPartialKey]);
end;

procedure Tfrm_InformaFalecimento.DBGrid1DblClick(Sender: TObject);
begin
  lblContrbuinte.Caption := qListaBeneficiadosnm_pastor.Value;
  BitBtn2.Enabled := false;
end;

procedure Tfrm_InformaFalecimento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qListaBeneficiados.close;
  dm.qValorMinimo.close;
  self := nil;
  action := caFree;
end;

procedure Tfrm_InformaFalecimento.FormShow(Sender: TObject);
begin
  qListaBeneficiados.Open;
end;

function Tfrm_InformaFalecimento.valorPensao(valor: Currency): Currency;
var
  v : Currency;
begin

  v := (Valor*0.8);

  dm.qValorMinimo.close;
  dm.qValorMinimo.Open;

  if v < dm.qValorMinimovl_salMin.Value then
    v := dm.qValorMinimovl_salMin.Value;

  result := v;


end;

end.
