inherited frm_ResumoContribuintes: Tfrm_ResumoContribuintes
  Caption = 'frm_ResumoContribuintes'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = 0
    Top = 0
    ExplicitLeft = 0
    ExplicitTop = 0
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 161
        Caption = 'RESUMO DE DOADORES'
        ExplicitWidth = 161
      end
    end
    inherited RLBand2: TRLBand
      inherited RLLabel3: TRLLabel
        Width = 69
        Caption = 'SITUACAO'
        ExplicitWidth = 69
      end
      inherited RLLabel4: TRLLabel
        Left = 188
        Width = 87
        Caption = 'QUANTIDADE'
        ExplicitLeft = 188
        ExplicitWidth = 87
      end
    end
    inherited RLBand3: TRLBand
      inherited RLDBText1: TRLDBText
        Width = 74
        DataField = 'ds_situacao'
        ExplicitWidth = 74
      end
      inherited RLDBText2: TRLDBText
        Left = 253
        Top = 2
        Width = 22
        Alignment = taRightJustify
        DataField = 'qtd'
        ExplicitLeft = 253
        ExplicitTop = 2
        ExplicitWidth = 22
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = qRelatorio
    Left = 472
    Top = 272
  end
  object qRelatorio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_sitJubilamento, '
      #9'case cd_sitJubilamento '
      #9#9'when 1 then '#39'JUBULADOS'#39
      #9#9'when 2 then '#39'PENSIONADOS'#39
      #9#9'when 3 then '#39'CANCELADOS'#39
      #9#9'else '#39'CONTRIBUINTES'#39
      #9'end as ds_situacao'
      #9',COUNT(*) qtd'
      'from tbContribuinte'
      'group by cd_sitJubilamento'
      'order by 2')
    Left = 544
    Top = 272
    object qRelatoriocd_sitJubilamento: TStringField
      FieldName = 'cd_sitJubilamento'
      FixedChar = True
      Size = 1
    end
    object qRelatoriods_situacao: TStringField
      FieldName = 'ds_situacao'
      ReadOnly = True
      Size = 13
    end
    object qRelatorioqtd: TIntegerField
      FieldName = 'qtd'
      ReadOnly = True
    end
  end
end
