object frm_CargaContribuicoes: Tfrm_CargaContribuicoes
  Left = 0
  Top = 0
  ClientHeight = 520
  ClientWidth = 713
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 83
    Height = 13
    Caption = 'CODIGO CABEPA'
  end
  object edtCodigo: TEdit
    Left = 16
    Top = 27
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'edtCodigo'
  end
  object Button1: TButton
    Left = 143
    Top = 25
    Width = 113
    Height = 25
    Caption = 'Carregar Arquivo'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 0
    Top = 54
    Width = 713
    Height = 466
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object OpenDialog1: TOpenDialog
    Left = 24
    Top = 64
  end
  object contribicoes: TADOTable
    Connection = DM.ADOConn
    CursorType = ctStatic
    TableName = 'tbContribuicoes'
    Left = 104
    Top = 64
    object contribicoescd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
    object contribicoesdt_anoRefContribuicao: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
    end
    object contribicoesdt_mesRefContribuicao: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
    end
    object contribicoesvl_ContribuicaoVencimento: TBCDField
      FieldName = 'vl_ContribuicaoVencimento'
      Precision = 10
      Size = 2
    end
    object contribicoescd_tipoContribuicao: TIntegerField
      FieldName = 'cd_tipoContribuicao'
    end
    object contribicoescd_sitQuitacao: TSmallintField
      FieldName = 'cd_sitQuitacao'
    end
  end
  object Pagamento: TADOTable
    Connection = DM.ADOConn
    CursorType = ctStatic
    TableName = 'tbPagtoContribuicao'
    Left = 184
    Top = 64
    object Pagamentovl_pagtoContribuicao: TBCDField
      FieldName = 'vl_pagtoContribuicao'
      Precision = 10
      Size = 2
    end
    object Pagamentodt_pagtoContribuicao: TDateTimeField
      FieldName = 'dt_pagtoContribuicao'
    end
    object Pagamentocd_formPagto: TSmallintField
      FieldName = 'cd_formPagto'
    end
    object Pagamentodt_anoRefContribuicao: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
    end
    object Pagamentodt_mesRefContribuicao: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
    end
    object Pagamentocd_tipoContribuicao: TIntegerField
      FieldName = 'cd_tipoContribuicao'
    end
    object Pagamentocd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
end
