unit Unit_main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, ImgList, ToolWin, StdCtrls, ExtCtrls, ActnList,
  Mask, rxToolEdit, rxCurrEdit,ShellAPI;

type
  Tfrm_Principal = class(TForm)
    MainMenu1: TMainMenu;
    CABEPA1: TMenuItem;
    Contribuintes: TMenuItem;
    Jubilados1: TMenuItem;
    N1: TMenuItem;
    Sair1: TMenuItem;
    abelasAuxiliares1: TMenuItem;
    C1: TMenuItem;
    ERstadoCivil1: TMenuItem;
    stBar: TStatusBar;
    Escolaridade1: TMenuItem;
    NAturalidade1: TMenuItem;
    NAcionalidade1: TMenuItem;
    FormaoTeolgica1: TMenuItem;
    ToolBar1: TToolBar;
    ImageList1: TImageList;
    Segurana1: TMenuItem;
    Usurios1: TMenuItem;
    Perfil1: TMenuItem;
    Objetos1: TMenuItem;
    Auditoria1: TMenuItem;
    regContribuicoes: TMenuItem;
    FormadePagamento1: TMenuItem;
    tbContribuinte: TToolButton;
    tbPagamento: TToolButton;
    Relatorios1: TMenuItem;
    Contribuies1: TMenuItem;
    Timer1: TTimer;
    ToolButton2: TToolButton;
    ipoBenefcio1: TMenuItem;
    SituaoRequerimento1: TMenuItem;
    ChequesPr1: TMenuItem;
    tbPgtoChequePre: TToolButton;
    tbImpressaoPagamento: TToolButton;
    N2: TMenuItem;
    FichaNadaConsta1: TMenuItem;
    N3: TMenuItem;
    Lis1: TMenuItem;
    FolhadePagamento1: TMenuItem;
    Lanamentos1: TMenuItem;
    Prvia1: TMenuItem;
    GerarContrCheque1: TMenuItem;
    VantagenseDescontos1: TMenuItem;
    RegistrodeContribuioemLote1: TMenuItem;
    CalculoPagamentoBeneficio1: TMenuItem;
    CargaContribuies1: TMenuItem;
    Ativos1: TMenuItem;
    Inativos1: TMenuItem;
    N4: TMenuItem;
    BuscaJubilados1: TMenuItem;
    rocarSenha1: TMenuItem;
    SimulaoJubilao1: TMenuItem;
    ActionList1: TActionList;
    VantagensDescontosPagos1: TMenuItem;
    ResumoPagametoPerodo1: TMenuItem;
    InformarFalecimento1: TMenuItem;
    ApagarContribuio1: TMenuItem;
    SalrioMnimo1: TMenuItem;
    RegistrodeContribuioemLoteporRef1: TMenuItem;
    N8: TMenuItem;
    Caixa1: TMenuItem;
    AjusteValorBeneficio1: TMenuItem;
    ListaJubilados1: TMenuItem;
    ListaparaArquivoMorto1: TMenuItem;
    BuscaArquivoMorto1: TMenuItem;
    ACAixa1: TMenuItem;
    AbrirCaixa1: TMenuItem;
    N9: TMenuItem;
    ipoMovimento1: TMenuItem;
    procedure C1Click(Sender: TObject);
    procedure ERstadoCivil1Click(Sender: TObject);
    procedure Escolaridade1Click(Sender: TObject);
    procedure FormaoTeolgica1Click(Sender: TObject);
    procedure NAturalidade1Click(Sender: TObject);
    procedure NAcionalidade1Click(Sender: TObject);
    procedure ContribuintesClick(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure tbContribuinteClick(Sender: TObject);
    procedure Perfil1Click(Sender: TObject);
    procedure FormadePagamento1Click(Sender: TObject);
    procedure Usurios1Click(Sender: TObject);
    procedure regContribuicoesClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure Contribuies1Click(Sender: TObject);
    procedure ipoBenefcio1Click(Sender: TObject);
    procedure ChequesPr1Click(Sender: TObject);
    procedure tbPgtoChequePreClick(Sender: TObject);
    procedure tbImpressaoPagamentoClick(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure acContribuinteExecute(Sender: TObject);
    procedure acPagamentoExecute(Sender: TObject);
    procedure acChequePreExecute(Sender: TObject);
    procedure acImpressaoPagamentoExecute(Sender: TObject);
    procedure FichaNadaConsta1Click(Sender: TObject);
    procedure RequerimentoJubilao1Click(Sender: TObject);
    procedure SituaoRequerimento1Click(Sender: TObject);
    procedure Lis1Click(Sender: TObject);
    procedure RegistrodeContribuioemLote1Click(Sender: TObject);
    procedure VantagenseDescontos1Click(Sender: TObject);
    procedure CargaContribuies1Click(Sender: TObject);
    procedure Jubilados1Click(Sender: TObject);
    procedure Prvia1Click(Sender: TObject);
    procedure CalculoPagamentoBeneficio1Click(Sender: TObject);
    procedure GerarContrCheque1Click(Sender: TObject);
    procedure Lanamentos1Click(Sender: TObject);
    procedure Inativos1Click(Sender: TObject);
    procedure Ativos1Click(Sender: TObject);
    procedure BuscaJubilados1Click(Sender: TObject);
    procedure rocarSenha1Click(Sender: TObject);
    procedure SimulaoJubilao1Click(Sender: TObject);
    procedure AjudadoSistema1Click(Sender: TObject);
    procedure VantagensDescontosPagos1Click(Sender: TObject);
    procedure ResumoPagametoPerodo1Click(Sender: TObject);
    procedure BEneficiadosDedutiveisdeIR1Click(Sender: TObject);
    procedure CdulaC1Click(Sender: TObject);
    procedure InformarFalecimento1Click(Sender: TObject);
    procedure ApagarContribuio1Click(Sender: TObject);
    procedure SalrioMnimo1Click(Sender: TObject);
    procedure RegistrodeContribuioemLoteporRef1Click(Sender: TObject);
    procedure AumentoBeneficio1Click(Sender: TObject);
    procedure Caixa1Click(Sender: TObject);
    procedure MensagemContraCheque1Click(Sender: TObject);
    procedure AjusteValorBeneficio1Click(Sender: TObject);
    procedure ListaJubilados1Click(Sender: TObject);
    procedure ListaparaArquivoMorto1Click(Sender: TObject);
    procedure BuscaArquivoMorto1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ipoMovimento1Click(Sender: TObject);
    procedure AbrirCaixa1Click(Sender: TObject);
   
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Principal: Tfrm_Principal;

implementation

uses Unit_Categorias, Unit_EstadoCivil, Unit_Escolaridade,
  Unit_FormTeologica, Unit_Naturalidade, Unit_Nacionalidade,
  Unit_Contribuintes, Unit1, Unit_Perfil, Unit_DM, 
  Unit_Usuario, Unit_Forma_Pagamento, Unit_RegistraPagamento, Unit_Login,
  Unit_FiltroContribuicaoPastor, Unit_TipoBeneficio, Unit_ChequesPre,
  Unit_ImpressaoPagamentos, Unit_FiltroRelatorioNadaConsta,
  Unit_RequerimentoBeneficio, Unit_SituacaoRequerimento,
  Unit_ListaSolicitacoesBeneficios, Unit_RegistraPagamentoLote,
  Unit_VantagemDesconto, Unit_CargaContribuicoes, Unit_Jubilados, Unit_Previas,
  Unit_CalculaPagBeneficio, Unit_FiltroContraCheque, FNLIB1, Unit_Lancamentos,
  Unit_CargaContribuicoesArquivos, Unit_RelValorBeneficio, Unit_RelBeneficiados,
  Unit_RelContribuintes, Unit_BuscaInativos, Unit_TrocaSenha,
  Unit_simularJubilacao, Unit_FiltroVanDescPaga, Unit_FiltroPagResumoPeriodo,
  Unit_RelDescontoIR, Unit_RelCedulaC, Unit_InformaFalecimento,
  Unit_RelCedulaCIndividual, Unit_apagarPagamento, Unit_SalMinimo,
  Unit_pagamentoLoteReferencia, Unit_calculaAumento, Unit_Caixa,
  Unit_MensagemCCheque, Unit_RelAjusteValorBeneficio, Unit_RelJubilados,
  Unit_ListaParaArquivoMorto, Unit_arquivoMorto, Unit_FiltroCedulaC,
  Unit_TipoMovimento, Unit_MovimentoCaixa;

{$R *.dfm}

procedure Tfrm_Principal.AbrirCaixa1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_MovimentoCaixa,frm_MovimentoCaixa);
  frm_MovimentoCaixa.Show;
end;

procedure Tfrm_Principal.acChequePreExecute(Sender: TObject);
begin
  tbPgtoChequePre.Click;
end;

procedure Tfrm_Principal.acContribuinteExecute(Sender: TObject);
begin
  //tbContribuinte.Click;
end;

procedure Tfrm_Principal.acImpressaoPagamentoExecute(Sender: TObject);
begin
  tbImpressaoPagamento.Click;
end;

procedure Tfrm_Principal.acPagamentoExecute(Sender: TObject);
begin
  tbPagamento.Click;
end;

procedure Tfrm_Principal.AjudadoSistema1Click(Sender: TObject);
begin
  ShellExecute(0,nil,'hh.exe cabepa.chm',nil ,nil,SW_NORMAL);

end;

procedure Tfrm_Principal.AjusteValorBeneficio1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_RelAjusteValorBeneficios,frm_RelAjusteValorBeneficios);
  frm_RelAjusteValorBeneficios.qRelatorio.open;
  frm_RelAjusteValorBeneficios.RLReport1.Preview();
  frm_RelAjusteValorBeneficios.qRelatorio.close;
  frm_RelAjusteValorBeneficios.close;



end;

procedure Tfrm_Principal.ApagarContribuio1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_apagarPagamento') then
    Application.CreateForm(Tfrm_apagarPagamento,frm_apagarPagamento);

  frm_apagarPagamento.Show;
end;

procedure Tfrm_Principal.Ativos1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_RelContribuntes,frm_RelContribuntes);
  frm_RelContribuntes.qListaAtivos.Open;
  frm_RelContribuntes.RLReport1.Preview();
  frm_RelContribuntes.qListaAtivos.Close;
  frm_RelContribuntes.close;
end;

procedure Tfrm_Principal.AumentoBeneficio1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_calculaAumento') then
    Application.CreateForm(Tfrm_calculaAumento,frm_calculaAumento);

  frm_calculaAumento.Show;
end;

procedure Tfrm_Principal.BEneficiadosDedutiveisdeIR1Click(Sender: TObject);
begin
   Application.CreateForm(Tfrm_RelDescontoIR,frm_RelDescontoIR);

   frm_RelDescontoIR.qRelatorio.Open;
   frm_RelDescontoIR.RLReport1.Preview();
   frm_RelDescontoIR.Free;

end;

procedure Tfrm_Principal.BuscaArquivoMorto1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frmArquivoMorto') then
    Application.CreateForm(TfrmArquivoMorto,frmArquivoMorto);

  frmArquivoMorto.Show;

end;

procedure Tfrm_Principal.BuscaJubilados1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_BuscaInativos') then
    Application.CreateForm(Tfrm_BuscaInativos,frm_BuscaInativos);

  frm_BuscaInativos.Show;
end;

procedure Tfrm_Principal.C1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_Categorias') then
    Application.CreateForm(Tfrm_Categorias,frm_Categorias);

  frm_Categorias.Show;
end;

procedure Tfrm_Principal.Caixa1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frmcaixa') then
    application.CreateForm(Tfrmcaixa,frmcaixa);

    frmcaixa.Show;
end;

procedure Tfrm_Principal.CalculoPagamentoBeneficio1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_CalculaPagBeneficio') then
    application.CreateForm(Tfrm_CalculaPagBeneficio,frm_CalculaPagBeneficio);
  frm_CalculaPagBeneficio.Show;
end;

procedure Tfrm_Principal.CargaContribuies1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_cargaContribuicoesArquivos') then
    application.CreateForm(Tfrm_cargaContribuicoesArquivos,frm_cargaContribuicoesArquivos);
  frm_cargaContribuicoesArquivos.Show;
end;

procedure Tfrm_Principal.CdulaC1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_FiltroCedulaC') then
    Application.CreateForm(Tfrm_FiltroCedulaC,frm_FiltroCedulaC);

  frm_FiltroCedulaC.Show;
end;

procedure Tfrm_Principal.ERstadoCivil1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_EstadoCivil') then
    Application.CreateForm(Tfrm_EstadoCivil,frm_EstadoCivil);

  frm_EstadoCivil.Show;
end;

procedure Tfrm_Principal.Escolaridade1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_Escolaridade') then
    Application.CreateForm(Tfrm_Escolaridade ,frm_Escolaridade);
  frm_Escolaridade.Show;
end;

procedure Tfrm_Principal.FormaoTeolgica1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_FormTeologica') then
    Application.CreateForm(Tfrm_FormTeologica,frm_FormTeologica);
  frm_FormTeologica.Show;
end;

procedure Tfrm_Principal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure Tfrm_Principal.FormCreate(Sender: TObject);
begin
 // ShowMessage( dm.ADOConn.ConnectionString);
end;

procedure Tfrm_Principal.FormShow(Sender: TObject);
begin
  frm_login.Show;
end;

procedure Tfrm_Principal.GerarContrCheque1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_FiltroContraCheque') then
    Application.CreateForm(Tfrm_FiltroContraCheque,frm_FiltroContraCheque);
  frm_FiltroContraCheque.Show;
end;

procedure Tfrm_Principal.Inativos1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_RelBeneficiados,frm_RelBeneficiados);
  frm_RelBeneficiados.qListaJubilados.Open;
  frm_RelBeneficiados.RLReport1.Preview();
  frm_RelBeneficiados.qListaJubilados.Close;
  frm_RelBeneficiados.close;

end;

procedure Tfrm_Principal.InformarFalecimento1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_InformaFalecimento') then
    Application.CreateForm(Tfrm_InformaFalecimento,frm_InformaFalecimento);

    frm_InformaFalecimento.show();
end;

procedure Tfrm_Principal.ipoBenefcio1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_TipoBeneficio') then
    Application.CreateForm(Tfrm_TipoBeneficio,frm_TipoBeneficio);
  frm_TipoBeneficio.Show;
end;

procedure Tfrm_Principal.ipoMovimento1Click(Sender: TObject);
begin
  //if not myExisteMDIChildForm(frm_TipoMovimento,'frm_TipoMovimento') then
    Application.CreateForm(Tfrm_TipoMovimento,frm_TipoMovimento);


  frm_TipoMovimento.Show;
end;

procedure Tfrm_Principal.Jubilados1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_Jubilados') then
    application.CreateForm(Tfrm_Jubilados,frm_Jubilados);
  frm_Jubilados.Show;
end;

procedure Tfrm_Principal.Lanamentos1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_Lancamentos') then
    Application.CreateForm(Tfrm_Lancamentos,frm_Lancamentos);
  frm_Lancamentos.Show;
end;

procedure Tfrm_Principal.Lis1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_ListaSolicitacoesBeneficios') then
    Application.CreateForm(Tfrm_ListaSolicitacoesBeneficios,frm_ListaSolicitacoesBeneficios);
  frm_ListaSolicitacoesBeneficios.show;
end;

procedure Tfrm_Principal.ListaJubilados1Click(Sender: TObject);
var
  frmRelJubilados : Tfrm_RelJubilados;
begin
  frmRelJubilados := Tfrm_RelJubilados.Create(self);

  frmRelJubilados.dsRelatorio.DataSet.Open;
  frmRelJubilados.RLReport1.Preview();

end;

procedure Tfrm_Principal.ListaparaArquivoMorto1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frmListaParaArquivoMorto') then
    Application.CreateForm(TfrmListaParaArquivoMorto,frmListaParaArquivoMorto);
  frmListaParaArquivoMorto.Show;
end;

procedure Tfrm_Principal.MensagemContraCheque1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_MensagemCCheque') then
    Application.CreateForm(Tfrm_MensagemCCheque,frm_MensagemCCheque);
  frm_MensagemCCheque.Show;
end;

procedure Tfrm_Principal.NAturalidade1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_Naturalidade') then
    Application.CreateForm(Tfrm_Naturalidade,frm_Naturalidade);
  frm_Naturalidade.Show;
end;

procedure Tfrm_Principal.NAcionalidade1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_Nacionalidade') then
    Application.CreateForm(Tfrm_Nacionalidade,frm_Nacionalidade);
  frm_Nacionalidade.Show;
end;

procedure Tfrm_Principal.ChequesPr1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_ChequesPre') then
    Application.CreateForm(Tfrm_ChequesPre,frm_ChequesPre);
  frm_ChequesPre.Show;
end;
                           
procedure Tfrm_Principal.Contribuies1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_FiltroContribuicaoPastor') then
    Application.CreateForm(Tfrm_FiltroContribuicaoPastor,frm_FiltroContribuicaoPastor);
  frm_FiltroContribuicaoPastor.show;

end;

procedure Tfrm_Principal.ContribuintesClick(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_Contribuinte') then
  begin
    Application.CreateForm(Tfrm_Contribuinte,frm_Contribuinte);
    frm_Contribuinte.Show;
  end;
end;


procedure Tfrm_Principal.Sair1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure Tfrm_Principal.SalrioMnimo1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_SalMinimo') then
    Application.CreateForm(Tfrm_SalMinimo,frm_SalMinimo);

  frm_SalMinimo.Show;
end;

procedure Tfrm_Principal.SimulaoJubilao1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_SimularJubilacao') then
    Application.CreateForm(Tfrm_SimularJubilacao,frm_SimularJubilacao);

  frm_SimularJubilacao.Show;
end;

procedure Tfrm_Principal.SituaoRequerimento1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_SituacaoRequerimento') then
    Application.CreateForm(Tfrm_SituacaoRequerimento,frm_SituacaoRequerimento);
  frm_SituacaoRequerimento.Show;
end;

procedure Tfrm_Principal.Timer1Timer(Sender: TObject);
begin
  stBar.Panels[4].Text := ' '+FormatDateTime ('dddd", "dd" de "mmmm" de "yyyy',now) + ' '+FormatDateTime('hh:nn:ss',now);
end;

procedure Tfrm_Principal.tbContribuinteClick(Sender: TObject);
begin
  //Application.CreateForm(Tfrm_Contribuinte,frm_Contribuinte);
  frm_Contribuinte.Show;
end;

procedure Tfrm_Principal.tbPgtoChequePreClick(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_ChequesPre') then
    Application.CreateForm(Tfrm_ChequesPre,frm_ChequesPre);
  frm_ChequesPre.Show;
end;

procedure Tfrm_Principal.tbImpressaoPagamentoClick(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_ImpressaoPagamentos') then
    Application.CreateForm(Tfrm_ImpressaoPagamentos,frm_ImpressaoPagamentos);
  frm_ImpressaoPagamentos.Show;
end;

procedure Tfrm_Principal.ToolButton6Click(Sender: TObject);
begin
  dm.testeImpressao;
end;

procedure Tfrm_Principal.Perfil1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_Perfil') then
    Application.CreateForm(Tfrm_Perfil,frm_Perfil);
  frm_Perfil.Show;
end;


procedure Tfrm_Principal.Prvia1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_Previas') then
    application.CreateForm(Tfrm_Previas,frm_Previas);
  frm_Previas.Show;
end;
                                      
procedure Tfrm_Principal.FichaNadaConsta1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_RelValorBeneficio') then
    Application.CreateForm(Tfrm_RelValorBeneficio,frm_RelValorBeneficio);
  frm_RelValorBeneficio.qValorBeneficio.Open;
  frm_RelValorBeneficio.RLReport1.Preview();
  frm_RelValorBeneficio.qValorBeneficio.close;
  frm_RelValorBeneficio.close;

end;

procedure Tfrm_Principal.FormadePagamento1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_FormaPagamento') then
    Application.CreateForm(Tfrm_FormaPagamento,frm_FormaPagamento);
  frm_FormaPagamento.Show;
end;

procedure Tfrm_Principal.Usurios1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_Usuario') then
    Application.CreateForm(Tfrm_Usuario,frm_Usuario);
  frm_Usuario.Show;
end;

procedure Tfrm_Principal.VantagensDescontosPagos1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_FiltroVanDescPaga') then
    Application.CreateForm(Tfrm_FiltroVanDescPaga,frm_FiltroVanDescPaga);

  frm_FiltroVanDescPaga.Show;
end;

procedure Tfrm_Principal.VantagenseDescontos1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_VantagemDesconto') then
    Application.CreateForm(Tfrm_VantagemDesconto,frm_VantagemDesconto);
  frm_VantagemDesconto.Show;
end;

procedure Tfrm_Principal.regContribuicoesClick(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_RegistraPagamento') then
  begin
  Application.CreateForm(Tfrm_RegistraPagamento,frm_RegistraPagamento);
  frm_RegistraPagamento.Top := 10;
  frm_RegistraPagamento.Left := 10;
  frm_RegistraPagamento.Width := 660;
  frm_RegistraPagamento.Height := 540;

  end;
  frm_RegistraPagamento.Show;
end;

procedure Tfrm_Principal.RegistrodeContribuioemLote1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_RegistroPagamentoLote') then
    Application.CreateForm(Tfrm_RegistroPagamentoLote,frm_RegistroPagamentoLote);
  frm_RegistroPagamentoLote.Show;

end;

procedure Tfrm_Principal.RegistrodeContribuioemLoteporRef1Click(
  Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_pagamentoLoteReferencia') then
    Application.CreateForm(Tfrm_pagamentoLoteReferencia,frm_pagamentoLoteReferencia);
  frm_pagamentoLoteReferencia.Show;


end;

procedure Tfrm_Principal.RequerimentoJubilao1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_RequerimentoBeneficio') then
    Application.CreateForm(Tfrm_RequerimentoBeneficio,frm_RequerimentoBeneficio);
  frm_RequerimentoBeneficio.Show;

end;

procedure Tfrm_Principal.ResumoPagametoPerodo1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_FiltroPagResumoPeriodo') then
    Application.CreateForm(Tfrm_FiltroPagResumoPeriodo,frm_FiltroPagResumoPeriodo);

  frm_FiltroPagResumoPeriodo.Show;

end;

procedure Tfrm_Principal.rocarSenha1Click(Sender: TObject);
begin
  if not myExisteMDIChildForm(frm_Principal,'frm_TrocaSenha') then
    Application.CreateForm(Tfrm_TrocaSenha,frm_TrocaSenha);

  frm_TrocaSenha.Show;
end;

end.
