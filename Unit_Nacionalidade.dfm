inherited frm_Nacionalidade: Tfrm_Nacionalidade
  Caption = 'Nacionalidade'
  ExplicitWidth = 728
  ExplicitHeight = 513
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel1: TPanel
    inherited btnRelatorio: TBitBtn
      OnClick = btnRelatorioClick
    end
  end
  inherited Panel2: TPanel
    object Label1: TLabel
      Left = 10
      Top = 10
      Width = 67
      Height = 16
      Caption = 'Nacionalidade'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Left = 10
      Top = 30
      Width = 325
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_nacPastor'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 64
      Width = 329
      Height = 120
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalcd_nacPastor: TIntegerField
      FieldName = 'cd_nacPastor'
    end
    object CDSPrincipalds_nacPastor: TStringField
      DisplayLabel = 'Nacionalidade'
      FieldName = 'ds_nacPastor'
      Required = True
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    SQL.Strings = (
      'select cd_nacPastor, ds_nacPastor'
      'from tbNacionalidade'
      'order by ds_nacPastor')
    object QPrincipalcd_nacPastor: TIntegerField
      FieldName = 'cd_nacPastor'
    end
    object QPrincipalds_nacPastor: TStringField
      FieldName = 'ds_nacPastor'
      Required = True
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 352
    Top = 240
  end
end
