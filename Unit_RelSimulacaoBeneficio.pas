unit Unit_RelSimulacaoBeneficio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, RLReport, jpeg, ADODB;

type
  Tfrm_RelSimulacaoBeneficio = class(Tfrm_RelatorioPadrao)
    RLLabel6: TRLLabel;
    lblQtdContribuicoes: TRLLabel;
    RLLabel7: TRLLabel;
    lblValorMedioContr: TRLLabel;
    RLLabel8: TRLLabel;
    lblValorTotal: TRLLabel;
    RLLabel9: TRLLabel;
    lblAnosContr: TRLLabel;
    RLLabel10: TRLLabel;
    lblValorBeneficio: TRLLabel;
    RLLabel11: TRLLabel;
    lblValorDizimo: TRLLabel;
    RLLabel12: TRLLabel;
    lblValorLiquido: TRLLabel;
    RLDraw1: TRLDraw;
    RLLabel13: TRLLabel;
    qListaContribuicoes: TADOQuery;
    qListaContribuicoesvl_ContribuicaoVencimento: TBCDField;
    qListaContribuicoesdt_anoRefContribuicao: TIntegerField;
    qListaContribuicoesdt_mesRefContribuicao: TIntegerField;
    RLLabel14: TRLLabel;
    RLDBText3: TRLDBText;
    RLLabel15: TRLLabel;
    lblPastor: TRLLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelSimulacaoBeneficio: Tfrm_RelSimulacaoBeneficio;

implementation

uses Unit_DM;

{$R *.dfm}

end.
