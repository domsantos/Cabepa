unit Unit_MensagemCCheque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, Grids, DBGrids, StdCtrls, DBCtrls, Mask, DB, ADODB,
  Provider, ActnList, DBClient, Buttons, ExtCtrls;

type
  Tfrm_MensagemCCheque = class(Tfrm_PadraoCadastro)
    QPrincipaldt_mes: TSmallintField;
    QPrincipaldt_ano: TSmallintField;
    QPrincipalds_mensagem: TStringField;
    CDSPrincipaldt_mes: TSmallintField;
    CDSPrincipaldt_ano: TSmallintField;
    CDSPrincipalds_mensagem: TStringField;
    DataSource1: TDataSource;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    DBMemo1: TDBMemo;
    DBGrid1: TDBGrid;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_MensagemCCheque: Tfrm_MensagemCCheque;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_MensagemCCheque.btnNovoClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

procedure Tfrm_MensagemCCheque.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Open;
end;

end.
