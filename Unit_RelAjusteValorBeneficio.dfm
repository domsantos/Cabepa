object frm_RelAjusteValorBeneficios: Tfrm_RelAjusteValorBeneficios
  Left = 0
  Top = 0
  Caption = 'frm_RelAjusteValorBeneficios'
  ClientHeight = 508
  ClientWidth = 790
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = 8
    Top = 8
    Width = 794
    Height = 1123
    Margins.LeftMargin = 5.000000000000000000
    Margins.RightMargin = 5.000000000000000000
    DataSource = dsRelatorio
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    object RLBand1: TRLBand
      Left = 19
      Top = 38
      Width = 756
      Height = 83
      BandType = btHeader
      object RLImage1: TRLImage
        Left = 3
        Top = 4
        Width = 87
        Height = 73
        AutoSize = True
        Picture.Data = {
          0A544A504547496D616765691A0000FFD8FFE1001845786966000049492A0008
          0000000000000000000000FFEC00114475636B79000100040000001E0000FFE1
          0371687474703A2F2F6E732E61646F62652E636F6D2F7861702F312E302F003C
          3F787061636B657420626567696E3D22EFBBBF222069643D2257354D304D7043
          656869487A7265537A4E54637A6B633964223F3E203C783A786D706D65746120
          786D6C6E733A783D2261646F62653A6E733A6D6574612F2220783A786D70746B
          3D2241646F626520584D5020436F726520352E332D633031312036362E313435
          3636312C20323031322F30322F30362D31343A35363A32372020202020202020
          223E203C7264663A52444620786D6C6E733A7264663D22687474703A2F2F7777
          772E77332E6F72672F313939392F30322F32322D7264662D73796E7461782D6E
          7323223E203C7264663A4465736372697074696F6E207264663A61626F75743D
          222220786D6C6E733A786D704D4D3D22687474703A2F2F6E732E61646F62652E
          636F6D2F7861702F312E302F6D6D2F2220786D6C6E733A73745265663D226874
          74703A2F2F6E732E61646F62652E636F6D2F7861702F312E302F73547970652F
          5265736F75726365526566232220786D6C6E733A786D703D22687474703A2F2F
          6E732E61646F62652E636F6D2F7861702F312E302F2220786D704D4D3A4F7269
          67696E616C446F63756D656E7449443D22786D702E6469643A30313830313137
          343037323036383131383232414132343042374334343945312220786D704D4D
          3A446F63756D656E7449443D22786D702E6469643A4145434542393834364343
          3831314534424543384145414238334139344245302220786D704D4D3A496E73
          74616E636549443D22786D702E6969643A414543454239383336434338313145
          34424543384145414238334139344245302220786D703A43726561746F72546F
          6F6C3D2241646F62652050686F746F73686F702043533620284D6163696E746F
          736829223E203C786D704D4D3A4465726976656446726F6D2073745265663A69
          6E7374616E636549443D22786D702E6969643A30313830313137343037323036
          38313138323241413234304237433434394531222073745265663A646F63756D
          656E7449443D22786D702E6469643A3031383031313734303732303638313138
          323241413234304237433434394531222F3E203C2F7264663A44657363726970
          74696F6E3E203C2F7264663A5244463E203C2F783A786D706D6574613E203C3F
          787061636B657420656E643D2272223F3EFFEE000E41646F62650064C0000000
          01FFDB008400100B0B0B0C0B100C0C10170F0D0F171B141010141B1F17171717
          171F1E171A1A1A1A171E1E23252725231E2F2F33332F2F404040404040404040
          40404040404001110F0F1113111512121514111411141A141616141A261A1A1C
          1A1A2630231E1E1E1E23302B2E2727272E2B35353030353540403F4040404040
          40404040404040FFC000110800B600C503012200021101031101FFC400AA0001
          0001050100000000000000000000000005010304060702010100030101000000
          000000000000000000010203040510000104010203020A050906040700000002
          00010304111205213106411351617191B12232B273148142723334A1D1526223
          431535368292C2D25416F0A22444C1536383A307171100020103020404030705
          01000000000000010211210331415161120471819122B14282F0A1C132521333
          D12363143492FFDA000C03010002110311003F00E8088880222200888802A2C4
          DC3718E880B946729C99D200CDF55B513BB93B33333785D5EA929CD5629A4611
          390048844B50B3BB670C4DCD4D1D2BB115BD0BA8A0E18ECEF52D998ADCB5AA43
          29C10C55CB413BC7EA919961DDF25C995FDAAC5A8AE58DA6E48F3C9008CB04EE
          CCC52427C3D7C7D6176C2B38513BDE3768852E5AE866DDBF5A844D2D8276D4FA
          40059C8CC9FEA808F17756696F156DCEF5B44B5EC30EB6867078C887F4873CD6
          2DEF57A8F6D297EEDE29C62CF269702FE771CACEB5629C16AAB4C2CF626228EB
          BE9D44DC35171EC6C3714E954566DCA2D8ABABBE8E878DC776ABB6BC0D6339B0
          6C03A5B38EC722F00B65965CB34504452CC6D1C60D92327C33378DD6AD74E6DC
          AD6E1285396D57EE8A95696270D224DC643C1133FB78E5E0598520EF5B15489E
          618AE4FA5E313E2273577D46043DAD91E2ACF1A4A35E3EEE55B91D5AFDC4A53D
          DB6DBC44352C04C43C5C45FD6C7870FC565A818A4F98DD2A47B9567A5B8C1A8E
          09237628A7161C18B1E338C3E74BAAEFDBADB019EB6D84CD3568DE7B53E189A2
          116D421C786A3F42878EB2518EEAB7D3D513D566D93AAAADD73EF208E4FD3112
          F3B657AD61AFBBD4DAF1AB4E78E39670B32C7A45455401111004444011110044
          440111510155837B75829C830347258B26DA86BC03ACF4B7D67E4CCDE5759ACE
          CFC9F38E0EA1EB4835FA8AEC33F03B8114954DFEB0C63A0C19FC4FC71E3568A4
          EB5BF4AAD086F4E6CBB7660DCB62B66119C7AE195B44A2E06C42CFCC5FC6CA13
          6FDD076FD0745A4B7B5BC6276E301721A86ECCE4F19F276CBF106E4A72EDB97F
          8956DB63D24162298AC37321161C03F899C9F0AEECF524A7B5D6A93B0F79146C
          07A7D9CB2D149460EAAAA4EAA35DBEC8AB4DBB3D3723AB4B6284B34F4A02DC36
          CBC6F6223AEE2E5199B7AECE24ED96776CF89656DB5ADC97A7DD6E47DC1CA030
          C35F2C44118BEAC9BB70D44EEA4A38A3881A38818007908B33337D0CBD2A39D6
          B45ADABBD0951E7E463DEA15AFC3DCD91D42CEC424CEE2404DC884878B3AB153
          66A95A6F9872927B1A5C0659E4290845F9B0E797D0A41416F7D535F66B415A58
          0E52306919C5D99999DDC71C7C8905397B21575D90974ABB25EAD582A4035EB8
          347106748B67B5F2FCFC6B066D82A1C2F1C247018CC5662940BD68E53F69C73D
          8FE050BFFE854BFD24BFDE15B351B4376A436C05C467063617E6CC4D9569432E
          3BC938D5FA909C25657A11DFC237063F9A96EFCD5C840C69F780D1C7191B61CC
          8433A9F0B16EF4E856DA6D1C56276B450C8566463776B07A5DCB58165B8F896C
          6A8A165927FD2C4F4230E84F186D15E790B4C635C0CC9F9333033BA89A3B5FF1
          609378B072416AC96AA72464E270C23C236F03EAF69D9D4C6E540370A32D2732
          8825666D418CF07CE30FD9E151C6DD4CD0FC88475F8B681BE25A584396AEE79E
          A66F03E14C5D9B8B51937BFE921AD2AAA92FBCCBD8EE4D72831D87629A339213
          316C31BC44E1AD9BC78520B5D876FF0098B1FC16390E3DB76D006B1A09C0EC4D
          27AF8221E38ED7F2ABE30FF07DCEA4154CDE9DE738CAB9939B0180EB63072777
          6E5876494136E8EEEB24A9B6BF009BA2AF8549C45455591708888022220088A8
          80AAF1DEC7DE3C5A9BBC66D4E196D5A5F8671E056AE5CAF46B958B05A4070CD8
          6C9113F061166E6EEA365AA1BC4637E009B6EBF0BB8C33481A0F0DC70639F581
          FC0EACA35BBB2D2BCC86F86A44D981B6EDC6E0433CD05D94DA7A4D13EAF98EF5
          F8C65197AAFA4DB9F632938EBEE17F4D2DF68C720B36B0B701E180DBC5C0C4BC
          8AD0496EEDFAA33C4D5B76A079973978A6AC7EA4851963C9C3B1D6C2B49CDAA6
          9D54D775E0D148C75E1C088A7B1B51DCFE66A9905728F13099BC8529FD5CEBE2
          DA7CAA5D1556529395DDF62E925A0445450485CDBAD6C34DBF482CF9680023FA
          71ADFDE5D12DD98AA5696CCCF88E117327F132E456ECC96ECCB665E27319197F
          69F2BB3B2856729EC953CD98E7764B896974FE92B0D3EC155D9F2F133C45E507
          76F42E624061A750B8EA6D4396C645FB596DFD03B9304B3EDB23E1A4FDAC39FD
          266C1B79B0EB7EEE1D58AABE57533C2E93A71B1BC22A2AAF30EA0A8AA8808AB3
          52FD5BB25FDB4426F98111B35A4270D4E1C04C0F0F87C707CAF34E8DE9EF36E7
          BA680922170AB5A37D431317B4444FCC9D4B2B17A5B10D5924AB0FCC4ECDFB38
          B2C39777C7177EC6ED57536ED6ABF6D77A15A2D6FC6858DCF78A9B60877DA8E4
          91FD58A36D47A5B899E1BB05B8BACB8668A788268498E2919880DB8B3B3F6AD7
          E3AD7B67B4FBA5D67BEF64586D9C61A8EBBB71668879BC7D8EDF4ACBE9D09182
          DC8311D7A52CCE74E191B49083B36A7D2FECB117166569422A354EB4DF67CBC8
          85275A3F426515155645C222200A8AAB1374BAF4694960475C8D81881F91486E
          C00CFF004BA949B692DC374B985BF13C0746FC82E756A4CE76199B3A5885C064
          C7EABBAF7BBDCA12ECB624EFC4A2963768CA32F5889FD860D3C73AB0AC9EDBBF
          0877C1B9F7965DB255CE31F972F08363D666F1AAECD4768B5186E51D18E0B6C4
          4320B37DDCA0EE2786E5CDB9AD7DA9275EAE87F2FADEBF1297AB54A757124A8B
          4CD4EBFCC7DFB440D2BFEBE96D5F95642A2AAC5970888802A22D6BAA7AA036F0
          2A34898AE9360CDB8B42CFFE2F02B42129C9462AEC89494555919D6FBEB4A7FC
          26B164237CD926E4E4DC83E8ED50BD3BB1C9BC5D607671AB160AC49E2FD16F19
          2B3B4ED17378B7DCC39C6754D31716067ED77ED75D3B6DDB6B6D9502A561C00F
          1727F6889F99178D7764C91C18D6385E6F7FC4E78C5E49754B422BA9BA723DC2
          807CA03059A838805B86A06FDDFE65CF2BCF3D2B413C4EE13C05A9B3CD887B1D
          BD2BB12D4FAB3A59ED6ADCB6F0FF00A86E33C2DFBC66FAC3FADE959F6D9D2FED
          CFF2CB46F997CB8EBEE8EA89FDA3748375A216E1E0EFC240ED036E62EB3572BD
          8B7BB1B2DCEF459CA13F56785F86A66ED6F0132E9B46F56BF582D553692236E0
          FDACFDACEDD8ECB2EE303C72AABC1E8FF02D8F2292E68C845455581A04444061
          5F93741D0DB745148E59D6531B8B078380B3BBAD7E7DC77B386413B810DD699E
          B474A089B5949CC5F5C8E5EAE9F5B38E4B6C50356300DF8ED6E02CD7ECEB8E9C
          40DAB4578BF7844DDA5E17F22D71B54758A7455D2AD949276BBB93350278EB44
          160FBD9C405A4919B1A899B8BE15E5011EFD75C9EDC90C6DB5B4EF59C989FBE0
          762EEDA426F674EA53CA928B4EFBF02C9A7A1544455242C4DCA90DFA72557278
          DCF0E123731317D405F43B2CA5ABC96E18E6BFB94A5DEEE50CE556856727F55D
          D98019A3CFD6D597757845B755AAD3C762B2696BB99C1B8750441F2F36DBDFD9
          6E03623904602FD62D5EB0F8DB0B3F6AA454698C32131CC44524C6DC8A4909CC
          F1E2CBA8CAB1DEDBF72A50CD6E4B4774247B51C8ECE22602C5AA3E1EAB65F0A7
          94CDD2CBA692BFB6B7DB711E75B5AE5511516658F134D141194B318C7183648C
          9F0CDE57751743AA36ABF724A901BE63173690DB48108FB4ECEFE0F1A8EEAFDF
          36C7DB67DBA399A5B32696D21EB30E0989F513706E4B4ADB685ADCAD0D4A98EF
          4D9F9BE96D2DCF2BAB0F6CA58E539B70E15E1C4C6795A9251B9B5F50F5AB3315
          4DA0B2FC8ED7637C3FCFE6509B274D5FDE64EF8F315477C9D83E67E1D19F69FC
          6B66DA3A22954719AF97CD4CDC74631133F93997D3E65B38888B308B330B7066
          6E0CCCA5E78638F46157DE6C2C7293ACFF00F2636DFB754DB6B0D6A81A231E6F
          CC89FF00489FB5D64AAAA17B2FE45CADB6EAEED9B69A10F3F55ECF0DE8E8F7AF
          21993039C6DAA30777C36A2CFA14C6571FAFF8F8BE30FBECBA07595CB1429D5B
          758B4CB1581767EC7F54B2CFE275D397B751963845DE7C789943236A4DFCA5AE
          A2E91877172B74710DD7E243C825F2F80BC7E75A8D1DC775E9DBA43A5E3267C4
          D5E4F64DBFE393B2E87B2EF55778A8D3C2FA651C34D13FB405F9BC0EAE6E5B4D
          0DD22EEAE44C78F64DB818FD92486770AE2CB1EA8E947AA12C6A5EE83A32CECF
          BFD0DDE2D50168999BF69017B63F9DBC6CA496877FA2B73A32FCCED32BCCC0FA
          8199F44C3E47E0CEB276FEB2B9489AAEFD5CC5DB877FA749FF00685F19F2B2AC
          B0465EEC32EB5FA7E641646AD354E7B1BA22C6A77E9DE89A5A930CC1DBA5F8B7
          95B9B2C85CED34E8EC6A1446E2D2D2DCE1DD82239E1EE8ABD8189B5480DAB581
          88F6B6783A975427D22EF8CE1B386E6A62E8F8ECC86AA4050DAE5BBB7CF1CEC7
          56BD9B85646226663785C98D849BEAEA76CA9AA96E0B913CB03BB8B1103E5B0E
          C40EE24D87F1B28CFE25BD5EE1B751F9789FFEE2EBE8FA5A21F5BCEA9B2C36E8
          DFB94ED1F7CF369B63308680723F524166E2CDC5996924DA6E4D26AEA29FA954
          E8D53C2A4DA2A22C8B9E6696382239A52D31C62E464FD8CCD97750AF7F6AD21B
          C58A051D890DA3AAEF1B1589B2DEAB80B71E2DE1525BA543BBB759A805A4E68C
          805DF965DB8654546377E60774DD6BF710EDF13843044FDF1148781391983C5C
          196904A8DD6FC2B47E1E6564DD4C8D960A731CBB9472CD3D837288FE6781C387
          CBC4C186D2A5D466C714FA6D5C9E3784AECCF30C45C08418440353763BB0E54A
          28C9F99DEBF6D098E851635BDBABDCF56CEB38FB6363210FA581DB2B2915536A
          EAC49ACF52ED3B651E9FB4556AC711368C188B6AF6C7EB735AE7447F3F8FE1C9
          E85B7758FF004F5AFEC7BE2B51E88FE7F1FC393D0BB70B6FB6CADBAEBAF8184D
          532C69C8E92888B84DC2F25ECBF91D7A5E4FD82F23A039056FC7C5F187DF65BC
          F5FF00F2987E38FBA4B46ABF8E87E307BECB79EBFF00E530FC71F7497A59BF9B
          09CB0FC93347A37EDEDF606CD491E3907CC4DE026ED65BE6CDD6742EB0C575DA
          A59E5EB3FEC89FC45D9F4AE768B5CB821935B3E2B52B0C928E9A703B30931331
          0BB3B3F1676E2CBCCB0C330E8980641FD136626F33AE4F4778DCF6FF00C25938
          C7F433A83FBA5965BCF496FD77781B037183541A349036977D5AB9B671D8B872
          F6B3C69CAA9C5793378658C9D294648FFB7F68197BE8AB3432F61C2E513FFF00
          1BB2CF8C340B0EA72C7693E5FCEBDA2E7726F56DF89A2496882A2AA28248ABB7
          EF9DC7DBF6B8C0A701639E799DFBB898B3A5B03C489F0BCC37375A76A1AFBA34
          52C568B445620620D3261C980C49DFDAC7075E6CCB36D5B94F71EBC9629DC10E
          F0A11D671491B38F10E6E24CADB4D6B7AB559C2B495A85591A739676D0721833
          E8100E78CBE5DDD6C92A691E9E9BCB7A94ADF575AE84E2222C4B987BAEE43B65
          37B45114CCCEC3A43B33DA4EFC87C2EA18AE6F57E6A803661AB5AE118B3D5769
          A466007938C84DA73C3B196CAED9E0A26CF4E53399ADD272A1705DC865871A72
          ED87D51BFAAF95A639416AAF7F7355FB8AC937A7A684854ACD56B840C672B067
          F69296A32777CBB93FD2AFAB5586718231B26D24CC2CD218B69622F0B3762BAA
          8F5659044450083EB2FE9EB3E50F7C56A5D11FCFE3F8727A16DBD65FD3D67CB1
          FBE2B52E88FE7F1FC393D0BB70FF00CB93EAF8184FF963E47494445C46E17893
          EECBC8FE85ED7897EE8FECBFA101C86A7E3A0F8C1EF32DE7AFFF0094C3F1C7DD
          25A2D4FC6C1F183DE65D62F6DD4F7089A1B9134D1896A617CB71E59E18F0AF43
          B992864C527F2D4E6C4AB19AE272145D47FDA9D3FF00E883CE5FE64FF6A74FFF
          00A20F397F994FFBD8FF004C87EC4B8A3972DCFF00FAEF9DFF00FDAFF1A9EFF6
          A74FFF00A20F397F9965D0DAB6FDB75FC942D0F798D78777CE9CE39BBF85679B
          BA84F1B8252ABA6A5A185C649B6AC662222E237088880C1DC21DDA430FE1F662
          AE0CCFAFBC8FBC777ECC716504573A81A85BBCF7E376A929C4F18C03EBF766C0
          EFA9DDF19537BA43B9D978EB52946B4279F98B1CE416E1818DBC2FE1EC51C7D3
          76E3092854B231ED53931CB198B94CCFC35B09E7EB69ED5B63714975386BBC6B
          6DCA493ADBABD49FCFA9ABC5944D2DA74F66308B12E7A4444011110044440417
          59FF004F58FB51FBE2B53E88FE7E1F0A4F42DAFAD3FA7A7FB51FBE2B54E88FE7
          E1F0A4F42EEC3FF2E4FABE0613FE58F91D25111709B85E26FBA3FB2FE85ED5B9
          FEE64FB25E8440E434FF001B07C50F7997625C7697E36BFC50F7997625DBDF6B
          0F066183497884445C46E1111004444011110044440111100444401111004444
          040F5AFF004FCFF6E3F7D96A9D11FCFC3E149E865B575B7F4FCDF6E3F7996BFD
          17B6DF8F748AEC95CC2A9466C3293605F2DC39AEDC4D2ED67574AF57C0C26BFB
          B1F237F4445C46E15B9FEE24FB05E85715AB1F8797EC17A116A0E454BF1B5FE2
          87BCCBB12E4DB46DB7EDD88A5AD5CE58A3941CCC5BD56C133F35D617677CD394
          127A26618159954445C66E111100444401111004444011110044440111533840
          11455EEA3A357211BFCC4ADF543D96F29281B5D47B958776036803C11F3FEF3F
          159CB2C23BD7C0D238A4F6A789B2EF14A2BD582BCE4C30F7A072EA7C6441F53B
          7D2BD96E7B644CC2F662166E0CCC4DC3CCB459259252D5299197849DDFD2BC2C
          DF72E8925645D76EB56CDF1B79DADFFEEA3F3ABD1DDA72FDDCF19BBF63133BFA
          573D451FEC3E089FD85C59D2178985CE19007DA21766F2BB2D021B96E07CC331
          C7F649F1E65255FA9F728B849A671FD66C3F9C55E3DC4774D147825B34CD9B6B
          A116DD461A71370885989FF48B9917D2EB294255EAAA3261AC01405E1F6C7CED
          C7F229682DD6B23AABCA3237EABB3BF996AB22936EB56EE66E0E366A85E45445
          2415444401111004444011110044440151551018F6ED8550D4E2721BFB31C62E
          444FF42D7AF3F50EE4EE2D5CE181F947961CFDA7776CADA1555250EAB55A5C8B
          465D37A26F99A5374DEECFFBA16F298AABF4DEECDFBB17F21B2DD1153F621CCB
          FEF4B91A31EC5BB073AC4FF65D8BD0EB165A96A1FBD8640F190BB32E86A8ED95
          0FB78ECD92B3BDD239BA2DFE7DB6858FBEAE04EFDB8C3F9DB8A8CB1D294CF2F5
          E4385FC0FEB8FE5E3F9551E092D28CBACD17AD51A9A296B3D37B941970169C5B
          B41F8FF75D464914B1168940A326EC26767FCAB27192D550D1493D1D4F0AA244
          24C42EE24DC9D9F0EA88A0924AB6FF00BA57C377BDE8B7D591B57E5E6A520EAD
          07C359AEE3E128DF3F90B0B594575926B47EA51E383D8DDA1EA1DAA5FDF776FE
          0919C7F2F25991DEA727DDCF197909BF3AE7A8B45DC4B749947816CD9D1DA407
          E44CFF004B2F253423C4A411F2BB32E779C2CBA7B55EBCEDDCC4FA3B642E01E7
          7FFC14ACEDD946A43C295DC8DC65DDB6D8BDBB31B7899F53FF00CB95EAADE0B7
          EB4119F75FF9A4DA45FECE78BAC0DBBA6EA55C4963FEA256EC76F519FC43DBF4
          A9866C706E4B58F5BBCA8B9194BA56957CC2AA22B95088880222200888802222
          00888802A22200ACDAF93EEDFE6FBBEEFF00F5318FF991143D36F3256A6BD6A2
          E95909F4CCF0978626371F749946CD4F6D6E306E024DE038A417F3B0BA22E59D
          3FC7E553A235FF00279D0C330117C0C827E31D5FE2165E511646A7A00127C118
          83784B57F845D66C14F6C77CD8DC045BC11C723BF9C859115A34DFA7CEBF8159
          576EAF2A131B7874C01B34720CB2F614ECEDE6D6222A7C74E1B4E34F66396111
          7563D3E5FA4E6C9AFCDF51555445A14088880222203FFFD9}
        Stretch = True
      end
      object RLLabel1: TRLLabel
        Left = 96
        Top = 0
        Width = 299
        Height = 27
        Caption = 'Associa'#231#227'o Casa do Pastor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel2: TRLLabel
        Left = 96
        Top = 32
        Width = 251
        Height = 18
        Caption = 'AJUSTE DE VALOR DE DOA'#199#195'O'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
    object RLBand4: TRLBand
      Left = 19
      Top = 211
      Width = 756
      Height = 54
      BandType = btFooter
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = False
      object RLSystemInfo1: TRLSystemInfo
        Left = 88
        Top = 32
        Width = 39
        Height = 16
      end
      object RLLabel5: TRLLabel
        Left = 0
        Top = 32
        Width = 83
        Height = 16
        Caption = 'Impresso em:'
      end
      object RLSystemInfo2: TRLSystemInfo
        Left = 624
        Top = 32
        Width = 87
        Height = 16
        Info = itPageNumber
      end
      object RLDBResult7: TRLDBResult
        Left = 183
        Top = 6
        Width = 120
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        DataField = 'vl_beneficio'
        DataSource = dsRelatorio
        DisplayMask = 'R$ #,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Info = riSum
        ParentFont = False
      end
      object RLDBResult8: TRLDBResult
        Left = 272
        Top = 6
        Width = 120
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        DataField = 'vl_escalonamento'
        DataSource = dsRelatorio
        DisplayMask = 'R$ #,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Info = riSum
        ParentFont = False
      end
      object RLDBResult9: TRLDBResult
        Left = 360
        Top = 6
        Width = 120
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        DataField = 'vl_descontos'
        DataSource = dsRelatorio
        DisplayMask = 'R$ #,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Info = riSum
        ParentFont = False
      end
      object RLDBResult10: TRLDBResult
        Left = 435
        Top = 6
        Width = 120
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        DataField = 'vl_liquidoatual'
        DataSource = dsRelatorio
        DisplayMask = 'R$ #,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Info = riSum
        ParentFont = False
      end
      object RLDBResult11: TRLDBResult
        Left = 528
        Top = 6
        Width = 120
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        DataField = 'vl_novobruto'
        DataSource = dsRelatorio
        DisplayMask = 'R$ #,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Info = riSum
        ParentFont = False
      end
      object RLDBResult12: TRLDBResult
        Left = 635
        Top = 6
        Width = 120
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        DataField = 'vl_novoliquido'
        DataSource = dsRelatorio
        DisplayMask = 'R$ #,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Info = riSum
        ParentFont = False
      end
      object RLLabel13: TRLLabel
        Left = 3
        Top = 3
        Width = 74
        Height = 16
        Caption = 'Totais Geral'
      end
    end
    object RLGroup1: TRLGroup
      Left = 19
      Top = 121
      Width = 756
      Height = 90
      DataFields = 'cd_sitJubilamento'
      object RLBand2: TRLBand
        Left = 0
        Top = 0
        Width = 756
        Height = 48
        BandType = btColumnHeader
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = False
        Borders.DrawRight = False
        Borders.DrawBottom = True
        object RLLabel3: TRLLabel
          Left = 2
          Top = 32
          Width = 20
          Height = 14
          Caption = 'No.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RLLabel4: TRLLabel
          Left = 29
          Top = 32
          Width = 84
          Height = 14
          Caption = 'Nome Jubilado'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RLLabel6: TRLLabel
          Left = 241
          Top = 32
          Width = 62
          Height = 14
          Caption = 'Valor Base'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RLLabel7: TRLLabel
          Left = 352
          Top = 32
          Width = 40
          Height = 14
          Caption = 'Dizimo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RLLabel8: TRLLabel
          Left = 418
          Top = 32
          Width = 62
          Height = 14
          Caption = 'Descontos'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RLLabel9: TRLLabel
          Left = 511
          Top = 32
          Width = 44
          Height = 14
          Caption = 'Liquido'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RLLabel10: TRLLabel
          Left = 585
          Top = 32
          Width = 63
          Height = 14
          Caption = 'Novo Bruto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RLLabel11: TRLLabel
          Left = 680
          Top = 32
          Width = 74
          Height = 14
          Caption = 'Novo Liquido'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object RLBand3: TRLBand
        Left = 0
        Top = 48
        Width = 756
        Height = 16
        object RLDBText1: TRLDBText
          Left = 2
          Top = 1
          Width = 64
          Height = 14
          DataField = 'cd_cadastro'
          DataSource = dsRelatorio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText2: TRLDBText
          Left = 28
          Top = 1
          Width = 54
          Height = 14
          DataField = 'nm_pastor'
          DataSource = dsRelatorio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText3: TRLDBText
          Left = 242
          Top = 1
          Width = 61
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_beneficio'
          DataSource = dsRelatorio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText4: TRLDBText
          Left = 302
          Top = 1
          Width = 90
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_escalonamento'
          DataSource = dsRelatorio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText5: TRLDBText
          Left = 412
          Top = 1
          Width = 68
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_descontos'
          DataSource = dsRelatorio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText6: TRLDBText
          Left = 485
          Top = 1
          Width = 70
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_liquidoatual'
          DataSource = dsRelatorio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText7: TRLDBText
          Left = 582
          Top = 1
          Width = 66
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_novobruto'
          DataSource = dsRelatorio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText8: TRLDBText
          Left = 683
          Top = 1
          Width = 71
          Height = 14
          Alignment = taRightJustify
          DataField = 'vl_novoliquido'
          DataSource = dsRelatorio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
      end
      object RLBand5: TRLBand
        Left = 0
        Top = 64
        Width = 756
        Height = 26
        BandType = btColumnFooter
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = True
        Borders.DrawRight = False
        Borders.DrawBottom = False
        object RLDBResult1: TRLDBResult
          Left = 214
          Top = 5
          Width = 89
          Height = 12
          Alignment = taRightJustify
          AutoSize = False
          DataField = 'vl_beneficio'
          DataSource = dsRelatorio
          DisplayMask = 'R$ #,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Info = riSum
          ParentFont = False
        end
        object RLDBResult2: TRLDBResult
          Left = 285
          Top = 5
          Width = 107
          Height = 12
          Alignment = taRightJustify
          AutoSize = False
          DataField = 'vl_escalonamento'
          DataSource = dsRelatorio
          DisplayMask = 'R$ #,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Info = riSum
          ParentFont = False
        end
        object RLDBResult3: TRLDBResult
          Left = 384
          Top = 5
          Width = 96
          Height = 12
          Alignment = taRightJustify
          AutoSize = False
          DataField = 'vl_descontos'
          DataSource = dsRelatorio
          DisplayMask = 'R$ #,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Info = riSum
          ParentFont = False
        end
        object RLDBResult4: TRLDBResult
          Left = 473
          Top = 5
          Width = 82
          Height = 12
          Alignment = taRightJustify
          AutoSize = False
          DataField = 'vl_liquidoatual'
          DataSource = dsRelatorio
          DisplayMask = 'R$ #,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Info = riSum
          ParentFont = False
        end
        object RLDBResult5: TRLDBResult
          Left = 557
          Top = 5
          Width = 91
          Height = 12
          Alignment = taRightJustify
          AutoSize = False
          DataField = 'vl_novobruto'
          DataSource = dsRelatorio
          DisplayMask = 'R$ #,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Info = riSum
          ParentFont = False
        end
        object RLDBResult6: TRLDBResult
          Left = 655
          Top = 5
          Width = 99
          Height = 12
          Alignment = taRightJustify
          AutoSize = False
          DataField = 'vl_novoliquido'
          DataSource = dsRelatorio
          DisplayMask = 'R$ #,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Info = riSum
          ParentFont = False
        end
        object RLLabel12: TRLLabel
          Left = 3
          Top = 3
          Width = 39
          Height = 16
          Caption = 'Totais'
        end
      end
    end
  end
  object qRelatorio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '
      '    cd_sitJubilamento,'
      '    cd_cadastro,'
      #9'nm_pastor,'
      #9'vl_beneficio,'
      '    vl_escalonamento,'
      #9'isnull(vl_descontos,0) vl_descontos,'
      
        #9'(vl_beneficio - vl_escalonamento - isnull(vl_descontos,0)) as v' +
        'l_liquidoatual,'
      #9'case '
      #9#9'when (vl_beneficio - vl_escalonamento) < 545'
      #9#9#9'then vl_beneficio'
      #9#9#9'else (vl_beneficio - vl_escalonamento)'
      #9'end as vl_novobruto,'
      #9'case '
      #9#9'when (vl_beneficio - vl_escalonamento) < 545'
      #9#9#9'then 54.5'
      
        #9#9#9'else convert(decimal(10,2), ((vl_beneficio - vl_escalonamento' +
        ')*0.1))'
      #9'end as vl_dizimo,'
      #9'case '
      #9#9'when (vl_beneficio - vl_escalonamento) < 545'
      #9#9#9'then vl_beneficio - isnull(vl_descontos,0)'
      
        #9#9'else vl_beneficio - vl_escalonamento  - isnull(vl_descontos,0)' +
        ' - case '
      #9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9'when (vl_beneficio - vl_escalonamento) < 545'
      #9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9'then 54.5'
      
        #9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9'else convert(decimal(10,2), ((vl_beneficio -' +
        ' vl_escalonamento)*0.1))'
      #9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9#9' end'
      #9'end as vl_novoliquido'
      #9
      'from vwListaAjusteValor '
      'order by '
      'cd_sitJubilamento, nm_pastor'
      ''
      ''
      '')
    Left = 384
    Top = 352
    object qRelatoriocd_sitJubilamento: TStringField
      FieldName = 'cd_sitJubilamento'
      FixedChar = True
      Size = 1
    end
    object qRelatorionm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qRelatoriovl_beneficio: TBCDField
      FieldName = 'vl_beneficio'
      ReadOnly = True
      currency = True
      Precision = 10
      Size = 2
    end
    object qRelatoriovl_escalonamento: TBCDField
      FieldName = 'vl_escalonamento'
      ReadOnly = True
      currency = True
      Precision = 10
      Size = 2
    end
    object qRelatoriovl_descontos: TBCDField
      FieldName = 'vl_descontos'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qRelatoriovl_liquidoatual: TBCDField
      FieldName = 'vl_liquidoatual'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qRelatoriovl_novobruto: TBCDField
      FieldName = 'vl_novobruto'
      ReadOnly = True
      currency = True
      Precision = 11
      Size = 2
    end
    object qRelatoriovl_dizimo: TBCDField
      FieldName = 'vl_dizimo'
      ReadOnly = True
      currency = True
      Precision = 10
      Size = 2
    end
    object qRelatoriovl_novoliquido: TBCDField
      FieldName = 'vl_novoliquido'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qRelatoriocd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object dsRelatorio: TDataSource
    DataSet = qRelatorio
    Left = 320
    Top = 352
  end
end
