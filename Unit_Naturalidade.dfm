inherited frm_Naturalidade: Tfrm_Naturalidade
  Caption = 'Naturalidade'
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel1: TPanel
    inherited btnRelatorio: TBitBtn
      OnClick = btnRelatorioClick
    end
  end
  inherited Panel2: TPanel
    object Label1: TLabel
      Left = 10
      Top = 10
      Width = 60
      Height = 16
      Caption = 'Naturalidade'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Left = 10
      Top = 30
      Width = 325
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_natpastor'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 64
      Width = 329
      Height = 120
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalcd_natpastor: TIntegerField
      FieldName = 'cd_natpastor'
    end
    object CDSPrincipalds_natpastor: TStringField
      DisplayLabel = 'Naturalidade'
      FieldName = 'ds_natpastor'
      Required = True
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    SQL.Strings = (
      'select cd_natpastor,ds_natpastor'
      'from tbNaturalidade'
      'order by ds_natpastor')
    object QPrincipalcd_natpastor: TIntegerField
      FieldName = 'cd_natpastor'
    end
    object QPrincipalds_natpastor: TStringField
      FieldName = 'ds_natpastor'
      Required = True
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 352
    Top = 240
  end
end
