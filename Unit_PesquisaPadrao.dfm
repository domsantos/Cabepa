object frm_PesquisaPadrao: Tfrm_PesquisaPadrao
  Left = 277
  Top = 292
  Width = 662
  Height = 365
  Caption = 'frmPesquisaPadrao'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 646
    Height = 65
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 54
      Height = 13
      Caption = 'Argumento:'
    end
    object Label2: TLabel
      Left = 312
      Top = 8
      Width = 68
      Height = 13
      Caption = 'Pesquisar Por:'
    end
    object edtArgumento: TEdit
      Left = 8
      Top = 24
      Width = 281
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
    end
    object cbPesquisa: TComboBox
      Left = 312
      Top = 24
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 1
      Text = 'cbPesquisa'
    end
    object btnPesquisa: TBitBtn
      Left = 488
      Top = 16
      Width = 121
      Height = 33
      Caption = '&Pesquisar'
      TabOrder = 2
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF55555555555559055555
        55555555577FF5555555555599905555555555557777F5555555555599905555
        555555557777FF5555555559999905555555555777777F555555559999990555
        5555557777777FF5555557990599905555555777757777F55555790555599055
        55557775555777FF5555555555599905555555555557777F5555555555559905
        555555555555777FF5555555555559905555555555555777FF55555555555579
        05555555555555777FF5555555555557905555555555555777FF555555555555
        5990555555555555577755555555555555555555555555555555}
      NumGlyphs = 2
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 65
    Width = 646
    Height = 262
    Align = alClient
    DataSource = dsPesquisa
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
  end
  object dsPesquisa: TDataSource
    Left = 584
    Top = 80
  end
  object ActionList1: TActionList
    Left = 584
    Top = 168
    object acPesquisa: TAction
      Caption = 'acPesquisa'
      ShortCut = 16464
      OnExecute = acPesquisaExecute
    end
    object acSair: TAction
      Caption = 'acSair'
      ShortCut = 121
      OnExecute = acSairExecute
    end
  end
end
