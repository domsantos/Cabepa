inherited frm_MensagemCCheque: Tfrm_MensagemCCheque
  Caption = 'Mensagem Contra Cheque'
  ClientHeight = 463
  ExplicitWidth = 728
  ExplicitHeight = 501
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel2: TPanel
    Height = 422
    object Label3: TLabel
      Left = 16
      Top = 52
      Width = 52
      Height = 16
      Caption = 'Mensagem'
    end
    object Label2: TLabel
      Left = 87
      Top = 6
      Width = 19
      Height = 16
      Caption = 'Ano'
      FocusControl = DBEdit2
    end
    object Label1: TLabel
      Left = 16
      Top = 6
      Width = 20
      Height = 16
      Caption = 'M'#234's'
      FocusControl = DBEdit1
    end
    object DBEdit2: TDBEdit
      Left = 87
      Top = 22
      Width = 74
      Height = 24
      DataField = 'dt_ano'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit1: TDBEdit
      Left = 16
      Top = 22
      Width = 65
      Height = 24
      DataField = 'dt_mes'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBMemo1: TDBMemo
      Left = 16
      Top = 74
      Width = 369
      Height = 89
      DataField = 'ds_mensagem'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBGrid1: TDBGrid
      Left = 16
      Top = 184
      Width = 585
      Height = 225
      DataSource = DataSource1
      TabOrder = 3
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipaldt_mes: TSmallintField
      DisplayLabel = 'M'#234's'
      FieldName = 'dt_mes'
    end
    object CDSPrincipaldt_ano: TSmallintField
      DisplayLabel = 'Ano'
      FieldName = 'dt_ano'
    end
    object CDSPrincipalds_mensagem: TStringField
      DisplayLabel = 'Mensagem'
      FieldName = 'ds_mensagem'
      Size = 400
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    SQL.Strings = (
      'select * from tbMesagemCCheque'
      'order by dt_ano desc, dt_mes desc')
    object QPrincipaldt_mes: TSmallintField
      FieldName = 'dt_mes'
    end
    object QPrincipaldt_ano: TSmallintField
      FieldName = 'dt_ano'
    end
    object QPrincipalds_mensagem: TStringField
      FieldName = 'ds_mensagem'
      Size = 400
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 640
    Top = 304
  end
end
