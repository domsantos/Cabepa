unit Unit_TrocaSenha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  Tfrm_TrocaSenha = class(TForm)
    Label1: TLabel;
    edtSenha: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    edtNSenha: TEdit;
    Label4: TLabel;
    edtRSenha: TEdit;
    BitBtn1: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_TrocaSenha: Tfrm_TrocaSenha;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_TrocaSenha.BitBtn1Click(Sender: TObject);
begin
  dm.qCheckUsuario.Close;
  dm.qCheckUsuario.Parameters.ParamByName('id').Value := dm.idLogin;
  dm.qCheckUsuario.Open;

  if dm.qCheckUsuariovl_senha.Value = edtSenha.Text then
  begin
    if edtNSenha.Text = edtRSenha.Text then
    begin

      dm.qTrocaSenha.Close;
      dm.qTrocaSenha.Parameters.ParamByName('id').Value := dm.idLogin;
      dm.qTrocaSenha.Parameters.ParamByName('senha').Value := edtNSenha.Text;

      try
         dm.qTrocaSenha.ExecSQL;
         ShowMessage('Senha trocada');
      except
        on e:Exception
          do showmessage('Erro ao trocar a senha #13'+e.Message);


      end;

    end
    else
    begin
      showmessage('Senha nova n�o confere');
      edtNSenha.SetFocus;
    end;
  end
  else
  begin
    showmessage('Senha atual n�o confere');
    edtSenha.SetFocus;
  end;

end;

procedure Tfrm_TrocaSenha.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

end.
