inherited frm_Perfil: Tfrm_Perfil
  Left = 240
  Top = 181
  Caption = 'Perfil'
  ClientHeight = 449
  ExplicitHeight = 487
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel2: TPanel
    Height = 408
    ExplicitHeight = 408
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 23
      Height = 16
      Caption = 'Perfil'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Left = 16
      Top = 24
      Width = 404
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_perfil'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 16
      Top = 56
      Width = 401
      Height = 209
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalid_perfil: TAutoIncField
      FieldName = 'id_perfil'
      ReadOnly = True
      Visible = False
    end
    object CDSPrincipalds_perfil: TStringField
      DisplayLabel = 'Perfil'
      FieldName = 'ds_perfil'
      Size = 25
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    SQL.Strings = (
      'select id_perfil,ds_perfil'
      'from tbPerfil'
      'order by ds_perfil')
    object QPrincipalid_perfil: TAutoIncField
      FieldName = 'id_perfil'
      ReadOnly = True
    end
    object QPrincipalds_perfil: TStringField
      FieldName = 'ds_perfil'
      Size = 25
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 632
    Top = 320
  end
end
