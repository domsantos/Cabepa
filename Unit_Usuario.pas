unit Unit_Usuario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, ADODB, Provider, ActnList, DB, DBClient, StdCtrls,
  Buttons, ExtCtrls, DBCtrls, Mask, Grids, DBGrids;

type
  Tfrm_Usuario = class(Tfrm_PadraoCadastro)
    QPrincipalid_usuario: TAutoIncField;
    QPrincipalnm_login: TStringField;
    QPrincipalnm_usuario: TStringField;
    QPrincipalds_email: TStringField;
    QPrincipalnr_fone_contato: TStringField;
    QPrincipallb_foto: TBlobField;
    QPrincipalid_perfil: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    QPerfil: TADOQuery;
    dsPErfil: TDataSource;
    Label5: TLabel;
    DBGrid1: TDBGrid;
    CDSPrincipalid_usuario: TAutoIncField;
    CDSPrincipalnm_login: TStringField;
    CDSPrincipalnm_usuario: TStringField;
    CDSPrincipalds_email: TStringField;
    CDSPrincipalnr_fone_contato: TStringField;
    CDSPrincipallb_foto: TBlobField;
    CDSPrincipalid_perfil: TIntegerField;
    procedure btnNovoClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Usuario: Tfrm_Usuario;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_Usuario.btnNovoClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

procedure Tfrm_Usuario.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.open();
end;

procedure Tfrm_Usuario.FormShow(Sender: TObject);
begin
  inherited;
  QPerfil.Open;
end;

end.
