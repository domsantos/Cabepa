object frm_cargaContribuicoesArquivos: Tfrm_cargaContribuicoesArquivos
  Left = 0
  Top = 0
  Caption = 'frm_cargaContribuicoesArquivos'
  ClientHeight = 335
  ClientWidth = 578
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 136
    Top = 49
    Width = 6
    Height = 286
    ExplicitWidth = 17
  end
  object listFiles: TFileListBox
    Left = 0
    Top = 49
    Width = 136
    Height = 286
    Align = alLeft
    ItemHeight = 13
    Mask = '*.txt'
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 578
    Height = 49
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 13
      Width = 134
      Height = 25
      Caption = 'Carregar arquivos'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object Memo1: TMemo
    Left = 142
    Top = 49
    Width = 436
    Height = 286
    Align = alClient
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object Pagamento: TADOTable
    Connection = DM.ADOConn
    CursorType = ctStatic
    TableName = 'tbPagtoContribuicao'
    Left = 184
    Top = 64
    object Pagamentovl_pagtoContribuicao: TBCDField
      FieldName = 'vl_pagtoContribuicao'
      Precision = 10
      Size = 2
    end
    object Pagamentodt_pagtoContribuicao: TDateTimeField
      FieldName = 'dt_pagtoContribuicao'
    end
    object Pagamentocd_formPagto: TSmallintField
      FieldName = 'cd_formPagto'
    end
    object Pagamentodt_anoRefContribuicao: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
    end
    object Pagamentodt_mesRefContribuicao: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
    end
    object Pagamentocd_tipoContribuicao: TIntegerField
      FieldName = 'cd_tipoContribuicao'
    end
    object Pagamentocd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object contribicoes: TADOTable
    Connection = DM.ADOConn
    CursorType = ctStatic
    TableName = 'tbContribuicoes'
    Left = 184
    Top = 112
    object contribicoesdt_anoRefContribuicao: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
    end
    object contribicoesdt_mesRefContribuicao: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
    end
    object contribicoesvl_ContribuicaoVencimento: TBCDField
      FieldName = 'vl_ContribuicaoVencimento'
      Precision = 10
      Size = 2
    end
    object contribicoescd_tipoContribuicao: TIntegerField
      FieldName = 'cd_tipoContribuicao'
    end
    object contribicoescd_sitQuitacao: TSmallintField
      FieldName = 'cd_sitQuitacao'
    end
    object contribicoescd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object qBuscaContribuicao: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ano'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'mes'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select count(*) as qtd from tbContribuicoes'
      'where cd_cabepa = :codigo'
      'and dt_anoRefContribuicao = :ano'
      'and dt_mesRefContribuicao = :mes')
    Left = 320
    Top = 80
    object qBuscaContribuicaoqtd: TIntegerField
      FieldName = 'qtd'
      ReadOnly = True
    end
  end
end
