inherited frm_FormaPagamento: Tfrm_FormaPagamento
  Left = 251
  Top = 215
  Caption = 'frm_FormaPagamento'
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel2: TPanel
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 87
      Height = 16
      Caption = 'Forma Pagamento'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 24
      Width = 417
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_formPagto'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 56
      Width = 417
      Height = 185
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalcd_formPagto: TSmallintField
      FieldName = 'cd_formPagto'
      ReadOnly = True
      Visible = False
    end
    object CDSPrincipalds_formPagto: TStringField
      DisplayLabel = 'Forma de Pagamento'
      FieldName = 'ds_formPagto'
      Size = 40
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    SQL.Strings = (
      'select cd_formPagto,ds_formPagto'
      'from tbFormaPagto'
      'order by ds_formPagto')
    Top = 73
    object QPrincipalcd_formPagto: TSmallintField
      FieldName = 'cd_formPagto'
      ReadOnly = True
    end
    object QPrincipalds_formPagto: TStringField
      FieldName = 'ds_formPagto'
      Size = 40
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 640
    Top = 312
  end
end
