unit Unit_calculaAumento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, rxToolEdit, rxCurrEdit, DB, ADODB;

type
  Tfrm_calculaAumento = class(TForm)
    edtValorRef: TCurrencyEdit;
    edtValorAumentado: TCurrencyEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label4: TLabel;
    edtValorInicial: TCurrencyEdit;
    edtValorFinal: TCurrencyEdit;
    edtPercentual: TCurrencyEdit;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    Label5: TLabel;
    Label6: TLabel;
    Percentual: TLabel;
    qListaAferadosValor: TADOQuery;
    qAtalizaValor: TADOQuery;
    qListaAferadosValorno_reqBeneficio: TIntegerField;
    qListaAferadosValorvl_Beneficio: TBCDField;
    qListaAfetadosPerc: TADOQuery;
    qListaAfetadosPercvl_Beneficio: TBCDField;
    qListaAfetadosPercno_reqBeneficio: TIntegerField;
    procedure BitBtn3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_calculaAumento: Tfrm_calculaAumento;

implementation

uses Unit_relSimulaPercentual, Unit_DM, Unit_RelSimulaAumentoValor;

{$R *.dfm}

procedure Tfrm_calculaAumento.BitBtn1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_RelSimulaAumentoValor,frm_RelSimulaAumentoValor);

  with frm_RelSimulaAumentoValor do
  begin
    qRelatorio.Close;
    qRelatorio.Parameters.ParamByName('valor').Value := edtValorRef.Value;
    qRelatorio.Parameters.ParamByName('aumento').Value := edtValorAumentado.Value;
    qRelatorio.Open;

    lblAumento.caption := lblAumento.caption  + ' ' + FormatCurr('R$ #,##0.00',edtValorAumentado.Value);

    RLReport1.Preview();

    Free;
    
  end;


end;

procedure Tfrm_calculaAumento.BitBtn2Click(Sender: TObject);
begin

  if edtValorRef.Text = '' then
  begin
    showmessage('Informe valor de refer�ncia');
    edtValorRef.SetFocus;
    exit;
  end;

  if edtValorAumentado.Text = '' then
  begin
    showmessage('Informe valor de aumento');
    edtValorAumentado.SetFocus;
    exit;
  end;

  if MessageDlg('Deseja registrar o aumento do valor', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
  begin
    showmessage('Opera��o cancelada');
    exit;
  end;

  qListaAferadosValor.Close;
  qListaAferadosValor.Parameters.ParamByName('valor').Value := edtValorRef.Value;
  qListaAferadosValor.Open;

  if qListaAferadosValor.RecordCount < 1 then
  begin
    showmessage('N�o h� Beneficiados para atribuir aumento!');
    exit;
  end;
  dm.ADOConn.BeginTrans;

  while not qListaAferadosValor.Eof do
  begin
    qAtalizaValor.Close;
    qAtalizaValor.Parameters.ParamByName('nr_req').Value :=
      qListaAferadosValorno_reqBeneficio.Value;
    qAtalizaValor.Parameters.ParamByName('valor').Value :=
      edtValorAumentado.Value;

    try
      qAtalizaValor.ExecSQL;
    except
      dm.ADOConn.RollbackTrans;
      showmessage('Erro ao atualizar valor'+#13+'Nenhum valor foi alterado!');
      exit;
    end;
    qListaAferadosValor.Next;
  end;

  dm.ADOConn.CommitTrans;
  showmessage('Opera��o realizada!');



end;

procedure Tfrm_calculaAumento.BitBtn3Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_RelSimulaAumentoPercentual,frm_RelSimulaAumentoPercentual);

  with frm_RelSimulaAumentoPercentual do
  begin
    QspSimulaAumento.Close;

    QspSimulaAumento.Parameters.ParamByName('inicio').Value := edtValorInicial.Value;
    QspSimulaAumento.Parameters.ParamByName('fim').Value := edtValorFinal.Value;
    QspSimulaAumento.Parameters.ParamByName('percentual').Value := edtPercentual.Value;

    QspSimulaAumento.Open;


    ValorRef.Caption := ValorRef.Caption + ' '+FormatCurr('R$ #,##0.00',edtValorInicial.Value) + ' a ' + FormatCurr('R$ #,##0.00',edtValorFinal.Value);
    Percentual.Caption := Percentual.Caption + ' '+ edtPercentual.Text;

    RLReport1.Preview();

    Free;

  end;
end;

procedure Tfrm_calculaAumento.BitBtn4Click(Sender: TObject);
var
  valor : Currency;
begin
  if edtValorInicial.Text = '' then
  begin
    showmessage('Informe valor inicial de refer�ncia');
    edtValorInicial.SetFocus;
    exit;
  end;
  if edtValorFinal.Text = '' then
  begin
    showmessage('Informe valor final de refer�ncia');
    edtValorFinal.SetFocus;
    exit;
  end;
  if edtPercentual.Text = '' then
  begin
    showmessage('Informe valor do percentualde aumento');
    edtPercentual.SetFocus;
    exit;
  end;

   if MessageDlg('Deseja registrar o aumento de percentual', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
  begin
    showmessage('Opera��o cancelada');
    exit;
  end;

  qListaAfetadosPerc.Close;
  qListaAfetadosPerc.Parameters.ParamByName('vl_inicial').Value := edtValorInicial.Value;
  qListaAfetadosPerc.Parameters.ParamByName('vl_final').Value := edtValorFinal.Value;
  qListaAfetadosPerc.Open;

  if qListaAfetadosPerc.RecordCount < 1 then
  begin
    showmessage('N�o h� Beneficiados para atribuir aumento!');
    exit;
  end;
  dm.ADOConn.BeginTrans;

  while not qListaAfetadosPerc.Eof do
  begin
    valor := (qListaAfetadosPercvl_Beneficio.Value * (edtPercentual.Value / 100)) +
              qListaAfetadosPercvl_Beneficio.Value;

    qAtalizaValor.Close;
    qAtalizaValor.Parameters.ParamByName('nr_req').Value :=
      qListaAfetadosPercno_reqBeneficio.Value;
    qAtalizaValor.Parameters.ParamByName('valor').Value := valor;


    try
      qAtalizaValor.ExecSQL;
    except
      dm.ADOConn.RollbackTrans;
      showmessage('Erro ao atualizar valor'+#13+'Nenhum valor foi alterado!');
      exit;
    end;
    qListaAfetadosPerc.Next;
  end;

  dm.ADOConn.CommitTrans;
  showmessage('Opera��o realizada!');


end;

procedure Tfrm_calculaAumento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

end.
