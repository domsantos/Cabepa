unit Unit_CalculaPagBeneficio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, DB, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, ADODB;

type
  Tfrm_CalculaPagBeneficio = class(TForm)
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    dsListaContrbuintes: TDataSource;
    DBNavigator1: TDBNavigator;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtMes: TEdit;
    edtAno: TEdit;
    rbAbrangencia: TRadioGroup;
    Label4: TLabel;
    lblContribuinte: TLabel;
    edtContribuinte: TEdit;
    Label6: TLabel;
    Button1: TButton;
    qVerificaRef: TADOQuery;
    qVerificaRefqtd: TIntegerField;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtMesKeyPress(Sender: TObject; var Key: Char);
    procedure edtAnoKeyPress(Sender: TObject; var Key: Char);
    procedure edtContribuinteKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_CalculaPagBeneficio: Tfrm_CalculaPagBeneficio;

implementation

uses Unit_DM, Unit_PesquisaContribuinte;

{$R *.dfm}

procedure Tfrm_CalculaPagBeneficio.BitBtn1Click(Sender: TObject);
begin

  dm.qListaPagBeneficio.Open;
end;

procedure Tfrm_CalculaPagBeneficio.BitBtn2Click(Sender: TObject);
begin

  if edtMes.Text = '' then
  begin
    showmessage('Informe m�s de refer�ncia');
    edtMes.SetFocus;
    exit;
  end;

  if edtAno.Text = '' then
  begin
    showmessage('Informe ano de refer�ncia');
    edtAno.SetFocus;
    exit;
  end;

  qVerificaRef.Close;
  qVerificaRef.Parameters.ParamByName('ano').Value := edtAno.Text;
  qVerificaRef.Parameters.ParamByName('mes').Value := edtMes.Text;
  qVerificaRef.Open;

  if qVerificaRefqtd.Value > 0 then
  begin
    showmessage('J� existe folha em Pr�via ou Fechada para esta refer�ncia');
    exit;
  end;



  if rbAbrangencia.ItemIndex = 0 then
  begin

  if not dm.qListaPagBeneficio.Active then
  begin
    showmessage('Verifique os Beneficiarios');
    exit;
  end;

  dm.qListaPagBeneficio.First;

  while not dm.qListaPagBeneficio.Eof do
  begin
    dm.spCalculoBeneficioMes.Parameters.ParamByName('@codigo').Value :=
      dm.qListaPagBeneficiocd_cadastro.Value;
    dm.spCalculoBeneficioMes.Parameters.ParamByName('@mes').Value :=
      strtoint(edtMes.Text);
    dm.spCalculoBeneficioMes.Parameters.ParamByName('@ano').Value :=
      strtoint(edtAno.Text);

    try
      dm.spCalculoBeneficioMes.ExecProc;
    except
      on E:Exception do
      begin
        showmessage('Esso ao gerar pagamento para: '+
                dm.qListaPagBeneficionm_beneficiado.Value+
                'Erro: '+E.Message);
      end;
    end;
    dm.qListaPagBeneficio.Next;
  end;

  end
  else
  begin
    if edtContribuinte.Text = '' then
    begin
      showmessage('Informe Contribuinte');
      edtContribuinte.SetFocus;
      exit;
    end;

    dm.spCalculoBeneficioMes.Parameters.ParamByName('@codigo').Value :=
      strtoint(edtContribuinte.Text);
    dm.spCalculoBeneficioMes.Parameters.ParamByName('@mes').Value :=
      strtoint(edtMes.Text);
    dm.spCalculoBeneficioMes.Parameters.ParamByName('@ano').Value :=
      strtoint(edtAno.Text);

    try
      dm.spCalculoBeneficioMes.ExecProc;
    finally
      showmessage('Esso ao gerar pagamento para: '+lblContribuinte.Caption);
    end;
  end;

  showmessage('Opera��o realizada com sucesso!');


end;

procedure Tfrm_CalculaPagBeneficio.Button1Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
  frm_PesquisaContribuinte.origem := 8;
  frm_PesquisaContribuinte.Show;
end;

procedure Tfrm_CalculaPagBeneficio.edtAnoKeyPress(Sender: TObject;
  var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_CalculaPagBeneficio.edtContribuinteKeyPress(Sender: TObject;
  var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_CalculaPagBeneficio.edtMesKeyPress(Sender: TObject;
  var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;

end;

procedure Tfrm_CalculaPagBeneficio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.qListaPagBeneficio.Close;
  self := nil;
  Action := caFree;
end;

procedure Tfrm_CalculaPagBeneficio.FormKeyPress(Sender: TObject; var Key: Char);
begin
  If key = #13 then
  Begin
  Key:= #0;
  Perform(Wm_NextDlgCtl,0,0);
  end;
end;

procedure Tfrm_CalculaPagBeneficio.FormShow(Sender: TObject);
begin
  edtMes.SetFocus;
end;

end.
