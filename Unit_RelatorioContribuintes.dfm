inherited frm_RelatorioContribuintes: Tfrm_RelatorioContribuintes
  Left = 251
  Top = 134
  Caption = 'frm_RelatorioContribuintes'
  ClientWidth = 831
  ExplicitWidth = 839
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Margins.BottomMargin = 12.000000000000000000
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 123
        Caption = 'Listagem de Doa'#231#227'o'
        ExplicitWidth = 123
      end
    end
    inherited RLBand2: TRLBand
      Height = 8
      ExplicitHeight = 8
    end
    inherited RLBand4: TRLBand
      Top = 401
      Height = 27
      ExplicitTop = 401
      ExplicitHeight = 27
      inherited RLSystemInfo1: TRLSystemInfo
        Top = 7
        Width = 39
        ExplicitTop = 7
        ExplicitWidth = 39
      end
      inherited RLLabel5: TRLLabel
        Top = 7
        ExplicitTop = 7
      end
      inherited RLSystemInfo2: TRLSystemInfo
        Top = 7
        ExplicitTop = 7
      end
    end
    inherited RLBand3: TRLBand
      Top = 129
      Height = 248
      Borders.Sides = sdCustom
      Borders.DrawBottom = True
      ExplicitTop = 129
      ExplicitHeight = 248
      inherited RLDBText1: TRLDBText
        Left = 112
        Top = 6
        Width = 75
        DataField = 'cd_cadastro'
        DisplayMask = '#####'
        ExplicitLeft = 112
        ExplicitTop = 6
        ExplicitWidth = 75
      end
      inherited RLDBText2: TRLDBText
        Left = 248
        Top = 6
        Width = 65
        DataField = 'nm_pastor'
        ExplicitLeft = 248
        ExplicitTop = 6
        ExplicitWidth = 65
      end
      object RLLabel6: TRLLabel
        Left = 0
        Top = 6
        Width = 48
        Height = 16
        Caption = 'C'#243'digo:'
      end
      object RLLabel7: TRLLabel
        Left = 200
        Top = 6
        Width = 42
        Height = 16
        Caption = 'Nome:'
      end
      object RLLabel8: TRLLabel
        Left = 576
        Top = 6
        Width = 69
        Height = 16
        Caption = 'Data Nasc:'
      end
      object RLDBText3: TRLDBText
        Left = 648
        Top = 6
        Width = 86
        Height = 16
        DataField = 'dt_nascpastor'
        DataSource = dsRelatorio
      end
      object RLLabel9: TRLLabel
        Left = 0
        Top = 24
        Width = 97
        Height = 16
        Caption = 'Tipo Sanguineo:'
      end
      object RLLabel10: TRLLabel
        Left = 174
        Top = 24
        Width = 63
        Height = 16
        Caption = 'Identidade'
      end
      object RLLabel11: TRLLabel
        Left = 320
        Top = 24
        Width = 95
        Height = 16
        Caption = #211'rg'#227'o Emissor:'
      end
      object RLLabel12: TRLLabel
        Left = 552
        Top = 24
        Width = 90
        Height = 16
        Caption = 'Data Emiss'#227'o:'
      end
      object RLDBText4: TRLDBText
        Left = 100
        Top = 24
        Width = 81
        Height = 16
        DataField = 'tp_sanguineo'
        DataSource = dsRelatorio
      end
      object RLDBText5: TRLDBText
        Left = 240
        Top = 24
        Width = 74
        Height = 16
        DataField = 'no_regGeral'
        DataSource = dsRelatorio
      end
      object RLDBText6: TRLDBText
        Left = 416
        Top = 24
        Width = 114
        Height = 16
        DataField = 'ds_orgaoemissorrg'
        DataSource = dsRelatorio
      end
      object RLDBText7: TRLDBText
        Left = 648
        Top = 24
        Width = 71
        Height = 16
        DataField = 'dt_emissao'
        DataSource = dsRelatorio
      end
      object RLLabel13: TRLLabel
        Left = 0
        Top = 46
        Width = 34
        Height = 16
        Caption = 'CPF:'
      end
      object RLDBText8: TRLDBText
        Left = 36
        Top = 46
        Width = 42
        Height = 16
        DataField = 'no_cpf'
        DataSource = dsRelatorio
      end
      object RLLabel14: TRLLabel
        Left = 136
        Top = 46
        Width = 76
        Height = 16
        Caption = 'Estado Civil:'
      end
      object RLLabel15: TRLLabel
        Left = 320
        Top = 46
        Width = 89
        Height = 16
        Caption = 'Nacionalidade:'
      end
      object RLLabel16: TRLLabel
        Left = 536
        Top = 46
        Width = 80
        Height = 16
        Caption = 'Naturalidade:'
      end
      object RLDBText9: TRLDBText
        Left = 216
        Top = 46
        Width = 66
        Height = 16
        DataField = 'ds_estCivil'
        DataSource = dsRelatorio
      end
      object RLDBText10: TRLDBText
        Left = 410
        Top = 46
        Width = 84
        Height = 16
        DataField = 'ds_nacPastor'
        DataSource = dsRelatorio
      end
      object RLDBText11: TRLDBText
        Left = 619
        Top = 46
        Width = 81
        Height = 16
        DataField = 'ds_natPastor'
        DataSource = dsRelatorio
      end
      object RLLabel17: TRLLabel
        Left = 0
        Top = 65
        Width = 83
        Height = 16
        Caption = 'Escolaridade:'
      end
      object RLLabel18: TRLLabel
        Left = 216
        Top = 65
        Width = 63
        Height = 16
        Caption = 'Categoria:'
      end
      object RLLabel19: TRLLabel
        Left = 0
        Top = 84
        Width = 121
        Height = 16
        Caption = 'Forma'#231#227'o Teol'#243'gica'
      end
      object RLLabel20: TRLLabel
        Left = 544
        Top = 65
        Width = 80
        Height = 16
        Caption = 'Data Filia'#231#227'o'
      end
      object RLDBText12: TRLDBText
        Left = 88
        Top = 65
        Width = 84
        Height = 16
        DataField = 'ds_escPastor'
        DataSource = dsRelatorio
      end
      object RLDBText13: TRLDBText
        Left = 280
        Top = 65
        Width = 78
        Height = 16
        DataField = 'ds_categoria'
        DataSource = dsRelatorio
      end
      object RLDBText14: TRLDBText
        Left = 128
        Top = 84
        Width = 105
        Height = 16
        DataField = 'ds_formTeologica'
        DataSource = dsRelatorio
      end
      object RLDBText15: TRLDBText
        Left = 632
        Top = 65
        Width = 62
        Height = 16
        DataField = 'dt_filiacao'
        DataSource = dsRelatorio
      end
      object RLLabel21: TRLLabel
        Left = 0
        Top = 104
        Width = 33
        Height = 16
        Caption = 'M'#227'e:'
      end
      object RLLabel22: TRLLabel
        Left = 360
        Top = 104
        Width = 27
        Height = 16
        Caption = 'Pai:'
      end
      object RLLabel23: TRLLabel
        Left = 0
        Top = 124
        Width = 55
        Height = 16
        Caption = 'Conjuge:'
      end
      object RLLabel24: TRLLabel
        Left = 360
        Top = 124
        Width = 69
        Height = 16
        Caption = 'Data Nasc.'
      end
      object RLLabel25: TRLLabel
        Left = 528
        Top = 124
        Width = 106
        Height = 16
        Caption = 'Cert. Casamento:'
      end
      object RLDBText16: TRLDBText
        Left = 40
        Top = 104
        Width = 54
        Height = 16
        DataField = 'nm_mae'
        DataSource = dsRelatorio
      end
      object RLDBText17: TRLDBText
        Left = 392
        Top = 104
        Width = 46
        Height = 16
        DataField = 'nm_pai'
        DataSource = dsRelatorio
      end
      object RLDBText18: TRLDBText
        Left = 56
        Top = 124
        Width = 74
        Height = 16
        DataField = 'nm_conjuge'
        DataSource = dsRelatorio
      end
      object RLDBText19: TRLDBText
        Left = 432
        Top = 124
        Width = 97
        Height = 16
        DataField = 'dt_nascConjuge'
        DataSource = dsRelatorio
      end
      object RLDBText20: TRLDBText
        Left = 640
        Top = 124
        Width = 113
        Height = 16
        DataField = 'no_certCasamento'
        DataSource = dsRelatorio
      end
      object RLLabel26: TRLLabel
        Left = 0
        Top = 144
        Width = 63
        Height = 16
        Caption = 'Endere'#231'o:'
      end
      object RLLabel27: TRLLabel
        Left = 360
        Top = 144
        Width = 45
        Height = 16
        Caption = 'Compl:'
      end
      object RLLabel28: TRLLabel
        Left = 0
        Top = 164
        Width = 35
        Height = 16
        Caption = 'CEP:'
      end
      object RLLabel29: TRLLabel
        Left = 152
        Top = 164
        Width = 42
        Height = 16
        Caption = 'Bairro:'
      end
      object RLLabel30: TRLLabel
        Left = 328
        Top = 164
        Width = 48
        Height = 16
        Caption = 'Cidade:'
      end
      object RLLabel31: TRLLabel
        Left = 560
        Top = 164
        Width = 25
        Height = 16
        Caption = 'UF:'
      end
      object RLDBText21: TRLDBText
        Left = 72
        Top = 144
        Width = 78
        Height = 16
        DataField = 'ds_endereco'
        DataSource = dsRelatorio
      end
      object RLDBText22: TRLDBText
        Left = 409
        Top = 144
        Width = 118
        Height = 16
        DataField = 'ds_compEndPastor'
        DataSource = dsRelatorio
      end
      object RLDBText23: TRLDBText
        Left = 38
        Top = 164
        Width = 84
        Height = 16
        DataField = 'no_cepPastor'
        DataSource = dsRelatorio
      end
      object RLDBText24: TRLDBText
        Left = 200
        Top = 164
        Width = 57
        Height = 16
        DataField = 'ds_bairro'
        DataSource = dsRelatorio
      end
      object RLDBText25: TRLDBText
        Left = 384
        Top = 164
        Width = 63
        Height = 16
        DataField = 'ds_cidade'
        DataSource = dsRelatorio
      end
      object RLDBText26: TRLDBText
        Left = 592
        Top = 164
        Width = 35
        Height = 16
        DataField = 'ds_uf'
        DataSource = dsRelatorio
      end
      object RLLabel32: TRLLabel
        Left = 0
        Top = 184
        Width = 87
        Height = 16
        Caption = 'Data Batismo:'
      end
      object RLLabel33: TRLLabel
        Left = 176
        Top = 184
        Width = 91
        Height = 16
        Caption = 'Local Batismo:'
      end
      object RLLabel34: TRLLabel
        Left = 464
        Top = 184
        Width = 140
        Height = 16
        Caption = 'Data Autor. Evangelizar'
      end
      object RLDBText27: TRLDBText
        Left = 88
        Top = 184
        Width = 68
        Height = 16
        DataField = 'dt_batismo'
        DataSource = dsRelatorio
      end
      object RLDBText28: TRLDBText
        Left = 272
        Top = 184
        Width = 100
        Height = 16
        DataField = 'ds_localBatismo'
        DataSource = dsRelatorio
      end
      object RLDBText29: TRLDBText
        Left = 608
        Top = 184
        Width = 106
        Height = 16
        DataField = 'dt_autEvangelista'
        DataSource = dsRelatorio
      end
      object RLLabel35: TRLLabel
        Left = 0
        Top = 204
        Width = 154
        Height = 16
        Caption = 'Consagra'#231#227'o Evangelista:'
      end
      object RLLabel36: TRLLabel
        Left = 232
        Top = 204
        Width = 113
        Height = 16
        Caption = 'Ordena'#231#227'o Pastor:'
      end
      object RLLabel37: TRLLabel
        Left = 424
        Top = 204
        Width = 119
        Height = 16
        Caption = 'Local Consagra'#231#227'o:'
      end
      object RLDBText30: TRLDBText
        Left = 160
        Top = 204
        Width = 130
        Height = 16
        DataField = 'dt_consagEvangelista'
        DataSource = dsRelatorio
      end
      object RLDBText31: TRLDBText
        Left = 344
        Top = 204
        Width = 106
        Height = 16
        DataField = 'dt_ordenacPastor'
        DataSource = dsRelatorio
      end
      object RLDBText32: TRLDBText
        Left = 546
        Top = 204
        Width = 128
        Height = 16
        DataField = 'ds_localConsagracao'
        DataSource = dsRelatorio
      end
      object RLLabel38: TRLLabel
        Left = 0
        Top = 224
        Width = 49
        Height = 16
        Caption = 'Campo:'
      end
      object RLLabel39: TRLLabel
        Left = 328
        Top = 224
        Width = 67
        Height = 16
        Caption = 'Supervis'#227'o'
      end
      object RLDBText33: TRLDBText
        Left = 56
        Top = 224
        Width = 64
        Height = 16
        DataField = 'ds_campo'
        DataSource = dsRelatorio
      end
      object RLDBText34: TRLDBText
        Left = 400
        Top = 224
        Width = 86
        Height = 16
        DataField = 'ds_supervisao'
        DataSource = dsRelatorio
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 377
      Width = 718
      Height = 24
      BandType = btSummary
      object sumario: TRLSystemInfo
        Left = 512
        Top = 8
        Width = 79
        Height = 16
        Info = itDetailCount
      end
      object RLLabel40: TRLLabel
        Left = 360
        Top = 8
        Width = 151
        Height = 16
        Caption = 'Numero de Contribuintes:'
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = qRelatorio
    Left = 640
    Top = 56
  end
  object qRelatorio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT c.cd_cadastro, c.cd_formTeologica, c.cd_natPastor, c.cd_e' +
        'scPastor, c.cd_nacPastor, '
      
        #9#9#9' c.cd_estCivil, c.cd_categoria, c.nm_pastor, c.no_regConv, c.' +
        'dt_nascPastor, '
      
        #9#9#9' c.tp_sanguineo, c.no_regGeral, c.no_cpf, c.ds_endereco, c.ds' +
        '_compEndPastor, '
      
        #9#9#9' c.no_cepPastor, c.dt_filiacao, c.nm_pai, c.nm_mae, c.nm_conj' +
        'uge, c.dt_nascConjuge, '
      
        #9#9#9' c.no_fone, c.dt_batismo, c.ds_localBatismo, c.dt_autEvangeli' +
        'sta, '
      
        #9#9#9' c.dt_consagEvangelista, c.dt_ordenacPastor, c.ds_localConsag' +
        'racao, '
      
        #9#9#9' c.ds_campo, c.ds_supervisao, c.no_certCasamento, c.ds_orgaoe' +
        'missorrg, '
      
        #9#9#9' c.dt_emissao, c.ds_bairro, c.ds_uf, c.ds_cidade, ct.ds_categ' +
        'oria,'
      
        #9#9#9' e.ds_escPastor,ec.ds_estCivil,f.ds_formTeologica,nc.ds_nacPa' +
        'stor,nt.ds_natPastor'
      'FROM tbContribuinte c'
      'inner join tbcategoria ct on c.cd_categoria = ct.cd_categoria'
      'inner join tbescolaridade e on c.cd_escPastor = e.cd_escPastor'
      'inner join tbestcivil ec on c.cd_estCivil = ec.cd_estCivil'
      
        'inner join tbFormTeologica f on c.cd_formTeologica = f.cd_formTe' +
        'ologica'
      
        'inner join tbNacionalidade nc on c.cd_nacPastor = nc.cd_nacPasto' +
        'r'
      'inner join tbNaturalidade nt on c.cd_natPastor = nt.cd_natPastor'
      'order by c.nm_pastor')
    Left = 582
    Top = 49
  end
end
