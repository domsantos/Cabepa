object frm_ListaSolicitacoesBeneficios: Tfrm_ListaSolicitacoesBeneficios
  Left = 0
  Top = 0
  Caption = 'Lista de Contribuintes a Jubilar'
  ClientHeight = 379
  ClientWidth = 648
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 648
    Height = 49
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 68
      Height = 16
      Caption = 'No. CABEPA'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 432
      Top = 22
      Width = 61
      Height = 13
      Caption = 'Selecionado:'
    end
    object lblNome: TLabel
      Left = 499
      Top = 22
      Width = 3
      Height = 13
    end
    object BitBtn1: TBitBtn
      Left = 247
      Top = 13
      Width = 169
      Height = 30
      Caption = 'Registrar Jubilado'
      TabOrder = 0
      OnClick = BitBtn1Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FFF7FFF7FFF7FFF7FFF7F7A6BD3528C3E8836A736A7368836
        8C3ED3527A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73B14A8736
        C53AC53AA536A532A532A536C53AC53A8736B14A9C73FF7FFF7FFF7FFF7FFF7F
        FF7FFF7F59676A3AC53AA536652A252625222522252225222526652AA536C53A
        6A3A5967FF7FFF7FFF7FFF7FFF7F38638836E53A652A2526452645264526452A
        452A4526452625262526652AC53A68363863FF7FFF7FFF7F9C6F4936C536452A
        4526452A652A652A44264526652A652A652A452A452A45264526C53649369C6F
        FF7FFF7FB14AA536652A452A652A652E652E43268B3AD34E662A652E652E652E
        652A652A4526652AA532B14AFF7F7A6B8732852E452A652E652E852E63268A36
        7A6B9E77AC3A642A8532852E652E652E652E452A852E67327A6BB34EA52E652E
        652E852E8532632A8B367B6BBD779D77385F652A843285328532852E652E652E
        652A852EB34E6C3A8532652E85328532632A8B3A7B6FBD77BD779D779E77D046
        632AA536853285328532652E652E852E6C3A6932853285328532842EAD3E7C6F
        BE7BBD777A679C73BE7B7B6B8836A332A536A53685328532652E852E69326832
        A736A636A536652E385FDF7FDF7B355B662E355BDE7BDF7B1557842EC436A536
        A536A532A636A736682E8932C93EC83EC83EA636EF4ABE7B15578532A232A93A
        9D73DE7BBE77CD42A332C63EC73EC83EC83AC93A69326A32EA42E942E946E946
        CA3ED046C93AE742E842C63AF24EDF7FDF7F7B6BAA3AE842EA46E942C942EA42
        6A326D3A0B470B4B0B4B0B4B0A47C83E0A4B0B4B0B4B2A4BC93E7967FF7FFF7F
        365BC93A0A4BEB46EB460B476D3AB34E0C472C4F0C4B0C4F2C4F2C4F2C4F2C4F
        2C4F2C4F0A4BCE46BD73FF7FFF7F1353EA420C4B0C4FEC46B34E7A6BAD3E4F57
        2D4F2D4F2D532D532D532D532D532D534D57EB46124FFF7FFF7F1453EB422D4F
        4F57AD3E7A6BFF7FB24A30534F5B2E534E574E574E574E574E574E574E574E57
        EC4635571457ED4A2E5350573053B24AFF7FFF7F9B6F6D3A9363505B4F575057
        4F5B4F5B4F5B6F5B4F5B705B6F5BED460D4B4F57715B935F6D3A9B6FFF7FFF7F
        FF7F3863AE42B5679363515B505B715B715F715F715F715F715B705B515B9363
        B567AE423863FF7FFF7FFF7FFF7FFF7F58638F42755FB66F94677263725F725F
        725F725F72639467B66F755F8F425863FF7FFF7FFF7FFF7FFF7FFF7FFF7F9C6F
        B24AF14A9663B86FD86FB76FB76FB86FB86F9663D14AB24A9C6FFF7FFF7FFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7F7A6BD452B146D14AF24EF24ED14AB146D452
        7A6BFF7FFF7FFF7FFF7FFF7FFF7F}
    end
    object edtCabepa: TEdit
      Left = 89
      Top = 13
      Width = 56
      Height = 27
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnKeyPress = edtCabepaKeyPress
    end
    object BitBtn2: TBitBtn
      Left = 151
      Top = 13
      Width = 90
      Height = 30
      Caption = 'Buscar'
      TabOrder = 2
      OnClick = BitBtn2Click
      Glyph.Data = {
        0A040000424D0A04000000000000420000002800000016000000160000000100
        100003000000C8030000120B0000120B00000000000000000000007C0000E003
        00001F000000FF7FDE7BDE7BBD77BD779C739C739C737C6F7C6F7C6F7C6F7C6F
        9C739C739C73BD77BD77BD77DE7BFF7FFF7FFF7F9C73B5560F42EF41EF419256
        925A91567156715671567156915691569156925A925AF6627B737C6B9D6BFF7F
        FF7F4A2D410C410C62104B5A6C626B626B626B5E4B5E4B5E4B5E4B5E4A5E4A5E
        4A5EB0663A5F151A982EFF7FFF7F29296210621083144C5E8C626C626C626C62
        6C5E6C5E6C5E6B5E4B5E4B5E4A624F4A351E5726FB42FF7FFF7F492962106210
        83144C5E8D668C626C626C626C626C626B626B626B5E6B624D52141E5722DB36
        BE6FFF7FFF7F492D6210621083146C5E8D668D628D628C626C626C628F5EB35A
        B35A905E332A371ABA329D67FF7FFF7FFF7F492D83148210A3186C5E8D668D66
        8C626C628E66D45EF75AF65AD65AF75AD752B9361057DC7BFF7FFF7FFF7F6A31
        A3188314A41C6D62AD668C66D16A9A7B5A6BF75E596BBC777873F466F862F562
        2673DB7FFF7FFF7FFF7F6A31A418A318C41C6D628D66136FDD7F146F18631663
        356F5673BC7B9977F4661963546BFF7FFF7FFF7FFF7F8A35C41CA418E4208D62
        CF6ABC7BF26E577339635A6FDD7B9B7757735773146B3967B946DE77FF7FFF7F
        FF7F8B35C520C41CE5248D62126F99771473DE7B39677C6FDE7F7877DD7B7777
        156B5A6B783ABE73FF7FFF7FFF7FAB35E520E52005258D62347379773573BC7B
        166B5B6BBD7757739B7B9A73386B5B67362ABE73FF7FFF7FFF7FAB390525E520
        06298D66F26E9B7BD06ADE7FF26E586F7B6B596B386B7B6F7B73B35A1622BE73
        FF7FFF7FFF7FAC39062506252629AE66CF6EBB7B126F3573DC7B9A777B73596F
        9C73596FD06A8F5ED94EFF7FFF7FFF7FFF7FCC3D26290625272DAE66CE6EF16E
        DD7B136FD06AF06EAE6A346FDD7BCF6AAD6A4C666B5DDE7BFF7FFF7FFF7FCC3D
        27292629472DAE66CF6ECE6AF16E9A779B7B9A77BB7B9A77F16EAD6ACF6A4C66
        83547B7BFF7FFF7FFF7FCD3D272D2729472DAE6ACF6ECF6ECE6ECE6AF06EF16E
        D06EAE6AAE6ACE6ACF6A4C6683547B7BFF7FFF7FFF7FED41472D272D4831AF6A
        EF6ECF6ECF6ECF6ECF6ECE6ECE6ECF6ECF6ECF6ACF6E4C6683507B7BFF7FFF7F
        FF7FED41472D472D6831CF6AEF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6ECF6E
        CF6E6D6A6B61BD7BFF7FFF7FFF7FED41482D472D6831CF6AEF6ECF6ECF6ECF6E
        CF6ECF6ECF6ECF6ECF6ECF6ECF6E126FFF7FFF7FFF7FFF7FFF7FED4548314831
        6835CF6AF072F06EF06EF06EF06EF06EF06ED06ECF6ECF6ECF6E1273FF7FFF7F
        FF7FFF7FFF7FF6622F4A304A504E357355775577557755775577557755775577
        557755773573997BFF7FFF7FFF7F}
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 49
    Width = 648
    Height = 330
    Align = alClient
    DataSource = dsListaJubilacao
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
  end
  object dsListaJubilacao: TDataSource
    DataSet = qListaJubilacao
    Left = 568
    Top = 120
  end
  object qListaJubilacao: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [cd_cadastro]'
      '      ,[nm_pastor]'
      '      ,[dt_nascPastor]'
      '      ,[no_cpf]'
      '      ,[nm_conjuge]'
      '      ,[dt_nascConjuge]'
      '  FROM [tbContribuinte]'
      '  WHERE '
      '      [cd_sitJubilamento] = 0'
      '  ORDER BY '
      '      [nm_pastor]'
      '      ,[nm_conjuge]')
    Left = 568
    Top = 176
    object qListaJubilacaocd_cadastro: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'cd_cadastro'
    end
    object qListaJubilacaonm_pastor: TStringField
      DisplayLabel = 'Contribuinte'
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qListaJubilacaodt_nascPastor: TDateTimeField
      DisplayLabel = 'Data Nasc.'
      FieldName = 'dt_nascPastor'
    end
    object qListaJubilacaono_cpf: TStringField
      DisplayLabel = 'CPF'
      FieldName = 'no_cpf'
      FixedChar = True
      Size = 14
    end
    object qListaJubilacaonm_conjuge: TStringField
      DisplayLabel = 'Conjuge'
      FieldName = 'nm_conjuge'
      Size = 60
    end
    object qListaJubilacaodt_nascConjuge: TDateTimeField
      DisplayLabel = 'Data Nasc.'
      FieldName = 'dt_nascConjuge'
    end
  end
  object qSpInsereBeneficio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'tipo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 2
      end
      item
        Name = 'valor'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = 10000c
      end
      item
        Name = 'tpJubilacao'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE'#9'@return_value int,'
      #9#9'@resultado bit'
      ''
      'EXEC'#9'@return_value = [dbo].[sp_InserePedidoBeneficio]'
      #9#9'@vCodigo = :codigo,'
      #9#9'@vTipo = :tipo,'
      #9#9'@vValor = :valor,'
      '                                @vTipoJubilacao = :tpJubilacao,'
      #9#9'@resultado = @resultado OUTPUT'
      ''
      'SELECT'#9'@resultado as N'#39'resultado'#39
      ''
      '')
    Left = 568
    Top = 240
    object qSpInsereBeneficioresultado: TBooleanField
      FieldName = 'resultado'
      ReadOnly = True
    end
  end
end
