unit Unit_FormTeologica;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, FMTBcd, ActnList, Provider, DBClient, DB, SqlExpr,
  StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, ADODB, Grids, DBGrids;

type
  Tfrm_FormTeologica = class(Tfrm_PadraoCadastro)
    QPrincipalcd_formteologica: TIntegerField;
    QPrincipalds_formteologica: TStringField;
    CDSPrincipalcd_formteologica: TIntegerField;
    CDSPrincipalds_formteologica: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnRelatorioClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_FormTeologica: Tfrm_FormTeologica;

implementation

uses Unit_PesquisaFormTeologica, Unit_RelatorioFormTeologica, Unit_DM,
  Unit_Nacionalidade;

{$R *.dfm}

procedure Tfrm_FormTeologica.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Open;
end;

procedure Tfrm_FormTeologica.btnRelatorioClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(Tfrm_RelatorioFormTeologica,frm_RelatorioFormTeologica);
  frm_RelatorioFormTeologica.dsRelatorio.DataSet := CDSPrincipal;
  CDSPrincipal.Open;

  frm_RelatorioFormTeologica.RLReport1.Preview();
end;

procedure Tfrm_FormTeologica.btnNovoClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

end.

