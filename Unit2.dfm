inherited frm_RelSimulaAumentoPercentual: Tfrm_RelSimulaAumentoPercentual
  Caption = 'frm_RelSimulaAumentoPercentual'
  ExplicitWidth = 870
  ExplicitHeight = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 227
        Caption = 'Simula'#231#227'o de Aumento por Percentual'
        ExplicitWidth = 227
      end
      object RLLabel6: TRLLabel
        Left = 96
        Top = 54
        Width = 121
        Height = 16
        Caption = 'Valor de Refer'#234'ncia:'
      end
      object RLLabel7: TRLLabel
        Left = 416
        Top = 52
        Width = 70
        Height = 16
        Caption = 'Percentual:'
      end
    end
    inherited RLBand2: TRLBand
      inherited RLLabel3: TRLLabel
        Width = 91
        Caption = 'BENEFICIADO'
        ExplicitWidth = 91
      end
      inherited RLLabel4: TRLLabel
        Left = 403
        Top = 28
        Width = 93
        Caption = 'VALOR ATUAL'
        ExplicitLeft = 403
        ExplicitTop = 28
        ExplicitWidth = 93
      end
      object RLLabel8: TRLLabel
        Left = 565
        Top = 28
        Width = 150
        Height = 16
        Caption = 'VALOR COM AUMENTO'
      end
    end
    inherited RLBand3: TRLBand
      inherited RLDBText1: TRLDBText
        Width = 65
        DataField = 'nm_pastor'
        ExplicitWidth = 65
      end
      inherited RLDBText2: TRLDBText
        Left = 376
        Top = 3
        Width = 121
        Height = 15
        Alignment = taRightJustify
        DataField = 'vl_Beneficio'
        DisplayMask = 'R$ #,##0.00'
        ExplicitLeft = 376
        ExplicitTop = 3
        ExplicitWidth = 121
        ExplicitHeight = 15
      end
      object RLDBText3: TRLDBText
        Left = 565
        Top = 3
        Width = 124
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_aumento'
        DataSource = dsRelatorio
        DisplayMask = 'R$ #,##0.00'
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = QspSimulaAumento
  end
  object QspSimulaAumento: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'inicio'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = Null
      end
      item
        Name = 'fim'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 19
        Size = 8
        Value = Null
      end
      item
        Name = 'percentual'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 6
        Size = 19
        Value = Null
      end>
    SQL.Strings = (
      'EXEC'#9'[dbo].[sp_simulaAumento]'
      #9#9'@valor_inicio = :inicio,'
      #9#9'@valor_final = :fim,'
      #9#9'@percentual = :percentual')
    Left = 560
    Top = 272
    object QspSimulaAumentovl_aumento: TBCDField
      FieldName = 'vl_aumento'
      Precision = 19
    end
    object QspSimulaAumentonr_requerimento: TIntegerField
      FieldName = 'nr_requerimento'
    end
    object QspSimulaAumentovl_Beneficio: TBCDField
      FieldName = 'vl_Beneficio'
      Precision = 19
    end
    object QspSimulaAumentonm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
  end
end
