unit Unit_Naturalidade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, FMTBcd, ActnList, Provider, DBClient, DB, SqlExpr,
  StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, ADODB, Grids, DBGrids;

type
  Tfrm_Naturalidade = class(Tfrm_PadraoCadastro)
    QPrincipalcd_natpastor: TIntegerField;
    QPrincipalds_natpastor: TStringField;
    CDSPrincipalcd_natpastor: TIntegerField;
    CDSPrincipalds_natpastor: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnRelatorioClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Naturalidade: Tfrm_Naturalidade;

implementation

uses Unit_PesquisaNaturalidade, Unit_RelatorioNaturalidade, Unit_DM;

{$R *.dfm}

procedure Tfrm_Naturalidade.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Open;
end;

procedure Tfrm_Naturalidade.btnRelatorioClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(Tfrm_RelatorioNaturalidade,frm_RelatorioNaturalidade);
  frm_RelatorioNaturalidade.dsRelatorio.DataSet := CDSPrincipal;
  CDSPrincipal.Open;
  frm_RelatorioNaturalidade.RLReport1.Preview();
end;

procedure Tfrm_Naturalidade.btnNovoClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

end.
