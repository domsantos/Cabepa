inherited frm_SalMinimo: Tfrm_SalMinimo
  Caption = 'Sal'#225'rio M'#237'nimo'
  ExplicitWidth = 728
  ExplicitHeight = 513
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel1: TPanel
    inherited btnNovo: TBitBtn
      Left = 3
      Top = 5
      ExplicitLeft = 3
      ExplicitTop = 5
    end
  end
  inherited Panel2: TPanel
    ExplicitTop = 44
    object Label1: TLabel
      Left = 80
      Top = 6
      Width = 19
      Height = 16
      Caption = 'Ano'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 15
      Top = 6
      Width = 20
      Height = 16
      Caption = 'M'#234's'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 142
      Top = 6
      Width = 25
      Height = 16
      Caption = 'Valor'
      FocusControl = DBEdit3
    end
    object DBEdit1: TDBEdit
      Left = 80
      Top = 22
      Width = 57
      Height = 24
      DataField = 'dt_anoSalMin'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit2: TDBEdit
      Left = 15
      Top = 22
      Width = 57
      Height = 24
      DataField = 'dt_mesSalMin'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit3: TDBEdit
      Left = 142
      Top = 22
      Width = 105
      Height = 24
      DataField = 'vl_salMin'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 52
      Width = 361
      Height = 301
      DataSource = DataSource1
      TabOrder = 3
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited ActionList1: TActionList
    Top = 241
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT [dt_anoSalMin]'
      '      ,[dt_mesSalMin]'
      '      ,[vl_salMin]'
      '  FROM [tbSalarioMinimo]'
      'ORDER BY [vl_salMin] desc')
    object QPrincipaldt_anoSalMin: TSmallintField
      DisplayLabel = 'Ano'
      FieldName = 'dt_anoSalMin'
    end
    object QPrincipaldt_mesSalMin: TSmallintField
      DisplayLabel = 'M'#234's'
      FieldName = 'dt_mesSalMin'
    end
    object QPrincipalvl_salMin: TBCDField
      DisplayLabel = 'Valor'
      FieldName = 'vl_salMin'
      currency = True
      Precision = 19
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 536
    Top = 64
  end
end
