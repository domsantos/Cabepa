unit Unit_RegistraPagamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Mask, DBCtrls, DB, ADODB, Buttons, Grids,
  DBGrids, ActnList, rxToolEdit, rxCurrEdit;

type
  Tfrm_RegistraPagamento = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Button1: TButton;
    Label2: TLabel;
    Panel3: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    qBuscaContribuinte: TADOQuery;
    qBuscaContribuintenm_pastor: TStringField;
    QFormaPagamento: TADOQuery;
    QPagamento: TADOQuery;
    edtContribuinte: TEdit;
    edtMes: TEdit;
    edtAno: TEdit;
    QFormaPagamentocd_formpagto: TSmallintField;
    QFormaPagamentods_formpagto: TStringField;
    DBGrid1: TDBGrid;
    Panel2: TPanel;
    Label7: TLabel;
    ComboBox1: TComboBox;
    Label6: TLabel;
    edtNomeBanco: TEdit;
    edtAgencia: TEdit;
    edtNumCheque: TEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    edtDataResg: TMaskEdit;
    Label11: TLabel;
    Button2: TButton;
    Button3: TButton;
    QListaPagamentos: TADOQuery;
    dsListaPagamento: TDataSource;
    ActionList1: TActionList;
    QInsert: TADOQuery;
    QPagamentovl_ContribuicaoVencimento: TBCDField;
    QListaPagamentosid_PagtoContribuincao: TAutoIncField;
    QListaPagamentosvl_pagtoContribuicao: TBCDField;
    QListaPagamentosdt_pagtoContribuicao: TDateTimeField;
    QListaPagamentosds_bancoCheque: TStringField;
    QListaPagamentosno_chequePagto: TStringField;
    QListaPagamentosdt_resgChequePre: TDateTimeField;
    QListaPagamentoscd_formPagto: TSmallintField;
    QListaPagamentosdt_anoRefContribuicao: TIntegerField;
    QListaPagamentosdt_mesRefContribuicao: TIntegerField;
    QListaPagamentosds_formPagto: TStringField;
    Label12: TLabel;
    Label13: TLabel;
    btnGravar: TButton;
    acNovo: TAction;
    acFechar: TAction;
    edtTipoContribuicao: TEdit;
    lblTipoContrib: TLabel;
    qTipoContribuicao: TADOQuery;
    qTipoContribuicaonm_tipoContribuicao: TStringField;
    QListaPagamentosds_agenciaCheque: TStringField;
    Label14: TLabel;
    qValorContribuicao: TADOQuery;
    qValorContribuicaovalor: TBCDField;
    vlContribuicao: TCurrencyEdit;
    valorPagt: TCurrencyEdit;
    dtRetroativa: TMaskEdit;
    ck_Retroativo: TCheckBox;
    QListaPagamentoscd_cadastro: TIntegerField;
    procedure Button1Click(Sender: TObject);
    procedure edtContribuinteExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnGravarClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure setAtributosEdits;
    procedure Button2Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure acNovoExecute(Sender: TObject);
    procedure edtMesKeyPress(Sender: TObject; var Key: Char);
    procedure edtContribuinteKeyPress(Sender: TObject; var Key: Char);
    procedure edtAnoKeyPress(Sender: TObject; var Key: Char);
    procedure edtAgenciaKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumChequeKeyPress(Sender: TObject; var Key: Char);
    procedure acFecharExecute(Sender: TObject);
    procedure edtTipoContribuicaoExit(Sender: TObject);
    procedure edtTipoContribuicaoKeyPress(Sender: TObject; var Key: Char);
    procedure edtMesExit(Sender: TObject);
    procedure limpaPagamento;
    procedure ck_RetroativoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    id : Smallint;
    
  end;

var
  frm_RegistraPagamento: Tfrm_RegistraPagamento;

implementation

uses Unit_DM, Unit_PesquisaContribuinte;

{$R *.dfm}

procedure Tfrm_RegistraPagamento.Button1Click(Sender: TObject);
begin
//  if edtContribuinte.Text <> '' then
  begin
    Label2.Caption := '';
    Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
    frm_PesquisaContribuinte.origem := 2;
    frm_PesquisaContribuinte.Show;
  end;
end;

procedure Tfrm_RegistraPagamento.edtContribuinteExit(Sender: TObject);
begin

  if edtContribuinte.Text <> '' then
  begin


    qBuscaContribuinte.Close;
    qBuscaContribuinte.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    qBuscaContribuinte.Open;

    if qBuscaContribuinte.RecordCount = 0 then
    begin
      showmessage('Contribuinte n�o encontrado');
      edtContribuinte.SetFocus;
    end
    else
      Label2.Caption := qBuscaContribuintenm_pastor.Value;
  end;
end;



procedure Tfrm_RegistraPagamento.FormShow(Sender: TObject);
var
  i:Smallint;
begin
  QFormaPagamento.Close;
  QFormaPagamento.SQL.Add('select cd_formpagto,ds_formpagto ');
  QFormaPagamento.SQL.Add('from tbFormaPagto ');
  QFormaPagamento.SQL.Add('order by ds_formpagto');
  QFormaPagamento.Open;
  QFormaPagamento.First;
  for i := 0 to QFormaPagamento.RecordCount -1 do
  begin
    ComboBox1.Items.Add(QFormaPagamentods_formpagto.Value);
    QFormaPagamento.Next;
  end;
  QFormaPagamento.close;
  edtContribuinte.SetFocus;
  edtAno.Text := FormatDateTime('YYYY',date);
end;

procedure Tfrm_RegistraPagamento.limpaPagamento;
begin
  edtContribuinte.Text := '';
  edtTipoContribuicao.Text := '';
  edtMes.Text := '';
  edtAno.Text := '';
  vlContribuicao.Clear;
  Label13.Caption := '';
  QListaPagamentos.Close;
  edtContribuinte.SetFocus;

end;

procedure Tfrm_RegistraPagamento.ComboBox1Change(Sender: TObject);
var
  opcao : string;
begin
  opcao := ComboBox1.Items.ValueFromIndex[ComboBox1.ItemIndex];
  QFormaPagamento.Close;
  QFormaPagamento.SQL.Clear;
  QFormaPagamento.SQL.Add('select cd_formpagto,ds_formpagto ');
  QFormaPagamento.SQL.Add('from tbFormaPagto where ds_formpagto like ''%'+opcao+'%''');
  QFormaPagamento.Open;

  id := QFormaPagamento.Fields.Fields[0].Value;
  if (id = 1) or (id = 4) then setAtributosEdits;
  if id = 2 then
  begin
    edtNomeBanco.Enabled := true;
    edtAgencia.Enabled := true;
    edtNumCheque.Enabled := true;
    edtDataResg.Text := datetostr(date);
  end;
  if id = 3 then
  begin
    edtNomeBanco.Enabled := true;
    edtAgencia.Enabled := true;
    edtNumCheque.Enabled := true;
    edtDataResg.Enabled := true;
  end


end;

procedure Tfrm_RegistraPagamento.BitBtn2Click(Sender: TObject);
begin
{
  Edit5.Text := '';
  Edit6.Text := '';
  Edit7.Text := '';
  MaskEdit1.Text := '';
  Panel2.Visible := false;
}
end;

procedure Tfrm_RegistraPagamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

procedure Tfrm_RegistraPagamento.btnGravarClick(Sender: TObject);
var
  valor,esperado : double;
begin

    if vlContribuicao.Value = 0 then
    begin
      showmessage('Informe um valor para a contribui��o!');
      vlContribuicao.SetFocus;
      exit;
    end;


    // Verifica se ja ha contribui��o para esta referencia
    valor := 0;
    QPagamento.Close;
    QPagamento.Parameters.ParamByName('codigo').Value := edtContribuinte.Text;
    QPagamento.Parameters.ParamByName('mes').Value := edtMes.Text;
    QPagamento.Parameters.ParamByName('ano').Value := edtAno.Text;
    QPagamento.Parameters.ParamByName('tipo').Value := edtTipoContribuicao.Text;

    QPagamento.Open;
    //howmessage(inttostr(QPagamento.RecordCount));

    if QPagamento.RecordCount > 0 then
    begin
      vlContribuicao.Value := QPagamentovl_ContribuicaoVencimento.Value;
      showmessage('Pagamento j� identificado para esta referencia!');
      QListaPagamentos.Close;
      QListaPagamentos.Parameters.ParamByName('codigo').Value := edtContribuinte.Text;
      QListaPagamentos.Parameters.ParamByName('mes').Value := edtMes.Text;
      QListaPagamentos.Parameters.ParamByName('ano').Value := edtAno.Text;
      QListaPagamentos.Parameters.ParamByName('tipo').Value := edtTipoContribuicao.Text;
      QListaPagamentos.Open;

      QListaPagamentos.First;
      valor := 0;
      while not QListaPagamentos.Eof do
      begin
        valor := valor + QListaPagamentosvl_pagtoContribuicao.Value;
        QListaPagamentos.Next;
      end;
      Label13.Caption := floattostr(valor);
      esperado := QPagamentovl_ContribuicaoVencimento.Value;
      if valor < esperado then
      begin
        if MessageDlg('Valor pago menor que esperado. Deseja inserir novo pagamento?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          //Panel1.Enabled := false;
          Panel2.Visible := true;
          ComboBox1.SetFocus;
          valorPagt.Value := vlContribuicao.Value - valor;
        end;
      end
      else
      begin
        if MessageDlg('Valor j� alcan�ado para esta refer�ncia. Imprimir boleto?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          DM.imprimeBoleto(strtoint(edtContribuinte.Text),strtoint(edtMes.Text),strtoint(edtAno.Text));
          showmessage('Impress�o Finalizada');
        end
        else
        begin
          limpaPagamento;
        end;

      end

        //showmessage('Valor j� alcan�ado para esta refer�ncia!');
    end

    else
    begin
       // inserir contribuicao
      with QInsert do
      begin
        close;
        sql.Clear;

        sql.Add('INSERT INTO [tbContribuicoes] ([cd_cadastro],[dt_anoRefContribuicao]');
        sql.Add(',[dt_mesRefContribuicao],[vl_ContribuicaoVencimento]');
        sql.Add(',[cd_tipoContribuicao],[cd_sitQuitacao]');
        sql.Add(',[dt_Contribuicao] ,[id_usuario])');
        sql.Add('VALUES ( :cd_cadastro, :dt_anoRefContribuicao, :dt_mesRefContribuicao');
        sql.Add(', :vl_ContribuicaoVencimento , :cd_tipoContribuicao');
        sql.Add(', :cd_sitQuitacao , :dt_Contribuicao, :id_usuario)');

        QListaPagamentos.Close;

        Parameters.ParamByName('cd_cadastro').Value := strtoint(edtContribuinte.Text);
        Parameters.ParamByName('dt_anoRefContribuicao').Value := strtoint(edtAno.Text);
        Parameters.ParamByName('dt_mesRefContribuicao').Value := strtoint(edtMes.Text);
        Parameters.ParamByName('vl_ContribuicaoVencimento').Value := vlContribuicao.Value;
        Parameters.ParamByName('cd_tipocontribuicao').Value := strtoint(edtTipoContribuicao.Text);
        Parameters.ParamByName('id_usuario').Value := dm.idLogin;

        if ck_Retroativo.Checked then
          Parameters.ParamByName('dt_Contribuicao').Value := StrToDate(dtRetroativa.Text)
        else
          Parameters.ParamByName('dt_Contribuicao').Value := now;
        begin

        end;

        //showmessage(sql.GetText);

        try
          ExecSQL;
          Panel2.Visible := true;
          //Panel2.Height := 270;
          //ComboBox1.ItemIndex := 0;
          ComboBox1.SetFocus;
          valorPagt.Value := vlContribuicao.Value;
          ck_Retroativo.Checked := false;

        except
          showmessage('Erro ao registrar contribui��o');
        end;
      end;
    end;


end;

procedure Tfrm_RegistraPagamento.ck_RetroativoClick(Sender: TObject);
begin
  if ck_Retroativo.Checked then
    dtRetroativa.Enabled := true
  else
    dtRetroativa.Enabled := false;

end;

procedure Tfrm_RegistraPagamento.Button3Click(Sender: TObject);
begin
  acNovo.Execute;
  setAtributosEdits;
  Panel2.Visible := false;
end;

procedure Tfrm_RegistraPagamento.setAtributosEdits;
begin
  edtNomeBanco.Enabled := false;
  edtAgencia.Enabled := false;
  edtNumCheque.Enabled := false;
  edtDataResg.Enabled := false;
end;

procedure Tfrm_RegistraPagamento.Button2Click(Sender: TObject);
var
  valor : double;
  pgtoCheque : Boolean;
  datapgto : TDate;
begin
  // Gravando dados

  //dm.ADOConn.BeginTrans;
  pgtoCheque := false;

  if ck_Retroativo.Checked then
    datapgto := StrToDate(dtRetroativa.Text)
  else
    datapgto := Now;

  with QInsert do
  begin
    Close;
    SQL.Clear;
    if (id = 1) or (id = 4)  then
    begin
      SQL.Add('INSERT INTO tbPagtoContribuicao (vl_pagtoContribuicao');
      SQL.Add(',dt_pagtoContribuicao,cd_formPagto,cd_cadastro');
      SQL.Add(',dt_anoRefContribuicao,dt_mesRefContribuicao,cd_tipocontribuicao) VALUES(');
      SQL.Add(''''+DM.formatarValorDecimal(valorPagt.Text)+''',cast('''+FormatDateTime('yyyymmdd', datapgto)+''' as datetime),');
      SQL.Add(inttostr(id)+','+edtContribuinte.Text+','+edtAno.Text+','+edtMes.Text+','+edtTipoContribuicao.Text+')');

      //Memo1.Lines.Add(sql.GetText);
    end
    else
    begin
      //showmessage(inttostr(id));
      if id = 3 then
        pgtoCheque := true;

      SQL.Add('INSERT INTO tbPagtoContribuicao (vl_pagtoContribuicao');
      SQL.Add(',dt_pagtoContribuicao,ds_bancoCheque,ds_agenciaCheque');
      SQL.Add(',no_chequePagto,dt_resgChequePre,cd_formPagto,cd_cadastro');
      SQL.Add(',dt_anoRefContribuicao,dt_mesRefContribuicao,cd_tipocontribuicao) ');
      SQL.Add(' VALUES ('''+DM.formatarValorDecimal(valorPagt.Text)+''',CAST('''+FormatDateTime('YYYYMMDD',datapgto)+''' AS DATETIME),');
      SQL.Add(''''+edtNomeBanco.Text+''','''+edtAgencia.Text+''',');
      SQL.Add(''''+edtNumCheque.Text+''',cast('''+FormatDateTime('yyyymmdd',strtodate(edtDataResg.Text))+''' as datetime),');
      SQL.Add(inttostr(id)+','+edtContribuinte.Text+','+edtAno.Text+','+edtMes.Text+','+edtTipoContribuicao.Text+')');
    end;


    try
      ExecSQL;

      QListaPagamentos.Close;
      QListaPagamentos.Parameters.ParamByName('codigo').Value := edtContribuinte.Text;
      QListaPagamentos.Parameters.ParamByName('mes').Value := edtMes.Text;
      QListaPagamentos.Parameters.ParamByName('ano').Value := edtAno.Text;
      QListaPagamentos.Parameters.ParamByName('tipo').Value := edtTipoContribuicao.Text;
      QListaPagamentos.Open;

      QListaPagamentos.First;
      valor := 0;
      while not QListaPagamentos.Eof do
      begin
        valor := valor + QListaPagamentosvl_pagtoContribuicao.Value;
        QListaPagamentos.Next;
      end;
      Label13.Caption := floattostr(valor);
      if valor < StrToFloat(vlContribuicao.Text) then
      begin

        if MessageDlg('Valor pago menor que esperado. Deseja inserir novo pagamento?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          ComboBox1.ItemIndex := -1;
          ComboBox1.SetFocus;
          valorPagt.Value := vlContribuicao.Value - valor;
        end
        else
          Button3.Click
      end
      else
      begin

      if pgtoCheque = false then
        dm.spAtualizaPagamento.Parameters.ParamByName('@codigo').Value := strtoint(edtContribuinte.Text);
        dm.spAtualizaPagamento.Parameters.ParamByName('@ano').Value := strtoint(edtAno.Text);
        dm.spAtualizaPagamento.Parameters.ParamByName('@mes').Value := strtoint(edtMes.Text);

        dm.spAtualizaPagamento.ExecProc;
        //dm.ADOConn.CommitTrans;
        if MessageDlg('Imprimir boleto?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          DM.imprimeBoleto(strtoint(edtContribuinte.Text),strtoint(edtMes.Text),strtoint(edtAno.Text));
          showmessage('Impress�o Finalizada')
        end;
        Button3.Click;
        limpaPagamento;
      end;
        
    except
      //dm.ADOConn.RollbackTrans;
      showmessage('Erro ao registrar pagamento');
    end;
  end;

  setAtributosEdits;
end;

procedure Tfrm_RegistraPagamento.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  If key = #13 then
  Begin
  Key:= #0;
  Perform(Wm_NextDlgCtl,0,0);
  end;
end;

procedure Tfrm_RegistraPagamento.acNovoExecute(Sender: TObject);
begin
  QListaPagamentos.Close;
  edtContribuinte.Text := '';
  edtContribuinte.SetFocus;
  edtAno.Text := FormatDateTime('YYYY',date);
  edtMes.Text := '';
  vlContribuicao.Text := '';
  Label2.Caption := '';
  Label13.Caption := '';
  edtTipoContribuicao.Text := '';
  lblTipoContrib.Caption := 'Tipo: ';
end;

procedure Tfrm_RegistraPagamento.edtMesExit(Sender: TObject);
var
  mes : Smallint;
begin

  mes := strtoint(edtMes.Text);

  if (mes < 1) or (mes > 12) then
  begin
    showmessage('O valor n�o � um m�s v�lido!');
    edtAno.SetFocus;
  end;


end;

procedure Tfrm_RegistraPagamento.edtMesKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_RegistraPagamento.edtContribuinteKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_RegistraPagamento.edtAnoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_RegistraPagamento.edtAgenciaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_RegistraPagamento.edtNumChequeKeyPress(Sender: TObject;
  var Key: Char);
begin
if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_RegistraPagamento.acFecharExecute(Sender: TObject);
begin
  frm_RegistraPagamento.Close;
end;

procedure Tfrm_RegistraPagamento.edtTipoContribuicaoExit(Sender: TObject);
var
  codigo : integer;
begin
  try

    codigo := strtoint(edtContribuinte.Text);
    qValorContribuicao.Close;
    qValorContribuicao.Parameters.ParamByName('codigo').Value := codigo;
    qValorContribuicao.Parameters.ParamByName('tipo').Value := strtoint(edtTipoContribuicao.Text);
    qValorContribuicao.Open;

    vlContribuicao.Text := floattostr(qValorContribuicaovalor.Value);


  except
    on EConvertError do
    begin
      showmessage('Digite um numero v�lido');
      edtContribuinte.SetFocus;
      abort;
    end;
  end;

  with qTipoContribuicao do
  begin
    close;
    Parameters.ParamByName('codigo').Value := strtoint(edtTipoContribuicao.Text);
    open;
    if RecordCount = 0 then
    begin
      lblTipoContrib.Caption := 'Tipo: N�o encontrado';
      edtTipoContribuicao.SetFocus;
    end
    else
      lblTipoContrib.Caption := 'Tipo: '+qTipoContribuicaonm_tipoContribuicao.Value;

  end
end;

procedure Tfrm_RegistraPagamento.edtTipoContribuicaoKeyPress(
  Sender: TObject; var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

end.



