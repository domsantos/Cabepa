unit Unit_PesquisaContribuinte;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PesquisaPadrao, DB, ADODB, ActnList, Grids, DBGrids,
  StdCtrls, Buttons, ExtCtrls;

type
  Tfrm_PesquisaContribuinte = class(Tfrm_PesquisaPadrao)
    QPesquisa: TADOQuery;
    QPesquisacd_formTeologica: TIntegerField;
    QPesquisacd_natPastor: TIntegerField;
    QPesquisacd_escPastor: TIntegerField;
    QPesquisacd_nacPastor: TIntegerField;
    QPesquisacd_estCivil: TIntegerField;
    QPesquisacd_categoria: TIntegerField;
    QPesquisanm_pastor: TStringField;
    QPesquisano_regConv: TIntegerField;
    QPesquisadt_nascPastor: TDateTimeField;
    QPesquisatp_sanguineo: TStringField;
    QPesquisano_regGeral: TStringField;
    QPesquisano_cpf: TStringField;
    QPesquisads_endereco: TStringField;
    QPesquisads_compEndPastor: TStringField;
    QPesquisano_cepPastor: TStringField;
    QPesquisadt_filiacao: TDateTimeField;
    QPesquisanm_pai: TStringField;
    QPesquisanm_mae: TStringField;
    QPesquisanm_conjuge: TStringField;
    QPesquisadt_nascConjuge: TDateTimeField;
    QPesquisano_fone: TStringField;
    QPesquisadt_batismo: TDateTimeField;
    QPesquisads_localBatismo: TStringField;
    QPesquisadt_autEvangelista: TDateTimeField;
    QPesquisadt_consagEvangelista: TDateTimeField;
    QPesquisadt_ordenacPastor: TDateTimeField;
    QPesquisads_localConsagracao: TStringField;
    QPesquisads_campo: TStringField;
    QPesquisads_supervisao: TStringField;
    QPesquisano_certCasamento: TIntegerField;
    QPesquisacd_sitJubilamento: TStringField;
    QPesquisacd_cadastro: TIntegerField;
    procedure btnPesquisaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1DrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
  private
    { Private declarations }
    Vorigem : byte;
    procedure setVorigem(const Value:byte);
  public
    { Public declarations }
    property origem : byte read Vorigem write setVorigem;
  end;

var
  frm_PesquisaContribuinte: Tfrm_PesquisaContribuinte;

implementation

uses Unit_DM, Unit_Contribuintes, Math, Unit_RegistraPagamento,
  Unit_FiltroContribuicaoPastor, Unit_ImpressaoPagamentos,
  Unit_FiltroRelatorioNadaConsta, Unit_RequerimentoBeneficio,
  Unit_RegistraPagamentoLote, Unit_CalculaPagBeneficio, Unit_FiltroContraCheque,
  Unit_simularJubilacao, Unit_apagarPagamento, Unit_pagamentoLoteReferencia;

{$R *.dfm}

procedure Tfrm_PesquisaContribuinte.btnPesquisaClick(Sender: TObject);

begin
  inherited;
  with QPesquisa do
  begin
    close;
    sql.Clear;


    sql.Add( 'SELECT cd_cadastro ,cd_formTeologica,cd_natPastor,cd_escPastor'+
            ',cd_nacPastor,cd_estCivil,cd_categoria,nm_pastor,no_regConv'+
            ',dt_nascPastor,tp_sanguineo,no_regGeral,no_cpf,ds_endereco'+
            ',ds_compEndPastor,no_cepPastor,dt_filiacao,nm_pai,nm_mae'+
            ',nm_conjuge,dt_nascConjuge,no_fone,dt_batismo,ds_localBatismo'+
            ',dt_autEvangelista,dt_consagEvangelista,dt_ordenacPastor'+
            ',ds_localConsagracao,ds_campo,ds_supervisao,no_certCasamento'+
            ',cd_sitJubilamento  FROM tbContribuinte where st_arquivoMorto = 0 and');

    if cbPesquisa.ItemIndex = 0 then
      sql.add( ' nm_pastor like '''+edtArgumento.Text+'%''')
    else if cbPesquisa.ItemIndex = 1 then
      sql.add(' cd_cadastro = '+edtArgumento.Text)
    else if cbPesquisa.ItemIndex = 2 then
      sql.add(' no_cpf = '''+edtArgumento.Text+'''')
    else if cbPesquisa.ItemIndex = 3 then
      sql.add(' nm_conjuge like '''+edtArgumento.Text+'%''')
    else
      sql.add( ' nm_pastor like '''+edtArgumento.Text+'%''');


    //showmessage(sql.GetText);


    try
      Open;

      if recordcount < 1 then
      begin
        showmessage('N�o foi encontrado nenhum registro com esse argumento');
        close;
        edtArgumento.SetFocus;
      end
    except
      on E:Exception do
      begin
        MessageDlg('Ocorreu um erro ao pesquisar a informa��o. Contate o suporte.\n'+E.Message, mtInformation, [mbOk], 0);
        abort;
       end
    end

  end

end;

procedure Tfrm_PesquisaContribuinte.DBGrid1DblClick(Sender: TObject);
begin

  if Vorigem = 1 then
  begin
    frm_Contribuinte.CDSPrincipal.Open;
    frm_Contribuinte.CDSPrincipal.Locate('cd_cadastro',QPesquisacd_cadastro.Value,[loPartialKey]);
    if frm_Contribuinte.CDSPrincipal.RecordCount > 0 then
    begin
      frm_Contribuinte.QCategoria.Open;
      frm_Contribuinte.QEstadoCivil.Open;
      frm_Contribuinte.QEscolaridade.Open;
      frm_Contribuinte.QNacionalidade.Open;
      frm_Contribuinte.QNaturalidade.Open;
      frm_Contribuinte.QFormTeologica.Open;
      frm_Contribuinte.qBancos.Open;

    end;
  end
  else if Vorigem = 2 then
  begin
    frm_RegistraPagamento.edtContribuinte.Text := inttostr(QPesquisacd_cadastro.Value);
    frm_RegistraPagamento.edtContribuinte.SetFocus;
  end
  else if Vorigem = 3 then
  begin
    frm_FiltroContribuicaoPastor.edtContribuinte.text := inttostr(QPesquisacd_cadastro.Value);
    frm_FiltroContribuicaoPastor.edtContribuinte.SetFocus;
  end
  else if Vorigem = 4 then
  begin
    frm_ImpressaoPagamentos.edtContribuinte.text := inttostr(QPesquisacd_cadastro.Value);
    frm_ImpressaoPagamentos.edtContribuinte.SetFocus;
  end
  else if Vorigem = 5 then
  begin
    frm_FiltroRelatorioNadaConsta.edtContribuinte.text := inttostr(QPesquisacd_cadastro.Value);
    frm_FiltroRelatorioNadaConsta.edtContribuinte.SetFocus;
  end
  else if Vorigem = 6 then
  begin
    frm_RequerimentoBeneficio.edtContribuinte.text := inttostr(QPesquisacd_cadastro.Value);
    frm_RequerimentoBeneficio.edtContribuinte.SetFocus;
  end
  else if Vorigem = 7 then
  begin
    frm_RegistroPagamentoLote.edtContribuinte.text := inttostr(QPesquisacd_cadastro.Value);
    frm_RegistroPagamentoLote.edtContribuinte.SetFocus;
  end
  else if Vorigem = 8 then
  begin
    frm_CalculaPagBeneficio.edtContribuinte.text := inttostr(QPesquisacd_cadastro.Value);
    frm_CalculaPagBeneficio.lblContribuinte.Caption := QPesquisanm_pastor.Value;
    frm_CalculaPagBeneficio.edtContribuinte.SetFocus;
  end
   else if Vorigem = 9 then
  begin
    frm_FiltroContraCheque.edtContribuinte.text := inttostr(QPesquisacd_cadastro.Value);
    frm_FiltroContraCheque.lblContribuinte.Caption := QPesquisanm_pastor.Value;
    frm_FiltroContraCheque.edtContribuinte.SetFocus;
  end
  else if Vorigem = 10 then
  begin
    frm_SimularJubilacao.edtContribuinte.text := inttostr(QPesquisacd_cadastro.Value);
    frm_SimularJubilacao.lblContribuinte.Caption := QPesquisanm_pastor.Value;
    frm_SimularJubilacao.edtContribuinte.SetFocus;
  end
  else if Vorigem = 11 then
  begin
    frm_apagarPagamento.edtContribuinte.text := inttostr(QPesquisacd_cadastro.Value);
    frm_apagarPagamento.lblContribuinte.Caption := QPesquisanm_pastor.Value;
    frm_apagarPagamento.edtContribuinte.SetFocus;
  end
  else if Vorigem = 12 then
  begin
    frm_pagamentoLoteReferencia.edtContribuinte.text := inttostr(QPesquisacd_cadastro.Value);
    frm_pagamentoLoteReferencia.lblContribuinte.Caption := QPesquisanm_pastor.Value;
    frm_pagamentoLoteReferencia.edtContribuinte.SetFocus;
  end;
  inherited;

end;

procedure Tfrm_PesquisaContribuinte.DBGrid1DrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
begin
  inherited;
  if QPesquisa.FieldByName('cd_sitJubilamento').Value = 1 then
    begin
      DBGrid1.Canvas.Brush.Color := clGreen;
      DBGrid1.Canvas.Font.Color := clWhite;
    end
    else if QPesquisa.FieldByName('cd_sitJubilamento').Value = 2 then

    begin
      DBGrid1.Canvas.Brush.Color := clBlue;
      DBGrid1.Canvas.Font.Color := clBlack;
    end
    else
    begin
      DBGrid1.Canvas.Brush.Color := clWhite;
      DBGrid1.Canvas.Font.Color := clBlack;
    end;

    DBGrid1.Canvas.FillRect(Rect);
    DBGrid1.DefaultDrawDataCell(Rect,Field,State);
  end;

procedure Tfrm_PesquisaContribuinte.setVorigem(const Value: byte);
begin
  Vorigem := Value;
end;

end.
