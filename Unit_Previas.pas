unit Unit_Previas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, Buttons, DBCtrls, Grids, DBGrids, ExtCtrls;

type
  Tfrm_Previas = class(TForm)
    Panel1: TPanel;
    dsPrevia: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    Label1: TLabel;
    edtMes: TEdit;
    Label2: TLabel;
    edtAno: TEdit;
    Previa: TBitBtn;
    btnRelatorio: TBitBtn;
    qVerificaRef: TADOQuery;
    qVerificaRefnr_registros: TIntegerField;
    BitBtn1: TBitBtn;
    qFechaFolha: TADOQuery;
    qListaPrevia: TADOQuery;
    qListaPrevianm_banco: TStringField;
    qListaPrevianm_pastor: TStringField;
    qListaPreviads_beneficio: TStringField;
    qListaPreviavl_vantagem: TBCDField;
    qListaPreviavl_desconto: TBCDField;
    qListaPreviavl_liquido: TBCDField;
    qListaPreviadt_mesPagamento: TSmallintField;
    qListaPreviadt_anoPagamento: TSmallintField;
    BitBtn2: TBitBtn;
    qCancelaFolha: TADOQuery;
    qApagaFolha: TADOQuery;
    BitBtn3: TBitBtn;
    qListaPreviacd_cadastro: TIntegerField;
    procedure PreviaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnRelatorioClick(Sender: TObject);
    procedure edtMesKeyPress(Sender: TObject; var Key: Char);
    procedure edtAnoKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Previas: Tfrm_Previas;

implementation

uses Unit_DM, Unit_RelPrevia;

{$R *.dfm}

procedure Tfrm_Previas.BitBtn1Click(Sender: TObject);
begin

  if edtMes.Text = '' then
  begin
    showmessage('Informe m�s de refer�ncia');
    edtMes.SetFocus;
    exit;
  end;

  if edtAno.Text = '' then
  begin
    showmessage('Informe ano de refer�ncia');
    edtAno.SetFocus;
    exit;
  end;

  if MessageDlg('Deseja fechar esta folha  para iniciar o pagamento?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    qFechaFolha.Parameters.ParamByName('mes').Value := edtMes.Text;
    qFechaFolha.Parameters.ParamByName('ano').Value := edtAno.Text;

    try
      qFechaFolha.ExecSQL;
      showmessage('Folha fechada!');
    except
      on e:exception do
      begin
        showmessage('Erro ao fechar a folha');
      end;

    end;

  end;

end;

procedure Tfrm_Previas.BitBtn2Click(Sender: TObject);
begin
  if MessageDlg('Deseja cancelar esta folha de pagamento?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    qCancelaFolha.Close;
    qCancelaFolha.Parameters.ParamByName('ano').Value := strtoint(edtano.Text);
    qCancelaFolha.Parameters.ParamByName('mes').Value := strtoint(edtmes.Text);
    qCancelaFolha.Parameters.ParamByName('op').Value := dm.idLogin;
    qApagaFolha.Parameters.ParamByName('mes').Value := strtoint(edtMes.Text);
    qApagaFolha.Parameters.ParamByName('ano').Value := strtoint(edtAno.Text);
    try
      qCancelaFolha.ExecSQL;
      qApagaFolha.ExecSQL;
      showmessage('Folha cancelada!');
      qListaPrevia.Close;
    except
      on e:exception do
      begin
        showmessage('Erro ao cancelar a folha!'+#13+e.Message);
      end;


    end;
  end;
end;

procedure Tfrm_Previas.BitBtn3Click(Sender: TObject);
begin
  qListaPrevia.Close;
  qListaPrevia.SQL.Clear;
  qListaPrevia.SQL.Add('exec sp_ListaResumoBeneficio');
	qListaPrevia.SQL.Add(' @mes = '+edtMes.Text+' ');
	qListaPrevia.SQL.Add(',@ano = '+edtAno.Text+' ');
  qListaPrevia.SQL.Add(',@codigo = 0 ');
  qListaPrevia.SQL.Add(',@status = ''D''');

  qListaPrevia.Open;
end;

procedure Tfrm_Previas.btnRelatorioClick(Sender: TObject);
begin


  if not qListaPrevia.Active then
  begin
    showmessage('H� ha registros a exibir');
    exit;
  end;

  if qListaPrevia.RecordCount <= 0 then
  begin
    showmessage('N�o h� registro de pagamento para esta refer�ncia.');
    exit;
  end;

  Application.CreateForm(Tfrm_RelPrevia,frm_RelPrevia);

  frm_RelPrevia.lblRef.Caption := 'Refer�ncia: '+edtMes.Text + '/'+edtAno.Text;
  frm_RelPrevia.RLReport1.Preview;
  frm_RelPrevia.Close;
end;

procedure Tfrm_Previas.edtAnoKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_Previas.edtMesKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure Tfrm_Previas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

procedure Tfrm_Previas.PreviaClick(Sender: TObject);
begin
  if edtMes.Text = '' then
  begin
    showmessage('Informe m�s de refer�ncia');
    edtMes.SetFocus;
    exit;
  end;
  if edtAno.Text = '' then
  begin
    showmessage('Informe ano de refer�ncia');
    edtAno.SetFocus;
    exit;
  end;
  
  qVerificaRef.Close;
  qVerificaRef.Parameters.ParamByName('mes').Value := edtMes.Text;
  qVerificaRef.Parameters.ParamByName('ano').Value := edtAno.Text;

  qVerificaRef.Open;
  if qVerificaRefnr_registros.Value <= 0 then
  begin
    showmessage('N�o h� registros de pr�via para esta refer�ncia');
    exit;
  end;

  qListaPrevia.Close;
  qListaPrevia.SQL.Clear;
  qListaPrevia.SQL.Add('exec sp_ListaResumoBeneficio');
	qListaPrevia.SQL.Add(' @mes = '+edtMes.Text+' ');
	qListaPrevia.SQL.Add(',@ano = '+edtAno.Text+' ');
  qListaPrevia.SQL.Add(',@codigo = 0 ');
  qListaPrevia.SQL.Add(',@status = ''P''');

  qListaPrevia.Open;

end;

end.

