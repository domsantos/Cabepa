unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DBCtrls, DB, StdCtrls, Mask, DBClient, Provider, ADODB;

type
  TForm1 = class(TForm)
    ADOQuery1: TADOQuery;
    DataSetProvider1: TDataSetProvider;
    ClientDataSet1: TClientDataSet;
    ADOQuery1cd_CABEPA: TIntegerField;
    ADOQuery1cd_formTeologica: TIntegerField;
    ADOQuery1cd_natPastor: TIntegerField;
    ADOQuery1cd_escPastor: TIntegerField;
    ADOQuery1cd_nacPastor: TIntegerField;
    ADOQuery1cd_estCivil: TIntegerField;
    ADOQuery1cd_categoria: TIntegerField;
    ADOQuery1nm_pastor: TStringField;
    ADOQuery1no_regConv: TIntegerField;
    ADOQuery1dt_nascPastor: TDateTimeField;
    ADOQuery1tp_sanguineo: TStringField;
    ADOQuery1no_regGeral: TStringField;
    ADOQuery1no_cpf: TStringField;
    ADOQuery1ds_endereco: TStringField;
    ADOQuery1ds_compEndPastor: TStringField;
    ADOQuery1no_cepPastor: TStringField;
    ADOQuery1dt_filiacao: TDateTimeField;
    ADOQuery1nm_pai: TStringField;
    ADOQuery1nm_mae: TStringField;
    ADOQuery1nm_conjuge: TStringField;
    ADOQuery1dt_nascConjuge: TDateTimeField;
    ADOQuery1no_fone: TStringField;
    ADOQuery1dt_batismo: TDateTimeField;
    ADOQuery1ds_localBatismo: TStringField;
    ADOQuery1dt_autEvangelista: TDateTimeField;
    ADOQuery1dt_consagEvangelista: TDateTimeField;
    ADOQuery1dt_ordenacPastor: TDateTimeField;
    ADOQuery1ds_localConsagracao: TStringField;
    ADOQuery1ds_campo: TStringField;
    ADOQuery1ds_supervisao: TStringField;
    ADOQuery1no_certCasamento: TIntegerField;
    ClientDataSet1cd_CABEPA: TIntegerField;
    ClientDataSet1cd_formTeologica: TIntegerField;
    ClientDataSet1cd_natPastor: TIntegerField;
    ClientDataSet1cd_escPastor: TIntegerField;
    ClientDataSet1cd_nacPastor: TIntegerField;
    ClientDataSet1cd_estCivil: TIntegerField;
    ClientDataSet1cd_categoria: TIntegerField;
    ClientDataSet1nm_pastor: TStringField;
    ClientDataSet1no_regConv: TIntegerField;
    ClientDataSet1dt_nascPastor: TDateTimeField;
    ClientDataSet1tp_sanguineo: TStringField;
    ClientDataSet1no_regGeral: TStringField;
    ClientDataSet1no_cpf: TStringField;
    ClientDataSet1ds_endereco: TStringField;
    ClientDataSet1ds_compEndPastor: TStringField;
    ClientDataSet1no_cepPastor: TStringField;
    ClientDataSet1dt_filiacao: TDateTimeField;
    ClientDataSet1nm_pai: TStringField;
    ClientDataSet1nm_mae: TStringField;
    ClientDataSet1nm_conjuge: TStringField;
    ClientDataSet1dt_nascConjuge: TDateTimeField;
    ClientDataSet1no_fone: TStringField;
    ClientDataSet1dt_batismo: TDateTimeField;
    ClientDataSet1ds_localBatismo: TStringField;
    ClientDataSet1dt_autEvangelista: TDateTimeField;
    ClientDataSet1dt_consagEvangelista: TDateTimeField;
    ClientDataSet1dt_ordenacPastor: TDateTimeField;
    ClientDataSet1ds_localConsagracao: TStringField;
    ClientDataSet1ds_campo: TStringField;
    ClientDataSet1ds_supervisao: TStringField;
    ClientDataSet1no_certCasamento: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBNavigator1: TDBNavigator;
    DBLookupComboBox1: TDBLookupComboBox;
    ADOQuery2: TADOQuery;
    DataSource2: TDataSource;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses Unit_DM;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  ClientDataSet1.Open;
  ADOQuery2.Open;
end;

end.
