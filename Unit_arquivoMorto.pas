unit Unit_arquivoMorto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons;

type
  TfrmArquivoMorto = class(TForm)
    Panel1: TPanel;
    dbgArquivo: TDBGrid;
    dsListaAquivoMorto: TDataSource;
    edtPesquisa: TEdit;
    Label1: TLabel;
    rbPesquisa: TRadioGroup;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmArquivoMorto: TfrmArquivoMorto;

implementation

uses Unit_DM, Unit_relArquivoMorto;

{$R *.dfm}

procedure TfrmArquivoMorto.BitBtn1Click(Sender: TObject);
begin

  if rbPesquisa.ItemIndex = 0 then
  begin
    dsListaAquivoMorto.DataSet.Locate('nm_pastor',edtPesquisa.Text,[loCaseInsensitive, loPartialKey]);
  end;

  if rbPesquisa.ItemIndex = 1 then
  begin
    dsListaAquivoMorto.DataSet.Locate('cd_cadastro',edtPesquisa.Text,[loCaseInsensitive, loPartialKey]);
  end;

end;

procedure TfrmArquivoMorto.BitBtn2Click(Sender: TObject);
var
  relArquivo : Tfrm_RelArquivoMorto;
begin
  relArquivo := Tfrm_RelArquivoMorto.Create(self);

  relArquivo.RLReport1.Preview();

end;

procedure TfrmArquivoMorto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.qListaArquivoMorto.Close;
  self := nil;
  Action := caFree;
end;

procedure TfrmArquivoMorto.FormShow(Sender: TObject);
begin
  dm.qListaArquivoMorto.Open;
end;

end.
