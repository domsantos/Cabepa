unit Unit_Caixa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, DB, ExtCtrls, ADODB, Mask, DBCtrls, Buttons;

type
  TfrmCaixa = class(TForm)
    qCaixa: TADOQuery;
    Panel1: TPanel;
    Panel2: TPanel;
    qCaixadt_anoRefContribuicao: TIntegerField;
    qCaixadt_mesRefContribuicao: TIntegerField;
    qCaixavl_ContribuicaoVencimento: TBCDField;
    qCaixacd_tipoContribuicao: TIntegerField;
    qCaixacd_sitQuitacao: TSmallintField;
    qCaixadt_Contribuicao: TDateTimeField;
    qCaixaid_usuario: TIntegerField;
    qCaixanm_usuario: TStringField;
    qCaixanm_tipoContribuicao: TStringField;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    blbValor: TLabel;
    Label2: TLabel;
    edtData: TMaskEdit;
    BitBtn1: TBitBtn;
    qUsuarios: TADOQuery;
    DataSource2: TDataSource;
    Label3: TLabel;
    qUsuariosid_usuario: TAutoIncField;
    qUsuariosnm_usuario: TStringField;
    BitBtn2: TBitBtn;
    qCaixacd_cadastro: TIntegerField;
    edtFim: TMaskEdit;
    Label4: TLabel;
    qCaixanm_pastor: TStringField;
    DBLookupComboBox1: TDBLookupComboBox;
    DBLookupComboBox2: TDBLookupComboBox;
    Label5: TLabel;
    qTipo: TADOQuery;
    DataSource3: TDataSource;
    qTipocd_tipoContribuicao: TAutoIncField;
    qTiponm_tipoContribuicao: TStringField;
    qTotal: TADOQuery;
    qTotaltotal: TBCDField;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa: TfrmCaixa;
  strDiaFim : String;

implementation

uses Unit_DM, Unit_ClassImpressoraMatricial, Unit_RelCaixa;

{$R *.dfm}

procedure TfrmCaixa.BitBtn1Click(Sender: TObject);
begin
  qCaixa.Close;
  qTotal.Close;

  strDiaFim := edtFim.Text;

  if edtFim.Text = '  /  /    ' then
  begin
    strDiaFim := edtData.Text;
  end;
  //ShowMessage( DM.formataData(edtData.Text)+' 00.00.00.000');
  qCaixa.Parameters.ParamByName('diainicio').Value :=  StrToDateTime(edtData.Text+' 00.00.00');
  qCaixa.Parameters.ParamByName('diafim').Value :=  StrToDateTime(strDiaFim+' 23.59.59');
  qCaixa.Parameters.ParamByName('id').Value := DBLookupComboBox1.KeyValue;

  if DBLookupComboBox2.KeyValue = 0 then
  begin
    qCaixa.Parameters.ParamByName('tipo').Value := '1,2';
  end
  else
  begin
    qCaixa.Parameters.ParamByName('tipo').Value := DBLookupComboBox2.KeyValue;
  end;

  qTotal.Parameters.ParamByName('diainicio').Value :=  StrToDateTime(edtData.Text+' 00.00.00');
  qTotal.Parameters.ParamByName('diafim').Value :=  StrToDateTime(strDiaFim+' 23.59.59');
  qTotal.Parameters.ParamByName('id').Value := DBLookupComboBox1.KeyValue;

  if DBLookupComboBox2.KeyValue = 0 then
  begin
    qTotal.Parameters.ParamByName('tipo').Value := '1,2';
  end
  else
  begin
     qTotal.Parameters.ParamByName('tipo').Value := DBLookupComboBox2.KeyValue;
  end;

  qCaixa.Open;
  qTotal.Open;

  blbValor.Caption := CurrToStrF(qTotaltotal.Value,ffCurrency,2);

end;

procedure TfrmCaixa.BitBtn2Click(Sender: TObject);
begin
  Application.CreateForm(Tfrm_RelCaixa,frm_RelCaixa);

  frm_RelCaixa.RLLabel9.Caption := edtData.Text;
  frm_RelCaixa.RLLabel11.Caption := DBLookupComboBox1.Text;
  frm_RelCaixa.lblPeriodo.Caption :=  edtData.Text+' 00.00.00 at� ' +  strDiaFim +' 23.59.59';

  frm_RelCaixa.RLReport1.Preview(); 
end;

procedure TfrmCaixa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;
end;

procedure TfrmCaixa.FormCreate(Sender: TObject);
begin
  qUsuarios.Open;
  qTipo.Open;
end;

end.
