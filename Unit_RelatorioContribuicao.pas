unit Unit_RelatorioContribuicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, RLReport, jpeg, ADODB;

type
  Tfrm_RelatorioContribuicao = class(Tform)
    QspListaContribuicao: TADOQuery;
    QspListaContribuicaocd_pastor: TIntegerField;
    QspListaContribuicaonr_mes: TIntegerField;
    QspListaContribuicaonr_ano: TIntegerField;
    QspListaContribuicaovl_pago: TBCDField;
    QspListaContribuicaotp_contribuicao: TIntegerField;
    QspListaContribuicaonm_tipoContribuicao: TStringField;
    QspListaContribuicaodt_contribuicao: TDateTimeField;
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel2: TRLLabel;
    lblRelatorio: TRLLabel;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    lblContribuinte: TRLLabel;
    lblRelatorioRef: TRLLabel;
    RLLabel5: TRLLabel;
    lblNo: TRLLabel;
    DataSource1: TDataSource;
    RLGroup1: TRLGroup;
    RLBand2: TRLBand;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLBand3: TRLBand;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    RLLabel10: TRLLabel;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLBand4: TRLBand;
    RLDBResult1: TRLDBResult;
    RLBand5: TRLBand;
    RLSystemInfo1: TRLSystemInfo;
    RLLabel11: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLDBResult2: TRLDBResult;
    RLLabel1: TRLLabel;

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelatorioContribuicao: Tfrm_RelatorioContribuicao;

implementation

uses Unit_DM;

{$R *.dfm}

end.
