inherited frm_FormTeologica: Tfrm_FormTeologica
  Caption = 'Forma'#231#227'o Teologica'
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel1: TPanel
    inherited btnRelatorio: TBitBtn
      OnClick = btnRelatorioClick
    end
  end
  inherited Panel2: TPanel
    object Label1: TLabel
      Left = 10
      Top = 10
      Width = 97
      Height = 16
      Caption = 'Forma'#231#227'o Teol'#243'gica'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Left = 10
      Top = 30
      Width = 319
      Height = 24
      CharCase = ecUpperCase
      DataField = 'ds_formteologica'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 64
      Width = 320
      Height = 161
      DataSource = DataSource1
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalcd_formteologica: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'cd_formteologica'
    end
    object CDSPrincipalds_formteologica: TStringField
      DisplayLabel = 'Forma'#231#227'o Teol'#243'gicads_formteologica'
      FieldName = 'ds_formteologica'
      Required = True
      Size = 40
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    SQL.Strings = (
      'select cd_formteologica, ds_formteologica'
      'from tbformteologica'
      'order by ds_formteologica')
    Left = 600
    Top = 121
    object QPrincipalcd_formteologica: TIntegerField
      FieldName = 'cd_formteologica'
    end
    object QPrincipalds_formteologica: TStringField
      FieldName = 'ds_formteologica'
      Required = True
      Size = 40
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 352
    Top = 240
  end
end
