unit Unit_Jubilados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBCtrls, Grids, DBGrids, ADODB, ExtCtrls, RXDBCtrl, StdCtrls,
  Buttons;

type
  Tfrm_Jubilados = class(TForm)
    Panel1: TPanel;
    qListaJubilados: TADOQuery;
    Panel2: TPanel;
    DBNavigator1: TDBNavigator;
    dsListaJubilados: TDataSource;
    RxDBGrid1: TRxDBGrid;
    BitBtn1: TBitBtn;
    qListaJubiladosnm_pastor: TStringField;
    qListaJubiladosnm_conjuge: TStringField;
    qListaJubiladosdt_nascPastor: TDateTimeField;
    qListaJubiladosno_cpf: TStringField;
    qListaJubiladosds_beneficio: TStringField;
    qListaJubiladosnm_beneficiado: TStringField;
    qListaJubiladosdt_Valor: TDateTimeField;
    qListaJubiladosvl_Beneficio: TBCDField;
    qListaJubiladosqt_pgtos: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Jubilados: Tfrm_Jubilados;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_Jubilados.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qListaJubilados.Close;
  self := nil;
  Action := caFree;
end;

procedure Tfrm_Jubilados.FormShow(Sender: TObject);
begin
  qListaJubilados.Open;
end;

end.
