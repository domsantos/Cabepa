unit Unit_RelatorioPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RLReport, jpeg, DB, RLPrintDialog, RLPreview, RLPreviewForm;

type
  Tfrm_RelatorioPadrao = class(TForm)
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLBand2: TRLBand;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    dsRelatorio: TDataSource;
    RLBand4: TRLBand;
    RLSystemInfo1: TRLSystemInfo;
    RLLabel5: TRLLabel;
    RLSystemInfo2: TRLSystemInfo;
    RLBand3: TRLBand;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelatorioPadrao: Tfrm_RelatorioPadrao;

implementation

{$R *.dfm}

procedure Tfrm_RelatorioPadrao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
end;

end.
