object frm_RelatorioContribuicao: Tfrm_RelatorioContribuicao
  Left = 0
  Top = 0
  Caption = 'frm_RelatorioContribuicao'
  ClientHeight = 375
  ClientWidth = 775
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = 8
    Top = 8
    Width = 794
    Height = 1123
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 139
      BandType = btHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = True
      object RLLabel2: TRLLabel
        Left = 5
        Top = 40
        Width = 190
        Height = 22
        Caption = 'Relat'#243'rio de Doa'#231#245'es'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object lblRelatorio: TRLLabel
        Left = 5
        Top = 63
        Width = 100
        Height = 22
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel3: TRLLabel
        Left = 3
        Top = 86
        Width = 67
        Height = 19
        Caption = 'Doador:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel4: TRLLabel
        Left = 3
        Top = 108
        Width = 189
        Height = 19
        Caption = 'Refer'#234'ncia do Relat'#243'rio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblContribuinte: TRLLabel
        Left = 76
        Top = 86
        Width = 117
        Height = 19
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblRelatorioRef: TRLLabel
        Left = 198
        Top = 108
        Width = 117
        Height = 19
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel5: TRLLabel
        Left = 624
        Top = 86
        Width = 30
        Height = 19
        Caption = 'No.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblNo: TRLLabel
        Left = 660
        Top = 86
        Width = 44
        Height = 19
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object RLGroup1: TRLGroup
      Left = 38
      Top = 177
      Width = 718
      Height = 80
      DataFields = 'cd_pastor'
      PageBreaking = pbBeforePrint
      object RLBand2: TRLBand
        Left = 0
        Top = 0
        Width = 718
        Height = 23
        BandType = btColumnHeader
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = False
        Borders.DrawRight = False
        Borders.DrawBottom = True
        object RLLabel6: TRLLabel
          Left = 3
          Top = 6
          Width = 87
          Height = 16
          Caption = 'REFER'#202'NCIA'
        end
        object RLLabel7: TRLLabel
          Left = 96
          Top = 6
          Width = 100
          Height = 16
          Caption = 'CONTRIBUI'#199#195'O'
        end
        object RLLabel8: TRLLabel
          Left = 472
          Top = 6
          Width = 38
          Height = 16
          Caption = 'DATA'
        end
        object RLLabel9: TRLLabel
          Left = 596
          Top = 6
          Width = 48
          Height = 16
          Caption = 'VALOR'
        end
      end
      object RLBand3: TRLBand
        Left = 0
        Top = 23
        Width = 718
        Height = 20
        object RLDBText1: TRLDBText
          Left = 6
          Top = 3
          Width = 47
          Height = 16
          DataField = 'nr_mes'
          DataSource = DataSource1
        end
        object RLDBText2: TRLDBText
          Left = 41
          Top = 3
          Width = 43
          Height = 16
          DataField = 'nr_ano'
          DataSource = DataSource1
        end
        object RLLabel10: TRLLabel
          Left = 29
          Top = 3
          Width = 8
          Height = 16
          Caption = '/'
        end
        object RLDBText3: TRLDBText
          Left = 96
          Top = 3
          Width = 122
          Height = 16
          DataField = 'nm_tipoContribuicao'
          DataSource = DataSource1
        end
        object RLDBText4: TRLDBText
          Left = 472
          Top = 3
          Width = 92
          Height = 16
          DataField = 'dt_contribuicao'
          DataSource = DataSource1
        end
        object RLDBText5: TRLDBText
          Left = 658
          Top = 3
          Width = 47
          Height = 16
          Alignment = taRightJustify
          DataField = 'vl_pago'
          DataSource = DataSource1
        end
      end
      object RLBand4: TRLBand
        Left = 0
        Top = 43
        Width = 718
        Height = 22
        BandType = btSummary
        object RLDBResult1: TRLDBResult
          Left = 619
          Top = 3
          Width = 86
          Height = 16
          Alignment = taRightJustify
          DataField = 'vl_pago'
          DataSource = DataSource1
          DisplayMask = 'R$ #,##,0.00'
          Info = riSum
        end
        object RLLabel12: TRLLabel
          Left = 512
          Top = 3
          Width = 59
          Height = 16
          Caption = 'Sub-Total'
        end
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 257
      Width = 718
      Height = 32
      BandType = btFooter
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = False
      object RLSystemInfo1: TRLSystemInfo
        Left = 110
        Top = 13
        Width = 39
        Height = 16
      end
      object RLLabel11: TRLLabel
        Left = 3
        Top = 13
        Width = 101
        Height = 16
        Caption = 'IMPRESSO EM:'
      end
      object RLLabel13: TRLLabel
        Left = 512
        Top = 6
        Width = 36
        Height = 16
        Caption = 'Total:'
      end
      object RLDBResult2: TRLDBResult
        Left = 619
        Top = 3
        Width = 86
        Height = 16
        Alignment = taRightJustify
        DataField = 'vl_pago'
        DataSource = DataSource1
        DisplayMask = 'R$ #,##0.00'
        Info = riSum
      end
    end
  end
  object RLLabel1: TRLLabel
    Left = 52
    Top = 55
    Width = 299
    Height = 27
    Caption = 'Associa'#231#227'o Casa do Pastor'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object QspListaContribuicao: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'data_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'data_fim'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'debito'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'tipocon'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'EXEC sp_listaContribuicoesPastor'
      #9#9'@id_pastor = :codigo,'
      #9#9'@vDataInicio = :data_inicio,'
      #9#9'@vDataFim = :data_fim,'
      '    @vDebito = :debito,'
      '    @vtpContr = :tipocon')
    Left = 584
    Top = 48
    object QspListaContribuicaocd_pastor: TIntegerField
      FieldName = 'cd_pastor'
    end
    object QspListaContribuicaonr_mes: TIntegerField
      FieldName = 'nr_mes'
    end
    object QspListaContribuicaonr_ano: TIntegerField
      FieldName = 'nr_ano'
    end
    object QspListaContribuicaovl_pago: TBCDField
      FieldName = 'vl_pago'
      DisplayFormat = 'R$ #,##0.00'
      EditFormat = ' '
      currency = True
      Precision = 19
    end
    object QspListaContribuicaotp_contribuicao: TIntegerField
      FieldName = 'tp_contribuicao'
    end
    object QspListaContribuicaonm_tipoContribuicao: TStringField
      FieldName = 'nm_tipoContribuicao'
      Size = 50
    end
    object QspListaContribuicaodt_contribuicao: TDateTimeField
      FieldName = 'dt_contribuicao'
    end
  end
  object DataSource1: TDataSource
    DataSet = QspListaContribuicao
    Left = 576
    Top = 96
  end
end
