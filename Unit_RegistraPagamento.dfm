object frm_RegistraPagamento: Tfrm_RegistraPagamento
  Left = 290
  Top = 158
  BorderIcons = [biSystemMenu]
  Caption = 'Registro de Contribui'#231#227'o'
  ClientHeight = 501
  ClientWidth = 582
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial Narrow'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 582
    Height = 305
    Align = alTop
    Color = clBtnHighlight
    ParentBackground = False
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 69
      Height = 16
      Caption = 'Contribuinte'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 144
      Top = 24
      Width = 287
      Height = 22
      Caption = 'Nenhum Contribuinte Selecionado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 152
      Width = 105
      Height = 16
      Caption = 'Valor Contribui'#231#227'o'
    end
    object Label4: TLabel
      Left = 8
      Top = 104
      Width = 25
      Height = 16
      Caption = 'M'#234's'
    end
    object Label5: TLabel
      Left = 64
      Top = 104
      Width = 23
      Height = 16
      Caption = 'Ano'
    end
    object Label12: TLabel
      Left = 8
      Top = 232
      Width = 191
      Height = 24
      Caption = 'Valor Acumulado: R$'
      FocusControl = Button1
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -21
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object Label13: TLabel
      Left = 180
      Top = 231
      Width = 6
      Height = 24
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -21
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object lblTipoContrib: TLabel
      Left = 64
      Top = 76
      Width = 28
      Height = 16
      Caption = 'Tipo:'
    end
    object Label14: TLabel
      Left = 8
      Top = 56
      Width = 100
      Height = 16
      Caption = 'Tipo Contribui'#231#227'o'
    end
    object Button1: TButton
      Left = 80
      Top = 24
      Width = 49
      Height = 23
      Caption = '&Buscar'
      TabOrder = 0
      OnClick = Button1Click
    end
    object edtContribuinte: TEdit
      Left = 9
      Top = 26
      Width = 65
      Height = 24
      TabOrder = 1
      OnExit = edtContribuinteExit
      OnKeyPress = edtContribuinteKeyPress
    end
    object edtMes: TEdit
      Left = 9
      Top = 122
      Width = 49
      Height = 24
      MaxLength = 2
      TabOrder = 3
      OnExit = edtMesExit
      OnKeyPress = edtMesKeyPress
    end
    object edtAno: TEdit
      Left = 64
      Top = 122
      Width = 57
      Height = 24
      MaxLength = 4
      TabOrder = 4
      OnKeyPress = edtAnoKeyPress
    end
    object Panel2: TPanel
      Left = 256
      Top = 53
      Width = 313
      Height = 241
      BorderWidth = 1
      BorderStyle = bsSingle
      Color = clSilver
      ParentBackground = False
      TabOrder = 9
      Visible = False
      object Label7: TLabel
        Left = 8
        Top = 56
        Width = 99
        Height = 16
        Caption = 'Valor Pagamento'
      end
      object Label6: TLabel
        Left = 8
        Top = 8
        Width = 125
        Height = 16
        Caption = 'Forma de Pagamento'
      end
      object Label8: TLabel
        Left = 8
        Top = 104
        Width = 103
        Height = 16
        Caption = 'Banco do Cheque'
      end
      object Label9: TLabel
        Left = 168
        Top = 104
        Width = 95
        Height = 16
        Caption = 'Agencia Cheque'
      end
      object Label10: TLabel
        Left = 8
        Top = 152
        Width = 68
        Height = 16
        Caption = 'No. Cheque'
      end
      object Label11: TLabel
        Left = 168
        Top = 152
        Width = 79
        Height = 16
        Caption = 'Data Resgate'
      end
      object ComboBox1: TComboBox
        Left = 8
        Top = 26
        Width = 201
        Height = 24
        ItemHeight = 16
        TabOrder = 0
        Text = 'Selecione'
        OnChange = ComboBox1Change
      end
      object edtNomeBanco: TEdit
        Left = 8
        Top = 120
        Width = 121
        Height = 24
        CharCase = ecUpperCase
        Enabled = False
        TabOrder = 2
      end
      object edtAgencia: TEdit
        Left = 168
        Top = 122
        Width = 121
        Height = 24
        Enabled = False
        TabOrder = 3
        OnKeyPress = edtAgenciaKeyPress
      end
      object edtNumCheque: TEdit
        Left = 8
        Top = 168
        Width = 121
        Height = 24
        Enabled = False
        TabOrder = 4
        OnKeyPress = edtNumChequeKeyPress
      end
      object edtDataResg: TMaskEdit
        Left = 168
        Top = 168
        Width = 120
        Height = 24
        Enabled = False
        EditMask = '!99/99/0000;1;_'
        MaxLength = 10
        TabOrder = 5
        Text = '  /  /    '
      end
      object Button2: TButton
        Left = 216
        Top = 200
        Width = 75
        Height = 25
        Caption = '&Gravar'
        TabOrder = 6
        OnClick = Button2Click
      end
      object Button3: TButton
        Left = 135
        Top = 201
        Width = 75
        Height = 25
        Caption = '&Cancelar'
        TabOrder = 7
        OnClick = Button3Click
      end
      object valorPagt: TCurrencyEdit
        Left = 8
        Top = 74
        Width = 121
        Height = 24
        Margins.Left = 4
        Margins.Top = 1
        TabOrder = 1
      end
    end
    object btnGravar: TButton
      Left = 135
      Top = 202
      Width = 89
      Height = 25
      Caption = '&Gravar'
      TabOrder = 8
      OnClick = btnGravarClick
    end
    object edtTipoContribuicao: TEdit
      Left = 9
      Top = 74
      Width = 49
      Height = 24
      TabOrder = 2
      OnExit = edtTipoContribuicaoExit
      OnKeyPress = edtTipoContribuicaoKeyPress
    end
    object vlContribuicao: TCurrencyEdit
      Left = 8
      Top = 172
      Width = 121
      Height = 24
      Margins.Left = 4
      Margins.Top = 1
      TabOrder = 5
    end
    object dtRetroativa: TMaskEdit
      Left = 135
      Top = 172
      Width = 97
      Height = 24
      Enabled = False
      EditMask = '!99/99/0000;1;_'
      MaxLength = 10
      TabOrder = 7
      Text = '  /  /    '
    end
    object ck_Retroativo: TCheckBox
      Left = 135
      Top = 149
      Width = 123
      Height = 17
      Caption = 'Pagamento Retrotivo'
      TabOrder = 6
      OnClick = ck_RetroativoClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 305
    Width = 582
    Height = 196
    Align = alClient
    UseDockManager = False
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 580
      Height = 194
      Align = alClient
      DataSource = dsListaPagamento
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
  end
  object qBuscaContribuinte: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select c.nm_pastor from tbContribuinte c where c.cd_cadastro = :' +
        'codigo')
    Left = 488
    Top = 312
    object qBuscaContribuintenm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
  end
  object QFormaPagamento: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cd_formpagto,ds_formpagto'
      'from tbFormaPagto'
      'order by ds_formpagto')
    Left = 368
    Top = 313
    object QFormaPagamentocd_formpagto: TSmallintField
      FieldName = 'cd_formpagto'
      ReadOnly = True
    end
    object QFormaPagamentods_formpagto: TStringField
      FieldName = 'ds_formpagto'
      Size = 40
    end
  end
  object QPagamento: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ano'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'mes'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'tipo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select vl_ContribuicaoVencimento'
      'from tbContribuicoes '
      'where cd_cadastro= :codigo'
      '  and dt_anoRefContribuicao = :ano'
      '  and dt_mesRefContribuicao = :mes'
      '  and cd_tipocontribuicao = :tipo')
    Left = 448
    Top = 9
    object QPagamentovl_ContribuicaoVencimento: TBCDField
      FieldName = 'vl_ContribuicaoVencimento'
      Precision = 10
      Size = 2
    end
  end
  object QListaPagamentos: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'ano'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'mes'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'tipo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      #9'p.id_PagtoContribuincao, '
      #9'p.vl_pagtoContribuicao, '
      #9'p.dt_pagtoContribuicao, '
      #9'p.ds_bancoCheque, '
      #9'p.ds_agenciaCheque, '
      #9'p.no_chequePagto, '
      #9'p.dt_resgChequePre, '
      #9'p.cd_formPagto, '
      #9'p.cd_cadastro, '
      #9'p.dt_anoRefContribuicao, '
      #9'p.dt_mesRefContribuicao ,'
      #9'f.ds_formPagto'
      'FROM tbPagtoContribuicao p'
      'inner join dbo.tbFormaPagto f ON p.cd_formPagto = f.cd_formPagto'
      'where p.cd_cadastro = :codigo'
      #9'and p.dt_anoRefContribuicao = :ano'
      #9'and p.dt_mesRefContribuicao =  :mes'
      '                and p.cd_tipoContribuicao = :tipo'
      'ORDER BY p.cd_formPagto')
    Left = 48
    Top = 392
    object QListaPagamentosid_PagtoContribuincao: TAutoIncField
      FieldName = 'id_PagtoContribuincao'
      ReadOnly = True
      Visible = False
    end
    object QListaPagamentosds_formPagto: TStringField
      DisplayLabel = 'Forma Pagto'
      FieldName = 'ds_formPagto'
      Size = 40
    end
    object QListaPagamentosvl_pagtoContribuicao: TBCDField
      DisplayLabel = 'Valor Pago'
      FieldName = 'vl_pagtoContribuicao'
      currency = True
      Precision = 10
      Size = 2
    end
    object QListaPagamentosdt_pagtoContribuicao: TDateTimeField
      DisplayLabel = 'Data Contribui'#231#227'o'
      FieldName = 'dt_pagtoContribuicao'
    end
    object QListaPagamentosdt_resgChequePre: TDateTimeField
      DisplayLabel = 'Darta ChequePre'
      FieldName = 'dt_resgChequePre'
    end
    object QListaPagamentosds_bancoCheque: TStringField
      DisplayLabel = 'banco Cheque'
      FieldName = 'ds_bancoCheque'
      Size = 40
    end
    object QListaPagamentosds_agenciaCheque: TStringField
      DisplayLabel = 'Agencia Cheque'
      FieldName = 'ds_agenciaCheque'
    end
    object QListaPagamentosno_chequePagto: TStringField
      DisplayLabel = 'No. cheque'
      FieldName = 'no_chequePagto'
    end
    object QListaPagamentoscd_formPagto: TSmallintField
      FieldName = 'cd_formPagto'
      Visible = False
    end
    object QListaPagamentosdt_anoRefContribuicao: TIntegerField
      FieldName = 'dt_anoRefContribuicao'
      Visible = False
    end
    object QListaPagamentosdt_mesRefContribuicao: TIntegerField
      FieldName = 'dt_mesRefContribuicao'
      Visible = False
    end
    object QListaPagamentoscd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
  end
  object dsListaPagamento: TDataSource
    DataSet = QListaPagamentos
    Left = 152
    Top = 392
  end
  object ActionList1: TActionList
    Left = 536
    Top = 8
    object acNovo: TAction
      Caption = 'acNovo'
      ShortCut = 16462
      OnExecute = acNovoExecute
    end
    object acFechar: TAction
      Caption = 'acFechar'
      ShortCut = 121
      OnExecute = acFecharExecute
    end
  end
  object QInsert: TADOQuery
    Connection = DM.ADOConn
    Parameters = <
      item
        Name = 'cd_cadastro'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dt_anoRefContribuicao'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dt_mesRefContribuicao'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'vl_ContribuicaoVencimento'
        Attributes = [paSigned]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Size = 19
        Value = Null
      end
      item
        Name = 'cd_tipoContribuicao'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cd_sitQuitacao'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'dt_Contribuicao'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'id_usuario'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [tbContribuicoes]'
      '           ([cd_cadastro]'
      '           ,[dt_anoRefContribuicao]'
      '           ,[dt_mesRefContribuicao]'
      '           ,[vl_ContribuicaoVencimento]'
      '           ,[cd_tipoContribuicao]'
      '           ,[cd_sitQuitacao]'
      '           ,[dt_Contribuicao]'
      '           ,[id_usuario])'
      '     VALUES'
      '           ( :cd_cadastro'
      '           , :dt_anoRefContribuicao'
      '           , :dt_mesRefContribuicao'
      '           , :vl_ContribuicaoVencimento'
      '           , :cd_tipoContribuicao'
      '           , :cd_sitQuitacao'
      '           , :dt_Contribuicao'
      '           , :id_usuario)')
    Left = 224
    Top = 393
  end
  object qTipoContribuicao: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select nm_tipoContribuicao'
      'from tbTipoContribuicao'
      'where cd_tipoContribuicao = :codigo')
    Left = 368
    Top = 368
    object qTipoContribuicaonm_tipoContribuicao: TStringField
      FieldName = 'nm_tipoContribuicao'
      Size = 50
    end
  end
  object qValorContribuicao: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codigo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'tipo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select TOP 1 t.vl_ContribuicaoVencimento as valor '
      'from tbContribuicoes t'
      'where '
      #9't.cd_cadastro = :codigo'
      #9'and t.cd_tipoContribuicao = :tipo'
      
        'order by t.dt_anoRefContribuicao desc, t.dt_mesRefContribuicao d' +
        'esc')
    Left = 488
    Top = 361
    object qValorContribuicaovalor: TBCDField
      FieldName = 'valor'
      ReadOnly = True
      Precision = 10
      Size = 2
    end
  end
end
