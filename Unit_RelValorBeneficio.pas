unit Unit_RelValorBeneficio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, RLReport, jpeg, ADODB;

type
  Tfrm_RelValorBeneficio = class(Tfrm_RelatorioPadrao)
    qValorBeneficio: TADOQuery;
    RLBand5: TRLBand;
    RLLabel6: TRLLabel;
    RLDBText3: TRLDBText;
    RLDBResult1: TRLDBResult;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLLabel9: TRLLabel;
    qValorBeneficionm_pastor: TStringField;
    qValorBeneficionm_conjuge: TStringField;
    qValorBeneficiodt_falecimento: TStringField;
    qValorBeneficiovl_beneficio: TBCDField;
    qValorBeneficiocd_cadastro: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelValorBeneficio: Tfrm_RelValorBeneficio;

implementation

uses Unit_DM;

{$R *.dfm}

end.
