unit Unit_RelDescontoIR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_RelatorioPadrao, DB, ADODB, RLReport, jpeg;

type
  Tfrm_RelDescontoIR = class(Tfrm_RelatorioPadrao)
    qRelatorio: TADOQuery;
    qRelatoriocodigo: TIntegerField;
    qRelatorionome: TStringField;
    qRelatoriovl_beneficio: TBCDField;
    qRelatoriovl_ir: TBCDField;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBText6: TRLDBText;
    qRelatoriovl_dizimo: TBCDField;
    qRelatoriovl_saldo: TBCDField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelDescontoIR: Tfrm_RelDescontoIR;

implementation

uses Unit_DM;

{$R *.dfm}

end.
