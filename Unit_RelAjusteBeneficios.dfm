inherited frm_RelAjusteValorBeneficio: Tfrm_RelAjusteValorBeneficio
  Caption = 'Relatorio Ajuste Valor Beneficio'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = -13
    Width = 1123
    Height = 794
    PageSetup.Orientation = poLandscape
    ExplicitLeft = -13
    ExplicitWidth = 1123
    ExplicitHeight = 794
    inherited RLBand1: TRLBand
      Width = 1047
      ExplicitWidth = 1047
    end
    inherited RLBand4: TRLBand
      Width = 1047
      ExplicitWidth = 1047
    end
    inherited RLGroup1: TRLGroup
      Width = 1047
      inherited bandaHead: TRLBand
        Width = 1047
      end
      inherited RLBand3: TRLBand
        Width = 1047
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = qRelatorio
  end
  object qRelatorio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '
      #9'cd_cabepa,'
      #9'nm_pastor,'
      #9'vl_beneficio,'
      #9'vl_dizimo,'
      #9'isnull(vl_descontos,0) vl_descontos,'
      
        #9'(vl_beneficio - vl_dizimo - isnull(vl_descontos,0)) as vl_liqui' +
        'doatual,'
      #9'case '
      #9#9'when (vl_beneficio - vl_dizimo) < 545'
      #9#9#9'then vl_beneficio'
      #9#9#9'else (vl_beneficio - vl_dizimo)'
      #9'end as vl_novobruto,'
      #9
      #9'case '
      #9#9'when (vl_beneficio - vl_dizimo) < 545'
      #9#9#9'then vl_beneficio - isnull(vl_descontos,0)'
      #9#9'else vl_beneficio - vl_dizimo  - isnull(vl_descontos,0)'
      #9'end as vl_novoliquido'
      #9
      'from vwListaAjusteValor '
      'order by '
      'cd_sitJubilamento, nm_pastor'
      ''
      '')
    Left = 472
    Top = 328
    object qRelatoriocd_cabepa: TIntegerField
      DisplayLabel = 'No. Cabepa'
      FieldName = 'cd_cabepa'
    end
    object qRelatorionm_pastor: TStringField
      DisplayLabel = 'Nome Jubilado'
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qRelatoriovl_beneficio: TBCDField
      DisplayLabel = 'V. Bruto Atual'
      FieldName = 'vl_beneficio'
      ReadOnly = True
      currency = True
      Precision = 10
      Size = 2
    end
    object qRelatoriovl_dizimo: TBCDField
      DisplayLabel = 'V. Escalonamento'
      FieldName = 'vl_dizimo'
      ReadOnly = True
      currency = True
      Precision = 10
      Size = 2
    end
    object qRelatoriovl_descontos: TBCDField
      DisplayLabel = 'V. Outos Desc.'
      FieldName = 'vl_descontos'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qRelatoriovl_liquidoatual: TBCDField
      DisplayLabel = 'V. Liq. Atual'
      FieldName = 'vl_liquidoatual'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qRelatoriovl_novobruto: TBCDField
      DisplayLabel = 'V. Novo Bruto'
      FieldName = 'vl_novobruto'
      ReadOnly = True
      currency = True
      Precision = 11
      Size = 2
    end
    object qRelatoriovl_novoliquido: TBCDField
      DisplayLabel = 'V. Novo L'#237'quido'
      FieldName = 'vl_novoliquido'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
  end
end
