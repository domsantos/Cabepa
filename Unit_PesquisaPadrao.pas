unit Unit_PesquisaPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids, DB, ExtCtrls, ActnList;

type
  Tfrm_PesquisaPadrao = class(TForm)
    Panel1: TPanel;
    dsPesquisa: TDataSource;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    edtArgumento: TEdit;
    Label2: TLabel;
    cbPesquisa: TComboBox;
    btnPesquisa: TBitBtn;
    ActionList1: TActionList;
    acPesquisa: TAction;
    acSair: TAction;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure acSairExecute(Sender: TObject);
    procedure acPesquisaExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_PesquisaPadrao: Tfrm_PesquisaPadrao;

implementation

{$R *.dfm}

procedure Tfrm_PesquisaPadrao.FormShow(Sender: TObject);
begin
  if dsPesquisa.DataSet.Active then dsPesquisa.DataSet.Active := true;
end;

procedure Tfrm_PesquisaPadrao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  self := nil;
  Action := caFree;

end;

procedure Tfrm_PesquisaPadrao.DBGrid1DblClick(Sender: TObject);
begin
  self.close;
end;

procedure Tfrm_PesquisaPadrao.acSairExecute(Sender: TObject);
begin
  self.close;
end;

procedure Tfrm_PesquisaPadrao.acPesquisaExecute(Sender: TObject);
begin
  btnPesquisa.Click;
end;

end.
