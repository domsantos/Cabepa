unit Unit_Nacionalidade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, FMTBcd, ActnList, Provider, DBClient, DB, SqlExpr,
  StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, ADODB, Grids, DBGrids;

type
  Tfrm_Nacionalidade = class(Tfrm_PadraoCadastro)
    QPrincipalcd_nacPastor: TIntegerField;
    QPrincipalds_nacPastor: TStringField;
    CDSPrincipalcd_nacPastor: TIntegerField;
    CDSPrincipalds_nacPastor: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure btnNovoClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnRelatorioClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Nacionalidade: Tfrm_Nacionalidade;

implementation

uses Unit_PesquisaNacionalidade, Unit_RelatorioNacionalidade, Unit_DM;

{$R *.dfm}

procedure Tfrm_Nacionalidade.btnNovoClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

procedure Tfrm_Nacionalidade.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Open;
end;

procedure Tfrm_Nacionalidade.btnRelatorioClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(Tfrm_RelatorioNacionalidade,frm_RelatorioNacionalidade);
  frm_RelatorioNacionalidade.dsRelatorio.DataSet := CDSPrincipal;
  CDSPrincipal.Open;
  frm_RelatorioNacionalidade.RLReport1.Preview();

end;

end.
