inherited frm_VantagemDesconto: Tfrm_VantagemDesconto
  Caption = 'Vantagem Desconto'
  ExplicitWidth = 728
  ExplicitHeight = 513
  PixelsPerInch = 96
  TextHeight = 16
  inherited Panel2: TPanel
    ExplicitTop = 44
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 61
      Height = 16
      Caption = 'Tipo Rubrica'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 186
      Top = 16
      Width = 37
      Height = 16
      Caption = 'Rubrica'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 8
      Top = 62
      Width = 62
      Height = 16
      Caption = 'Data Cria'#231#227'o'
    end
    object Label4: TLabel
      Left = 111
      Top = 62
      Width = 40
      Height = 16
      Caption = 'Situa'#231#227'o'
      FocusControl = DBEdit4
    end
    object Label5: TLabel
      Left = 264
      Top = 62
      Width = 21
      Height = 16
      Caption = 'Tipo'
      FocusControl = DBEdit5
    end
    object Label6: TLabel
      Left = 157
      Top = 81
      Width = 89
      Height = 16
      Caption = '1 - Ativa 2 - Inativa'
    end
    object Label7: TLabel
      Left = 310
      Top = 82
      Width = 129
      Height = 16
      Caption = '1 - Calculada 2 - Informada'
    end
    object Label8: TLabel
      Left = 54
      Top = 38
      Width = 126
      Height = 16
      Caption = '1 - Vantagem 2 - Desconto'
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 32
      Width = 40
      Height = 24
      DataField = 'no_Rubrica'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 186
      Top = 35
      Width = 324
      Height = 24
      CharCase = ecUpperCase
      DataField = 'nm_Rubrica'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit4: TDBEdit
      Left = 111
      Top = 78
      Width = 40
      Height = 24
      DataField = 'cd_sitRubrica'
      DataSource = DataSource1
      TabOrder = 3
    end
    object DBEdit5: TDBEdit
      Left = 264
      Top = 79
      Width = 40
      Height = 24
      DataField = 'cd_tipCalcRubrica'
      DataSource = DataSource1
      TabOrder = 4
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 128
      Width = 601
      Height = 257
      DataSource = DataSource1
      TabOrder = 5
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Arial Narrow'
      TitleFont.Style = []
    end
    object DBNavigator1: TDBNavigator
      Left = 6
      Top = 391
      Width = 225
      Height = 25
      DataSource = DataSource1
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
      TabOrder = 6
    end
    object DBDateEdit1: TDBDateEdit
      Left = 8
      Top = 78
      Width = 97
      Height = 24
      Margins.Top = 1
      DataField = 'dt_criaRubrica'
      DataSource = DataSource1
      NumGlyphs = 2
      TabOrder = 2
    end
  end
  inherited CDSPrincipal: TClientDataSet
    object CDSPrincipalcd_tipRubrica: TSmallintField
      FieldName = 'cd_tipRubrica'
      Visible = False
    end
    object CDSPrincipalno_Rubrica: TSmallintField
      DisplayLabel = 'No. Rubrica'
      FieldName = 'no_Rubrica'
    end
    object CDSPrincipalnm_Rubrica: TStringField
      DisplayLabel = 'Rubrica'
      FieldName = 'nm_Rubrica'
    end
    object CDSPrincipaldt_criaRubrica: TDateTimeField
      DisplayLabel = 'Data Cria'#231#227'o'
      FieldName = 'dt_criaRubrica'
    end
    object CDSPrincipalcd_sitRubrica: TSmallintField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'cd_sitRubrica'
    end
    object CDSPrincipalcd_tipCalcRubrica: TSmallintField
      DisplayLabel = 'Tipo'
      FieldName = 'cd_tipCalcRubrica'
    end
  end
  inherited QPrincipal: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT [cd_tipRubrica]'
      '      ,[no_Rubrica]'
      '      ,[nm_Rubrica]'
      '      ,[dt_criaRubrica]'
      '      ,[cd_sitRubrica]'
      '      ,[cd_tipCalcRubrica]'
      '  FROM [tbVantDesc]')
    object QPrincipalcd_tipRubrica: TSmallintField
      FieldName = 'cd_tipRubrica'
    end
    object QPrincipalno_Rubrica: TSmallintField
      FieldName = 'no_Rubrica'
    end
    object QPrincipalnm_Rubrica: TStringField
      FieldName = 'nm_Rubrica'
    end
    object QPrincipaldt_criaRubrica: TDateTimeField
      FieldName = 'dt_criaRubrica'
    end
    object QPrincipalcd_sitRubrica: TSmallintField
      FieldName = 'cd_sitRubrica'
    end
    object QPrincipalcd_tipCalcRubrica: TSmallintField
      FieldName = 'cd_tipCalcRubrica'
    end
  end
  object DataSource1: TDataSource
    DataSet = CDSPrincipal
    Left = 560
    Top = 72
  end
end
