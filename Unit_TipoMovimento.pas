unit Unit_TipoMovimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, ADODB, Provider, ActnList, DB, DBClient, StdCtrls,
  Buttons, ExtCtrls, Mask, DBCtrls, Grids, DBGrids;

type
  Tfrm_TipoMovimento = class(Tfrm_PadraoCadastro)
    QPrincipalcdTipoMovimento: TAutoIncField;
    QPrincipaldsTipoMovimento: TStringField;
    CDSPrincipalcdTipoMovimento: TAutoIncField;
    CDSPrincipaldsTipoMovimento: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBGrid1: TDBGrid;
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_TipoMovimento: Tfrm_TipoMovimento;

implementation

{$R *.dfm}

procedure Tfrm_TipoMovimento.btnNovoClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

procedure Tfrm_TipoMovimento.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  CDSPrincipal.Open;
end;

end.
