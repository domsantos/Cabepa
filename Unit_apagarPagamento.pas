unit Unit_apagarPagamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, DB, ADODB, Grids, DBGrids, Buttons;

type
  Tfrm_apagarPagamento = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    edtContribuinte: TEdit;
    Button1: TButton;
    lblContribuinte: TLabel;
    BitBtn1: TBitBtn;
    dsPagamentos: TDataSource;
    DBGrid1: TDBGrid;
    qListaPagamentos: TADOQuery;
    qspExcluiPagamento: TADOQuery;
    qListaPagamentosdt_Contribuicao: TDateTimeField;
    qListaPagamentosdt_anoRefContribuicao: TIntegerField;
    qListaPagamentosdt_mesRefContribuicao: TIntegerField;
    qListaPagamentosvl_ContribuicaoVencimento: TBCDField;
    qListaPagamentosnm_tipoContribuicao: TStringField;
    BitBtn2: TBitBtn;
    qListaPagamentoscd_tipoContribuicao: TAutoIncField;
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure edtContribuinteExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_apagarPagamento: Tfrm_apagarPagamento;

implementation

uses Unit_DM, Unit_PesquisaContribuinte;

{$R *.dfm}

procedure Tfrm_apagarPagamento.BitBtn1Click(Sender: TObject);
var
  msg : string;
  mes, ano, tipo : integer;
begin

  if not qListaPagamentos.Active then
  begin
    showmessage('Informe um pagamento!');
    exit;
  end;


  mes := qListaPagamentosdt_mesRefContribuicao.Value;
  ano := qListaPagamentosdt_anoRefContribuicao.Value;
  tipo := qListaPagamentoscd_tipoContribuicao.Value;
  msg := ' Referencia: '+inttostr(mes)+ '/' + inttostr(ano);


  if Dialogs.MessageDlg('Deseja realmente excluir o pagamento de '+ qListaPagamentosnm_tipoContribuicao.Value
  +#13+'da refer�ncia '+msg+'?',
    mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
  begin
    dm.ADOConn.BeginTrans;
    qspExcluiPagamento.Close;

    qspExcluiPagamento.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    qspExcluiPagamento.Parameters.ParamByName('ano').Value := ano;
    qspExcluiPagamento.Parameters.ParamByName('mes').Value := mes;
    qspExcluiPagamento.Parameters.ParamByName('tipo').Value := tipo;

    qspExcluiPagamento.Parameters.ParamByName('codigo1').Value := strtoint(edtContribuinte.Text);
    qspExcluiPagamento.Parameters.ParamByName('ano1').Value := ano;
    qspExcluiPagamento.Parameters.ParamByName('mes1').Value := mes;
    qspExcluiPagamento.Parameters.ParamByName('tipo1').Value := tipo;


    try
      qspExcluiPagamento.ExecSQL;
      dm.ADOConn.CommitTrans;
      showmessage('Pagamento apagado!');
      qListaPagamentos.Close;
      qListaPagamentos.Open;
    except
      dm.ADOConn.RollbackTrans;
      showmessage('Erro ao excluir pagamento');
    end;
    end;



end;

procedure Tfrm_apagarPagamento.BitBtn2Click(Sender: TObject);
begin

  if edtContribuinte.Text <> '' then
  begin
    qListaPagamentos.Close;
    qListaPagamentos.Parameters.ParamByName('codigo').Value := strtoint(edtContribuinte.Text);
    qListaPagamentos.Open;
  end
  else
  begin
    showmessage('Digite um n�mero v�lido');
    edtContribuinte.SetFocus;
  end;

end;

procedure Tfrm_apagarPagamento.Button1Click(Sender: TObject);
begin
  lblContribuinte.Caption := '';
  Application.CreateForm(Tfrm_PesquisaContribuinte,frm_PesquisaContribuinte);
  frm_PesquisaContribuinte.origem := 11;
  frm_PesquisaContribuinte.Show;
end;

procedure Tfrm_apagarPagamento.edtContribuinteExit(Sender: TObject);
var
  codigo : integer;
begin

  if edtContribuinte.Text = '' then
  begin
    showmessage('Digite um No. de Contribuinte!');
  end
  else
  begin

    try
      codigo := strtoint(edtContribuinte.Text);

      qListaPagamentos.Close;

      qListaPagamentos.Parameters.ParamByName('codigo').Value := codigo;
      qListaPagamentos.Open;

    except
      on EConvertError do
      begin
        showmessage('Digite um numero v�lido!');
        edtContribuinte.SetFocus;
      end;
    end;
  end;


end;

procedure Tfrm_apagarPagamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
  Action := caFree;

end;

end.
