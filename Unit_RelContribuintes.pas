unit Unit_RelContribuintes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, RLReport, jpeg;

type
  Tfrm_RelContribuntes = class(TForm)
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel2: TRLLabel;
    RLLabel9: TRLLabel;
    RLSystemInfo1: TRLSystemInfo;
    RLLabel10: TRLLabel;
    RLSystemInfo2: TRLSystemInfo;
    qListaAtivos: TADOQuery;
    qListaAtivosnm_pastor: TStringField;
    qListaAtivosnm_conjuge: TStringField;
    qListaAtivosdt_nascPastor: TDateTimeField;
    dsListaAtivos: TDataSource;
    RLBand2: TRLBand;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLBand3: TRLBand;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLLabel1: TRLLabel;
    qListaAtivoscd_cadastro: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_RelContribuntes: Tfrm_RelContribuntes;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_RelContribuntes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  self := nil;
end;

end.
