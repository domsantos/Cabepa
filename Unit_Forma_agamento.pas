unit Unit_Forma_agamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_Padrao, ADODB, Provider, ActnList, DB, DBClient, StdCtrls,
  Buttons, ExtCtrls, Mask, DBCtrls, Grids, DBGrids;

type
  Tfrm_FormaPagamento = class(Tfrm_PadraoCadastro)
    QPrincipalcd_formPagto: TSmallintField;
    QPrincipalds_formPagto: TStringField;
    CDSPrincipalcd_formPagto: TSmallintField;
    CDSPrincipalds_formPagto: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure btnPesquisarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_FormaPagamento: Tfrm_FormaPagamento;

implementation

uses Unit_DM;

{$R *.dfm}

procedure Tfrm_FormaPagamento.btnPesquisarClick(Sender: TObject);
begin
  inherited;

  CDSPrincipal.Open;
end;

end.
