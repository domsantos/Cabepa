object frm_ContraCheque: Tfrm_ContraCheque
  Left = 0
  Top = 0
  Caption = 'frm_ContraCheque'
  ClientHeight = 648
  ClientWidth = 907
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = 8
    Top = 0
    Width = 794
    Height = 1123
    AllowedBands = [btHeader, btColumnHeader, btDetail]
    DataSource = dsListaBeneficiados
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    object RLGroup1: TRLGroup
      Left = 38
      Top = 38
      Width = 718
      Height = 643
      DataFields = 'cd_cadastro'
      PageBreaking = pbBeforePrint
      object RLSubDetail1: TRLSubDetail
        Left = 0
        Top = 0
        Width = 718
        Height = 346
        AllowedBands = [btColumnHeader, btDetail, btFooter]
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = False
        Borders.DrawRight = False
        Borders.DrawBottom = True
        Borders.FixedBottom = True
        DataSource = dsListaVantagemDesconto
        object RLBand4: TRLBand
          Left = 0
          Top = 145
          Width = 718
          Height = 26
          object RLDBText6: TRLDBText
            Left = 3
            Top = 6
            Width = 43
            Height = 16
            DataField = 'dt_tipo'
            DataSource = dsListaVantagemDesconto
          end
          object RLDBText7: TRLDBText
            Left = 592
            Top = 5
            Width = 63
            Height = 16
            DataField = 'vl_Rubrica'
            DataSource = dsListaVantagemDesconto
          end
        end
        object RLBand2: TRLBand
          Left = 0
          Top = 0
          Width = 718
          Height = 145
          BandType = btColumnHeader
          Borders.Sides = sdCustom
          Borders.DrawLeft = False
          Borders.DrawTop = True
          Borders.DrawRight = False
          Borders.DrawBottom = False
          object RLLabel8: TRLLabel
            Left = 3
            Top = 127
            Width = 62
            Height = 16
            Caption = 'Descri'#231#227'o'
          end
          object RLLabel10: TRLLabel
            Left = 592
            Top = 128
            Width = 34
            Height = 16
            Caption = 'Valor'
          end
          object RLLabel5: TRLLabel
            Left = 3
            Top = 97
            Width = 52
            Height = 16
            Caption = 'Doa'#231#227'o:'
          end
          object RLDBText2: TRLDBText
            Left = 135
            Top = 97
            Width = 74
            Height = 16
            Alignment = taRightJustify
            DataField = 'vl_vantagem'
            DataSource = dsListaBeneficiados
            DisplayMask = '##.#00.0'
          end
          object RLLabel6: TRLLabel
            Left = 264
            Top = 99
            Width = 47
            Height = 16
            Caption = 'Outros:'
          end
          object RLDBText3: TRLDBText
            Left = 407
            Top = 97
            Width = 72
            Height = 16
            Alignment = taRightJustify
            DataField = 'vl_desconto'
            DataSource = dsListaBeneficiados
            DisplayMask = '#.##0.00'
          end
          object RLLabel7: TRLLabel
            Left = 520
            Top = 97
            Width = 79
            Height = 16
            Caption = 'Valor Doado:'
          end
          object RLDBText4: TRLDBText
            Left = 658
            Top = 97
            Width = 56
            Height = 16
            Alignment = taRightJustify
            DataField = 'vl_liquido'
            DataSource = dsListaBeneficiados
            DisplayMask = '#.##0.00'
          end
          object RLDraw1: TRLDraw
            Left = 0
            Top = 114
            Width = 718
            Height = 1
          end
          object RLLabel4: TRLLabel
            Left = 3
            Top = 68
            Width = 51
            Height = 17
            Caption = 'NOME:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object RLDBText1: TRLDBText
            Left = 63
            Top = 68
            Width = 74
            Height = 17
            DataField = 'nm_pastor'
            DataSource = dsListaBeneficiados
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object RLLabel1: TRLLabel
            Left = 6
            Top = 4
            Width = 299
            Height = 27
            Caption = 'Associa'#231#227'o Casa do Pastor'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -24
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object RLLabel13: TRLLabel
            Left = 7
            Top = 29
            Width = 323
            Height = 16
            Caption = 'RUA PR. JOAO TRIGUEIRO,N.3 CEP 66.652-170 UNA'
          end
          object lblReferencia: TRLLabel
            Left = 604
            Top = 35
            Width = 110
            Height = 18
            Alignment = taRightJustify
            Caption = 'REFER'#202'NCIA:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object RLSystemInfo1: TRLSystemInfo
            Left = 666
            Top = 1
            Width = 51
            Height = 16
            Info = itRecNo
          end
          object RLDraw2: TRLDraw
            Left = 0
            Top = 62
            Width = 718
            Height = 1
          end
        end
        object RLBand1: TRLBand
          Left = 0
          Top = 171
          Width = 718
          Height = 123
          BandType = btFooter
          Borders.Sides = sdCustom
          Borders.DrawLeft = False
          Borders.DrawTop = True
          Borders.DrawRight = False
          Borders.DrawBottom = False
          object RLDBText10: TRLDBText
            Left = 617
            Top = 6
            Width = 56
            Height = 16
            Alignment = taRightJustify
            DataField = 'vl_liquido'
            DataSource = dsListaBeneficiados
          end
          object RLLabel15: TRLLabel
            Left = 487
            Top = 6
            Width = 79
            Height = 16
            Caption = 'Valor Doado:'
          end
          object RLLabel32: TRLLabel
            Left = 8
            Top = 6
            Width = 68
            Height = 16
            Caption = 'Mensagem'
          end
          object RLDBMemo1: TRLDBMemo
            Left = 8
            Top = 28
            Width = 457
            Height = 16
            DataField = 'ds_mensagem'
            DataSource = dsListaBeneficiados
          end
          object RLDBText5: TRLDBText
            Left = 48
            Top = -24
            Width = 70
            Height = 16
          end
        end
      end
      object RLSubDetail2: TRLSubDetail
        Left = 0
        Top = 346
        Width = 718
        Height = 400
        AllowedBands = [btColumnHeader, btDetail, btFooter]
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = False
        Borders.DrawRight = False
        Borders.DrawBottom = True
        Borders.FixedBottom = True
        DataSource = dsListaVantagemDesconto
      end
    end
  end
  object qListaVantagemDesconto: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    DataSource = dsListaBeneficiados
    Parameters = <
      item
        Name = 'cd_cabepa'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'dt_anoPagamento'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'dt_mesPagamento'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select p.no_Rubrica,sum(p.vl_Rubrica) as vl_Rubrica,'
      'case p.no_Rubrica'
      #9#9'when 1 then '#39'DOA'#199#195'O'#39
      #9#9'when 2 then '#39'OUTROS'#39#9' '
      #9'end dt_tipo '
      'from tbPagamentoMensal p'
      'inner join tbVantDesc v on v.cd_tipRubrica = p.cd_tipRubrica'
      #9#9#9#9#9#9'and v.no_Rubrica = p.no_Rubrica'
      'where cd_cadastro = :cd_cabepa'
      #9'and dt_anoPagamento = :dt_anoPagamento'
      #9'and dt_mesPagamento = :dt_mesPagamento'
      'group by p.no_Rubrica'
      'order by 3 ')
    Left = 536
    Top = 424
    object qListaVantagemDescontono_Rubrica: TSmallintField
      FieldName = 'no_Rubrica'
    end
    object qListaVantagemDescontovl_Rubrica: TBCDField
      FieldName = 'vl_Rubrica'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qListaVantagemDescontodt_tipo: TStringField
      FieldName = 'dt_tipo'
      ReadOnly = True
      Size = 6
    end
  end
  object qListaBeneficiados: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'mes'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = 1
      end
      item
        Name = 'ano'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = 2010
      end
      item
        Name = 'codigo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'banco'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = 1
      end>
    SQL.Strings = (
      'exec sp_ListaResumoBeneficioBanco'
      #9'@mes = :mes'
      #9',@ano = :ano'
      '               ,@codigo = :codigo'
      '               ,@status = '#39'D'#39
      '               ,@banco = :banco')
    Left = 56
    Top = 408
    object qListaBeneficiadosnm_banco: TStringField
      FieldName = 'nm_banco'
      Size = 40
    end
    object qListaBeneficiadosnm_pastor: TStringField
      FieldName = 'nm_pastor'
      Size = 60
    end
    object qListaBeneficiadosvl_vantagem: TBCDField
      FieldName = 'vl_vantagem'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qListaBeneficiadosvl_desconto: TBCDField
      FieldName = 'vl_desconto'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qListaBeneficiadosvl_liquido: TBCDField
      FieldName = 'vl_liquido'
      ReadOnly = True
      currency = True
      Precision = 32
      Size = 2
    end
    object qListaBeneficiadosdt_mesPagamento: TSmallintField
      FieldName = 'dt_mesPagamento'
      ReadOnly = True
    end
    object qListaBeneficiadosdt_anoPagamento: TSmallintField
      FieldName = 'dt_anoPagamento'
      ReadOnly = True
    end
    object qListaBeneficiadosds_mensagem: TStringField
      FieldName = 'ds_mensagem'
      ReadOnly = True
      Size = 400
    end
    object qListaBeneficiadoscd_cadastro: TIntegerField
      FieldName = 'cd_cadastro'
    end
    object qListaBeneficiadosds_beneficio: TStringField
      FieldName = 'ds_beneficio'
      ReadOnly = True
      Size = 6
    end
  end
  object dsListaBeneficiados: TDataSource
    DataSet = qListaBeneficiados
    Left = 88
    Top = 408
  end
  object dsListaVantagemDesconto: TDataSource
    DataSet = qListaVantagemDesconto
    Left = 568
    Top = 424
  end
end
