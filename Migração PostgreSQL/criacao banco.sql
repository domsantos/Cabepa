/*
created		06/03/2015
modified		07/03/2015
project		
model			
company		
author		
version		
database		postgresql 8.0 
*/


/* create tables */


create table "banco"
(
	"cd_banco" serial not null,
	"nm_banco" varchar(40),
 primary key ("cd_banco")
) with oids;


create table "beneficio_contribuicao"
(
	"id_beneficio_contribuicao" serial not null,
	"no_anoscontribuicao" smallint not null,
	"vl_percentsalmin" smallint not null,
 primary key ("id_beneficio_contribuicao")
) with oids;


create table "caixa"
(
	"id_caixa" integer not null,
	"nr_caixa" smallint,
	"dt_abertura" date,
	"dt_fechamento" date,
	"id_usuario" integer not null,
 primary key ("id_caixa")
) with oids;


create table "categoria"
(
	"cd_categoria" serial not null,
	"ds_categoria" varchar(20) not null,
 primary key ("cd_categoria")
) with oids;


create table "contribuicao"
(
	"id_contribuicao" bigserial not null,
	"cd_cadastro" integer not null,
	"id_usuario" integer not null,
	"dt_anorefcontribuicao" integer not null,
	"dt_mesrefcontribuicao" integer not null,
	"vl_contribuicaovencimento" numeric not null,
	"cd_sitquitacao" smallint,
	"dt_contribuicao" date default (getdate()),
	"cd_tipocontribuicao" integer not null,
 primary key ("id_contribuicao")
) with oids;


create table "contribuinte"
(
	"cd_cadastro" serial not null,
	"cd_estado_civil" integer not null,
	"cd_formacao_teologica" integer not null,
	"cd_naturalidade" integer not null,
	"cd_banco" integer,
	"cd_escolaridade" integer not null,
	"cd_nacionalidade" integer not null,
	"cd_categoria" integer not null,
	"nm_pastor" varchar(60) not null,
	"no_registro_convencao" integer,
	"dt_nascimento" date not null,
	"tp_sanguineo" char(20),
	"no_rr" char(20),
	"no_cpf" char(14),
	"ds_endereco" varchar(60),
	"ds_complemento" varchar(40),
	"no_cep" char(8),
	"dt_filiacao" date,
	"nm_pai" varchar(60),
	"nm_mae" varchar(60),
	"nm_conjuge" varchar(60),
	"dt_nascimento_conjuge" date,
	"no_fone" varchar(15),
	"dt_batismo" date,
	"ds_local_batismo" varchar(40),
	"dt_autevangelista" date,
	"dt_consagevangelista" date,
	"dt_ordenacpastor" date,
	"ds_localconsagracao" varchar(40),
	"ds_campo" varchar(40),
	"ds_supervisao" varchar(40),
	"no_certcasamento" integer,
	"ds_orgaoemissorrg" varchar(30),
	"dt_emissao" date,
	"ds_bairro" varchar(40),
	"ds_uf" varchar(50),
	"ds_cidade" varchar(50),
	"dt_falecimento" char(3),
	"st_arquivomorto" bit(1) default ((0)),
 primary key ("cd_cadastro")
) with oids;


create table "escolaridade"
(
	"cd_escolaridade" serial not null,
	"ds_escolaridade" varchar(20) not null,
 primary key ("cd_escolaridade")
) with oids;


create table "faixa_dizimo"
(
	"id_faixadizimo" serial not null,
	"nr_ano" smallint not null,
	"id_faixa" smallint not null,
	"vl_indice" numeric,
	"vl_teto" money,
 primary key ("id_faixadizimo")
) with oids;


create table "forma_pagto"
(
	"cd_formpagto" smallint not null,
	"ds_formpagto" varchar(40) not null,
 primary key ("cd_formpagto")
) with oids;


create table "formacao_teologica"
(
	"cd_formacao_teologica" serial not null,
	"ds_formacao_teologica" varchar(40) not null,
 primary key ("cd_formacao_teologica")
) with oids;


create table "movimento_caixa"
(
	"id_movimento" bigserial not null,
	"dt_movimento" timestamp,
	"vl_movimento" numeric(15,2),
	"tp_movimento" char(1),
	"id_caixa" integer not null,
 primary key ("id_movimento")
) with oids;


create table "nacionalidade"
(
	"cd_nacionalidade" serial not null,
	"ds_nacionalidade" varchar(20) not null,
 primary key ("cd_nacionalidade")
) with oids;


create table "naturalidade"
(
	"cd_naturalidade" serial not null,
	"ds_naturalidade" varchar(20) not null,
 primary key ("cd_naturalidade")
) with oids;


create table "pagamento_mensal"
(
	"id_pagamento" bigserial not null,
	"cd_cadastro" integer not null,
	"dt_anopagamento" smallint not null,
	"dt_mespagamento" smallint not null,
	"no_freqrubrica" smallint not null,
	"vl_rubrica" numeric not null,
	"st_pagamentomensal" char(1) default ('p'),
	"cd_vantagem_desconto" integer not null,
 primary key ("id_pagamento")
) with oids;


create table "pagamento_contribuicao"
(
	"id_pagtocontribuincao" bigserial not null,
	"vl_pagtocontribuicao" numeric not null,
	"dt_pagtocontribuicao" date not null,
	"ds_bancocheque" varchar(40),
	"ds_agenciacheque" varchar(20),
	"no_chequepagto" varchar(20),
	"dt_resgchequepre" date,
	"cd_formpagto" smallint not null,
	"cd_cadastro" integer not null,
	"dt_anorefcontribuicao" integer not null,
	"dt_mesrefcontribuicao" integer not null,
	"sq_chequepagto" smallint,
	"cd_tipocontribuicao" integer,
 primary key ("id_pagtocontribuincao")
) with oids;


create table "bencao"
(
	"id_bencao" serial not null,
	"cd_cadastro" integer not null,
	"cd_tipo_bencao" integer not null,
	"dt_requerimento" date not null,
	"cd_sitrequerimento" smallint not null,
	"vl_beneficio" numeric(15,2),
 primary key ("id_bencao")
) with oids;


create table "salario_minimo"
(
	"id_salariominimo" serial not null,
	"dt_anosalmin" smallint not null,
	"dt_messalmin" smallint not null,
	"vl_salmin" money not null,
 primary key ("id_salariominimo")
) with oids;


create table "tipo_bencao"
(
	"cd_tipo_bencao" serial not null,
	"ds_tipo_bencao" varchar(20) not null,
 primary key ("cd_tipo_bencao")
) with oids;


create table "tipo_contribuicao"
(
	"cd_tipocontribuicao" serial not null,
	"nm_tipocontribuicao" varchar(50),
 primary key ("cd_tipocontribuicao")
) with oids;


create table "tipo_movimento"
(
	"cdtipomovimento" integer not null,
	"dstipomovimento" varchar(50)
) with oids;


create table "usuario"
(
	"id_usuario" integer not null,
	"nm_login" varchar(25) not null,
	"nm_usuario" varchar(100) not null,
	"ds_email" varchar(100),
	"nr_fone_contato" varchar(11),
	"lb_foto" bytea,
	"id_perfil" integer not null,
	"vl_senha" varchar(20) default ((123456)),
 primary key ("id_usuario")
) with oids;


create table "vantagem_desconto"
(
	"cd_vantagem_desconto" serial not null,
	"tp_rubrica" smallint not null,
	"nm_rubrica" varchar(20) not null,
	"dt_criarubrica" date not null,
	"cd_sitrubrica" smallint,
	"cd_tipcalcrubrica" smallint not null,
 primary key ("cd_vantagem_desconto")
) with oids;


create table "estado_civil"
(
	"cd_estado_civil" serial not null,
	"ds_estado_civil" varchar(30),
 primary key ("cd_estado_civil")
) with oids;


