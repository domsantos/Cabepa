inherited frm_RelDescontoIR: Tfrm_RelDescontoIR
  Caption = 'Relat'#243'rio Desconto IR'
  ExplicitWidth = 870
  ExplicitHeight = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = 0
    ExplicitLeft = 0
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 304
        Height = 18
        Caption = 'Listagem de Beneficiados Dedut'#237'veis de IR'
        Font.Height = -16
        ParentFont = False
        ExplicitWidth = 304
        ExplicitHeight = 18
      end
    end
    inherited RLBand2: TRLBand
      inherited RLLabel3: TRLLabel
        Width = 41
        Height = 17
        Font.Name = 'Sakkal Majalla'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 41
        ExplicitHeight = 17
      end
      inherited RLLabel4: TRLLabel
        Left = 69
        Width = 54
        Height = 17
        Font.Name = 'Sakkal Majalla'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 69
        ExplicitWidth = 54
        ExplicitHeight = 17
      end
      object RLLabel6: TRLLabel
        Left = 369
        Top = 32
        Width = 47
        Height = 17
        Caption = 'Valor Base'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Sakkal Majalla'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel7: TRLLabel
        Left = 496
        Top = 32
        Width = 32
        Height = 17
        Caption = 'Dizimo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Sakkal Majalla'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel8: TRLLabel
        Left = 615
        Top = 32
        Width = 11
        Height = 17
        Caption = 'IR'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Sakkal Majalla'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel9: TRLLabel
        Left = 679
        Top = 32
        Width = 36
        Height = 17
        Caption = 'Liquido'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Sakkal Majalla'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    inherited RLBand3: TRLBand
      Height = 16
      ExplicitHeight = 16
      inherited RLDBText1: TRLDBText
        Top = 1
        Width = 35
        Height = 14
        DataField = 'codigo'
        Font.Height = -11
        ParentFont = False
        ExplicitTop = 1
        ExplicitWidth = 35
        ExplicitHeight = 14
      end
      inherited RLDBText2: TRLDBText
        Left = 69
        Top = 1
        Width = 29
        Height = 14
        DataField = 'nome'
        Font.Height = -11
        ParentFont = False
        ExplicitLeft = 69
        ExplicitTop = 1
        ExplicitWidth = 29
        ExplicitHeight = 14
      end
      object RLDBText3: TRLDBText
        Left = 336
        Top = 2
        Width = 80
        Height = 14
        Alignment = taRightJustify
        DataField = 'vl_beneficio'
        DataSource = dsRelatorio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLDBText4: TRLDBText
        Left = 448
        Top = 2
        Width = 80
        Height = 14
        Alignment = taRightJustify
        DataField = 'vl_dizimo'
        DataSource = dsRelatorio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLDBText5: TRLDBText
        Left = 544
        Top = 2
        Width = 80
        Height = 14
        Alignment = taRightJustify
        DataField = 'vl_ir'
        DataSource = dsRelatorio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLDBText6: TRLDBText
        Left = 632
        Top = 2
        Width = 80
        Height = 14
        Alignment = taRightJustify
        DataField = 'vl_saldo'
        DataSource = dsRelatorio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
    inherited RLBand4: TRLBand
      Top = 185
      ExplicitTop = 185
      inherited RLSystemInfo1: TRLSystemInfo
        Width = 39
        ExplicitWidth = 39
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = qRelatorio
    Left = 384
  end
  object qRelatorio: TADOQuery
    Connection = DM.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'declare '
      #9'@valor money'
      #9',@codigo int'
      #9',@vl_ir money'
      #9',@nome varchar(100)'
      #9',@return_value money'
      #9',@retorno money'
      #9',@vlDizimo money'
      #9',@dizimo  money'
      #9
      'declare cPastor cursor static for'
      #9#9'select'
      #9#9#9#9'c.cd_CABEPA,'
      #9#9#9#9'case c.cd_sitJubilamento'
      
        #9#9#9#9#9'when 1 then (select MAX(p.vl_Rubrica) from tbPagamentoMensa' +
        'l p '
      #9#9#9#9#9#9#9'where p.cd_CABEPA = c.cd_CABEPA and p.cd_tipRubrica = 1)'
      
        #9#9#9#9#9'when 2 then (select MAX(p.vl_Rubrica) from tbPagamentoMensa' +
        'l p '
      #9#9#9#9#9#9#9'where p.cd_CABEPA = c.cd_CABEPA and p.cd_tipRubrica = 11)'
      #9#9#9#9#9'end vl_pag,'
      #9#9#9#9'case c.cd_sitJubilamento'
      #9#9#9#9#9'when 1 then c.nm_pastor'
      #9#9#9#9#9'when 2 then c.nm_conjuge'
      #9#9#9#9'end nm_beneficiado'
      #9#9#9'from tbContribuinte c'
      #9#9#9'where c.cd_sitJubilamento in(1,2)'
      ''
      ''
      ''
      'declare @tbIR table ('
      #9#9'codigo int not null,'
      #9#9'nome varchar(100) not null,'
      #9#9'vl_beneficio money not null,'
      #9#9'vl_ir money not null,'
      #9#9'vl_dizimo money null,'
      #9#9'vl_saldo money  null)'
      ''
      'open cPastor '
      ''
      'fetch cPastor into @codigo,@valor,@nome'
      ''
      'while(@@FETCH_STATUS <> -1)'
      'begin'
      #9
      #9'EXEC'#9'@return_value = [dbo].[sp_calculaIRenda]'
      #9#9'@valor = @valor,'
      #9#9'@retorno = @retorno OUTPUT,'
      #9#9'@dizimo = @dizimo OUTPUT'
      #9
      ''
      #9'if @retorno > 0'
      #9'begin'
      
        #9#9'insert into @tbIR (codigo,nome,vl_beneficio,vl_ir,vl_dizimo,vl' +
        '_saldo)'
      
        #9#9#9'values (@codigo,@nome,@valor,@retorno,@dizimo,@valor - @dizim' +
        'o - @retorno)'
      #9'end'
      #9'fetch cPastor into @codigo,@valor,@nome'
      'end'
      ''
      ''
      'close cPastor'
      'deallocate cPastor'
      ''
      ''
      'select * from @tbIR order by nome')
    Left = 440
    Top = 328
    object qRelatoriocodigo: TIntegerField
      FieldName = 'codigo'
    end
    object qRelatorionome: TStringField
      FieldName = 'nome'
      Size = 100
    end
    object qRelatoriovl_beneficio: TBCDField
      FieldName = 'vl_beneficio'
      currency = True
      Precision = 19
    end
    object qRelatoriovl_ir: TBCDField
      FieldName = 'vl_ir'
      currency = True
      Precision = 19
    end
    object qRelatoriovl_dizimo: TBCDField
      FieldName = 'vl_dizimo'
      currency = True
      Precision = 19
    end
    object qRelatoriovl_saldo: TBCDField
      FieldName = 'vl_saldo'
      currency = True
      Precision = 19
    end
  end
end
