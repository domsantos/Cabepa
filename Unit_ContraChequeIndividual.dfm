inherited frm_ContraChequeIndividual: Tfrm_ContraChequeIndividual
  ExplicitWidth = 915
  ExplicitHeight = 675
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = 24
    Top = 8
    ExplicitLeft = 24
    ExplicitTop = 8
    inherited RLGroup1: TRLGroup
      inherited RLSubDetail1: TRLSubDetail
        inherited RLBand4: TRLBand
          inherited RLDBText7: TRLDBText
            Left = 652
            ExplicitLeft = 652
          end
        end
        inherited RLBand2: TRLBand
          inherited RLLabel13: TRLLabel
            Width = 285
            Caption = ' SUELY ,N.2 CEP 66.652-170 BAIRRO DO UNA'
            ExplicitWidth = 285
          end
        end
      end
    end
  end
  inherited qListaBeneficiados: TADOQuery
    Parameters = <
      item
        Name = 'mes'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'ano'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'codigo'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'exec sp_ListaResumoBeneficio '
      #9'@mes = :mes'
      #9',@ano = :ano'
      '               ,@codigo = :codigo')
  end
end
