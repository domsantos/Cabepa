inherited frm_RelArquivoMorto: Tfrm_RelArquivoMorto
  Caption = 'frm_RelArquivoMorto'
  ExplicitWidth = 870
  ExplicitHeight = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited RLReport1: TRLReport
    Left = 8
    ExplicitLeft = 8
    inherited RLBand1: TRLBand
      inherited RLLabel2: TRLLabel
        Width = 235
        Caption = 'Lista de Contribuintes em Arquivo Morto'
        ExplicitWidth = 235
      end
    end
    inherited RLBand2: TRLBand
      inherited RLLabel4: TRLLabel
        Width = 140
        Caption = 'NOME CONTRIBUINTE'
        ExplicitWidth = 140
      end
      object RLLabel6: TRLLabel
        Left = 600
        Top = 32
        Width = 82
        Height = 16
        Caption = 'DATA NASC.'
      end
    end
    inherited RLBand3: TRLBand
      inherited RLDBText1: TRLDBText
        Width = 82
        DataField = 'cd_CABEPA'
        ExplicitWidth = 82
      end
      inherited RLDBText2: TRLDBText
        Width = 111
        DataField = 'nm_pastor'
        ExplicitWidth = 111
      end
      object RLDBText3: TRLDBText
        Left = 600
        Top = 3
        Width = 66
        Height = 16
        DataField = 'dt_nascPastor'
        DataSource = dsRelatorio
      end
    end
  end
  inherited dsRelatorio: TDataSource
    DataSet = DM.qListaArquivoMorto
    Left = 688
    Top = 56
  end
end
